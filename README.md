![Logo](./figs/RATLogo.png)
# Rat-Materials
<em>Rat like materials, especially shiny ones ...</em>

## Introduction
Material database is required for many models. The goal here is to implement a cryogenic database that can be accessed directly from models. 

## The Current Sharing model
An important feature of the database over other applications is that it allows to put a mixture of materials in parallel (or series tbd.). The database then automatically calculates the averaged properties, for example the combined density and heat capacity. However, to calculate the resistivity or corresponding electric field for a certain current this is more complicated, as some of the materials are superconductors and are therefore non-linear. To resolve this, a current sharing model using bi-section method is included. This method solves the electric field over a conductor from the current density (and other properties). 

## Installation
The C++ integration requires the Rat-Common library. This you need to install first. The C++ integration can be build and and installed using

```bash
mkdir release
cd release
cmake ..
make -jX
```

where X is the number of cores you wish to use for the build. When the library is buil install using

```bash
sudo make install
```

## Command Line Interface
To display the material properties table for a CERN specified Fujikura HTS tape, for various temperatures between 4.5 and 77 K (specified with the -t flag), a magnetic field of 5T (specified with the -b) and a field angle of 0 rad (perpendicular field), run the matprops cli application in the build directory as:
```
./Release/bin/matprops json/materials/htstape/Fujikura_CERN.json -t 4.5,10,20,30,40,60,77 -b 5 -a 0
```
which should yield the following terminal output:
```
Material Name: Fujikura HTS Tape CERN
Material Data Table
         parameters |                                         fundamental |    superconductor
    B  alpha      T |      d       cp     delH       df        k      rho |       Jc       Nv
    T    deg      K | kg/m^3   J/K/kg     J/kg    m^2/s    W/m/K    Ohm.m |    A/m^2 unitless
=============================================================================================
 5.00   0.00   4.50 | 7729.2 1.14e+00 3.63e+04 1.62e-02 1.43e+02 1.33e-09 | 1.24e+09 2.00e+01
 5.00   0.00  10.00 | 7729.2 2.94e+00 1.21e+05 1.38e-02 3.13e+02 1.33e-09 | 1.08e+09 2.00e+01
 5.00   0.00  20.00 | 7729.2 9.80e+00 5.71e+05 7.27e-03 5.51e+02 1.36e-09 | 8.06e+08 2.00e+01
 5.00   0.00  30.00 | 7729.2 2.52e+01 1.86e+06 2.99e-03 5.82e+02 1.51e-09 | 5.82e+08 2.00e+01
 5.00   0.00  40.00 | 7729.2 5.19e+01 4.76e+06 1.17e-03 4.70e+02 1.92e-09 | 4.02e+08 2.00e+01
 5.00   0.00  60.00 | 7729.2 1.25e+02 1.84e+07 2.82e-04 2.73e+02 3.76e-09 | 1.44e+08 2.00e+01
 5.00   0.00  77.00 | 7729.2 1.81e+02 3.86e+07 1.51e-04 2.11e+02 6.16e-09 | 0.00e+00 2.00e+01
=============================================================================================
```
In addition also a 1D normal zone propagation model is available. For example to analyse the normal zone propagation for a CERN specified Fujikura HTS tape, run the program as:
```
./Release/bin/matnzp json/materials/htstape/Fujikura_CERN.json -t 20 -b 5 -j 500,600,700,800,900 -n
```
which should yield the following terminal output:
```
Material Name: Fujikura HTS Tape CERN
Analysis Table
                  parameters |                fundamental |               numerical/theoretical
 crdns.   flux  angle   temp |       Je      Tcs        E |     t300     Vnzp     R200      MQE
 A/mm^2      T    rad      K |   A/mm^2        K      V/m |        s      m/s  Ohm m^2   J m^-2
===============================================================================================
  500.0   5.00   0.00  20.00 | 8.06e+08 3.43e+01 3.58e-09 | 7.19e-02 1.35e-01 4.55e-10 0.00e+00
  600.0   5.00   0.00  20.00 | 8.06e+08 2.91e+01 2.06e-07 | 5.09e-02 2.02e-01 4.01e-10 0.00e+00
  700.0   5.00   0.00  20.00 | 8.06e+08 2.45e+01 5.90e-06 | 3.87e-02 2.77e-01 3.77e-10 0.00e+00
  800.0   5.00   0.00  20.00 | 8.06e+08 2.03e+01 8.65e-05 | 3.07e-02 4.56e-01 3.69e-10 0.00e+00
  900.0   5.00   0.00  20.00 | 8.06e+08 1.64e+01 8.99e-04 | 2.53e-02 7.79e-01 3.83e-10 0.00e+00
===============================================================================================
```
The command line interface is installed to ${CMAKE_INSTALL_PREFIX}/bin, from where it can be called directly using only matprops and matnzp.

## Using the Library in your code
Please include the following headers
```cpp
#include "rat/mat/serializer.hh"
#include "rat/mat/conductor.hh"
```
A material can be instantiated directly from a json input file. A database with input files is located in the json directory.
```cpp
std::string fname = "json/matprops/htstape/Fujikura_CERN.json";
rat::mat::ShConductorPr mat = rat::mat::Serializer::create_conductor(fname);
```
Calculating properties for scalar input is achieved using:
```cpp
const double T = 20.0; // [K]
const double B = 5.0; // [T]
const double a = 0.0; // [rad]
const double cp = mat->calc_specific_heat(T); // [J K^-1 kg^-1]
const double vcp = mat->calc_volumetric_specific_heat(T); // [J K^-1 m^-3]
const double k = mat->calc_thermal_conductivity(T); // [W m^-1 K^-1]
const double rho = mat->calc_electrical_resistivity(T, B); // [Ohm m]
const double jc = mat->calc_critical_current_density(T, B, a); // [A m^-2]
const double N = mat->calc_nvalue(T, B, a); // unitless
const double d = mat->calc_density(T); // [kg m^-3]
```
For optimisation reasons separate vectorised functions are supplied which take Armadillo vectors as input, which is used as:
```cpp
const arma::Col<double> T = {4.5,20,40,60,77}; // [K]
const arma::Col<double> B = {5.0,5.0,5.0,5.0,5.0}; // [T]
const arma::Col<double> a = {0.0,0.0,0.0,0.0,0.0}; // [rad]
const arma::Col<double> cp = mat->calc_specific_heat(T); // [J K^-1 kg^-1]
const arma::Col<double> vcp = mat->calc_volumetric_specific_heat(T); // [J K^-1 m^-3]
const arma::Col<double> k = mat->calc_thermal_conductivity(T); // [W m^-1 K^-1]
const arma::Col<double> rho = mat->calc_electrical_resistivity(T, B); // [Ohm m]
const arma::Col<double> jc = mat->calc_critical_current_density(T, B, a); // [A m^-2]
const arma::Col<double> N = mat->calc_nvalue(T, B, a); // unitless
const arma::Col<double> d = mat->calc_density(T); // [kg m^-3]
```

## Authors
* Jeroen van Nugteren
* Nikkie Deelen

## License
This project is licensed under the [MIT](LICENSE).