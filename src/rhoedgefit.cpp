// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "rhoedgefit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	RhoEdgeFit::RhoEdgeFit(){
		set_name("rhoedgefit");
	}

	RhoEdgeFit::RhoEdgeFit(
		const ShFitPr &rho_fit,
		const fltp stabilizer_thickness, 
		const fltp tape_width, 
		const fltp surface_resistivity) : RhoEdgeFit(){
		set_fit(rho_fit);
		stabilizer_thickness_ = stabilizer_thickness;
		tape_width_ = tape_width;
		surface_resistivity_ = surface_resistivity;
	}

	// factory
	ShRhoEdgeFitPr RhoEdgeFit::create(){
		return std::make_shared<RhoEdgeFit>();
	}

	// factory from file
	ShRhoEdgeFitPr RhoEdgeFit::create(
		const ShFitPr &rho_fit,
		const fltp stabilizer_thickness, 
		const fltp tape_width, 
		const fltp surface_resistivity){
		return std::make_shared<RhoEdgeFit>(rho_fit, stabilizer_thickness, tape_width, surface_resistivity);
	}

	// set thermal conductivity fit
	void RhoEdgeFit::set_fit(const ShFitPr &rho_fit){
		assert(rho_fit!=NULL);
		rho_fit_ = rho_fit;
	}

	// get fit
	const ShFitPr& RhoEdgeFit::get_fit()const{
		return rho_fit_;
	}

	// calc critical current without field angle nor magnitude
	fltp RhoEdgeFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// fit functions for vector
	fltp RhoEdgeFit::calc_property(
		const fltp temperature, 
		const fltp magnetic_field_magnitude) const{
		
		// calculate thermal conductivity
		const fltp rho = rho_fit_->calc_property(temperature, magnetic_field_magnitude); // [Ohm m]

		// calculate edge resistance
		const fltp edge_resistance = std::sqrt(surface_resistivity_*rho/stabilizer_thickness_); // Ohm m

		// convert to volume resistivity of the stabilizer layer only
		// times two for full tape width
		const fltp value = 2*edge_resistance*stabilizer_thickness_/tape_width_;

		// rurn resistivity
		return value;
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> RhoEdgeFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// fit functions for vector
	arma::Col<fltp> RhoEdgeFit::calc_property(
		const arma::Col<fltp> &temperature, 
		const arma::Col<fltp> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());

		// calculate thermal conductivity
		const arma::Col<fltp> rho = rho_fit_->calc_property(temperature, magnetic_field_magnitude);

		// calculate edge resistance
		const arma::Col<fltp> edge_resistance = arma::sqrt(surface_resistivity_*rho/stabilizer_thickness_); // Ohm m

		// convert to volume resistivity of the stabilizer layer only
		const arma::Col<fltp> value = 2*edge_resistance*stabilizer_thickness_/tape_width_;

		// check output
		assert(value.n_elem==temperature.n_elem);
		assert(value.is_finite());
		assert(arma::all(value>0));

		// rurn resistivity
		return value;
	}

	// validity check
	bool RhoEdgeFit::is_valid(const bool enable_throws)const{
		if(rho_fit_==NULL){if(enable_throws){rat_throw_line("k or rho fit not set");} return false;};
		if(!rho_fit_->is_valid()){if(enable_throws){rat_throw_line("k or rho fit not valid");} return false;};
		return true;
	}

	// serialization type
	std::string RhoEdgeFit::get_type(){
		return "rat::mat::rhoedgefit";
	}

	// serialization
	void RhoEdgeFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);
		js["type"] = get_type();
		js["rho_fit"] = cmn::Node::serialize_node(rho_fit_, list);
		js["stabilizer_thickness"] = stabilizer_thickness_;
		js["tape_width"] = tape_width_;
		js["surface_resistivity"] = surface_resistivity_;
	}

	// deserialization
	void RhoEdgeFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		set_fit(cmn::Node::deserialize_node<Fit>(js["rho_fit"], list, factory_list, pth));
		stabilizer_thickness_ = js["stabilizer_thickness"].ASFLTP();
		tape_width_ = js["tape_width"].ASFLTP();
		surface_resistivity_ = js["surface_resistivity"].ASFLTP();
	}

}}