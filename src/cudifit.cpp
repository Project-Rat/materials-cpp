// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "cudifit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	CudiFit::CudiFit(){
		set_name("cudifit");
		p_ = RAT_CONST(0.37); 
		q_ = RAT_CONST(0.0005);
	}

	// default copper resistivity fit based on RRR
	CudiFit::CudiFit(const fltp RRR) : CudiFit(){
		C0_ = RAT_CONST(1.7); C1_ = RAT_CONST(2.33e9); 
		C2_ = RAT_CONST(9.57e5); C3_ = RAT_CONST(163.0); RRR_ = RRR; 
		t1_ = RAT_CONST(4.0); t2_ = RAT_CONST(300.0); 
	}

	// factory
	ShCudiFitPr CudiFit::create(){
		return std::make_shared<CudiFit>();
	}

	// factory
	ShCudiFitPr CudiFit::create(const fltp RRR){
		return std::make_shared<CudiFit>(RRR);
	}
	

	// setters
	void CudiFit::set_RRR(const fltp RRR){
		RRR_ = RRR;
	}

	void CudiFit::set_C0(const fltp C0){
		C0_ = C0;
	}

	void CudiFit::set_C1(const fltp C1){
		C1_ = C1;
	}

	void CudiFit::set_C2(const fltp C2){
		C2_ = C2;
	}

	void CudiFit::set_C3(const fltp C3){
		C3_ = C3;
	}

	void CudiFit::set_p(const fltp p){
		p_ = p;
	}

	void CudiFit::set_q(const fltp q){
		q_ = q;
	}


	// getters
	fltp CudiFit::get_RRR()const{
		return RRR_;
	}

	fltp CudiFit::get_C0()const{
		return C0_;
	}

	fltp CudiFit::get_C1()const{
		return C1_;
	}

	fltp CudiFit::get_C2()const{
		return C2_;
	}
	
	fltp CudiFit::get_C3()const{
		return C3_;
	}

	fltp CudiFit::get_p()const{
		return p_;
	}
	
	fltp CudiFit::get_q()const{
		return q_;
	}

	// calc critical current without field angle nor magnitude
	fltp CudiFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// vector fit function
	arma::Col<fltp> CudiFit::calc_property(
		const arma::Col<fltp> &temperature, 
		const arma::Col<fltp> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);

		// clamp temperature as to avoid negative values in fit
		const arma::Col<fltp> T = arma::clamp(temperature,t1_,arma::Datum<fltp>::inf);
		const arma::Col<fltp> B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const arma::Col<fltp> TTT = T%T%T; 
		const arma::Col<fltp> TTTTT = TTT%T%T;
		const arma::Col<fltp> rho = (C0_/RRR_ + RAT_CONST(1.0)/(C1_/TTTTT + C2_/TTT + C3_/T))*RAT_CONST(1e-8) + B*(p_ + q_*RRR_)*RAT_CONST(1e-10);

		// return calculated conductivity
		return rho; // [Ohm m]
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> CudiFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}
	
	// scalar fit function
	fltp CudiFit::calc_property(
		const fltp temperature, 
		const fltp magnetic_field_magnitude) const{

		// clamp temperature as to avoid negative values in fit
		const fltp T = std::max(temperature,t1_);
		const fltp B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const fltp TTT = T*T*T; 
		const fltp TTTTT = TTT*T*T;
		const fltp value = (C0_/RRR_ + RAT_CONST(1.0)/(C1_/TTTTT + C2_/TTT + C3_/T))*RAT_CONST(1e-8) + B*(p_ + q_*RRR_)*RAT_CONST(1e-10);

		// return calculated conductivity
		return value; // [Ohm m]
	}

	// validity check
	bool CudiFit::is_valid(const bool enable_throws)const{
		if(t2_<=t1_){if(enable_throws){
			rat_throw_line("second temperature must be larger than first temperature");} return false;};
		return true;
	}

	// get type
	std::string CudiFit::get_type(){
		return "rat::mat::cudifit";
	}

	// method for serialization into json
	void CudiFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
		js["C2"] = C2_;
		js["C3"] = C3_;
		js["p"] = p_;
		js["q"] = q_;
		js["RRR"] = RRR_;
	}

	// method for deserialisation from json
	void CudiFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		C0_ = js["C0"].ASFLTP();
		C1_ = js["C1"].ASFLTP();
		C2_ = js["C2"].ASFLTP();
		C3_ = js["C3"].ASFLTP();
		RRR_ = js["RRR"].ASFLTP();
		if(js.isMember("p"))p_ = js["p"].ASFLTP();
		if(js.isMember("q"))q_ = js["q"].ASFLTP();
	}

}}