// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "database.hh"

#include "nist8sumfit.hh"
#include "nist8fracfit.hh"
#include "cudifit.hh"
#include "wflfit.hh"
#include "constfit.hh"
#include "htscore.hh"
#include "multifit.hh"
#include "lubellkramerfit.hh"
#include "godekefit.hh"
#include "polyfit.hh"
#include "superconductor.hh"
#include "linearfit.hh"

// code specific to Rat
namespace rat{namespace mat{


	// COPPER OFHC
	// heat capacity (Source NIST)
	ShFitPr Database::copper_ofhc_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{-1.91844,-0.15973,8.61013,-18.996,
			21.9661,-12.7328,3.54322,-0.3797,0.0});
		fit->set_name("Copper OFHC");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("UNS C10100/C10200");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::copper_ofhc_rho(const fltp RRR){
		const ShFitPr fit = CudiFit::create(RRR);
		fit->set_name("Copper OFHC");
		fit->set_property_str("Electrical Resistivity");
		fit->set_is_public(true);
		fit->set_source("CUDI Manual");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::copper_ofhc_k(const fltp RRR){
		const ShFitPr fit = WFLFit::create(copper_ofhc_rho(RRR));
		fit->set_name("Copper OFHC");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// density
	ShFitPr Database::copper_ofhc_d(){
		const ShFitPr fit = ConstFit::create(8960.0);
		fit->set_name("Copper OFHC");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Copper");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::copper_ofhc(const fltp RRR){
		// create fits
		const ShFitPr cp_fit = copper_ofhc_cp();
		const ShFitPr k_fit = copper_ofhc_k(RRR);
		const ShFitPr rho_fit = copper_ofhc_rho(RRR);
		const ShFitPr d_fit = copper_ofhc_d();

		// create conductor
		const ShBaseConductorPr copper = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);

		// name conductor
		copper->set_name("Copper OFHC RRR" + std::to_string(static_cast<int>(RRR)));
		copper->set_classification("metals");

		// return conductor object
		return copper;
	}


	// BRASS
	// heat capacity (Source NIST)
	ShFitPr Database::brass_65_35_cp(){
		const ShFitPr fit = Nist8FracFit::create(4,300,
			{33.336073248239465,358.93727785100788,
			-50.305156653388913,-98.466858483788457, 
			-329.43700500869573,33.146057569535792, 
			101.2617676144698,1.4828692209868819, 
			3.3461400858476709});
		fit->set_name("Brass");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::brass_65_35_rho(){
		// const ShFitPr fit = WFLFit::create(brass_65_35_k());
		// fit->set_name("Brass");
		// fit->set_property_str("Electrical Resistivity");
		// fit->set_is_public(true);
		// fit->set_source("Wikipedia");
		// fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		const ShPolyFitPr fit = PolyFit::create();
		fit->set_fit_parameters(1e-9*arma::Col<fltp>{-8.3396e-14,4.6845e-11,7.3463e-10,-5.2321e-06,1.2379e-03,-5.8268e-03,4.4566e+01});
		fit->set_temperature_range(1.0,300.0);
		fit->set_is_public(true);
		fit->set_name("Brass");
		fit->set_property_str("Electrical Resistivity");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::brass_65_35_k(){
		const ShFitPr fit = Nist8SumFit::create(5,110,
			{0.021035,-1.01835,4.54083,-5.03374,
			3.20536,-1.12933,0.174057,-0.0038151,0});
		fit->set_name("Brass");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("UNS C26000");
		return fit;
	}

	// density
	ShFitPr Database::brass_65_35_d(){
		const ShFitPr fit = ConstFit::create(8470.0);
		fit->set_name("Brass");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_link("https://en.wikipedia.org/wiki/Brass");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::brass_65_35(){
		// create fits
		const ShFitPr cp_fit = brass_65_35_cp();
		const ShFitPr k_fit = brass_65_35_k();
		const ShFitPr rho_fit = brass_65_35_rho();
		const ShFitPr d_fit = brass_65_35_d();

		// create conductor
		const ShBaseConductorPr brass = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);

		// name conductor
		brass->set_name("Brass 65-35");
		brass->set_classification("alloys");

		// return conductor object
		return brass;
	}


	// STAINLESS 316
	// heat capacity (Source NIST)
	ShFitPr Database::stainless316_cp(){
		// fit between 4 and 50 K
		const ShFitPr fit1 = Nist8SumFit::create(4,50,
			{12.2486,-80.6422,218.743,-308.854,
			239.5296,-89.9982,3.15315,8.44996,-1.91368});
		fit1->set_name("Stainless 316");
		fit1->set_property_str("Heat Capacity");
		fit1->set_source("NIST");
		fit1->set_link("https://trc.nist.gov/cryogenics/materials/316Stainless/316Stainless_rev.htm");
		fit1->set_standard("UNS S31600");
		
		// fit between 50 and 300 K
		const ShFitPr fit2 = Nist8SumFit::create(50,300,
			{-1879.464,3643.198,76.70125,-6176.028,
			7437.6247,-4305.7217,1382.4627,-237.22704,17.05262});
		fit2->set_name("Stainless 316");
		fit2->set_property_str("Heat Capacity");
		fit2->set_source("NIST");
		fit2->set_link("https://trc.nist.gov/cryogenics/materials/316Stainless/316Stainless_rev.htm");
		fit2->set_standard("UNS S31600");

		// combined fit
		const ShMultiFitPr fit = MultiFit::create();
		fit->add_fit(fit1);
		fit->add_fit(fit2);
		fit->set_t1(4);
		fit->set_t2(299.99);
		fit->set_name("Stainless 316");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/316Stainless/316Stainless_rev.htm");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::stainless316_rho(){
		// const ShFitPr fit = WFLFit::create(stainless316_k());
		// fit->set_property_str("Electrical Resistivity");
		// fit->set_is_public(true);
		// fit->set_source("Wikipedia");
		// fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");


		const ShPolyFitPr fit = PolyFit::create();
		fit->set_fit_parameters({-4.0841e-21,3.4417e-18,-1.0090e-15,1.0363e-13,2.8033e-12,-9.9750e-11,5.3923e-07});
		fit->set_temperature_range(1.0,300.0);
		fit->set_is_public(true);
		fit->set_name("Stainless 316");
		fit->set_property_str("Electrical Resistivity");
		return fit;
	}



	// thermal conductivity (Source NIST)
	ShFitPr Database::stainless316_k(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{-1.4087,1.3982, 0.2543,-0.6260,
			0.2334,0.4256,-0.4658,0.1650,-0.0199});
		fit->set_name("Stainless 316");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/316Stainless/316Stainless_rev.htm");
		return fit;
	}

	// density
	ShFitPr Database::stainless316_d(){
		const ShFitPr fit = ConstFit::create(8027.0);
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_link("https://en.wikipedia.org/wiki/Brass");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::stainless316(){
		// create fits
		const ShFitPr cp_fit = stainless316_cp();
		const ShFitPr k_fit = stainless316_k();
		const ShFitPr rho_fit = stainless316_rho();
		const ShFitPr d_fit = stainless316_d();

		// create conductor and return
		const ShBaseConductorPr stainless = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		stainless->set_name("Stainless 316");
		stainless->set_classification("stainless");
		return stainless;
	}


	// ALUMINUM 5083-O
	// heat capacity (Source NIST)
	ShFitPr Database::aluminum5083O_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{46.6467,-314.292,866.662,-1298.3,1162.27,
			-637.795,210.351,-38.3094,2.96344});
		fit->set_name("Aluminum 5083-O");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/5083%20Aluminum/5083Aluminum_rev.htm");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::aluminum5083O_rho(){
		const ShFitPr kfit = aluminum5083O_k();
		kfit->set_t1(15.0);
		const ShFitPr fit = WFLFit::create(kfit);
		fit->set_t1(15.0);
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// thermal conductivity (Source NIST)
	ShFitPr Database::aluminum5083O_k(){
		const ShFitPr fit = Nist8SumFit::create(1,300,
			{-0.90933,5.751,-11.112,13.612,
			-9.3977,3.6873,-0.77295,0.067336,0});
		fit->set_name("Aluminum 5083-O");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/5083%20Aluminum/5083Aluminum_rev.htm");
		return fit;
	}

	// density
	ShFitPr Database::aluminum5083O_d(){
		const ShFitPr fit = ConstFit::create(2650.0);
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_link("https://www.azom.com/article.aspx?ArticleID=2804");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::aluminum5083O(){
		// create fits
		const ShFitPr cp_fit = aluminum5083O_cp();
		const ShFitPr k_fit = aluminum5083O_k();
		const ShFitPr rho_fit = aluminum5083O_rho();
		const ShFitPr d_fit = aluminum5083O_d();

		// create conductor and return
		const ShBaseConductorPr aluminum = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		aluminum->set_name("Aluminum 5083-O");
		aluminum->set_classification("metals");
		return aluminum;
	}


	// ALUMINUM 6061-T6
	// heat capacity (Source NIST)
	ShFitPr Database::aluminum6061t6_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{46.6467,-314.292,866.662,-1298.3,1162.27,
			-637.795,210.351,-38.3094,2.96344});
		fit->set_name("Aluminum 6061-T6");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("UNS A96061");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/6061%20Aluminum/6061_T6Aluminum_rev.htm");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::aluminum6061t6_rho(){
		const ShFitPr kfit = aluminum6061t6_k();
		kfit->set_t1(10.0);
		const ShFitPr fit = WFLFit::create(kfit);
		fit->set_t1(10.0);
		fit->set_name("Aluminum 6061-T6");
		fit->set_property_str("Electrical Resistivity");
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// thermal conductivity (Source NIST)
	ShFitPr Database::aluminum6061t6_k(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{0.07918,1.0957,-0.07277,0.08084,
			0.02803,-0.09464,0.04179,-0.00571,0});
		fit->set_name("Aluminum 6061-T6");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("UNS A96061");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/6061%20Aluminum/6061_T6Aluminum_rev.htm");
		return fit;

	}

	// density
	ShFitPr Database::aluminum6061t6_d(){
		const ShFitPr fit = ConstFit::create(2700.0);
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_link("https://www.kloecknermetals.com/blog/7075-aluminum-vs-6061-aluminum/");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::aluminum6061t6(){
		// create fits
		const ShFitPr cp_fit = aluminum6061t6_cp();
		const ShFitPr k_fit = aluminum6061t6_k();
		const ShFitPr rho_fit = aluminum6061t6_rho();
		const ShFitPr d_fit = aluminum6061t6_d();

		// create conductor and return
		const ShBaseConductorPr aluminum = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		aluminum->set_name("Aluminum 6061-T6");
		aluminum->set_classification("metals");
		return aluminum;
	}


	// ALUMINUM UNS A91100
	// heat capacity (Source NIST)
	ShFitPr Database::aluminumUNSA91100_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{46.6467,-314.292,866.662,-1298.3,1162.27,
			-637.795,210.351,-38.3094,2.96344});
		fit->set_name("Aluminum 5083-O");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/5083%20Aluminum/5083Aluminum_rev.htm");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::aluminumUNSA91100_rho(){
		const ShFitPr fit = WFLFit::create(aluminumUNSA91100_k());
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// thermal conductivity (Source NIST)
	ShFitPr Database::aluminumUNSA91100_k(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{23.39172,-148.5733,422.1917,-653.6664,
			607.0402,-346.152,118.4276,-22.2781,1.770187});
		fit->set_name("Aluminum 5083-O");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/1100%20Aluminum/1100%20Aluminum_rev.htm");
		return fit;
	}

	// density
	ShFitPr Database::aluminumUNSA91100_d(){
		const ShFitPr fit = ConstFit::create(2650.0);
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_link("https://www.azom.com/article.aspx?ArticleID=2804");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::aluminumUNSA91100(){
		// create fits
		const ShFitPr cp_fit = aluminumUNSA91100_cp();
		const ShFitPr k_fit = aluminumUNSA91100_k();
		const ShFitPr rho_fit = aluminumUNSA91100_rho();
		const ShFitPr d_fit = aluminumUNSA91100_d();

		// create conductor and return
		const ShBaseConductorPr aluminum = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		aluminum->set_name("Aluminum 1100 UNS A91100");
		aluminum->set_classification("metals");
		return aluminum;
	}


	// POLYIMIDE
	// heat capacity (Source NIST)
	ShFitPr Database::polyimide_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{-1.3684,0.65892,2.8719,0.42651,
			-3.0088,1.9558,-0.51998,0.051574,0});
		fit->set_name("Polyimide");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("-");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/Polyimide%20Kapton/PolyimideKapton_rev.htm");
		return fit;
	}

	// thermal conductivity (Source NIST)
	ShFitPr Database::polyimide_k(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{5.73101,-39.5199,79.9313,-83.8572,
			50.9157,-17.9835,3.42413,-0.27133,0});
		fit->set_name("Polyimide");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("NIST");
		fit->set_standard("-");
		fit->set_link("https://trc.nist.gov/cryogenics/materials/Polyimide%20Kapton/PolyimideKapton_rev.htm");
		return fit;
	}

	// density
	ShFitPr Database::polyimide_d(){
		const ShFitPr fit = ConstFit::create(1420.0);
		fit->set_name("Polyimide");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::polyimide(){
		// create fits
		const ShFitPr cp_fit = polyimide_cp();
		const ShFitPr k_fit = polyimide_k();
		const ShFitPr rho_fit = NULL; // insulator
		const ShFitPr d_fit = polyimide_d();

		// create conductor and return
		const ShBaseConductorPr polyimide = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		polyimide->set_name("Polyimide");
		polyimide->set_classification("plastics");
		return polyimide;
	}

	// alias for kapton
	ShBaseConductorPr Database::kapton(){
		return polyimide();
	}



	// NbTi LHC
	// critical current (Source CERN)
	ShFitPr Database::nbti_lhc_jc(){
		const ShFitPr fit = LubellKramerFit::create(
			31.4,0.63,1.0,2.3,1.7,14.5,9.2,3000.0e6);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("CERN");
		fit->set_standard("-");
		fit->set_link("https://cds.cern.ch/record/411159/files/lhc-project-report-358.pdf");
		return fit;
	}

	// power law N-value
	ShFitPr Database::nbti_lhc_nv(){
		const ShFitPr fit = ConstFit::create(50.0);
		fit->set_name("NbTi LHC");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("CERN");
		fit->set_standard("-");
		fit->set_link("https://cds.cern.ch/record/411159/files/lhc-project-report-358.pdf");
		return fit;
	}

	// heat capacity
	ShFitPr Database::nbti_lhc_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,250,
			{-3.2398, 13.9497, -47.9425, 104.3045,-127.3105, 
			90.9736, -37.7734, 8.4171, -0.7770});
		fit->set_name("NbTi LHC");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::nbti_lhc_rho(){
		ShFitPr fit = rat::mat::LinearFit::create(RAT_CONST(596e-9),RAT_CONST(0.3560e-9));
		fit->set_name("NbTi LHC");
		fit->set_property_str("Electrical Resistivity");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::nbti_lhc_k(){
		const ShFitPr fit = Nist8SumFit::create(4,250,
			{20.1575, -126.7509, 304.5748, 
			-380.0358, 274.4712, -116.9727, 
			28.2591,  -3.3839, 0.1325});
		fit->set_name("NbTi LHC");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// density
	ShFitPr Database::nbti_lhc_d(){
		const ShFitPr fit = ConstFit::create(8570.0);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}


	// conductor
	ShSuperConductorPr Database::nbti_lhc(){
		// create fits
		const ShFitPr jc_fit = nbti_lhc_jc();
		const ShFitPr nv_fit = nbti_lhc_nv();
		const ShFitPr cp_fit = nbti_lhc_cp();
		const ShFitPr k_fit = nbti_lhc_k();
		const ShFitPr rho_fit = nbti_lhc_rho();
		const ShFitPr d_fit = nbti_lhc_d();

		// create conductor and return
		const ShSuperConductorPr nbti = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		nbti->set_name("NbTi LHC");
		nbti->set_electric_field_criterion(10e-6);
		nbti->set_classification("superconductors");
		return nbti;
	}

	// a nbti wire specified with diameter, copper 
	// to superconductor ratio and RRR of stabilizer
	ShLTSWirePr Database::nbti_wire_lhc(
		const fltp diameter, 
		const fltp fcu2sc, 
		const fltp RRR){
		const ShLTSWirePr wire = LTSWire::create();
		wire->set_diameter(diameter);
		wire->set_fnc2sc(fcu2sc);
		wire->set_sc(nbti_lhc());
		wire->set_nc(copper_ofhc(RRR));
		wire->set_name("LHC Strand (CERN)");
		wire->set_classification("ltswires");
		wire->setup();
		return wire;
	}


	// NbTi LHC
	// critical current (Source CERN)
	ShFitPr Database::nbti_twente_jc(){
		const ShFitPr fit = LubellKramerFit::create(
			27.04,0.57,0.9,2.32,1.7,14.5,9.2,3000.0e6);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("Twente");
		fit->set_standard("-");
		fit->set_link("https://ieeexplore.ieee.org/document/1062311");
		return fit;
	}

	// power law N-value
	ShFitPr Database::nbti_twente_nv(){
		const ShFitPr fit = ConstFit::create(50.0);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("Twente");
		fit->set_standard("-");
		fit->set_link("https://ieeexplore.ieee.org/document/1062311");
		return fit;
	}

	// heat capacity
	ShFitPr Database::nbti_twente_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,250,
			{-3.2398, 13.9497, -47.9425, 104.3045,-127.3105, 
			90.9736, -37.7734, 8.4171, -0.7770});
		fit->set_name("NbTi Twente");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::nbti_twente_rho(){
		ShFitPr fit = rat::mat::LinearFit::create(RAT_CONST(596e-9),RAT_CONST(0.3560e-9));
		fit->set_name("NbTi LHC");
		fit->set_property_str("Electrical Resistivity");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::nbti_twente_k(){
		const ShFitPr fit = Nist8SumFit::create(4,250,
			{20.1575, -126.7509, 304.5748, 
			-380.0358, 274.4712, -116.9727, 
			28.2591,  -3.3839, 0.1325});
		fit->set_name("G10");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// density
	ShFitPr Database::nbti_twente_d(){
		const ShFitPr fit = ConstFit::create(8570.0);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}


	// conductor
	ShSuperConductorPr Database::nbti_twente(){
		// create fits
		const ShFitPr jc_fit = nbti_twente_jc();
		const ShFitPr nv_fit = nbti_twente_nv();
		const ShFitPr cp_fit = nbti_twente_cp();
		const ShFitPr k_fit = nbti_twente_k();
		const ShFitPr rho_fit = nbti_twente_rho();
		const ShFitPr d_fit = nbti_twente_d();

		// create conductor and return
		const ShSuperConductorPr nbti = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		nbti->set_name("NbTi Twente");
		nbti->set_electric_field_criterion(10e-6);
		nbti->set_classification("superconductors");
		return nbti;
	}

	// a nbti wire specified with diameter, copper 
	// to superconductor ratio and RRR of stabilizer
	ShLTSWirePr Database::nbti_wire_twente(
		const fltp diameter, 
		const fltp fcu2sc, 
		const fltp RRR){
		const ShLTSWirePr wire = LTSWire::create();
		wire->set_diameter(diameter);
		wire->set_fnc2sc(fcu2sc);
		wire->set_sc(nbti_twente());
		wire->set_nc(copper_ofhc(RRR));
		wire->setup();
		wire->set_name("LHC Strand (Twente)");
		wire->set_classification("ltswires");
		return wire;
	}




	// NB3SN RRP
	// critical current (Source God himself)
	ShFitPr Database::nb3sn_rrp_jc(){
		const ShFitPr fit = GodekeFit::create(47.6,6.4,0.00136,213e3,29.7,17.9,0.5,2.0);
		fit->set_name("Nb3Sn RRP");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}


	// power law N-value
	ShFitPr Database::nb3sn_rrp_nv(){
		const ShFitPr fit = ConstFit::create(30.0);
		fit->set_name("Nb3Sn RRP");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		return fit;
		fit->set_link("-");
	}

	// heat capacity
	ShFitPr Database::nb3sn_rrp_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::nb3sn_rrp_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::nb3sn_rrp_k(){
		return NULL;
	}

	// density
	ShFitPr Database::nb3sn_rrp_d(){
		const ShFitPr fit = ConstFit::create(8950.0);
		fit->set_name("NbTi LHC");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::nb3sn_rrp(){
		// create fits
		const ShFitPr jc_fit = nb3sn_rrp_jc();
		const ShFitPr nv_fit = nb3sn_rrp_nv();
		const ShFitPr cp_fit = nb3sn_rrp_cp();
		const ShFitPr k_fit = nb3sn_rrp_k();
		const ShFitPr rho_fit = nb3sn_rrp_rho();
		const ShFitPr d_fit = nb3sn_rrp_d();

		// create conductor and return
		const ShSuperConductorPr nb3sn = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		nb3sn->set_name("Nb3Sn RRP");
		nb3sn->set_electric_field_criterion(10e-6);
		nb3sn->set_classification("superconductors");
		return nb3sn;
	}

	// a Nb3Sn RRP wire specified with diameter, copper 
	// to superconductor ratio and RRR of stabilizer
	ShLTSWirePr Database::nb3sn_wire_rrp(
		const fltp diameter, 
		const fltp fcu2sc, 
		const fltp RRR){
		const ShLTSWirePr wire = LTSWire::create();
		wire->set_diameter(diameter);
		wire->set_fnc2sc(fcu2sc);
		wire->set_sc(nb3sn_rrp());
		wire->set_nc(copper_ofhc(RRR));
		wire->setup();
		wire->set_classification("ltswires");
		wire->set_name("Nb3Sn RRP Wire");
		return wire;
	}

	// fresca 2 cable
	ShRutherfordCablePr Database::nb3sn_fresca2_cable(const bool insulated){
		ShRutherfordCablePr rf = RutherfordCable::create();
		rf->set_num_strands(40);
		rf->set_width(21.4e-3);
		rf->set_thickness(1.82e-3);
		rf->set_keystone(0.0);
		rf->set_pitch(120e-3);
		rf->set_dinsu(insulated ? 100e-6 : 0.0);
		rf->set_fcabling(0.9);
		rf->set_strand(nb3sn_wire_rrp(1e-3, 1.25, 150));
		rf->set_insu(g10_normal());
		rf->set_voids(NULL);
		if(insulated){
			rf->set_name("Fresca2 Rutherford Cable Insu");
		}else{
			rf->set_name("Fresca2 Rutherford Cable");
		}
		rf->set_classification("ltscables");
		rf->setup();
		return rf;
	}


	// NB3SN PIT
	// critical current (Source God himself)
	ShFitPr Database::nb3sn_pit_jc(){
		const ShFitPr fit = GodekeFit::create(
			47.6,6.4,0.00136,200e3,29.7,17.9,0.5,2.0,0.0);
		fit->set_name("Nb3Sn PIT");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// power law N-value
	ShFitPr Database::nb3sn_pit_nv(){
		const ShFitPr fit = ConstFit::create(30.0);
		fit->set_name("Nb3Sn PIT");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::nb3sn_pit_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::nb3sn_pit_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::nb3sn_pit_k(){
		return NULL;
	}

	// density
	ShFitPr Database::nb3sn_pit_d(){
		const ShFitPr fit = ConstFit::create(8950.0);
		fit->set_name("Nb3Sn PIT");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::nb3sn_pit(){
		// create fits
		const ShFitPr jc_fit = nb3sn_pit_jc();
		const ShFitPr nv_fit = nb3sn_pit_nv();
		const ShFitPr cp_fit = nb3sn_pit_cp();
		const ShFitPr k_fit = nb3sn_pit_k();
		const ShFitPr rho_fit = nb3sn_pit_rho();
		const ShFitPr d_fit = nb3sn_pit_d();

		// create conductor and return
		const ShSuperConductorPr nb3sn = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		nb3sn->set_name("Nb3Sn PIT");
		nb3sn->set_classification("superconductors");
		nb3sn->set_electric_field_criterion(10e-6);
		return nb3sn;
	}



	// NB3SN ITER
	// critical current (Source God himself)
	ShFitPr Database::nb3sn_iter_jc(){
		const ShFitPr fit = GodekeFit::create(
			47.6,6.4,0.00136,180e3,29.7,17.9,0.5,2.0,0.0);
		fit->set_name("Nb3Sn Iter");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// power law N-value
	ShFitPr Database::nb3sn_iter_nv(){
		const ShFitPr fit = ConstFit::create(30.0);
		fit->set_name("Nb3Sn Iter");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("A. Godeke");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::nb3sn_iter_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::nb3sn_iter_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::nb3sn_iter_k(){
		return NULL;
	}

	// density
	ShFitPr Database::nb3sn_iter_d(){
		const ShFitPr fit = ConstFit::create(8950.0);
		fit->set_name("Nb3Sn Iter");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::nb3sn_iter(){
		// create fits
		const ShFitPr jc_fit = nb3sn_iter_jc();
		const ShFitPr nv_fit = nb3sn_iter_nv();
		const ShFitPr cp_fit = nb3sn_iter_cp();
		const ShFitPr k_fit = nb3sn_iter_k();
		const ShFitPr rho_fit = nb3sn_iter_rho();
		const ShFitPr d_fit = nb3sn_iter_d();

		// create conductor and return
		const ShSuperConductorPr nb3sn = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		nb3sn->set_name("Nb3Sn ITER");
		nb3sn->set_electric_field_criterion(10e-6);
		return nb3sn;
	}


	// REBCO FUJIKURA CERN
	// critical current (Source CERN)
	ShFleiterFitPr Database::rebco_fujikura_cern_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.03); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(1.86e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(4.45); fit->set_pab(1.0);
		fit->set_qab(5.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(68.3e12); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);
		fit->set_name("REBCO Fujikura CERN");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("CERN");
		fit->set_standard("-");
		fit->set_link("https://cds.cern.ch/record/2237681?ln=en");
		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_fujikura_cern_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Fujikura CERN");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("CERN");
		fit->set_standard("-");
		fit->set_link("https://cds.cern.ch/record/2237681?ln=en");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_fujikura_cern_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_fujikura_cern_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_fujikura_cern_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_fujikura_cern_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Fujikura CERN");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_fujikura_cern(){
		// create fits
		const ShFitPr jc_fit = rebco_fujikura_cern_jc();
		const ShFitPr nv_fit = rebco_fujikura_cern_nv();
		const ShFitPr cp_fit = rebco_fujikura_cern_cp();
		const ShFitPr k_fit = rebco_fujikura_cern_k();
		const ShFitPr rho_fit = rebco_fujikura_cern_rho();
		const ShFitPr d_fit = rebco_fujikura_cern_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Fujikura CERN");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_fujikura_core_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_fujikura_cern(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Fujikura CERN");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_fujikura_tape_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_fujikura_core_cern(
			width-2*tstab, thickness - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_name("REBCO Tape Fujikura CERN");
		tape->set_classification("htstape");
		return tape;
	}


	// REBCO FARADAY RAT
	ShFleiterFitPr Database::rebco_faraday_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.1); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(7.2e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(4.45); fit->set_pab(1.0);
		fit->set_qab(3.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(1.6e14); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);
		fit->set_name("REBCO Faraday RAT");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("RAT");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/15095703");
		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_faraday_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Faraday RAT");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_faraday_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_faraday_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_faraday_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_faraday_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Faraday");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_faraday_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_faraday_rat_jc();
		const ShFitPr nv_fit = rebco_faraday_rat_nv();
		const ShFitPr cp_fit = rebco_faraday_rat_cp();
		const ShFitPr k_fit = rebco_faraday_rat_k();
		const ShFitPr rho_fit = rebco_faraday_rat_rho();
		const ShFitPr d_fit = rebco_faraday_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Faraday Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for superox hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_faraday_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_faraday_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Faraday RAT");
		core->set_classification("htscore");
		return core;
	}

	// superox tape
	ShHTSTapePr Database::rebco_faraday_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_faraday_core_rat(
			width-2*tstab, thickness - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_name("REBCO Tape Faraday RAT");
		tape->set_classification("htstape");
		return tape;
	}


	// REBCO SUPEROX YBCO RAT
	ShFleiterFitPr Database::rebco_superox_ybco_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.1); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(3.0e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(4.45); fit->set_pab(1.0);
		fit->set_qab(3.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(0.95e14); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);
		fit->set_name("REBCO SuperOX YBCO RAT");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("RAT");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/13708690");
		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_superox_ybco_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO SuperOX YBCO RAT");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_superox_ybco_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_superox_ybco_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_superox_ybco_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_superox_ybco_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO SuperOX YBCO");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_superox_ybco_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_superox_ybco_rat_jc();
		const ShFitPr nv_fit = rebco_superox_ybco_rat_nv();
		const ShFitPr cp_fit = rebco_superox_ybco_rat_cp();
		const ShFitPr k_fit = rebco_superox_ybco_rat_k();
		const ShFitPr rho_fit = rebco_superox_ybco_rat_rho();
		const ShFitPr d_fit = rebco_superox_ybco_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO SuperOX YBCO Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}


	// core for superox hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_superox_ybco_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_superox_ybco_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core SuperOX YBCO RAT");
		core->set_classification("htscore");
		return core;
	}

	// superox tape
	ShHTSTapePr Database::rebco_superox_ybco_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_superox_ybco_core_rat(
			width-2*tstab, thickness - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_name("REBCO Tape SuperOX YBCO RAT");
		tape->set_classification("htstape");
		return tape;
	}





	// REBCO THEVA PROLINE RAT
	// critical current (Source RAT JvN)
	// based on data from RRI
	ShFleiterFitPr Database::rebco_theva_proline_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		
		fit->set_g0(0.05); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(1.25e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(2.8); fit->set_pab(1.0);
		fit->set_qab(12.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(3.8e13); fit->set_pkoff(-24*arma::Datum<fltp>::tau/360); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(false);

		fit->set_name("REBCO Theva Proline Rat");
		fit->set_property_str("Critical Current");
		
		fit->set_source("Robinson Research Institute");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/3759327");

		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_theva_proline_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Theva Proline Rat");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("Rat");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_theva_proline_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_theva_proline_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_theva_proline_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_theva_proline_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Theva Proline Rat");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_theva_proline_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_theva_proline_rat_jc();
		const ShFitPr nv_fit = rebco_theva_proline_rat_nv();
		const ShFitPr cp_fit = rebco_theva_proline_rat_cp();
		const ShFitPr k_fit = rebco_theva_proline_rat_k();
		const ShFitPr rho_fit = rebco_theva_proline_rat_rho();
		const ShFitPr d_fit = rebco_theva_proline_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Theva Proline Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_theva_proline_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_theva_proline_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Theva Proline Rat");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_theva_proline_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR, 
		const arma::uword num_cores){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_theva_proline_core_rat(
			width-2*tstab, thickness/num_cores - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_num_cores(num_cores);
		tape->set_name("REBCO Tape Theva Proline Rat");
		tape->set_classification("htstape");
		return tape;
	}



	// REBCO THEVA PROLINE RAT
	// critical current (Source RAT JvN)
	// based on data from RRI
	ShFleiterFitPr Database::rebco_theva_proline_adv_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		
		fit->set_g0(0.05); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(3.6e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(2.8); fit->set_pab(1.0);
		fit->set_qab(12.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(6.5e13); fit->set_pkoff(-24*arma::Datum<fltp>::tau/360); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(false);

		fit->set_name("REBCO Theva Proline Adv. Rat");
		fit->set_property_str("Critical Current");
		
		fit->set_source("Robinson Research Institute");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/25893079");

		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_theva_proline_adv_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Theva Proline Adv. Rat");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("Rat");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_theva_proline_adv_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_theva_proline_adv_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_theva_proline_adv_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_theva_proline_adv_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Theva Proline Adv. Rat");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_theva_proline_adv_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_theva_proline_adv_rat_jc();
		const ShFitPr nv_fit = rebco_theva_proline_adv_rat_nv();
		const ShFitPr cp_fit = rebco_theva_proline_adv_rat_cp();
		const ShFitPr k_fit = rebco_theva_proline_adv_rat_k();
		const ShFitPr rho_fit = rebco_theva_proline_adv_rat_rho();
		const ShFitPr d_fit = rebco_theva_proline_adv_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Theva Proline Adv. Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_theva_proline_adv_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_theva_proline_adv_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Theva Proline Adv. Rat");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_theva_proline_adv_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR, 
		const arma::uword num_cores){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_theva_proline_adv_core_rat(
			width-2*tstab, thickness/num_cores - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_num_cores(num_cores);
		tape->set_name("REBCO Tape Theva Proline Adv. Rat");
		tape->set_classification("htstape");
		return tape;
	}


	// SUPERPOWER ADV PINNING
	// critical current (Source RAT JvN)
	// based on data from RRI
	ShFleiterFitPr Database::rebco_superpower_adv_pinning_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.1); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(2.4e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(4.45); fit->set_pab(1.0);
		fit->set_qab(4.5); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(6.9e13); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);

		fit->set_name("REBCO SupwerPower Adv Pinning Rat");
		fit->set_property_str("Critical Current");
		
		fit->set_source("Robinson Research Institute");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/4256624");

		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_superpower_adv_pinning_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO SuperPower Adv Pinning Rat");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("Rat");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_superpower_adv_pinning_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_superpower_adv_pinning_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_superpower_adv_pinning_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_superpower_adv_pinning_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO SuperPower Adv Pinning Rat");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_superpower_adv_pinning_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_superpower_adv_pinning_rat_jc();
		const ShFitPr nv_fit = rebco_superpower_adv_pinning_rat_nv();
		const ShFitPr cp_fit = rebco_superpower_adv_pinning_rat_cp();
		const ShFitPr k_fit = rebco_superpower_adv_pinning_rat_k();
		const ShFitPr rho_fit = rebco_superpower_adv_pinning_rat_rho();
		const ShFitPr d_fit = rebco_superpower_adv_pinning_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO SuperPower Adv Pinning Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_superpower_adv_pinning_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_superpower_adv_pinning_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core SuperPower Adv Pinning Rat");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_superpower_adv_pinning_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR, 
		const arma::uword num_cores){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_superpower_adv_pinning_core_rat(
			width-2*tstab, thickness/num_cores - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_num_cores(num_cores);
		tape->set_name("REBCO Tape SuperPower Adv Pinning Rat");
		tape->set_classification("htstape");
		return tape;
	}

	// REBCO FUJIKURA FESC RAT
	// critical current (Source RAT JvN)
	// based on data from Tohoku University
	ShFleiterFitPr Database::rebco_fujikura_fesc_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.04); fit->set_g1(0.1); fit->set_g2(0.01); fit->set_g3(0.1);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(4.2e12); fit->set_nu(1.6);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(5.2); fit->set_pab(1.0);
		fit->set_qab(3.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(9.1e13); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);

		fit->set_name("REBCO Fujikura FESC Rat");
		fit->set_property_str("Critical Current");
		
		fit->set_source("Tohoku University, Institute for Materials Research, High Field Laboratory for Superconducting Materials");
		fit->set_standard("-");
		fit->set_link("https://www.fujikura.co.jp/eng/products/newbusiness/superconductors/01/superconductor.pdf");

		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_fujikura_fesc_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Fujikura FESC Rat");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("Rat");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_fujikura_fesc_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_fujikura_fesc_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_fujikura_fesc_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_fujikura_fesc_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Fujikura CERN");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_fujikura_fesc_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_fujikura_fesc_rat_jc();
		const ShFitPr nv_fit = rebco_fujikura_fesc_rat_nv();
		const ShFitPr cp_fit = rebco_fujikura_fesc_rat_cp();
		const ShFitPr k_fit = rebco_fujikura_fesc_rat_k();
		const ShFitPr rho_fit = rebco_fujikura_fesc_rat_rho();
		const ShFitPr d_fit = rebco_fujikura_fesc_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Fujikura FESC Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_fujikura_fesc_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_fujikura_fesc_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Fujikura FESC Rat");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_fujikura_fesc_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR, 
		const arma::uword num_cores){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_fujikura_fesc_core_rat(
			width-2*tstab, thickness/num_cores - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_num_cores(num_cores);
		tape->set_name("REBCO Tape Fujikura FESC Rat");
		tape->set_classification("htstape");
		return tape;
	}


	// REBCO SHANGHAI HIGH FIELD RAT
	// critical current (Source RAT JvN)
	// based on data from Robinson Research Institute
	ShFleiterFitPr Database::rebco_shanghai_high_field_rat_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.1); fit->set_g1(0.25); fit->set_g2(0.06); fit->set_g3(0.058);
		fit->set_Tc0(93); fit->set_pc(0.5); fit->set_qc(2.5); fit->set_Bi0c(140); 
		fit->set_gammac(2.44); fit->set_alphac(3.1e12); fit->set_nu(1.85);
		fit->set_n(1.0); fit->set_n1(1.4); fit->set_n2(4.8); fit->set_pab(1.0);
		fit->set_qab(3.0); fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.63);
		fit->set_alphaab(8.2e13); fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);
		fit->set_is_public(true);

		fit->set_name("REBCO Shanghai High Field Rat");
		fit->set_property_str("Critical Current");
		
		fit->set_source("Robinson Research Institute");
		fit->set_standard("-");
		fit->set_link("https://htsdb.wimbush.eu/dataset/5331145");

		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_shanghai_high_field_rat_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Shanghai High Field Rat");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("Rat");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_shanghai_high_field_rat_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_shanghai_high_field_rat_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_shanghai_high_field_rat_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_shanghai_high_field_rat_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Shanghai High Field Rat");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_shanghai_high_field_rat(){
		// create fits
		const ShFitPr jc_fit = rebco_shanghai_high_field_rat_jc();
		const ShFitPr nv_fit = rebco_shanghai_high_field_rat_nv();
		const ShFitPr cp_fit = rebco_shanghai_high_field_rat_cp();
		const ShFitPr k_fit = rebco_shanghai_high_field_rat_k();
		const ShFitPr rho_fit = rebco_shanghai_high_field_rat_rho();
		const ShFitPr d_fit = rebco_shanghai_high_field_rat_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Shanghai High Field Rat");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for fujikura hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_shanghai_high_field_core_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_shanghai_high_field_rat(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Shanghai High Field Rat");
		core->set_classification("htscore");
		return core;
	}

	// fujikura tape
	ShHTSTapePr Database::rebco_shanghai_high_field_tape_rat(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR, 
		const arma::uword num_cores){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_shanghai_high_field_core_rat(
			width-2*tstab, thickness/num_cores - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_num_cores(num_cores);
		tape->set_name("REBCO Tape Shanghai High Field Rat");
		tape->set_classification("htstape");
		return tape;
	}



	// REBCO THEVA CERN
	// critical current (Source CERN)
	ShFleiterFitPr Database::rebco_theva_cern_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.18); fit->set_g1(0.58); fit->set_g2(0.09); fit->set_g3(0.072);
		fit->set_Tc0(92.5); fit->set_pc(0.66); fit->set_qc(9.3); fit->set_Bi0c(140);
		fit->set_gammac(1.82); fit->set_alphac(2.0e12); fit->set_nu(1.81);
		fit->set_n(1.0); fit->set_n1(0.89); fit->set_n2(1.89); fit->set_pab(0.92);
		fit->set_qab(8.3); fit->set_Bi0ab(250); fit->set_a(0.16); fit->set_gammaab(1.61);
		fit->set_alphaab(22e12); fit->set_pkoff(24.0*2*arma::Datum<fltp>::pi/360); fit->set_tsc(2e-06);
		fit->set_type0_flip(false); fit->set_ignore_pkoff(false);
		fit->set_name("REBCO Theva CERN");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("T. Nes (CERN)");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_theva_cern_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Theva CERN");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("T. Nes (CERN)");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_theva_cern_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_theva_cern_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_theva_cern_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_theva_cern_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Theva CERN");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_theva_cern(){
		// create fits
		const ShFitPr jc_fit = rebco_theva_cern_jc();
		const ShFitPr nv_fit = rebco_theva_cern_nv();
		const ShFitPr cp_fit = rebco_theva_cern_cp();
		const ShFitPr k_fit = rebco_theva_cern_k();
		const ShFitPr rho_fit = rebco_theva_cern_rho();
		const ShFitPr d_fit = rebco_theva_cern_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Theva CERN");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for Theva hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_theva_core_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		const ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_theva_cern(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Theva CERN");
		core->set_classification("htscore");
		return core;
	}

	// theva tape
	ShHTSTapePr Database::rebco_theva_tape_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_theva_core_cern(
			width-2*tstab, thickness - 2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_name("REBCO Tape Theva CERN");
		tape->set_classification("htstape");
		return tape;
	}


	// REBCO SHANGHAI CERN
	// critical current (Source T. Nes CERN)
	ShFleiterFitPr Database::rebco_shanghai_cern_jc(){
		const ShFleiterFitPr fit = FleiterFit::create();
		fit->set_g0(0.05); fit->set_g1(0.19); fit->set_g2(0.001); fit->set_g3(0.018);
		fit->set_Tc0(93); fit->set_pc(0.8); fit->set_qc(9.7); fit->set_Bi0c(140);
		fit->set_gammac(2.46); fit->set_alphac(8.2e12); fit->set_nu(1.0); fit->set_n(1.0);
		fit->set_n1(1.4); fit->set_n2(6.9); fit->set_pab(0.86); fit->set_qab(1.6);
		fit->set_Bi0ab(250); fit->set_a(0.1); fit->set_gammaab(1.6); fit->set_alphaab(35e12);
		fit->set_pkoff(0); fit->set_tsc(2e-06); 
		fit->set_type0_flip(false); fit->set_ignore_pkoff(true);
		fit->set_name("REBCO Shanghai CERN");
		fit->set_property_str("Critical Current");
		fit->set_is_public(true);
		fit->set_source("T. Nes (CERN)");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// power law N-value
	ShFitPr Database::rebco_shanghai_cern_nv(){
		const ShFitPr fit = ConstFit::create(20.0);
		fit->set_name("REBCO Shanghai CERN");
		fit->set_property_str("N-Value");
		fit->set_is_public(true);
		fit->set_source("T. Nes (CERN)");
		fit->set_standard("-");
		fit->set_link("-");
		return fit;
	}

	// heat capacity
	ShFitPr Database::rebco_shanghai_cern_cp(){
		return NULL;
	}

	// electrical resistivity
	ShFitPr Database::rebco_shanghai_cern_rho(){
		return NULL;
	}

	// thermal conductivity
	ShFitPr Database::rebco_shanghai_cern_k(){
		return NULL;
	}

	// density
	ShFitPr Database::rebco_shanghai_cern_d(){
		const ShFitPr fit = ConstFit::create(6380.0);
		fit->set_name("REBCO Shanghai CERN");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// conductor
	ShSuperConductorPr Database::rebco_shanghai_cern(){
		// create fits
		const ShFitPr jc_fit = rebco_shanghai_cern_jc();
		const ShFitPr nv_fit = rebco_shanghai_cern_nv();
		const ShFitPr cp_fit = rebco_shanghai_cern_cp();
		const ShFitPr k_fit = rebco_shanghai_cern_k();
		const ShFitPr rho_fit = rebco_shanghai_cern_rho();
		const ShFitPr d_fit = rebco_shanghai_cern_d();

		// create conductor and return
		const ShSuperConductorPr rebco = 
			std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
		rebco->set_name("REBCO Shanghai CERN");
		rebco->set_electric_field_criterion(100e-6);
		rebco->set_classification("superconductors");
		return rebco;
	}

	// core for Shanghai hts tape
	// this contains substrate and superconductor
	// but not stabilizer
	ShHTSCorePr Database::rebco_shanghai_core_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc){
		if(tsubstr + tsc>thickness)
			rat_throw_line("layers combined exceed core thickness");
		const ShHTSCorePr core = HTSCore::create(
			width, thickness, 
			tsc, rebco_shanghai_cern(), 
			tsubstr, stainless316());
		core->set_name("REBCO Core Shanghai CERN");
		core->set_classification("htscore");
		return core;
	}

	// shanghai tape
	ShHTSTapePr Database::rebco_shanghai_tape_cern(
		const fltp width, const fltp thickness,
		const fltp tsubstr, const fltp tsc, 
		const fltp tstab, const fltp RRR){
		if(tsubstr + tsc + tstab>thickness)
			rat_throw_line("layers combined exceed tape thickness");
		const ShHTSCorePr core = rebco_shanghai_core_cern(
			width-2*tstab, thickness-2*tstab, tsubstr, tsc);
		const ShHTSTapePr tape = HTSTape::create(width,thickness,core,copper_ofhc(RRR));
		tape->set_name("REBCO Tape Shanghai CERN");
		tape->set_classification("htstape");
		return tape;
	}




	// G10
	// heat capacity
	ShFitPr Database::g10_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,300,
			{-2.4083,7.6006,-8.2982,7.3301,
			-4.2386,1.4294,-0.24396,0.015236,0});
		fit->set_name("G10");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// density
	ShFitPr Database::g10_d(){
		const ShFitPr fit = ConstFit::create(1948.0);
		fit->set_name("G10");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		return fit;
	}

	// G10 wih anisotropy build in
	// warp direction is in-plane
	// normal direction is dir
	ShAnisotropicPr Database::g10(){
		const ShAnisotropicPr g10 = Anisotropic::create(g10_warp(), g10_normal(), g10_warp());
		g10->set_name("G10");
		g10->set_classification("composites");
		return g10;
	}

	// G10 NORMAL DIRECTION
	// thermal conductivity
	ShFitPr Database::g10_normal_k(){
		const ShFitPr fit = Nist8SumFit::create(10,300,
			{-4.1236,13.788,-26.068,26.272,
			-14.663,4.4954,-0.6905,0.0397,0});
		fit->set_name("G10 Normal");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// normal G10
	ShBaseConductorPr Database::g10_normal(){
		// create fits
		const ShFitPr cp_fit = g10_cp();
		const ShFitPr k_fit = g10_normal_k();
		const ShFitPr rho_fit = NULL; // insulator
		const ShFitPr d_fit = g10_d();

		// create conductor and return
		const ShBaseConductorPr g10 = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		g10->set_name("G10 Normal");
		g10->set_classification("composites");
		return g10;
	}

	// G10 WARP DIRECTION
	// thermal conductivity
	ShFitPr Database::g10_warp_k(){
		const ShFitPr fit = Nist8SumFit::create(12,300,
			{-2.64827,8.80228,-24.8998,41.1625,
			-39.8754,23.1778,-7.95635,1.48806,-0.11701});
		fit->set_name("G10 Warp");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}	

	// warp g10
	ShBaseConductorPr Database::g10_warp(){
		// create fits
		const ShFitPr cp_fit = g10_cp();
		const ShFitPr k_fit = g10_warp_k();
		const ShFitPr rho_fit = NULL; // insulator
		const ShFitPr d_fit = g10_d();

		// create conductor and return
		const ShBaseConductorPr g10 = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		g10->set_name("G10 Warp");
		g10->set_classification("composites");
		return g10;
	}


	// INDIUM
	// heat capacity (Source NIST)
	ShFitPr Database::indium_cp(){
		return Nist8SumFit::create(4,295,
			{-2.4259351,12.613611,-46.472893,104.64717,
			-127.18630,88.805612,-35.915625,7.8307989,
			-0.71218931});
	}

	// electrical resistivity
	ShFitPr Database::indium_rho(){
		const ShFitPr fit = WFLFit::create(indium_k());
		fit->set_name("Indium");
		fit->set_property_str("Electrical Resistivity");
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::indium_k(){
		const ShFitPr fit = Nist8FracFit::create(4,300,
			{0.30824,-6.19488,-8.95483,8.62819,
			14.37326,-4.19622,-7.16747,0.76291,1.41103});
		fit->set_name("Indium");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// density
	ShFitPr Database::indium_d(){
		const ShFitPr fit = ConstFit::create(7330.0);
		fit->set_name("Indium");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::indium(){
		// create fits
		const ShFitPr cp_fit = indium_cp();
		const ShFitPr k_fit = indium_k();
		const ShFitPr rho_fit = indium_rho();
		const ShFitPr d_fit = indium_d();

		// create conductor
		const ShBaseConductorPr indium = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);

		// name conductor
		indium->set_name("Indium");
		indium->set_classification("metals");

		// return conductor object
		return indium;
	}


	// SILVER
	// heat capacity (Source NIST)
	ShFitPr Database::silver_cp(){
		const ShFitPr fit = Nist8SumFit::create(4,295,
			{-1.257900,-0.548135,4.514223,-4.889068,
			4.506086,-2.302068,0.326303,0.102638,-0.026439});
		fit->set_name("Silver");
		fit->set_property_str("Heat Capacity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// electrical resistivity
	ShFitPr Database::silver_rho(){
		const ShFitPr fit = WFLFit::create(silver_k());
		fit->set_name("Silver");
		fit->set_property_str("Electrical Resistivity");
		fit->set_is_public(true);
		fit->set_source("Wikipedia");
		fit->set_link("https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law");
		return fit;
	}

	// thermal conductivity
	ShFitPr Database::silver_k(){
		const ShFitPr fit = Nist8FracFit::create(4,300,
			{1.8430078,-0.4263983,-0.6153774,0.1318708,
			0.2504744,-0.0219176,-0.0503132,0.0016109,0.0040387});
		fit->set_name("Silver");
		fit->set_property_str("Thermal Conductivity");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// density
	ShFitPr Database::silver_d(){
		const ShFitPr fit = ConstFit::create(10470.0);
		fit->set_name("Silver");
		fit->set_property_str("Density");
		fit->set_is_public(true);
		fit->set_source("-");
		fit->set_link("-");
		return fit;
	}

	// conductor
	ShBaseConductorPr Database::silver(){
		// create fits
		const ShFitPr cp_fit = silver_cp();
		const ShFitPr k_fit = silver_k();
		const ShFitPr rho_fit = silver_rho();
		const ShFitPr d_fit = silver_d();

		// create conductor
		const ShBaseConductorPr silver = 
			std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);

		// name conductor
		silver->set_name("Silver");
		silver->set_classification("metals");

		// return conductor object
		return silver;
	}

	// void 
	ShBaseConductorPr Database::void_material(){
		const ShBaseConductorPr void_conductor = BaseConductor::create(NULL,NULL,NULL,NULL);
		void_conductor->set_name("Void");
		return void_conductor;
	}

	// GLYNCABLE
	ShRutherfordCablePr Database::nbti_cct_cable(
		const fltp spacing, const fltp diameter, 
		const fltp fcu2sc,  const fltp RRR){
		
		// construct conductor
		rat::mat::ShRutherfordCablePr nbti_cable = rat::mat::RutherfordCable::create();
		nbti_cable->set_strand(rat::mat::Database::nbti_wire_lhc(diameter,fcu2sc,RRR));
		nbti_cable->set_num_strands(1);
		nbti_cable->set_width(spacing);
		nbti_cable->set_thickness(spacing);
		nbti_cable->set_keystone(0);
		nbti_cable->set_pitch(0);
		nbti_cable->set_dinsu(0);
		nbti_cable->set_fcabling(1.0);
		nbti_cable->set_name("NbTi CCT Cable");
		nbti_cable->set_classification("ltscables");
		nbti_cable->setup();
		return nbti_cable;
	} 


}}