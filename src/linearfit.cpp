// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "linearfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LinearFit::LinearFit(){
		C0_ = 1; C1_ = 10;
		set_name("linearfit");
	}

	LinearFit::LinearFit(const fltp C0, const fltp C1){
		C0_ = C0; C1_ = C1;
		set_name("linearfit");
	}

	// LinearFit::LinearFit(const fltp t0, const fltp t1, const fltp C0, const fltp C1){
	// 	C0_ = C0; C1_ = C1;
	// 	set_name("linearfit");
	// }

	// factories
	ShLinearFitPr LinearFit::create(){
		return std::make_shared<LinearFit>();
	}

	ShLinearFitPr LinearFit::create(const fltp C0, const fltp C1){
		return std::make_shared<LinearFit>(C0, C1);
	}

	// ShLinearFitPr LinearFit::create(const fltp t0, const fltp t1, const fltp C0, const fltp C1){
	// 	return std::make_shared<LinearFit>(t0, t1, C0, C1);
	// }

	// setters
	void LinearFit::set_C0(const fltp C0){
		C0_ = C0;
	}
	
	void LinearFit::set_C1(const fltp C1){
		C1_ = C1;
	}

	// getters
	fltp LinearFit::get_C0()const{
		return C0_;
	}

	fltp LinearFit::get_C1()const{
		return C1_;
	}

	// scalar fit function
	fltp LinearFit::calc_property(
		const fltp X) const{

		// clamp temperature as to avoid negative values
		const fltp value = C0_ + C1_ * X;

		// return calculated value
		return value; // [User's unit]
	}

	// vector fit function
	arma::Col<fltp> LinearFit::calc_property(
		const arma::Col<fltp> &X) const{

		// clamp temperature as to avoid negative values in fit
		arma::Col<fltp> value = C0_ + C1_ * X;

		// return calculated value
		return value; // [User's unit]
	}

	// get type
	std::string LinearFit::get_type(){
		return "rat::mat::linearfit";
	}

	// method for serialization into json
	void LinearFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
	}

	// method for deserialisation from json
	void LinearFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		C0_ = js["C0"].ASFLTP();
		C1_ = js["C1"].ASFLTP();
	}

}}