// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "debeyeresistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	DebeyeResistivityFit::DebeyeResistivityFit(){
		C0_ = 1; C1_ = 10; C2_ = 0.5;
		set_name("debeyeresistivityfit");
	}

	DebeyeResistivityFit::DebeyeResistivityFit(
		const fltp C0, const fltp C1, const fltp C2){
		C0_ = C0; C1_ = C1; C2_ = C2;		
		t1_ = 1.0; t2_ = 300.0;
		set_name("debeyeresistivityfit");
	}

	DebeyeResistivityFit::DebeyeResistivityFit(
		const fltp t0, const fltp t1, 
		const fltp C0, const fltp C1, 
		const fltp C2){
		C0_ = C0; C1_ = C1; C2_ = C2;		
		t1_ = t0; t2_ = t1;
		set_name("debeyeresistivityfit");
	}

	// factory
	ShDebeyeResistivityFitPr DebeyeResistivityFit::create(){
		return std::make_shared<DebeyeResistivityFit>();
	}

	ShDebeyeResistivityFitPr DebeyeResistivityFit::create(
		const fltp C0, const fltp C1, const fltp C2){
		return std::make_shared<DebeyeResistivityFit>(C0, C1, C2);
	}

	ShDebeyeResistivityFitPr DebeyeResistivityFit::create(
		const fltp t0, const fltp t1, const fltp C0, 
		const fltp C1, const fltp C2){
		return std::make_shared<DebeyeResistivityFit>(t0,t1, C0, C1, C2);
	}

	// setters
	void DebeyeResistivityFit::set_C0(const fltp C0){
		C0_ = C0;
	}
	
	void DebeyeResistivityFit::set_C1(const fltp C1){
		C1_ = C1;
	}

	void DebeyeResistivityFit::set_C2(const fltp C2){
		C2_ = C2;
	}


	// getters
	fltp DebeyeResistivityFit::get_C0()const{
		return C0_;
	}

	fltp DebeyeResistivityFit::get_C1()const{
		return C1_;
	}

	fltp DebeyeResistivityFit::get_C2()const{
		return C2_;
	}


	// scalar fit function
	fltp DebeyeResistivityFit::calc_property(
		const fltp temperature) const{

		// clamp temperature as to avoid negative values
		const fltp T = std::max(temperature, t1_);

		const fltp value = C0_ + C1_ * ((T - C2_) * (T - C2_) * (T - C2_));

		// return calculated value
		return value; // [User's unit]
	}

	// vector fit function
	arma::Col<fltp> DebeyeResistivityFit::calc_property(
		const arma::Col<fltp> &temperature) const{

		// clamp temperature as to avoid negative values in fit
		const arma::Col<fltp> T = arma::clamp(temperature, t1_, arma::Datum<fltp>::inf);

		arma::Col<fltp> value = C0_ + C1_ * ((T - C2_) % (T - C2_) % (T - C2_));

		// return calculated value
		return value; // [User's unit]
	}

	// get type
	std::string DebeyeResistivityFit::get_type(){
		return "rat::mat::debeyeresistivityfit";
	}

	// method for serialization into json
	void DebeyeResistivityFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
		js["C2"] = C2_;		
	}

	// method for deserialisation from json
	void DebeyeResistivityFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		C0_ = js["C0"].ASFLTP();
		C1_ = js["C1"].ASFLTP();
		C2_ = js["C2"].ASFLTP();
	}

}}