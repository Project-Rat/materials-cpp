// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "parallelconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	ParallelConductor::ParallelConductor(){
		set_name("parallel conductor");
	}

	// constructor
	ParallelConductor::ParallelConductor(
		const fltp fraction, const ShConductorPr& conductor) : ParallelConductor(){
		add_conductor(fraction, conductor);
	}

	// constructor
	ParallelConductor::ParallelConductor(
		const fltp fraction1, const ShConductorPr& conductor1,
		const fltp fraction2, const ShConductorPr& conductor2) : ParallelConductor(fraction1,conductor1){
		add_conductor(fraction2, conductor2);
	}

	// constructor
	ParallelConductor::ParallelConductor(
		const FracShConPrList &conductor_list) : ParallelConductor(){
		for(auto it=conductor_list.begin();it!=conductor_list.end();it++)
			add_conductor((*it).first,(*it).second);
	}


	// factory
	ShParallelConductorPr ParallelConductor::create(){
		return std::make_shared<ParallelConductor>();
	}

	// factory
	ShParallelConductorPr ParallelConductor::create(
		const fltp fraction, const ShConductorPr& conductor){
		return std::make_shared<ParallelConductor>(fraction, conductor);
	}

	// factory
	ShParallelConductorPr ParallelConductor::create(
		const fltp fraction1, const ShConductorPr& conductor1,
		const fltp fraction2, const ShConductorPr& conductor2){
		return std::make_shared<ParallelConductor>(fraction1, conductor1, fraction2, conductor2);
	}

	ShParallelConductorPr ParallelConductor::create(
		const FracShConPrList &conductor_list){
		return std::make_shared<ParallelConductor>(conductor_list);
	}

	// accumulate individual conductors in a list
	// if a series conductor is encountered it is added as whole
	void ParallelConductor::accu_parallel_conductors(
		FracConPrList &parallel_conductors, 
		const Direction dir) const{

		// check if any conductors set
		// if(conductor_list_.empty())rat_throw_line("no conductors present");

		// this conductor is parallel so we take our children
		for(auto it = conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// allocate a new list
			FracConPrList parallel_conductors_child;

			// add conductors to the list
			conductor->accu_parallel_conductors(parallel_conductors_child, dir);

			// multiply factions and add conductor to the general list
			for(auto it2 = parallel_conductors_child.begin();
				it2!=parallel_conductors_child.end();it2++)(*it2).first *= fraction;

			// combine lists
			parallel_conductors.insert(
				parallel_conductors.end(), 
				parallel_conductors_child.begin(), 
				parallel_conductors_child.end());
		}
	}

	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	fltp ParallelConductor::calc_specific_heat(
		const fltp temperature) const{

		// calculate density for each conductor
		arma::Col<fltp> density(conductor_list_.size()); 
		arma::Col<fltp> volume_fraction(conductor_list_.size());
		arma::Col<fltp> specific_heat(conductor_list_.size());
		arma::uword cnt=0llu; 
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,cnt++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			density(cnt) = conductor->calc_density(temperature); // kg/m^3
			volume_fraction(cnt) = fcp.first; 
			specific_heat(cnt) = conductor->calc_specific_heat(temperature); // [J kg^-1 K^-1]
		}

		// convert volume fractions to mass fractions
		const arma::Col<fltp> mass_fraction = volume_fraction%density/arma::accu(volume_fraction%density); // unitless

		// add specific heats by mass fraction
		const fltp combined_specific_heat = arma::accu(mass_fraction%specific_heat); // [J kg^-1 K^-1]

		// return values
		return combined_specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	fltp ParallelConductor::calc_thermal_conductivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		fltp thermal_conductivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional thermal conductivities
			thermal_conductivity += fraction*conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude, dir);
		}

		// return values
		return thermal_conductivity;
	}

	// electrical resistivity [Ohm m^2]
	fltp ParallelConductor::calc_electrical_resistivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		fltp electrical_conductivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional electrical conductivities
			electrical_conductivity += fraction/conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude, dir);
		}

		// take inverse to get resistivity and return
		return RAT_CONST(1.0)/electrical_conductivity;
	}

	// is insulating
	bool ParallelConductor::is_insulator(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(!conductor->is_insulator(dir)){
				return false;
			}
		}
		return true;
	}

	// is thermally insulating
	bool ParallelConductor::is_thermal_insulator(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(!conductor->is_thermal_insulator(dir)){
				return false;
			}
		}
		return true;
	}

	// is insulating
	bool ParallelConductor::is_superconductor(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(conductor->is_superconductor(dir)){
				return true;
			}
		}
		return false;
	}

	// is anisotropic
	bool ParallelConductor::is_anisotropic()const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(conductor->is_anisotropic()){
				return true;
			}
		}
		return false;
	}


	// material density [kg m^-3]
	fltp ParallelConductor::calc_density(
		const fltp temperature) const{

		// allocate
		fltp density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	fltp ParallelConductor::calc_critical_current_density(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		
		// allocate
		fltp critical_current_density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional critical current densities
			critical_current_density += fraction*conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
		}

		// return values
		return critical_current_density;
	}

	// calculate power law nvalue
	fltp ParallelConductor::calc_nvalue(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle, 
		const Direction dir) const{
		
		// allocate
		fltp nvalue = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			// const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// take highest n-value (no fraction)
			nvalue = std::max(nvalue, conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle, dir));
		}

		// return values
		return nvalue;
	}
	

	// electric field [V/m]
	fltp ParallelConductor::calc_electric_field(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor, 
		const Direction dir) const{

		// if it is not a superconductor it is linear
		if(!is_superconductor(dir)){
			return current_density*calc_electrical_resistivity(
				temperature,magnetic_field_magnitude,dir);
		}

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors, dir);
		if(parallel_conductors.empty())rat_throw_line("there is no conductors");

		// estimate lower bound
		fltp lower_electric_field = RAT_CONST(0.0); 
		fltp higher_electric_field = RAT_CONST(1e30);

		// walk over conductors
		// this is the field you get by running
		// all current through the least resistive layer
		arma::uword idx = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,idx++){
			// get conductor
			const FracConPr& fcp = (*it);
			const fltp fraction = fcp.first;
			const Conductor* conductor = fcp.second;

			// we assume all current runs through this material
			const fltp con_current_density = std::abs(current_density)/fraction;

			// calculate electric field
			const fltp con_electric_field = conductor->calc_electric_field(
				con_current_density, temperature, magnetic_field_magnitude,
				magnetic_field_angle, scaling_factor, dir);

			// find lowest electric field value
			higher_electric_field = std::min(higher_electric_field, con_electric_field);
		}

		// check if already converged
		if(higher_electric_field - lower_electric_field<abstol_bisection_)
			return rat::cmn::Extra::sign(current_density)*higher_electric_field;

		// allocate midpoint output electric field
		fltp mid_electric_field;

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(arma::uword i=0;;i++){
			// update mid point
			mid_electric_field = (lower_electric_field + higher_electric_field)/2;

			// calculate intermeditae value
			const fltp vmid = calc_current_density(
				parallel_conductors,
				mid_electric_field, temperature, magnetic_field_magnitude,
				magnetic_field_angle, scaling_factor, dir) - 
				std::abs(current_density);

			// update mid-point
			if(vmid<0)lower_electric_field = mid_electric_field;
			else higher_electric_field = mid_electric_field;
			
			// check convergence
			if((higher_electric_field-lower_electric_field)<abstol_bisection_)break;
			if((higher_electric_field-lower_electric_field)/higher_electric_field<reltol_bisection_)break;
			
			// limit
			if(i>bisection_max_iter_)rat_throw_line("bisection method does not converge");
		}

		// return signed electric field
		return rat::cmn::Extra::sign(current_density)*mid_electric_field;
	}





	// // electric field [V/m]
	// fltp ParallelConductor::calc_electric_field(
	// 	const fltp current_density,
	// 	const fltp temperature,
	// 	const fltp magnetic_field_magnitude,
	// 	const fltp magnetic_field_angle,
	// 	const fltp scaling_factor, 
	// 	const Direction dir) const{
		
	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = parallel_conductors.size();

	// 	// allocate
	// 	arma::Row<fltp> rho_vec(num_conductors);
	// 	arma::Row<fltp> jc_vec(num_conductors);
	// 	arma::Row<fltp> nvalue_vec(num_conductors);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_vec(i)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_vec(i) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_vec(i) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_electric_field_fast(current_density, rho_vec, jc_vec, nvalue_vec, dir);
	// }


	// // electric field using pre-calculated electrical 
	// // resistivity en critical current density [V/m]
	// // this uses bisection method
	// fltp ParallelConductor::calc_electric_field_fast(
	// 	const fltp current_density,
	// 	const arma::Row<fltp> &electrical_resistivity,
	// 	const arma::Row<fltp> &critical_current_density,
	// 	const arma::Row<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// estimate lower bound
	// 	fltp lower_electric_field = 0; 
	// 	fltp higher_electric_field = RAT_CONST(1e30);

	// 	// walk over conductors
	// 	// this is the field you get by running
	// 	// all current through the least resistive layer
	// 	arma::uword idx = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,idx++){
	// 		// get conductor
	// 		const fltp fraction = (*it).first;
	// 		const Conductor* conductor = (*it).second;

	// 		// we assume all current runs through this material
	// 		const fltp con_current_density = std::abs(current_density)/fraction;

	// 		// calculate electric field
	// 		const fltp con_electric_field = conductor->calc_electric_field_fast(
	// 			con_current_density, electrical_resistivity.col(idx), 
	// 			critical_current_density.col(idx), nvalue.col(idx), dir);

	// 		// find lowest electric field value
	// 		higher_electric_field = std::min(higher_electric_field, con_electric_field);
	// 	}

	// 	// check if already converged
	// 	if(higher_electric_field - lower_electric_field<tol_bisection_)
	// 		return rat::cmn::Extra::sign(current_density)*higher_electric_field;

	// 	// allocate midpoint output electric field
	// 	fltp mid_electric_field;
		
	// 	// start bisection algorithm
	// 	// find an electric field where all the 
	// 	// supplied current density is soaked by the materials
	// 	for(arma::uword i=0;;i++){
	// 		// update mid point
	// 		mid_electric_field = (lower_electric_field + higher_electric_field)/2;

	// 		// calculate intermeditae value
	// 		const fltp vmid = calc_current_density_fast(
	// 			mid_electric_field, electrical_resistivity,
	// 			critical_current_density, nvalue, dir) - 
	// 			std::abs(current_density);

	// 		// update mid-point
	// 		if(vmid<0)lower_electric_field = mid_electric_field;
	// 		else higher_electric_field = mid_electric_field;
			
	// 		// check convergence
	// 		if((higher_electric_field-lower_electric_field)<tol_bisection_)break;
			
	// 		// limit
	// 		if(i>bisection_max_iter_)rat_throw_line("bisection method does not converge");
	// 	}
		
	// 	// return signed electric field
	// 	return rat::cmn::Extra::sign(current_density)*mid_electric_field;
	// }

	// calculate current density [A/m^2]
	fltp ParallelConductor::calc_current_density(
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		
		// check insulation
		if(is_insulator())return 0;

		// no electric field
		if(electric_field==0)return 0;

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors, dir);
		if(parallel_conductors.empty())rat_throw_line("there is no conductors");

		return calc_current_density(
			parallel_conductors,electric_field,temperature,
			magnetic_field_magnitude,magnetic_field_angle,
			scaling_factor,dir);
	}

	// calculate current density [A/m^2] 
	// with parallel conductors
	fltp ParallelConductor::calc_current_density(
		const FracConPrList& parallel_conductors,
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// allocate
		fltp current_density = RAT_CONST(0.0);

		// walk over conductors
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++){
			// get conductor
			const FracConPr& fcp = (*it);
			const fltp fraction = fcp.first;
			const Conductor* conductor = fcp.second;

			// accumulate current density
			current_density += fraction*conductor->calc_current_density(
				electric_field, temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor, dir);
		}

		// return values
		return current_density;
	}


	// // electric field [V/m]
	// fltp ParallelConductor::calc_current_density(
	// 	const fltp electric_field,
	// 	const fltp temperature,
	// 	const fltp magnetic_field_magnitude,
	// 	const fltp magnetic_field_angle,
	// 	const fltp scaling_factor,
	// 	const Direction dir) const{
		
	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = parallel_conductors.size();

	// 	// allocate
	// 	arma::Row<fltp> rho_vec(num_conductors);
	// 	arma::Row<fltp> jc_vec(num_conductors);
	// 	arma::Row<fltp> nvalue_vec(num_conductors);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_vec(i)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_vec(i) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_vec(i) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_current_density_fast(electric_field, rho_vec, jc_vec, nvalue_vec, dir);
	// }

	// // calculate current density [A m^-2]
	// fltp ParallelConductor::calc_current_density_fast(
	// 	const fltp electric_field,
	// 	const arma::Row<fltp> &electrical_resistivity,
	// 	const arma::Row<fltp> &critical_current_density,
	// 	const arma::Row<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// allocate
	// 	fltp current_density = 0;

	// 	// walk over conductors
	// 	arma::uword i=0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const fltp fraction = (*it).first;
	// 		const Conductor* conductor = (*it).second;

	// 		// accumulate current density
	// 		current_density += fraction*conductor->calc_current_density_fast(
	// 			electric_field, electrical_resistivity.col(i), 
	// 			critical_current_density.col(i), nvalue.col(i), dir);
	// 	}

	// 	// return values
	// 	return current_density;
	// }


	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<fltp> ParallelConductor::calc_specific_heat(
		const arma::Col<fltp> &temperature) const{

		// calculate density for each conductor
		arma::Mat<fltp> density(temperature.n_elem,conductor_list_.size()); 
		arma::Row<fltp> volume_fraction(conductor_list_.size());
		arma::Mat<fltp> specific_heat(temperature.n_elem, conductor_list_.size());
		arma::uword cnt=0llu; 
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,cnt++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			density.col(cnt) = conductor->calc_density(temperature); // kg/m^3
			volume_fraction(cnt) = fcp.first; 
			specific_heat.col(cnt) = conductor->calc_specific_heat(temperature); // [J kg^-1 K^-1]
		}

		// convert volume fractions to mass fractions
		const arma::Mat<fltp> mass_fraction = (volume_fraction%density.each_row()).each_col()/arma::sum(volume_fraction%density.each_row(),1); // unitless

		// add specific heats by mass fraction
		const arma::Col<fltp> combined_specific_heat = arma::sum(specific_heat%mass_fraction,1); // [J kg^-1 K^-1]

		// return values
		return combined_specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<fltp> ParallelConductor::calc_thermal_conductivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> thermal_conductivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional thermal conductivities
			thermal_conductivity += fraction*conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude, dir);
		}

		// return values
		return thermal_conductivity;
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<fltp> ParallelConductor::calc_electrical_resistivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> electrical_conductivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional electrical conductivities
			electrical_conductivity += fraction/conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude, dir);
		}

		// take inverse to get resistivity and return
		return RAT_CONST(1.0)/electrical_conductivity;
	}

	// material density [kg m^-3]
	arma::Col<fltp> ParallelConductor::calc_density(
		const arma::Col<fltp> &temperature) const{
		
		// allocate
		arma::Col<fltp> density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	arma::Col<fltp> ParallelConductor::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> critical_current_density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional critical current densities
			critical_current_density += fraction*conductor->calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
		}

		// return values
		return critical_current_density;
	}

	// critical current density [J m^-2]
	arma::Col<fltp> ParallelConductor::calc_nvalue(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> nvalue(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			// const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// take highest n-value (no fraction)
			nvalue = arma::max(nvalue, conductor->calc_nvalue(
				temperature, magnetic_field_magnitude, magnetic_field_angle, dir));
		}

		// return values
		return nvalue;
	}


	// electric field calculation [V/m]
	arma::Col<fltp> ParallelConductor::calc_electric_field(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// if it is not a superconductor it is linear
		if(!is_superconductor(dir))return current_density%calc_electrical_resistivity(
			temperature,magnetic_field_magnitude,dir);

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors, dir);
		if(parallel_conductors.empty())rat_throw_line("there is no conductors");

		// estimate lower bound
		arma::Col<fltp> lower_electric_field(current_density.n_elem,arma::fill::zeros);
		arma::Col<fltp> higher_electric_field(current_density.n_elem,arma::fill::value(arma::Datum<fltp>::inf)); 
		// higher_electric_field.fill(arma::Datum<fltp>::inf);

		// walk over conductors
		arma::uword i = 0;
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
			// get conductor
			const FracConPr& fcp = (*it);
			const fltp fraction = fcp.first;
			const Conductor* conductor = fcp.second;

			// we assume all current runs through this material
			const arma::Col<fltp> con_current_density = arma::abs(current_density)/fraction;
			
			// calculate electric field
			const arma::Col<fltp> con_electric_field = conductor->calc_electric_field(
				con_current_density, temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir);
			
			// find lowest electric field value
			higher_electric_field = arma::min(higher_electric_field,con_electric_field);
		}

		// calculate current density at these bounds
		// arma::Row<fltp> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma,dir) - J;
		// arma::Row<fltp> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma,dir) - J;

		// allocate midpoint output electric field
		arma::Col<fltp> mid_electric_field(current_density.n_elem);

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(arma::uword j=0;;j++){
			// update mid point
			mid_electric_field = (lower_electric_field + higher_electric_field)/2;

			// calculate intermeditae value
			const arma::Col<fltp> vmid = calc_current_density(
				parallel_conductors, mid_electric_field,
				temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir) - 
				arma::abs(current_density);

			// update lower and upper bounds
			const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
			const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

			// update mid-point
			if(!idx1.is_empty())lower_electric_field(idx1) = mid_electric_field(idx1); 
			if(!idx2.is_empty())higher_electric_field(idx2) = mid_electric_field(idx2);

			assert(arma::all(higher_electric_field>=lower_electric_field));

			// std::cout<<higher_electric_field<<std::endl;

			// check convergence
			if(arma::max(higher_electric_field-lower_electric_field)<abstol_bisection_)break;
			if(arma::max((higher_electric_field-lower_electric_field)/higher_electric_field)<reltol_bisection_)break;

			// throw error if too many iter
			if(j>bisection_max_iter_)rat_throw_line("bisection method does not converge");
		}

		// return signed electric field
		return arma::sign(current_density)%mid_electric_field;
	}


	// // electric field calculation [V/m]
	// arma::Col<fltp> ParallelConductor::calc_electric_field(
	// 	const arma::Col<fltp> &current_density,
	// 	const arma::Col<fltp> &temperature,
	// 	const arma::Col<fltp> &magnetic_field_magnitude,
	// 	const arma::Col<fltp> &magnetic_field_angle,
	// 	const arma::Col<fltp> &scaling_factor,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = parallel_conductors.size();

	// 	// allocate
	// 	arma::Mat<fltp> rho_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> jc_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> nvalue_mat(temperature.n_elem,num_conductors);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_mat.col(i)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_mat.col(i) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_mat.col(i) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_electric_field_fast(current_density, rho_mat, jc_mat, nvalue_mat, dir);
	// }


	// std::mutex mylock;

	// // electric field using pre-calculated electrical 
	// // resistivity en critical current density [V/m]
	// arma::Col<fltp> ParallelConductor::calc_electric_field_fast(
	// 	const arma::Col<fltp> &current_density,
	// 	const arma::Mat<fltp> &electrical_resistivity,
	// 	const arma::Mat<fltp> &critical_current_density, 
	// 	const arma::Mat<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// check sizes
	// 	assert(electrical_resistivity.n_cols==parallel_conductors.size());
	// 	assert(critical_current_density.n_cols==parallel_conductors.size());
	// 	assert(nvalue.n_cols==parallel_conductors.size());
	// 	assert(current_density.n_rows==electrical_resistivity.n_rows);
	// 	assert(current_density.n_rows==critical_current_density.n_rows);
	// 	assert(current_density.n_rows==nvalue.n_rows);

	// 	// estimate lower bound
	// 	arma::Col<fltp> lower_electric_field(current_density.n_elem,arma::fill::zeros);
	// 	arma::Col<fltp> higher_electric_field(current_density.n_elem,arma::fill::value(arma::Datum<fltp>::inf)); 
	// 	// higher_electric_field.fill(arma::Datum<fltp>::inf);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const fltp fraction  = (*it).first; assert(fraction>0);
	// 		const Conductor* conductor = (*it).second;

	// 		// we assume all current runs through this material
	// 		const arma::Col<fltp> con_current_density = arma::abs(current_density)/fraction;
			
	// 		// calculate electric field
	// 		const arma::Col<fltp> con_electric_field = conductor->calc_electric_field_fast(
	// 			con_current_density, electrical_resistivity.col(i), 
	// 			critical_current_density.col(i), nvalue.col(i), dir);
			
	// 		// find lowest electric field value
	// 		higher_electric_field = arma::min(higher_electric_field,con_electric_field);
	// 	}

	// 	// calculate current density at these bounds
	// 	// arma::Row<fltp> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma,dir) - J;
	// 	// arma::Row<fltp> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma,dir) - J;

	// 	// allocate midpoint output electric field
	// 	arma::Col<fltp> mid_electric_field(current_density.n_elem);

	// 	// start bisection algorithm
	// 	// find an electric field where all the 
	// 	// supplied current density is soaked by the materials
	// 	for(arma::uword j=0;;j++){
	// 		// update mid point
	// 		mid_electric_field = (lower_electric_field + higher_electric_field)/2;

	// 		// calculate intermeditae value
	// 		const arma::Col<fltp> vmid = calc_current_density_fast(
	// 			mid_electric_field,electrical_resistivity,
	// 			critical_current_density,nvalue,dir) - 
	// 			arma::abs(current_density);

	// 		// update lower and upper bounds
	// 		const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
	// 		const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

	// 		// update mid-point
	// 		if(!idx1.is_empty())lower_electric_field(idx1) = mid_electric_field(idx1); 
	// 		if(!idx2.is_empty())higher_electric_field(idx2) = mid_electric_field(idx2);

	// 		assert(arma::all(higher_electric_field>=lower_electric_field));

	// 		// std::cout<<higher_electric_field<<std::endl;

	// 		// check convergence
	// 		if(arma::max(higher_electric_field-lower_electric_field)<tol_bisection_)break;

	// 		// throw error if too many iter
	// 		if(j>bisection_max_iter_)rat_throw_line("bisection method does not converge");
	// 	}

	// 	// return signed electric field
	// 	return arma::sign(current_density)%mid_electric_field;
	// }

	// calculate current density [A m^-2]
	arma::Col<fltp> ParallelConductor::calc_current_density(
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// check insulation
		if(is_insulator())return 0;

		// create a list of parallel conductors
		FracConPrList parallel_conductors;

		// add conductors to the list
		accu_parallel_conductors(parallel_conductors, dir);
		if(parallel_conductors.empty())rat_throw_line("there is no conductors");

		// return values
		return calc_current_density(
			parallel_conductors,electric_field,
			temperature,magnetic_field_magnitude,
			magnetic_field_angle,scaling_factor,dir);
	}

	// calculate current density [A m^-2]
	arma::Col<fltp> ParallelConductor::calc_current_density(
		const FracConPrList& parallel_conductors,
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// allocate
		arma::Col<fltp> current_density(electric_field.n_elem,arma::fill::zeros);

		// walk over conductors
		for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++){
			// get conductor
			const FracConPr& fcp = (*it);
			const fltp fraction = fcp.first;
			const Conductor* conductor = fcp.second;

			// accumulate current density
			current_density += fraction*conductor->calc_current_density(
				electric_field, 
				temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor,dir);
		}

		// return current density
		return current_density;
	}


	// // calculate current density [A m^-2]
	// arma::Col<fltp> ParallelConductor::calc_current_density(
	// 	const arma::Col<fltp> &electric_field,
	// 	const arma::Col<fltp> &temperature,
	// 	const arma::Col<fltp> &magnetic_field_magnitude,
	// 	const arma::Col<fltp> &magnetic_field_angle,
	// 	const arma::Col<fltp> &scaling_factor,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = parallel_conductors.size();

	// 	// allocate
	// 	arma::Mat<fltp> rho_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> jc_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> nvalue_mat(temperature.n_elem,num_conductors);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_mat.col(i)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_mat.col(i) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_mat.col(i) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_current_density_fast(electric_field, rho_mat, jc_mat, nvalue_mat, dir);
	// }

	// // current density from electric field 
	// // output in [A m^-2]
	// arma::Col<fltp> ParallelConductor::calc_current_density_fast(
	// 	const arma::Col<fltp> &electric_field,
	// 	const arma::Mat<fltp> &electrical_resistivity,
	// 	const arma::Mat<fltp> &critical_current_density, 
	// 	const arma::Mat<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList parallel_conductors;

	// 	// add conductors to the list
	// 	accu_parallel_conductors(parallel_conductors, dir);
	// 	if(parallel_conductors.empty())rat_throw_line("there is no conductors");

	// 	// check matrix sizes
	// 	assert(electrical_resistivity.n_cols == parallel_conductors.size());
	// 	assert(critical_current_density.n_cols == parallel_conductors.size());
	// 	assert(nvalue.n_cols == parallel_conductors.size());
	// 	assert(electric_field.n_rows == electrical_resistivity.n_rows);
	// 	assert(electric_field.n_rows == critical_current_density.n_rows);
	// 	assert(electric_field.n_rows == nvalue.n_rows);

	// 	// allocate
	// 	arma::Col<fltp> current_density(electric_field.n_elem,arma::fill::zeros);

	// 	// walk over conductors
	// 	arma::uword i=0;
	// 	for(auto it=parallel_conductors.begin();it!=parallel_conductors.end();it++,i++){
	// 		// get conductor
	// 		const fltp fraction = (*it).first; assert(fraction>0);
	// 		const Conductor* conductor = (*it).second;

	// 		// accumulate current density
	// 		current_density += fraction*conductor->calc_current_density_fast(
	// 			electric_field, 
	// 			electrical_resistivity.col(i), 
	// 			critical_current_density.col(i), 
	// 			nvalue.col(i),dir);
	// 	}

	// 	// return values
	// 	return current_density;
	// }


	// serialization type
	std::string ParallelConductor::get_type(){
		return "rat::mat::parallelconductor";
	}

	// serialization
	void ParallelConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Conductor::serialize(js,list);
		ConductorList::serialize(js,list);

		// type
		js["type"] = get_type();
	}

	// deserialization
	void ParallelConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		Conductor::deserialize(js,list,factory_list,pth);
		ConductorList::deserialize(js,list,factory_list,pth);
	}

}}