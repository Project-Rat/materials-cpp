// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "nist8sumfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	Nist8SumFit::Nist8SumFit(){
		set_name("nist8sum");
	}

	// default constructor
	Nist8SumFit::Nist8SumFit(const fltp t1, const fltp t2, const arma::Col<fltp>::fixed<9> &fit_parameters) : Nist8SumFit(){
		set_fit_parameters(fit_parameters); set_temperature_range(t1,t2);
	}

	// factory
	ShNist8SumFitPr Nist8SumFit::create(){
		return std::make_shared<Nist8SumFit>();
	}

	// factory
	ShNist8SumFitPr Nist8SumFit::create(const fltp t1, const fltp t2, const arma::Col<fltp>::fixed<9> &fit_parameters){
		return std::make_shared<Nist8SumFit>(t1,t2,fit_parameters);
	}


	// set fit parameters
	void Nist8SumFit::set_fit_parameters(const arma::Col<fltp>::fixed<9> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}


	void Nist8SumFit::set_fit_parameters(const arma::uword index, const fltp value){
		fit_parameters_(index) = value;
	}

	
	// getters
	fltp Nist8SumFit::get_fit_parameters(const arma::uword index)const{
		return fit_parameters_(index);
	}


	// specific heat output in [J m^-3 K^-1]
	arma::Col<fltp> Nist8SumFit::calc_property(const arma::Col<fltp> &temperature)const{

		// check input
		assert(temperature.is_finite());

		// limit lower temperature
		const arma::Col<fltp> T = arma::clamp(temperature,t1_,arma::Datum<fltp>::inf);

		// check if fit parameters set
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// calculate nist fit using for loop to avoid power
		arma::Col<fltp> A(T.n_elem,arma::fill::zeros);
		arma::Col<fltp> log10T = arma::log10(T);
		arma::Col<fltp> plt(T); plt.fill(RAT_CONST(1.0));
		for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
			A += fit_parameters_(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Col<fltp> values = arma::exp10(A);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(T>t2_);
		if(!extrap.is_empty()){
			const fltp dt = (t2_-t1_)/1000;
			const arma::Col<fltp>::fixed<2> Te = {t2_-dt, t2_-1e-10f};
			const arma::Col<fltp>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (T(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// check output
		assert(values.n_elem==T.n_elem);
		assert(values.is_finite());
		
		// check with scalar version
		#ifndef NDEBUG
		for(arma::uword i=0;i<T.n_elem;i++)
			assert(std::abs(values(i) - calc_property(T(i)))/values(i)<RAT_CONST(1e-6));
		#endif

		// return answer
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	fltp Nist8SumFit::calc_property(const fltp temperature)const{
		// check input
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// allocate
		fltp value = 0;

		// extrapolation beyond end
		if(temperature>t2_){
			const fltp dt = (t2_-t1_)/1000;
			const fltp t1 = t2_-dt;
			const fltp t2 = t2_;
			const fltp s1 = calc_property(t1);
			const fltp s2 = calc_property(t2);
			value = s2 + (temperature - t2_)*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<t1_){
			value = calc_property(t1_);
		}

		// calculate normally
		else{
			fltp A = 0, plt = 1.0;
			fltp log10T = std::log10(temperature);
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				A += fit_parameters_(i)*plt; plt*=log10T;
			}

			// 10^A and multiply with density
			value = std::pow(RAT_CONST(10.0),A);
		}

		// return answer
		return value;
	}


	// get type
	std::string Nist8SumFit::get_type(){
		return "rat::mat::nist8sumfit";
	}

	// method for serialization into json
	void Nist8SumFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);
	}

	// method for deserialisation from json
	void Nist8SumFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		fit_parameters_(0) = js["a"].ASFLTP();
		fit_parameters_(1) = js["b"].ASFLTP();
		fit_parameters_(2) = js["c"].ASFLTP();
		fit_parameters_(3) = js["d"].ASFLTP();
		fit_parameters_(4) = js["e"].ASFLTP();
		fit_parameters_(5) = js["f"].ASFLTP();
		fit_parameters_(6) = js["g"].ASFLTP();
		fit_parameters_(7) = js["h"].ASFLTP();
		fit_parameters_(8) = js["i"].ASFLTP();
	}

}}