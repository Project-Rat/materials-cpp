// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "htscore.hh"

#include "parallelconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	HTSCore::HTSCore(){
		set_name("htscore");
	}

	// constructor
	HTSCore::HTSCore(
		const fltp width, 
		const fltp thickness, 
		const fltp tsc, 
		const ShSuperConductorPr &sc, 
		const fltp tsubstr, 
		const ShBaseConductorPr &substr) : HTSCore(){
		width_ = width; thickness_ = thickness; 
		tsc_ = tsc; tsubstr_ = tsubstr;
		sc_ = sc; substr_ = substr; 
		setup();
	}

	// factory
	ShHTSCorePr HTSCore::create(){
		return std::make_shared<HTSCore>();
	}

	// factory with input
	ShHTSCorePr HTSCore::create(
		const fltp width,
		const fltp thickness, 
		const fltp tsc, 
		const ShSuperConductorPr &sc, 
		const fltp tsubstr, 
		const ShBaseConductorPr &substr){
		return std::make_shared<HTSCore>(
			width, thickness, tsc, sc, tsubstr, substr);
	}


	// setters
	void HTSCore::set_width(const fltp width){
		width_ = width; setup();
	}

	void HTSCore::set_thickness(const fltp thickness){
		thickness_ = thickness; setup();
	}

	void HTSCore::set_tsubstr(const fltp tsubstr){
		tsubstr_ = tsubstr; setup();
	}

	void HTSCore::set_tsc(const fltp tsc){
		tsc_ = tsc; setup();
	}

	void HTSCore::set_sc(const ShSuperConductorPr &sc){
		sc_ = sc; setup();
	}

	void HTSCore::set_substr(const ShBaseConductorPr &substr){
		substr_ = substr; setup();
	}


	// getters
	fltp HTSCore::get_width() const{
		return width_;
	}

	fltp HTSCore::get_thickness() const{
		return thickness_;
	}

	fltp HTSCore::get_area() const{
		return width_*thickness_;
	}

	fltp HTSCore::get_tsc() const{
		return tsc_;
	}

	fltp HTSCore::get_tsubstr() const{
		return tsubstr_;
	}

	const ShSuperConductorPr& HTSCore::get_sc() const{
		return sc_;
	}

	const ShBaseConductorPr& HTSCore::get_substr() const{
		return substr_;
	}


	// critical current [A/m]
	fltp HTSCore::calc_critical_current_per_width(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor) const{
		return thickness_*calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor);
	}

	// setup function
	void HTSCore::setup(){
		// do not setup as long as invalid
		if(!is_valid(false))return;

		// add conductor by fraction
		const ShConductorPr sccon = ParallelConductor::create(
			tsc_/thickness_, sc_, 
			tsubstr_/thickness_, substr_);
		set_longitudinal(sccon);
		set_normal(substr_);
		set_transverse(sccon);
	}
	
	// is superconductor
	bool HTSCore::is_superconductor(const Direction dir)const{
		if(dir==Direction::LONGITUDINAL)return true; else return false;
	}

	// check validity
	bool HTSCore::is_valid(const bool enable_throws) const{
		// check mesh
		if(sc_==NULL){if(enable_throws){rat_throw_line("superconductor points to NULL");} return false;}
		if(substr_==NULL){if(enable_throws){rat_throw_line("substate points to NULL");} return false;}
		if(tsc_<=0){if(enable_throws){rat_throw_line("superconductor thickness must be larger than zero");} return false;}
		if(tsubstr_<=0){if(enable_throws){rat_throw_line("substrate thickness must be larger than zero");} return false;}
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;}
		if(width_<=0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;}
		if(thickness_ - tsubstr_ - tsc_<=0){if(enable_throws){rat_throw_line("combined superonductor and substrate thickness can not exceed overall thickness");} return false;}
		return true;
	}

	// serialization type
	std::string HTSCore::get_type(){
		return "rat::mat::htscore";
	}

	// serialization
	void HTSCore::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// skipping anisotropic
		Conductor::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// dimensions
		js["width"] = width_;
		js["thickness"] = thickness_;

		// layer thicknesses
		js["tsc"] = tsc_;
		js["tsubstr"] = tsubstr_;

		// layers
		js["sc"] = cmn::Node::serialize_node(sc_, list);
		js["substr"] = cmn::Node::serialize_node(substr_, list);
	}

	// deserialization
	void HTSCore::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// skipping anisotropic
		Conductor::deserialize(js,list,factory_list,pth);

		// backwards compatibility with v2.011.0
		// remove later when no longer needed
		if(js.isMember("in_plane_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["in_plane_conductor"], list, factory_list, pth);
		if(js.isMember("transverse_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["transverse_conductor"], list, factory_list, pth);
		
		// dimensions
		width_ = js["width"].ASFLTP();
		thickness_ = js["thickness"].ASFLTP();

		// layer thicknesses
		tsc_ = js["tsc"].ASFLTP();
		tsubstr_ = js["tsubstr"].ASFLTP();

		// layers
		sc_ = cmn::Node::deserialize_node<SuperConductor>(js["sc"], list, factory_list, pth);
		substr_ = cmn::Node::deserialize_node<BaseConductor>(js["substr"], list, factory_list, pth);

		
		// setup as parallel conductor
		setup();
	}

}}