// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "rutherfordcable.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	RutherfordCable::RutherfordCable(){
		set_name("rutherford");
	}

	// factory
	ShRutherfordCablePr RutherfordCable::create(){
		return std::make_shared<RutherfordCable>();
	}


	// setters
	void RutherfordCable::set_num_strands(const arma::uword num_strands){
		num_strands_ = num_strands; setup();
	}

	void RutherfordCable::set_width(const fltp width){
		width_ = width; setup();
	}
	
	void RutherfordCable::set_thickness(const fltp thickness){
		thickness_ = thickness; setup();
	}

	void RutherfordCable::set_keystone(const fltp keystone){
		keystone_ = keystone; setup();
	}

	void RutherfordCable::set_pitch(const fltp pitch){
		pitch_ = pitch; setup();
	}

	void RutherfordCable::set_dinsu(const fltp dinsu){
		dinsu_ = dinsu; setup();
	}

	void RutherfordCable::set_fcabling(const fltp fcabling){
		fcabling_ = fcabling; setup();
	}

	// set superconductor
	void RutherfordCable::set_strand(const ShLTSWirePr &strand){
		strand_ = strand; setup();
	}

	// set insulation material
	void RutherfordCable::set_insu(const ShConductorPr &insu){
		insu_ = insu; setup();
	}

	// set void material
	void RutherfordCable::set_voids(const ShConductorPr &voids){
		voids_ = voids; setup();
	}


	// getters
	arma::uword RutherfordCable::get_num_strands()const{
		return num_strands_;
	}

	fltp RutherfordCable::get_width()const{
		return width_;
	}

	fltp RutherfordCable::get_thickness()const{
		return thickness_;
	}

	fltp RutherfordCable::get_area()const{
		return width_*thickness_;
	}

	fltp RutherfordCable::get_keystone()const{
		return keystone_;
	}

	fltp RutherfordCable::get_pitch()const{
		return pitch_;
	}

	fltp RutherfordCable::get_dinsu()const{
		return dinsu_;
	}

	fltp RutherfordCable::get_fcabling()const{
		return fcabling_;
	}

	ShLTSWirePr RutherfordCable::get_strand() const{
		return strand_;
	}

	


	// setup function
	void RutherfordCable::setup(){
		// check validity
		if(!is_valid(false))return;

		// calculate bare and total area
		const fltp bare_area = thickness_*width_;
		const fltp total_area = (thickness_+2*dinsu_)*(width_+2*dinsu_);
		
		// get strand diameter from strand
		const fltp strand_radius = strand_->get_diameter()/2;

		// calculate filling fraction
		const fltp strand_area = num_strands_*arma::Datum<fltp>::pi*strand_radius*strand_radius;

		// check sizes
		if(strand_area>bare_area)rat_throw_line("strands do not fit in the bare cable");

		// calculate filling of strands in cable
		const fltp strand_fraction = strand_area/total_area;
		const fltp void_fraction = (bare_area - strand_area)/total_area;
		const fltp insu_fraction = (total_area - bare_area)/total_area;

		// add conductors by fraction
		clear_conductors();
		add_conductor(strand_fraction, strand_);
		if(void_fraction>0 && voids_!=NULL)add_conductor(void_fraction, voids_);
		if(insu_fraction>0 && insu_!=NULL)add_conductor(insu_fraction, insu_);
	}


	// critical current density [J m^-2]
	fltp RutherfordCable::calc_critical_current_density(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// calculate scaling
		fltp fpitch = 1.0; 
		if(pitch_!=0){
			const fltp pitch_angle = std::atan(2*(width_ + thickness_)/pitch_);
			fpitch = std::cos(pitch_angle);
		}

		// forward calculation and scale
		return fcabling_*fpitch*ParallelConductor::calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> RutherfordCable::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// calculate scaling
		fltp fpitch = 1.0; 
		if(pitch_!=0){
			const fltp pitch_angle = std::atan(2*(width_ + thickness_)/pitch_);
			fpitch = std::cos(pitch_angle);
		}

		// forward calculation and scale
		return fcabling_*fpitch*ParallelConductor::calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
	}

	// is superconductor
	bool RutherfordCable::is_superconductor(const Direction /*dir*/)const{
		return true;
	}

	// check validity
	bool RutherfordCable::is_valid(const bool enable_throws) const{
		if(strand_==NULL){if(enable_throws){rat_throw_line("strand points to NULL");} return false;}
		if(!strand_->is_valid(enable_throws))return false;
		if(num_strands_<=0){if(enable_throws){rat_throw_line("num strands must be larger than zero");} return false;}
		if(width_<0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;}
		if(thickness_<0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;}
		if(pitch_<0){if(enable_throws){rat_throw_line("pitch must be larger than or equal to zero");} return false;}
		if(dinsu_<0){if(enable_throws){rat_throw_line("insulation thickness must be larger than or equal to zero");} return false;}
		if(fcabling_<0){if(enable_throws){rat_throw_line("cabling fraction must be larger than or equal to zero");} return false;}
		if(fcabling_>RAT_CONST(1.0)){if(enable_throws){rat_throw_line("cabling faction can not be larger than one");} return false;}
		if((num_strands_*arma::Datum<fltp>::pi*(strand_->get_diameter()/2)*(strand_->get_diameter()/2))>(thickness_*width_))
			{if(enable_throws){rat_throw_line("strands do not fit in the bare cable");} return false;}

		// check mesh
		return true;
	}


	// serialization type
	std::string RutherfordCable::get_type(){
		return "rat::mat::rutherfordcable";
	}

	// serialization
	void RutherfordCable::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// name
		rat::cmn::Node::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// dimensions
		js["num_strands"] = static_cast<int>(num_strands_);
		js["width"] = width_;
		js["thickness"] = thickness_;
		js["keystone"] = keystone_;
		js["pitch"] = pitch_;
		js["dinsu"] = dinsu_;
		js["fcabling"] = fcabling_;

		// layers
		js["strand"] = cmn::Node::serialize_node(strand_, list);
		js["insu"] = cmn::Node::serialize_node(insu_, list);
		js["voids"] = cmn::Node::serialize_node(voids_, list);
	}

	// deserialization
	void RutherfordCable::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Node::deserialize(js,list,factory_list, pth);

		// dimensions
		set_width(js["width"].ASFLTP());
		set_thickness(js["thickness"].ASFLTP());
		set_keystone(js["keystone"].ASFLTP());
		set_pitch(js["pitch"].ASFLTP());
		set_dinsu(js["dinsu"].ASFLTP());
		set_num_strands(js["num_strands"].asUInt64());
		set_fcabling(js["fcabling"].ASFLTP());

		// layers
		set_strand(cmn::Node::deserialize_node<LTSWire>(js["strand"], list, factory_list, pth));
		set_insu(cmn::Node::deserialize_node<Conductor>(js["insu"], list, factory_list, pth));
		set_voids(cmn::Node::deserialize_node<Conductor>(js["voids"], list, factory_list, pth));

		// setup as parallel conductor
		setup();
	}

}}