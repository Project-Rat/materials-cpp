// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "fleiterbristowfit.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FleiterBristowFit::FleiterBristowFit(){
		set_name("fleiterfit2"); set_t1(1.0); set_t2(100);
	}

	// factory
	ShFleiterBristowFitPr FleiterBristowFit::create(){
		return std::make_shared<FleiterBristowFit>();
	}

	// calc critical current without field angle nor magnitude
	fltp FleiterBristowFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// calc critical current without field angle
	fltp FleiterBristowFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude) const{
		return calc_property(temperature, magnetic_field_magnitude, RAT_CONST(0.0));
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	fltp FleiterBristowFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle) const{

		// return critical current density
		return arma::as_scalar(calc_property(arma::Col<fltp>{temperature},arma::Col<fltp>{magnetic_field_magnitude},arma::Col<fltp>{magnetic_field_angle}));
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> FleiterBristowFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// calc critical current without field angle
	arma::Col<fltp> FleiterBristowFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude) const{
		return calc_property(temperature, magnetic_field_magnitude, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}



	// function for brining angle in interval 0 to 2*pi
	// arma::Col<fltp> FleiterBristowFit::angle_mod(const arma::Col<fltp> &alpha){
	// 	arma::Col<fltp> alpha_fixed(alpha.n_elem);
	// 	for(arma::uword i=0;i<alpha.n_elem;i++)
	// 		alpha_fixed(i) = std::fmod(alpha(i),arma::Datum<fltp>::pi);
	// 	return alpha_fixed;
	// }

	// irrerversibility field equation output in fraction Birr / Bi0
	// in Fleiter fit for c-plane n1=0.0, n2=1.0 and a=1.0;
	arma::Col<fltp> FleiterBristowFit::calc_irreversibility_field(const arma::Col<fltp> &t, 
		const fltp n, const fltp n1, const fltp n2, const fltp a){
		if(n1==0.0)return a*(RAT_CONST(1.0)-arma::pow(arma::clamp(t,0.0,1.0),n));
		else return arma::pow(RAT_CONST(1.0)-arma::pow(arma::clamp(t,0.0,1.0),n1),n2) + 
			a*(RAT_CONST(1.0)-arma::pow(arma::clamp(t,0.0,1.0),n));
	}

	// calculate temperature dependence
	// in Fleiter fit for c-plane n1=0.0, n2=1.0 and a=1.0;
	arma::Col<fltp> FleiterBristowFit::calc_temperature_dependence(
		const arma::Col<fltp> &t, const arma::Col<fltp>& birr, const fltp gamma){
		return (t<1.0)%arma::pow(birr,gamma);
	}

	// calculate field dependence
	arma::Col<fltp> FleiterBristowFit::calc_field_dependence(const arma::Col<fltp> &b, 
		const fltp p, const fltp q1, const fltp q2){
		return (b<1.0)%(arma::pow(RAT_CONST(1.0)-arma::clamp(b,0.0,1.0),q1))%
			arma::exp(-p*arma::pow(arma::clamp(b,0.0,1.0),q2)); 
	}

	// angular dependence factor (width of the peak?)
	arma::Col<fltp> FleiterBristowFit::calc_angular_dependence_factor(
		const arma::Col<fltp> &B, const arma::Col<fltp> &T, 
		const fltp g0, const fltp g1, const fltp g2, const fltp g3){
		return g0 + g1*arma::exp(-(g2*arma::exp(g3*T))%B);
	}

	// calculate angular dependence for primary peak
	arma::Col<fltp> FleiterBristowFit::calc_angular_dependence_primary(
		const arma::Col<fltp> &alpha, const arma::Col<fltp> &g, const fltp nu, const fltp peak_offset){
		return RAT_CONST(1.0)/(RAT_CONST(1.0)+arma::pow(
			(arma::abs(arma::abs(alpha + peak_offset) - 
			arma::Datum<fltp>::pi/RAT_CONST(2.0)))/g,nu));
	}

	// calculate angular dependence for secondary peak
	arma::Col<fltp> FleiterBristowFit::calc_angular_dependence_secondary(
		const arma::Col<fltp> &alpha, const fltp k, const fltp peak_offset){
		return arma::pow((RAT_CONST(1.0) + arma::cos(2*(alpha + peak_offset)))/2,k);
	}

	// angular dependence
	arma::Col<fltp> FleiterBristowFit::calc_critical_current(
		const arma::Col<fltp> &alpha,
		const arma::Col<fltp> &Jcc, const arma::Col<fltp> &Jcab, 
		const arma::Col<fltp> &Jcr, const arma::Col<fltp> &g, 
		const fltp nu, const fltp k, const fltp peak_offset, 
		const fltp second_peak_offset){

		// calculate required height of peak
		const arma::Col<fltp> Jprimary = arma::clamp(Jcab-Jcc,RAT_CONST(0.0),arma::Datum<fltp>::inf);

		// height of platteau is lowest
		const arma::Col<fltp> Jplatteau = arma::min(Jcc,Jcab);

		// calculate height of secondary peak
		const arma::Col<fltp> Jsecondary = Jcr - Jplatteau; //arma::max(Jcr - Jplatteau, -Jplatteau);

		// angular dependency
		return Jplatteau + 
			calc_angular_dependence_primary(alpha, g, nu, peak_offset)%Jprimary + 
			calc_angular_dependence_secondary(alpha, k, second_peak_offset)%Jsecondary;
	}

	// parallel fit
	arma::Col<fltp> FleiterBristowFit::calc_Jc(
		const arma::Col<fltp> &T, const fltp Tc0, 
		const arma::Col<fltp> &B, const fltp Bi0,
		const fltp n, const fltp n1, const fltp n2, const fltp a, 
		const fltp p, const fltp q1, const fltp q2, const fltp gamma, const fltp alpha){

		// calculate unitless temperature
		const arma::Col<fltp> t = T/Tc0;

		// irreversibility field for ab-plane
		const arma::Col<fltp> bi = calc_irreversibility_field(t,n,n1,n2,a);
		
		// parallel
		arma::Col<fltp> b = B/(bi*Bi0);
		b(arma::find(t>=RAT_CONST(1.0))).fill(RAT_CONST(1.0));

		// parallel 
		const arma::Col<fltp> Jc = alpha*
			calc_field_dependence(b, p, q1, q2)%
			calc_temperature_dependence(t, bi, gamma);

		// return value
		return Jc;
	}

	

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<fltp> FleiterBristowFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle) const{
		
		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.n_elem==magnetic_field_angle.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		assert(magnetic_field_angle.is_finite());

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<fltp>& B = magnetic_field_magnitude; //arma::clamp(magnetic_field_magnitude,RAT_CONST(0.1),arma::Datum<fltp>::inf);
		const arma::Col<fltp> T = arma::clamp(temperature,RAT_CONST(1.0),arma::Datum<fltp>::inf);
		arma::Col<fltp> alpha = cmn::Extra::modulus(magnetic_field_angle,arma::Datum<fltp>::pi);

		// calculate critical currents at fixed angles
		const arma::Col<fltp> Jcab = calc_Jc(T,Tc0_,B,Bi0ab_,n_,n1_,n2_,a_,pab_,qab1_,qab2_, gammaab_, alphaab_);
		const arma::Col<fltp> Jcc = calc_Jc(T,Tc0_,B,Bi0c_,n_,RAT_CONST(0.0),RAT_CONST(1.0),RAT_CONST(1.0),pc_,qc1_,qc2_, gammac_, alphac_);
		const arma::Col<fltp> Jcr = calc_Jc(T,Tc0_,B,Bi0r_,nr_,nr1_,nr2_,ar_,pr_,qr1_,qr2_, gammar_, alphar_);

		// output average critical current
		if(force_perpendicular_ && force_parallel_)return (Jcc + Jcab)/2;

		// output perpendicular critical current
		if(force_perpendicular_)return Jcc;

		// output parallel critical current
		if(force_parallel_)return Jcab;

		// anisotropy factor
		const arma::Col<fltp> g = calc_angular_dependence_factor(B,T,g0_,g1_,g2_,g3_);

		// calculate critical current
		arma::Col<fltp> Jc = calc_critical_current(alpha,Jcc,Jcab,Jcr,g,nu_,k_,!ignore_pkoff_*pkoff_,spkoff_);

		// flipped type-0 pair
		if(type0_flip_)Jc = (Jc + calc_critical_current(-alpha,Jcc,Jcab,Jcr,g,nu_,k_,!ignore_pkoff_*pkoff_,spkoff_))/2;

		// check critical current
		if(!Jc.is_finite())rat_throw_line("Inf or Nan detected");

		// return critical current density
		return Jc;
	}

	// serialization
	// get type
	std::string FleiterBristowFit::get_type(){
		return "rat::mat::fleiterbristowfit";
	}

	// method for serialization into json
	void FleiterBristowFit::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();
		
		// temperature dependence
		js["Tc0"] = Tc0_;
		js["n"] = n_;
		js["n1"] = n1_;
		js["n2"] = n2_;

		// AB-plane
		js["pab"] = pab_;
		js["qab1"] = qab1_;
		js["qab2"] = qab2_;
		js["Bi0ab"] = Bi0ab_;
		js["a"] = a_;
		js["gammaab"] = gammaab_;
		js["alphaab"] = alphaab_;

		// C-axis
		js["pc"] = pc_;
		js["qc1"] = qc1_;
		js["qc2"] = qc2_;
		js["Bi0c"] = Bi0c_;
		js["gammac"] = gammac_;
		js["alphac"] = alphac_;

		// R-axis
		js["pr"] = pr_;
		js["qr1"] = qr1_;
		js["qr2"] = qr2_;
		js["Bi0r"] = Bi0r_;
		js["gammar"] = gammar_;
		js["alphar"] = alphar_;
		js["nr"] = nr_;
		js["nr1"] = nr1_;
		js["nr2"] = nr2_;
		js["ar"] = ar_;

		// anisotropy
		js["k"] = k_;
		js["g0"] = g0_;
		js["g1"] = g1_;
		js["g2"] = g2_;
		js["g3"] = g3_;
		js["nu"] = nu_;
		js["pkoff"] = pkoff_;
		js["spkoff"] = spkoff_;

		// typical layer thicknesses and width
		js["tsc"] = tsc_;

		// type-0 pair flipping
		js["type0_flip"] = type0_flip_;
		js["ignore_pkoff"] = ignore_pkoff_;
		js["force_perpendicular"] = force_perpendicular_;
		js["force_parallel"] = force_parallel_;
	}

	// method for deserialisation from json
	void FleiterBristowFit::deserialize(
		const Json::Value &js, cmn::DSList &list,
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// general
		Tc0_ = js["Tc0"].ASFLTP();
		n_ = js["n"].ASFLTP();
		n1_ = js["n1"].ASFLTP();
		n2_ = js["n2"].ASFLTP();

		// AB-plane
		pab_ = js["pab"].ASFLTP();
		qab1_ = js["qab1"].ASFLTP();
		qab2_ = js["qab2"].ASFLTP();
		Bi0ab_ = js["Bi0ab"].ASFLTP();
		a_ = js["a"].ASFLTP();
		gammaab_ = js["gammaab"].ASFLTP();
		alphaab_ = js["alphaab"].ASFLTP();

		// C-axis
		pc_ = js["pc"].ASFLTP();
		qc1_ = js["qc1"].ASFLTP();
		qc2_ = js["qc2"].ASFLTP();
		Bi0c_ = js["Bi0c"].ASFLTP();
		gammac_ = js["gammac"].ASFLTP();
		alphac_ = js["alphac"].ASFLTP();

		// R-axis
		pr_ = js["pr"].ASFLTP();
		qr1_ = js["qr1"].ASFLTP();
		qr2_ = js["qr2"].ASFLTP();
		Bi0r_ = js["Bi0r"].ASFLTP();
		gammar_ = js["gammar"].ASFLTP();
		alphar_ = js["alphar"].ASFLTP();
		nr_ = js["nr"].ASFLTP();
		nr1_ = js["nr1"].ASFLTP();
		nr2_ = js["nr2"].ASFLTP();
		ar_ = js["ar"].ASFLTP();

		// anisotropy
		k_ = js["k"].ASFLTP();
		g0_ = js["g0"].ASFLTP();
		g1_ = js["g1"].ASFLTP();
		g2_ = js["g2"].ASFLTP();
		g3_ = js["g3"].ASFLTP();
		nu_ = js["nu"].ASFLTP();
		pkoff_ = js["pkoff"].ASFLTP();
		spkoff_ = js["spkoff"].ASFLTP();

		// typical layer thickness
		tsc_ = js["tsc"].ASFLTP();

		// type-0 pair flipping
		type0_flip_ = js["type0_flip"].asBool();
		ignore_pkoff_ = js["ignore_pkoff"].asBool();
		force_perpendicular_ = js["force_perpendicular"].asBool();
		force_parallel_ = js["force_parallel"].asBool();
	}

}}