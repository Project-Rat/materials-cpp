// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "htstape.hh"

// materials headers
#include "rhoedgefit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	HTSTape::HTSTape(){
		set_name("htstape");
	}

	// constructor
	HTSTape::HTSTape(
		const fltp width, 
		const fltp thickness, 
		const ShHTSCorePr& core, 
		const ShBaseConductorPr& stab) : HTSTape(){
		width_ = width; thickness_ = thickness; core_ = core; stab_ = stab; setup();
	}

	// factory
	ShHTSTapePr HTSTape::create(){
		return std::make_shared<HTSTape>();
	}

	// factory with input
	ShHTSTapePr HTSTape::create(
		const fltp width, 
		const fltp thickness, 
		const ShHTSCorePr& core, 
		const ShBaseConductorPr& stab){
		return std::make_shared<HTSTape>(width, thickness, core, stab);
	}


	// setters
	void HTSTape::set_width(const fltp width){
		width_ = width; setup();
	}

	void HTSTape::set_thickness(const fltp thickness){
		thickness_ = thickness; setup();
	}

	void HTSTape::set_is_flipped_pair(const bool is_flipped_pair){
		is_flipped_pair_ = is_flipped_pair; setup();
	}

	void HTSTape::set_core(const ShHTSCorePr &core){
		core_ = core; setup();
	}

	void HTSTape::set_stab(const ShBaseConductorPr &stab){
		stab_ = stab; setup();
	}

	void HTSTape::set_num_cores(const arma::uword num_cores){
		num_cores_ = num_cores; setup();
	}


	// getters
	fltp HTSTape::get_width() const{
		return width_;
	}

	fltp HTSTape::get_thickness() const{
		return thickness_;
	}

	fltp HTSTape::get_area() const{
		return width_*thickness_;
	}

	bool HTSTape::get_is_flipped_pair()const{
		return is_flipped_pair_;
	}

	const ShHTSCorePr& HTSTape::get_core() const{
		return core_;
	}

	const ShBaseConductorPr& HTSTape::get_stab() const{
		return stab_;
	}

	arma::uword HTSTape::get_num_cores() const{
		return num_cores_; 
	}

	// is superconductor
	bool HTSTape::is_superconductor(const Direction dir)const{
		if(dir==Direction::LONGITUDINAL)return true; else return false;
	}

	// setup function
	void HTSTape::setup(){
		// do not setup as long as invalid
		if(!is_valid(false))return;

		// get core dimensions
		const fltp core_width = core_->get_width();
		const fltp core_thickness = core_->get_thickness();
		const fltp core_area = num_cores_*core_width*core_thickness;

		// get tape area
		const fltp tape_area = width_*thickness_;

		// stab area
		const fltp stab_area = tape_area-core_area;

		// stab thickness
		const fltp stab_thickness = thickness_ - core_thickness;

		// check
		if(core_area>tape_area)
			rat_throw_line("core does not fit in tape dimensions");

		// add in-plane conductor by fraction
		// this is regular direction for superconducting current
		const ShParallelConductorPr in_plane = ParallelConductor::create();
		if(core_area>0)in_plane->add_conductor(core_area/tape_area, core_);
		if(stab_area>0)in_plane->add_conductor(stab_area/tape_area, stab_);
		set_longitudinal(in_plane);
		
		// add conductor by fraction
		// assumed here is that all current has to go around the egdes of the tape
		const ShParallelConductorPr normal = ParallelConductor::create();
		if(stab_area>0)normal->add_conductor((width_-core_width)/width_, stab_);
		set_normal(normal);

		// set transverse edge joints
		// using 2 times edge joint equation for tape transverse direction
		// this material is not used for heat capacity nor density
		// if it is a type0 pair the opposing stabilizer is not available
		// because it is already used by the current of the other tape
		const ShParallelConductorPr transverse = ParallelConductor::create();
		transverse->add_conductor(stab_thickness/thickness_,BaseConductor::create(
			NULL,stab_->get_thermal_conductivity_fit(),
			RhoEdgeFit::create(stab_->get_electrical_resistivity_fit(),
			is_flipped_pair_ ? stab_thickness : 2*stab_thickness,width_),NULL));
		
		// added substrate such that thermal conductivity 
		// is same as longitudinal direction
		transverse->add_conductor(core_->get_tsubstr()/thickness_, core_->get_substr());
		set_transverse(transverse); // this should be edge joints
	}

	// critical current density [J m^-2]
	fltp HTSTape::calc_critical_current_density(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// flipped pair averaged calculation
		if(is_flipped_pair_){
			return (Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir) + 
				Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, -magnetic_field_angle, scaling_factor, dir))/2;
		}

		// normal calculation
		else{
			return Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
		}
	}

	// critical current density [J m^-2]
	arma::Col<fltp> HTSTape::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// flipped pair averaged calculation
		if(is_flipped_pair_){
			return (Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir) + 
				Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, -magnetic_field_angle, scaling_factor, dir))/2;
		}

		// normal calculation
		else{
			return Anisotropic::calc_critical_current_density(
				temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
		}
	}

	// check validity
	bool HTSTape::is_valid(const bool enable_throws) const{
		// check mesh
		if(core_==NULL){if(enable_throws){rat_throw_line("core points to NULL");} return false;}
		if(stab_==NULL){if(enable_throws){rat_throw_line("stabilizer points to NULL");} return false;}
		if(!core_->is_valid(enable_throws))return false;
		if(!stab_->is_valid(enable_throws))return false;
		if(width_<=0){if(enable_throws){rat_throw_line("width must be larger than zero");} return false;}
		if(thickness_<=0){if(enable_throws){rat_throw_line("thickness must be larger than zero");} return false;}
		if(num_cores_*core_->get_area()>width_*thickness_){if(enable_throws){rat_throw_line("core does not fit in tape");} return false;}
		return true;
	}

	// serialization type
	std::string HTSTape::get_type(){
		return "rat::mat::htstape";
	}

	// serialization
	void HTSTape::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// skipping anisotropic
		Conductor::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// dimensions
		js["width"] = width_;
		js["thickness"] = thickness_;

		// number of cores
		js["num_cores"] = static_cast<unsigned int>(num_cores_);

		// layers
		js["core"] = cmn::Node::serialize_node(core_, list);
		js["stab"] = cmn::Node::serialize_node(stab_, list);

		// type 0 flip
		js["is_flipped_pair"] = is_flipped_pair_;
	}

	// deserialization
	void HTSTape::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// skipping anisotropic
		Conductor::deserialize(js,list,factory_list,pth);

		// backwards compatibility with v2.011.0
		// remove later when no longer needed
		if(js.isMember("in_plane_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["in_plane_conductor"], list, factory_list, pth);
		if(js.isMember("transverse_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["transverse_conductor"], list, factory_list, pth);
		
		// dimensions
		width_ = js["width"].ASFLTP();
		thickness_ = js["thickness"].ASFLTP();

		// layers
		core_ = cmn::Node::deserialize_node<HTSCore>(js["core"], list, factory_list, pth);
		stab_ = cmn::Node::deserialize_node<BaseConductor>(js["stab"], list, factory_list, pth);

		// type 0 flip
		if(js.isMember("is_flipped_pair"))
			is_flipped_pair_ = js["is_flipped_pair"].asBool();

		// number of cores
		if(js.isMember("num_cores"))
			num_cores_ = js["num_cores"].asUInt64();

		// setup as parallel conductor
		setup();
	}

}}