// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "inputconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	InputConductor::InputConductor(const mat::ShConductorPr &conductor){
		set_input_conductor(conductor);
	}

	// set conductor
	void InputConductor::set_input_conductor(const mat::ShConductorPr &conductor){
		conductor_ = conductor;
	}

	// set conductor
	ShConductorPr InputConductor::get_input_conductor() const{
		return conductor_;
	}

	// is valid
	bool InputConductor::is_valid(const bool enable_throws) const{
		// it is allowed here to contain no conductor
		// but not an invalid one
		if(conductor_!=NULL)
			if(!conductor_->is_valid(enable_throws))
				return false;
		return true;
	}

	// get type
	std::string InputConductor::get_type(){
		return "rat::mat::inputconductor";
	}

	// method for serialization into json
	void InputConductor::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Node::serialize(js,list);

		// type
		js["type"] = get_type();

		// serialize conductor

		js["material"] = cmn::Node::serialize_node(conductor_, list);
	}

	// method for deserialisation from json
	void InputConductor::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent objects
		Node::deserialize(js,list,factory_list,pth);

		// deserialize conductor
		if(js.isMember("material")){
			set_input_conductor(cmn::Node::deserialize_node<mat::Conductor>(js["material"], list, factory_list, pth));
		}
	}


}}
