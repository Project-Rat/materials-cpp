// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "v2o3resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	V2O3ResistivityFit::V2O3ResistivityFit(){
		C0_ = 0.02; C1_ = 0.4; C2_ = 14; C3_ = 150;
		set_name("v2o3resistivityfit");
	}

	V2O3ResistivityFit::V2O3ResistivityFit(
		const fltp C0, const fltp C1, const fltp C2, const fltp C3){
		C0_ = C0; C1_ = C1; C2_ = C2; C3_ = C3;
		t1_ = 1.0; t2_ = 300.0;
		set_name("v2o3resistivityfit");
	}

	V2O3ResistivityFit::V2O3ResistivityFit(
		const fltp t1, const fltp t2, 
		const fltp C0, const fltp C1, 
		const fltp C2, const fltp C3){
		C0_ = C0; C1_ = C1; C2_ = C2; C3_ = C3;
		t1_ = t1; t2_ = t2;
		set_name("v2o3resistivityfit");
	}

	// factory
	ShV2O3ResistivityFitPr V2O3ResistivityFit::create(){
		return std::make_shared<V2O3ResistivityFit>();
	}

	ShV2O3ResistivityFitPr V2O3ResistivityFit::create(
		const fltp C0, const fltp C1, const fltp C2, const fltp C3){
		return std::make_shared<V2O3ResistivityFit>(C0, C1, C2, C3);
	}

	ShV2O3ResistivityFitPr V2O3ResistivityFit::create(
		const fltp t1, const fltp t2, const fltp C0, 
		const fltp C1, const fltp C2, const fltp C3){
		return std::make_shared<V2O3ResistivityFit>(t1,t2, C0, C1, C2, C3);
	}

	// setters
	void V2O3ResistivityFit::set_C0(const fltp C0){
		C0_ = C0;
	}
	
	void V2O3ResistivityFit::set_C1(const fltp C1){
		C1_ = C1;
	}

	void V2O3ResistivityFit::set_C2(const fltp C2){
		C2_ = C2;
	}

	void V2O3ResistivityFit::set_C3(const fltp C3){
		C3_ = C3;
	}


	// getters
	fltp V2O3ResistivityFit::get_C0()const{
		return C0_;
	}

	fltp V2O3ResistivityFit::get_C1()const{
		return C1_;
	}

	fltp V2O3ResistivityFit::get_C2()const{
		return C2_;
	}

	fltp V2O3ResistivityFit::get_C3()const{
		return C3_;
	}

	// scalar fit function
	fltp V2O3ResistivityFit::calc_property(
		const fltp temperature) const{

		// clamp temperature as to avoid negative values
		const fltp T = std::min(std::max(temperature, t1_), t2_);

		const fltp value = C0_ + (C1_ - C0_) / ( RAT_CONST(1.0) + std::exp( ( T - C3_ ) / C2_ ) ) ;

		// return calculated value
		return value;
	}

	// vector fit function
	arma::Col<fltp> V2O3ResistivityFit::calc_property(
		const arma::Col<fltp> &temperature) const{

		// clamp temperature as to avoid negative values in fit
		const arma::Col<fltp> T = arma::clamp( temperature, t1_, t2_);

		const arma::Col<fltp> value = C0_ + (C1_ - C0_) / ( RAT_CONST(1.0) + arma::exp( ( T - C3_ ) / C2_ ) ) ;

		// return calculated value
		return value;
	}

	// get type
	std::string V2O3ResistivityFit::get_type(){
		return "rat::mat::v2o3resistivityfit";
	}

	// method for serialization into json
	void V2O3ResistivityFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
		js["C2"] = C2_;
		js["C3"] = C3_;
	}

	// method for deserialisation from json
	void V2O3ResistivityFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		C0_ = js["C0"].ASFLTP();
		C1_ = js["C1"].ASFLTP();
		C2_ = js["C2"].ASFLTP();
		C3_ = js["C3"].ASFLTP();
	}

}}