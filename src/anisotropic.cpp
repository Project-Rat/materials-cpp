// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "anisotropic.hh"

// common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{
	// constructor
	Anisotropic::Anisotropic(){
		set_name("anisotropic");
	}

	Anisotropic::Anisotropic(
		const ShConductorPr &longitudinal, 
		const ShConductorPr &normal, 
		const ShConductorPr &transverse) : Anisotropic(){
		set_longitudinal(longitudinal);
		set_normal(normal);
		set_transverse(transverse);
	}

	// factory
	ShAnisotropicPr Anisotropic::create(){
		return std::make_shared<Anisotropic>();
	}
	ShAnisotropicPr Anisotropic::create(
		const ShConductorPr &longitudinal, 
		const ShConductorPr &normal){
		return std::make_shared<Anisotropic>(longitudinal, normal, longitudinal);
	}
	ShAnisotropicPr Anisotropic::create(
		const ShConductorPr &longitudinal, 
		const ShConductorPr &normal, 
		const ShConductorPr &transverse){
		return std::make_shared<Anisotropic>(longitudinal, normal, transverse);
	}
	
	// modify conductor list
	arma::uword Anisotropic::add_conductor(const ShConductorPr &conductor){
		const arma::uword index = conductor_list_.size()+1;
		conductor_list_.insert({index,conductor}); return index;
	}

	const ShConductorPr& Anisotropic::get_conductor(const arma::uword index)const{
		auto it = conductor_list_.find(index);
		if(it==conductor_list_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	bool Anisotropic::delete_conductor(const arma::uword index){
		auto it = conductor_list_.find(index);
		if(it==conductor_list_.end())return false;
		(*it).second = NULL; return true;
	}

	arma::uword Anisotropic::num_conductors() const{
		return conductor_list_.size();
	}

	void Anisotropic::reindex(){
		std::map<arma::uword, ShConductorPr> new_conductors; arma::uword idx = 1;
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++)
			if((*it).second!=NULL)new_conductors.insert({idx++, (*it).second});
		conductor_list_ = new_conductors;
	}

	// set conductor for in plane direction
	void Anisotropic::set_longitudinal(const ShConductorPr &longitudinal){
		conductor_list_[1] = longitudinal;
	}

	// set conductor for dir direction
	void Anisotropic::set_normal(const ShConductorPr &normal){
		conductor_list_[2] = normal;
	}

	// set conductor for dir direction
	void Anisotropic::set_transverse(const ShConductorPr &transverse){
		conductor_list_[3] = transverse;
	}

	const ShConductorPr& Anisotropic::get_longitudinal()const{
		auto it = conductor_list_.find(1);
		if(it==conductor_list_.end())rat_throw_line("longitudinal conductor not set");
		return (*it).second;
	}

	const ShConductorPr& Anisotropic::get_normal()const{
		auto it = conductor_list_.find(2);
		if(it==conductor_list_.end())rat_throw_line("normal conductor not set");
		return (*it).second;
	}

	const ShConductorPr& Anisotropic::get_transverse()const{
		auto it = conductor_list_.find(3);
		if(it==conductor_list_.end())rat_throw_line("transverse conductor not set");
		return (*it).second;
	}

	// get conductor by direction
	const ShConductorPr& Anisotropic::get_conductor(const Direction dir)const{
		if(!is_dir(dir))rat_throw_line("conductor not set for this direction");
		if(dir == Direction::LONGITUDINAL){
			return get_longitudinal();
		}else if(dir == Direction::NORMAL){
			return get_normal();
		}else if(dir == Direction::TRANSVERSE){
			return get_transverse();
		}else{
			rat_throw_line("direction not recognized");
		}
	}


	// check direction
	bool Anisotropic::is_dir(const Direction dir)const{
		if(dir == Direction::LONGITUDINAL){
			return conductor_list_.find(1)!=conductor_list_.end();
		}else if(dir == Direction::NORMAL){
			return conductor_list_.find(2)!=conductor_list_.end();
		}else if(dir == Direction::TRANSVERSE){
			return conductor_list_.find(3)!=conductor_list_.end();
		}else{
			rat_throw_line("direction not recognized");
		}
	}



	// accumulation method
	void Anisotropic::accu_parallel_conductors(
		FracConPrList &parallel_conductors,
		const Direction dir) const{
		return get_conductor(dir)->accu_parallel_conductors(parallel_conductors, dir);
	}

	// accumulation method for series conductors
	void Anisotropic::accu_series_conductors(
		FracConPrList &series_conductors,
		const Direction dir) const{
		return get_conductor(dir)->accu_series_conductors(series_conductors, dir);
	}

	// calculate specific heat [J kg^-1 K^-1]
	fltp Anisotropic::calc_specific_heat(
		const fltp temperature) const{
		if(get_longitudinal()==NULL)rat_throw_line("material not set for this direction");
		return get_longitudinal()->calc_specific_heat(temperature);
	}
	
	// thermal conductivity [W m^-1 K^-1]
	fltp Anisotropic::calc_thermal_conductivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		return get_conductor(dir)->calc_thermal_conductivity(temperature, magnetic_field_magnitude, dir);
	}
	
	// electrical resistivity [Ohm m^2]
	fltp Anisotropic::calc_electrical_resistivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		return get_conductor(dir)->calc_electrical_resistivity(temperature, magnetic_field_magnitude, dir);
	}

	// is insulating
	bool Anisotropic::is_insulator(const Direction dir)const{
		return get_conductor(dir)->is_insulator(dir);
	}

	// is insulating
	bool Anisotropic::is_thermal_insulator(const Direction dir)const{
		return get_conductor(dir)->is_thermal_insulator(dir);
	}

	// is insulating
	bool Anisotropic::is_superconductor(const Direction dir)const{
		return get_conductor(dir)->is_superconductor(dir);
	}


	// critical current density [J m^-2]
	fltp Anisotropic::calc_critical_current_density(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_critical_current_density(
			temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// critical current density [J m^-2]
	fltp Anisotropic::calc_nvalue(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const Direction dir) const{
		return get_conductor(dir)->calc_nvalue(
			temperature, magnetic_field_magnitude, 
			magnetic_field_angle, dir);
	}

	// material density [kg m^-3]
	fltp Anisotropic::calc_density(
		const fltp temperature) const{
		if(get_longitudinal()==NULL)rat_throw_line("longitudinal material is set NULL");
		return get_longitudinal()->calc_density(temperature); 
	}


	// electric field [V m^-1]
	fltp Anisotropic::calc_electric_field(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_electric_field(
			current_density, temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// calculate current density [A m^-2]
	fltp Anisotropic::calc_current_density(
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_current_density(
			electric_field, temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// material properties for vector input
	// ===
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<fltp> Anisotropic::calc_specific_heat(
		const arma::Col<fltp> &temperature) const{
		if(get_longitudinal()==NULL)rat_throw_line("longitudinal is not set");
		return get_longitudinal()->calc_specific_heat(temperature);
	}
	
	// thermal conductivity [W m^-1 K^-1]
	arma::Col<fltp> Anisotropic::calc_thermal_conductivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		return get_conductor(dir)->calc_thermal_conductivity(
			temperature, magnetic_field_magnitude, dir);
	}
	
	// electrical resistivity [Ohm m^2]
	arma::Col<fltp> Anisotropic::calc_electrical_resistivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		return get_conductor(dir)->calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> Anisotropic::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_critical_current_density(
			temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> Anisotropic::calc_nvalue(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const Direction dir) const{
		return get_conductor(dir)->calc_nvalue(
			temperature, magnetic_field_magnitude, 
			magnetic_field_angle, dir);
	}

	// material density [kg m^-3]
	arma::Col<fltp> Anisotropic::calc_density(
		const arma::Col<fltp> &temperature) const{
		if(get_longitudinal()==NULL)rat_throw_line("longitudinal material is set NULL");
		return get_longitudinal()->calc_density(temperature);
	}

	// electric field [V/m]
	arma::Col<fltp> Anisotropic::calc_electric_field(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_electric_field(
			current_density, temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// electric field [V m^-1] section averaged
	// required to supply the cross-sectional areas of the
	// elements making up a section in the same order as
	// the other properties
	arma::Col<fltp> Anisotropic::calc_electric_field_av(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir) const{
		return get_conductor(dir)->calc_electric_field_av(
			current_density, temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, 
			nelements, cross_num_nodes, cross_areas, dir);
	}


	// calculate current density [A m^-2]
	arma::Col<fltp> Anisotropic::calc_current_density(
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{
		return get_conductor(dir)->calc_current_density(
			electric_field, temperature, 
			magnetic_field_magnitude, 
			magnetic_field_angle,
			scaling_factor, dir);
	}

	// check validity
	bool Anisotropic::is_valid(const bool enable_throws) const{
		if(!is_dir(Direction::LONGITUDINAL)){if(enable_throws){rat_throw_line("longitudinal conductor not set");} return false;}
		if(!is_dir(Direction::NORMAL)){if(enable_throws){rat_throw_line("normal conductor not set");} return false;}
		if(!is_dir(Direction::TRANSVERSE)){if(enable_throws){rat_throw_line("transverse conductor not set");} return false;}
		if(conductor_list_.find(4)!=conductor_list_.end()){if(enable_throws){rat_throw_line("can not have more than 3 materials");} return false;}
		return true;
	}

	// serialization type
	std::string Anisotropic::get_type(){
		return "rat::mat::anisotropic";
	}

	// serialization
	void Anisotropic::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Conductor::serialize(js,list);

		// type
		js["type"] = get_type();

		// components
		if(is_dir(Direction::LONGITUDINAL))js["longitudinal"] = cmn::Node::serialize_node(get_longitudinal(), list);
		if(is_dir(Direction::NORMAL))js["normal"] = cmn::Node::serialize_node(get_normal(), list);
		if(is_dir(Direction::TRANSVERSE))js["transverse"] = cmn::Node::serialize_node(get_transverse(), list);
	}

	// deserialization
	void Anisotropic::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		Conductor::deserialize(js,list,factory_list,pth);

		// backwards compatibility with v2.011.0
		// remove later when no longer needed
		if(js.isMember("in_plane_conductor")){
			set_longitudinal(cmn::Node::deserialize_node<Conductor>(js["in_plane_conductor"], list, factory_list, pth));
			set_transverse(get_longitudinal());
		}
		if(js.isMember("transverse_conductor"))
			set_normal(cmn::Node::deserialize_node<Conductor>(js["transverse_conductor"], list, factory_list, pth));
		
		// components
		if(js.isMember("longitudinal"))
			set_longitudinal(cmn::Node::deserialize_node<Conductor>(
				js["longitudinal"], list, factory_list, pth));

		if(js.isMember("normal"))
			set_normal(cmn::Node::deserialize_node<Conductor>(
				js["normal"], list, factory_list, pth));

		if(js.isMember("transverse"))
			set_transverse(cmn::Node::deserialize_node<Conductor>(
				js["transverse"], list, factory_list, pth));
	}

}}