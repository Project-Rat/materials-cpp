// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "scalefit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	ScaleFit::ScaleFit(){

	}

	ScaleFit::ScaleFit(const fltp scale_factor, const ShFitPr& fit) : ScaleFit(){
		set_fit(fit);
		set_scale_factor(scale_factor);
	}

	// factory
	ShScaleFitPr ScaleFit::create(){
		return std::make_shared<ScaleFit>();
	}

	ShScaleFitPr ScaleFit::create(const fltp scale_factor, const ShFitPr& fit){
		return std::make_shared<ScaleFit>(scale_factor,fit);
	}

	// setters
	void ScaleFit::set_scale_factor(const fltp scale_factor){
		scale_factor_ = scale_factor;
	}
	void ScaleFit::set_fit(const ShFitPr& fit){
		fit_ = fit;
	}

	// getters
	fltp ScaleFit::get_scale_factor()const{
		return scale_factor_;
	}
	
	const ShFitPr& ScaleFit::get_fit()const{
		return fit_;
	}

	// calculate property as function of scalar temperature
	fltp ScaleFit::calc_property(const fltp temperature) const{
		return scale_factor_*fit_->calc_property(temperature);
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	fltp ScaleFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude) const{
		return scale_factor_*fit_->calc_property(temperature,magnetic_field_magnitude);
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	fltp ScaleFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle) const{
		return scale_factor_*fit_->calc_property(temperature,magnetic_field_magnitude,magnetic_field_angle);
	}


	// calculate property as function of vector temperature
	arma::Col<fltp> ScaleFit::calc_property(const arma::Col<fltp> &temperature) const{
		return scale_factor_*fit_->calc_property(temperature);
	}

	// calculate property as function of vector
	// temperature and magnetic field magnitude
	arma::Col<fltp> ScaleFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude) const{
		return scale_factor_*fit_->calc_property(temperature,magnetic_field_magnitude);
	}
	
	// calculate property as function of vector
	// temperature, magnetic field magnitude and angle
	arma::Col<fltp> ScaleFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle) const{
		return scale_factor_*fit_->calc_property(temperature,magnetic_field_magnitude,magnetic_field_angle);
	}

	// serialization type
	std::string ScaleFit::get_type(){
		return "scalefit";
	}

	// serialization
	void ScaleFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);
		js["type"] = get_type();
		js["scale_factor"] = scale_factor_;
		js["fit"] = cmn::Node::serialize_node(fit_, list);
	}

	// deserialization
	void ScaleFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		set_fit(cmn::Node::deserialize_node<Fit>(js["fit"], list, factory_list, pth));
		set_scale_factor(js["scale_factor"].ASFLTP());
	}

}}