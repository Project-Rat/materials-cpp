// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "conductorlist.hh"

// code specific to Rat
namespace rat{namespace mat{

	// add conductor
	arma::uword ConductorList::add_conductor(const ShConductorPr &conductor){
		const fltp fraction = RAT_CONST(0.0);
		return add_conductor(fraction, conductor);
	}

	// add conductor fraction pair
	arma::uword ConductorList::add_conductor(const FracShConPr& conductor){
		const arma::uword index = conductor_list_.size()+1;
		conductor_list_.insert({index,conductor}); return index;
	}

	// add conductor and fraction
	arma::uword ConductorList::add_conductor(const fltp fraction, const ShConductorPr &conductor){
		const arma::uword index = conductor_list_.size()+1;
		conductor_list_.insert({index,{fraction,conductor}}); return index;
	}

	// retreive conductor at index
	const ShConductorPr& ConductorList::get_conductor(const arma::uword index) const{
		auto it = conductor_list_.find(index);
		if(it==conductor_list_.end())rat_throw_line("index does not exist");
		return (*it).second.second;
	}

	// get conductor list
	const FracShConPrMap& ConductorList::get_conductors()const{
		return conductor_list_;
	}

	// delete conductor at index
	bool ConductorList::delete_conductor(const arma::uword index){
		auto it = conductor_list_.find(index);
		if(it==conductor_list_.end())return false;
		(*it).second.second = NULL; return true;
	}

	// number of conductors
	arma::uword ConductorList::num_conductors() const{
		return conductor_list_.size();
	}

	// re-index nodes after deleting
	void ConductorList::reindex(){
		std::map<arma::uword, FracShConPr> new_conductors; arma::uword idx = 1;
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++)
			if((*it).second.second!=NULL)new_conductors.insert({idx++, (*it).second});
		conductor_list_ = new_conductors;
	}

	// clear conductor list
	void ConductorList::clear_conductors(){
		conductor_list_.clear();
	}

	// get fractions
	fltp ConductorList::get_fraction(const arma::uword index){
		return conductor_list_.at(index).first;
	}
	
	// set fractions
	void ConductorList::set_fraction(const arma::uword index, const fltp fraction){
		conductor_list_.at(index).first = fraction;
	}

	// validity check

	// serialization
	std::string ConductorList::get_type(){
		return "rat::mat::conductorlist";
	}

	// method for serialization into json
	void ConductorList::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		cmn::Node::serialize(js,list);
		js["type"] = get_type();

		// serialize conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// create sub object to hold both fraction and conductor
			Json::Value js_con;
			js_con["conductor"] = cmn::Node::serialize_node(conductor, list);
			js_con["fraction"] = fraction;

			// serialize nodes
			js["conductor_list"].append(js_con);
		}
	}

	// method for deserialization from json
	void ConductorList::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		
		// parent
		cmn::Node::deserialize(js,list,factory_list,pth);
		
		// number of condutcors
		for(auto it = js["conductor_list"].begin();it!=js["conductor_list"].end();it++){
			// get sub object
			Json::Value js_con = (*it);

			// get fraction and conductor and add to self
			add_conductor(js_con["fraction"].ASFLTP(), 
				cmn::Node::deserialize_node<Conductor>(
				js_con["conductor"], list, factory_list, pth));
		}
	}

}}