// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "polyfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	PolyFit::PolyFit(){
		set_name("polyfit");
	}

	// default constructor
	PolyFit::PolyFit(const arma::Row<fltp> &T, const arma::Row<fltp> &v, const arma::uword N) : PolyFit(){
		set_fit_parameters(arma::polyfit(T,v,N));
		set_temperature_range(T.front(),T.back());
	}

	// default constructor
	PolyFit::PolyFit(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters) : PolyFit(){
		set_temperature_range(t1,t2);
		set_fit_parameters(fit_parameters);
	}

	// factory
	ShPolyFitPr PolyFit::create(){
		return std::make_shared<PolyFit>();
	}

	// factory
	ShPolyFitPr PolyFit::create(const arma::Row<rat::fltp> &T, const arma::Row<rat::fltp> &v, const arma::uword N){
		return std::make_shared<PolyFit>(T,v,N);
	}

	// factory
	ShPolyFitPr PolyFit::create(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters){
		return std::make_shared<PolyFit>(t1, t2, fit_parameters);
	}


	// setters
	void PolyFit::set_fit_parameters(const arma::Col<fltp> &fit_parameters){
		fit_parameters_ = fit_parameters;
	}

	// getters
	const arma::Col<fltp>& PolyFit::get_fit_parameters()const{
		return fit_parameters_;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<fltp> PolyFit::calc_property(const arma::Col<fltp> &temperature)const{
		// limit lower temperature
		const arma::Col<fltp> T = arma::clamp(temperature,t1_,arma::Datum<fltp>::inf);

		// use armadillo polyval
		arma::Col<fltp> values = arma::polyval(fit_parameters_, T);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(T>t2_);
		if(!extrap.is_empty()){
			const fltp dt = (t2_-t1_)/1000;
			const arma::Col<fltp>::fixed<2> Te = {t2_-dt, t2_-1e-10f};
			const arma::Col<fltp>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (T(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// return calculated values
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	fltp PolyFit::calc_property(const fltp temperature)const{
		// allocate
		fltp value = 0;

		// extrapolation beyond end
		if(temperature>t2_){
			const fltp dt = (t2_-t1_)/1000;
			const fltp t1 = t2_-dt;
			const fltp t2 = t2_;
			const fltp s1 = calc_property(t1);
			const fltp s2 = calc_property(t2);
			value = s2 + (temperature - t2_)*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<t1_){
			value = calc_property(t1_);
		}

		// calculate normally
		else{
			value = 0; fltp Tpow = 1.0;
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				value += fit_parameters_(fit_parameters_.n_elem-1-i)*Tpow; Tpow *= temperature;
			}
		}

		// return answer
		return value;
	}

	// validity check
	bool PolyFit::is_valid(const bool enable_throws)const{
		if(fit_parameters_.empty()){if(enable_throws){rat_throw_line("fit parameters are empty");} return false;};
		if(t2_<=t1_){if(enable_throws){
			rat_throw_line("second temperature must be larger than first temperature");} return false;};
		return true;
	}

	// get type
	std::string PolyFit::get_type(){
		return "rat::mat::polyfit";
	}

	// method for serialization into json
	void PolyFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// create alphabet
		const std::string alphabet("abcdefghijklmnopqrstuvwxyz");

		// fit parameters
		arma::uword cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end() && cnt<fit_parameters_.n_elem;it++,cnt++)
			js[std::string(1,*it)] = fit_parameters_(cnt);
	}

	// method for deserialisation from json
	void PolyFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// create alphabet
		const std::string alphabet("abcdefghijklmnopqrstuvwxyz");

		// count number of parameters
		arma::uword cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end();it++,cnt++)
			if(!js[std::string(1,*it)].isNumeric())break;

		// allocate
		fit_parameters_.set_size(cnt);

		// retreive values
		cnt = 0;
		for(auto it=alphabet.begin();it!=alphabet.end() && cnt<fit_parameters_.n_elem;it++,cnt++)
			fit_parameters_(cnt) = js[std::string(1,*it)].ASFLTP();
	}

}}