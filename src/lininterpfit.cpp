// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "lininterpfit.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LinInterpFit::LinInterpFit(){
		set_name("lininterpfit");
	}

	LinInterpFit::LinInterpFit(
		const arma::Col<fltp> &temperatures, 
		const arma::Col<fltp> &values) : LinInterpFit(){
		set_temperatures(temperatures);
		set_values(values);
	}

	// factories
	ShLinInterpFitPr LinInterpFit::create(){
		return std::make_shared<LinInterpFit>();
	}

	ShLinInterpFitPr LinInterpFit::create(
		const arma::Col<fltp> &temperatures, 
		const arma::Col<fltp> &values){
		return std::make_shared<LinInterpFit>(temperatures, values);
	}

	// setters
	void LinInterpFit::set_temperatures(const arma::Col<fltp> &temperatures){
		temperatures_ = temperatures;
	}
	
	void LinInterpFit::set_values(const arma::Col<fltp> &values){
		values_ = values;
	}

	void LinInterpFit::set_is_monotonic(const bool is_monotonic){
		is_monotonic_ = is_monotonic;
	}

	// getters
	const arma::Col<fltp>& LinInterpFit::get_temperatures()const{
		return temperatures_;
	}

	const arma::Col<fltp>& LinInterpFit::get_values()const{
		return values_;
	}

	// scalar fit function
	fltp LinInterpFit::calc_property(const fltp temperature) const{
		return arma::as_scalar(calc_property(arma::Col<fltp>{temperature}));
	}

	// vector fit function
	arma::Col<fltp> LinInterpFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		if(temperatures_.n_elem!=values_.n_elem)
			rat_throw_line("Temperature array length does not match values");

		arma::Col<fltp> value;
		if(is_monotonic_)cmn::Extra::lininterp1f(temperatures_, values_, temperature, value, extrap_);
		else cmn::Extra::interp1(temperatures_, values_, temperature, value, "linear", extrap_);
		return value;
	}

	// get type
	std::string LinInterpFit::get_type(){
		return "rat::mat::lininterpfit";
	}

	// method for serialization into json
	void LinInterpFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// lookup table
		for(arma::uword i=0;i<temperatures_.n_elem;i++)
			js["temperatures"].append(temperatures_(i));
		for(arma::uword i=0;i<values_.n_elem;i++)
			js["values"].append(values_(i));

		js["is_monotonic"] = is_monotonic_;

		// extrapolation
		js["extrap"] = extrap_;
	}

	// method for deserialisation from json
	void LinInterpFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// lookup table
		temperatures_.set_size(js["temperatures"].size()); arma::uword idx = 0;
		for(auto it = js["temperatures"].begin();it!=js["temperatures"].end();it++)
			temperatures_(idx++) = (*it).ASFLTP();
		values_.set_size(js["values"].size()); idx = 0;
		for(auto it = js["values"].begin();it!=js["values"].end();it++)
			values_(idx++) = (*it).ASFLTP();

		// extrapolation
		extrap_ = js["extrap"].asBool();
		if(js.isMember("is_monotonic"))
			set_is_monotonic(js["is_monotonic"].asBool());
	}

}}
