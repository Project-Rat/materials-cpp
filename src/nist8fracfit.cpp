// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "nist8fracfit.hh"

// code specific to Rat
namespace rat{namespace mat{


	// default constructor
	Nist8FracFit::Nist8FracFit(){
		set_name("nist8frac");
	}

	// default constructor
	Nist8FracFit::Nist8FracFit(
		const fltp t1, const fltp t2, 
		const arma::Col<fltp> &fit_parameters) : Nist8FracFit(){
		set_temperature_range(t1,t2);
		set_fit_parameters(fit_parameters);
	}

	// factory
	ShNist8FracFitPr Nist8FracFit::create(){
		return std::make_shared<Nist8FracFit>();
	}

	// factory
	ShNist8FracFitPr Nist8FracFit::create(
		const fltp t1, const fltp t2, 
		const arma::Col<fltp> &fit_parameters){
		return std::make_shared<Nist8FracFit>(t1,t2,fit_parameters);
	}


	// set fit parameters
	void Nist8FracFit::set_fit_parameters(
		const arma::Col<fltp> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	void Nist8FracFit::set_fit_parameters(const arma::uword index, const fltp value){
		fit_parameters_(index) = value;
	}

	
	
	// get fit parameters
	fltp Nist8FracFit::get_fit_parameters(const arma::uword index)const{
		return fit_parameters_(index);
	}



	// specific heat output in [J m^-3 K^-1]
	arma::Col<fltp> Nist8FracFit::calc_property(
		const arma::Col<fltp> &temperature)const{

		// limit lower temperature
		const arma::Col<fltp> temperature_lim = arma::clamp(
			temperature,t1_,arma::Datum<fltp>::inf);

		// calculate fit
		arma::Col<fltp> k1(temperature_lim.n_elem,arma::fill::zeros); 
		arma::Col<fltp> k2(temperature_lim.n_elem,arma::fill::ones);
		for(arma::uword i=0;i<5;i++){
			const arma::Col<fltp> tp = arma::pow(temperature_lim,i*RAT_CONST(0.5));
			const fltp f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const fltp f2 = fit_parameters_(2*i-1);
				k2 += f2*tp;
			}
		}

		// take exp10
		arma::Col<fltp> values = arma::exp10(k1/k2);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(temperature_lim>t2_);
		if(!extrap.is_empty()){
			const fltp dt = (t2_-t1_)/1000;
			
			assert(dt>0);
			const arma::Col<fltp>::fixed<2> Te = {
				t2_-dt, 
				t2_-RAT_CONST(1e-10)};
			assert(arma::all(Te>0));
			assert(Te(1)>Te(0));
			const arma::Col<fltp>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (temperature_lim(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
			assert(arma::all(values(extrap)>0));
		}

		// check output
		assert(values.is_finite()); assert(arma::all(values>0));

		// return thermal conductivity
		return values;
	}

	// specific heat output in [J m^-3 K^-1]
	fltp Nist8FracFit::calc_property(
		const fltp temperature)const{

		// catch below range
		if(temperature<t1_)
			return calc_property(t1_);

		// calculate fit
		fltp k1 = 0, k2 = 1.0;
		for(arma::uword i=0;i<5;i++){
			const fltp tp = std::pow(temperature,i*RAT_CONST(0.5));
			const fltp f1 = fit_parameters_(2*i);
			k1 += f1*tp; 
			if(i!=0){
				const fltp f2 = fit_parameters_(2*i-1);
				if(i!=0)k2 += f2*tp;
			}
		}

		// take exp10
		const fltp k = std::pow(RAT_CONST(10.0),k1/k2);

		// return thermal conductivity
		return k;
	}

	
	// get type
	std::string Nist8FracFit::get_type(){
		return "rat::mat::nist8fracfit";
	}

	// method for serialization into json
	void Nist8FracFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);
	}

	// method for deserialisation from json
	void Nist8FracFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		
		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// fit parameters
		fit_parameters_(0) = js["a"].ASFLTP();
		fit_parameters_(1) = js["b"].ASFLTP();
		fit_parameters_(2) = js["c"].ASFLTP();
		fit_parameters_(3) = js["d"].ASFLTP();
		fit_parameters_(4) = js["e"].ASFLTP();
		fit_parameters_(5) = js["f"].ASFLTP();
		fit_parameters_(6) = js["g"].ASFLTP();
		fit_parameters_(7) = js["h"].ASFLTP();
		fit_parameters_(8) = js["i"].ASFLTP();
	}

}}