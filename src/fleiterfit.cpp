// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "fleiterfit.hh"

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FleiterFit::FleiterFit(){
		set_name("fleiterfit"); set_t1(1); set_t2(100);
	}

	// factory
	ShFleiterFitPr FleiterFit::create(){
		return std::make_shared<FleiterFit>();
	}

	// calc critical current without field angle nor magnitude
	fltp FleiterFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// calc critical current without field angle
	fltp FleiterFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude) const{
		return calc_property(temperature, magnetic_field_magnitude, RAT_CONST(0.0));
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	fltp FleiterFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle) const{

		// prevent zero field at which a anomalous behaviour occurs
		const fltp B = std::max(magnetic_field_magnitude,RAT_CONST(0.01));
		const fltp T = std::max(temperature,RAT_CONST(1.0));
		const fltp alpha = std::fmod(magnetic_field_angle-!ignore_pkoff_*pkoff_,arma::Datum<fltp>::pi)+!ignore_pkoff_*pkoff_;

		// calculate unitless temperature
		const fltp t = T/Tc0_;

		// irreversibility field for ab-plane
		const fltp Bi_ab = Bi0ab_*(
			std::pow(1.0-std::pow(t,n1_),n2_)+a_*(1.0-std::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const fltp bab = B/Bi_ab;

		// irreversibility field for c-plane
		const fltp Bi_c = Bi0c_*(RAT_CONST(1.0)-std::pow(t,n_));

		// unitless field for c-plane
		const fltp bc = B/Bi_c;

		// critical current density c-plane
		fltp Jc_c = (alphac_/B)*std::pow(bc,pc_)*
			std::pow(1.0-bc,qc_)*std::pow(1.0-std::pow(t,n_),gammac_);

		// critical current density ab-plane
		fltp Jc_ab = (alphaab_/B)*std::pow(bab,pab_)*std::pow(RAT_CONST(1.0)-bab,qab_)*
			std::pow(std::pow(RAT_CONST(1.0)-std::pow(t,n1_),n2_)+a_*(RAT_CONST(1.0)-std::pow(t,n_)),gammaab_);

		// temperature over Tcs
		if(t>=RAT_CONST(1.0)){Jc_c = 0; Jc_ab = 0;};
		if(bc>=RAT_CONST(1.0))Jc_c = 0;
		if(bab>=RAT_CONST(1.0))Jc_ab = 0;
		
		// anisotropy factor
		const fltp g = g0_ + g1_*std::exp(-(g2_*std::exp(g3_*T))*B);

		// Sort out angular dependence
		// const fltp pkoff_rad = 2*arma::Datum<fltp>::pi*pkoff_/RAT_CONST(360.0);

		// allocate
		fltp Jc;
		
		// average
		if(force_perpendicular_ && force_parallel_){
			Jc = (Jc_c + Jc_ab)/2;
		}

		// critical current from c-axis only
		else if(force_perpendicular_){
			Jc = Jc_c;
		}
		
		// critical current from ab-plane only
		else if(force_parallel_){
			Jc = Jc_ab;
		}

		else if(disable_clamping_){
			// angular dependency
			Jc = Jc_c+(Jc_ab-Jc_c)/(1.0+std::pow((std::abs(std::abs(alpha + 
				!ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/2.0f))/g, nu_));

			// flipped type-0 pair
			if(type0_flip_){
				Jc += Jc_ab+(Jc_ab-Jc_c)/(1.0+std::pow((std::abs(std::abs(alpha - 
				!ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/2.0f))/g, nu_));
				Jc/=2;
			}
		}

		// calculate angular dependent critical current density
		else{
			// angular dependency
			Jc = std::min(Jc_c,Jc_ab)+
				(std::max(Jc_ab-Jc_c,RAT_CONST(0.0))/
				(1.0+std::pow((std::abs(std::abs(alpha + 
				!ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/2.0f))/g, nu_)));

			// flipped type-0 pair
			if(type0_flip_){
				Jc += std::min(Jc_c,Jc_ab)+
				(std::max(Jc_ab-Jc_c,RAT_CONST(0.0))/
				(1.0+std::pow((std::abs(std::abs(alpha - 
				!ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/2.0f))/g, nu_)));
				Jc/=2;
			}
		}

		// return critical current density
		return Jc;
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> FleiterFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// calc critical current without field angle
	arma::Col<fltp> FleiterFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude) const{
		return calc_property(temperature, magnetic_field_magnitude, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<fltp> FleiterFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle) const{
		
		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.n_elem==magnetic_field_angle.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		assert(magnetic_field_angle.is_finite());

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<fltp> B = arma::clamp(magnetic_field_magnitude,RAT_CONST(0.1),arma::Datum<fltp>::inf);
		const arma::Col<fltp> T = arma::clamp(temperature,RAT_CONST(1.0),arma::Datum<fltp>::inf);
		arma::Col<fltp> alpha = cmn::Extra::modulus(magnetic_field_angle-!ignore_pkoff_*pkoff_ ,arma::Datum<fltp>::pi) + !ignore_pkoff_*pkoff_;

		// calculate unitless temperature
		const arma::Col<fltp> t = T/Tc0_;

		// irreversibility field for ab-plane
		const arma::Col<fltp> Bi_ab = Bi0ab_*(
			arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(RAT_CONST(1.0)-arma::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const arma::Col<fltp> bab = B/Bi_ab;

		// irreversibility field for c-plane
		const arma::Col<fltp> Bi_c = Bi0c_*(1.0-arma::pow(t,n_));

		// unitless field for c-plane
		const arma::Col<fltp> bc = B/Bi_c;

		// critical current density c-plane
		arma::Col<fltp> Jc_c = (alphac_/B)%arma::pow(bc,pc_)%
			arma::pow(1.0-bc,qc_)%arma::pow(RAT_CONST(1.0)-arma::pow(t,n_),gammac_);

		// critical current density ab-plane
		arma::Col<fltp> Jc_ab = (alphaab_/B)%arma::pow(bab,pab_)%arma::pow(RAT_CONST(1.0)-bab,qab_)%
			arma::pow(arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(RAT_CONST(1.0)-arma::pow(t,n_)),gammaab_);

		// temperature over Tcs
		Jc_c(arma::find(t>=RAT_CONST(1.0))).fill(RAT_CONST(0.0)); 
		Jc_ab(arma::find(t>=RAT_CONST(1.0))).fill(RAT_CONST(0.0));

		// field over Bi
		Jc_c(arma::find(bc>=RAT_CONST(1.0))).fill(RAT_CONST(0.0)); 
		Jc_ab(arma::find(bab>=RAT_CONST(1.0))).fill(RAT_CONST(0.0));

		// anisotropy factor
		const arma::Col<fltp> g = g0_ + g1_*arma::exp(-(g2_*arma::exp(g3_*T))%B);

		// // Sort out angular dependence
		// const fltp pkoff_rad = 2*arma::Datum<fltp>::pi*pkoff_/360.0;

		// allocate 
		arma::Col<fltp> Jc;

		// average
		if(force_perpendicular_ && force_parallel_){
			Jc = (Jc_c + Jc_ab)/2;
		}

		// critical current from c-axis only
		else if(force_perpendicular_){
			Jc = Jc_c;
		}

		else if(force_parallel_){
			Jc = Jc_ab;
		}

		else if(disable_clamping_){
			Jc = Jc_c + (Jc_ab-Jc_c)/(RAT_CONST(1.0)+arma::pow((arma::abs(arma::abs(alpha + !ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/RAT_CONST(2.0)))/g,nu_));
			if(type0_flip_){
				Jc += Jc_c + (Jc_ab-Jc_c)/(RAT_CONST(1.0)+arma::pow((arma::abs(arma::abs(alpha - !ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/RAT_CONST(2.0)))/g,nu_));
				Jc/=2;
			}
		}

		// calculate angular dependent critical current density
		else{
			// angular dependency
			Jc = arma::min(Jc_c,Jc_ab)+
				(arma::clamp(Jc_ab-Jc_c,RAT_CONST(0.0),arma::Datum<fltp>::inf)/
				(RAT_CONST(1.0)+arma::pow((arma::abs(arma::abs(alpha + !ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/RAT_CONST(2.0)))/g,nu_)));

			// flipped type-0 pair
			if(type0_flip_){
				Jc += arma::min(Jc_c,Jc_ab)+
					(arma::clamp(Jc_ab-Jc_c,RAT_CONST(0.0),arma::Datum<fltp>::inf)/
					(RAT_CONST(1.0)+arma::pow((arma::abs(arma::abs(alpha - !ignore_pkoff_*pkoff_) - arma::Datum<fltp>::pi/RAT_CONST(2.0)))/g,nu_)));
				Jc/=2;
			}
		}

		// check output
		assert(Jc.n_elem==temperature.n_elem);
		assert(Jc.is_finite());

		// return critical current density
		return Jc;
	}

	// typical layer thickness
	void FleiterFit::set_tsc(const fltp tsc){
		tsc_ = tsc;
	}

	// general parameters
	void FleiterFit::set_Tc0(const fltp Tc0){
		Tc0_ = Tc0;
	}

	void FleiterFit::set_n(const fltp n){
		n_ = n;
	}

	void FleiterFit::set_n1(const fltp n1){
		n1_ = n1;
	}

	void FleiterFit::set_n2(const fltp n2){
		n2_ = n2;
	}

	// Parameters for ab-plane
	void FleiterFit::set_pab(const fltp pab){
		pab_ = pab;
	}

	void FleiterFit::set_qab(const fltp qab){
		qab_ = qab;
	}

	void FleiterFit::FleiterFit::set_Bi0ab(const fltp Bi0ab){
		Bi0ab_ = Bi0ab;
	}

	void FleiterFit::set_a(const fltp a){
		a_ = a;
	}

	void FleiterFit::set_gammaab(const fltp gammaab){
		gammaab_ = gammaab;
	}

	void FleiterFit::set_alphaab(const fltp alphaab){
		alphaab_ = alphaab;
	}

	// Parameters for c-plane
	void FleiterFit::set_pc(const fltp pc){
		pc_ = pc;
	}

	void FleiterFit::set_qc(const fltp qc){
		qc_ = qc;
	}

	void FleiterFit::set_Bi0c(const fltp Bi0c){
		Bi0c_ = Bi0c;
	}

	void FleiterFit::set_gammac(const fltp gammac){
		gammac_ = gammac;
	}

	void FleiterFit::set_alphac(const fltp alphac){
		alphac_ = alphac;
	}

	// Parameters for anisotropy
	void FleiterFit::set_g0(const fltp g0){
		g0_ = g0;
	}

	void FleiterFit::set_g1(const fltp g1){
		g1_ = g1;
	}

	void FleiterFit::set_g2(const fltp g2){
		g2_ = g2;
	}

	void FleiterFit::set_g3(const fltp g3){
		g3_ = g3;
	}

	void FleiterFit::set_nu(const fltp nu){
		nu_ = nu;
	}

	void FleiterFit::set_pkoff(const fltp pkoff){
		pkoff_ = pkoff;
	}

	void FleiterFit::set_force_perpendicular(const bool force_perpendicular){
		force_perpendicular_ = force_perpendicular;
	}

	void FleiterFit::set_force_parallel(const bool force_parallel){
		force_parallel_ = force_parallel;
	}

	void FleiterFit::set_disable_clamping(const bool disable_clamping){
		disable_clamping_ = disable_clamping;
	}

	// type-0 pair flipping
	void FleiterFit::set_type0_flip(const bool type0_flip){
		type0_flip_ = type0_flip;
	}

	void FleiterFit::set_ignore_pkoff(const bool ignore_pkoff){
		ignore_pkoff_ = ignore_pkoff;
	}

	// typical layer thickness
	fltp FleiterFit::get_tsc() const{
		return tsc_;
	}

	// general parameters
	fltp FleiterFit::get_Tc0() const{
		return Tc0_;
	}

	fltp FleiterFit::get_n() const{
		return n_;
	}

	fltp FleiterFit::FleiterFit::get_n1() const{
		return n1_;
	}

	fltp FleiterFit::get_n2() const{
		return n2_;
	}

	// Parameters for ab-plane
	fltp FleiterFit::get_pab() const{
		return pab_;
	}

	fltp FleiterFit::get_qab() const{
		return qab_;
	}

	fltp FleiterFit::get_Bi0ab() const{
		return Bi0ab_;
	}

	fltp FleiterFit::get_a() const{
		return a_;
	}

	fltp FleiterFit::get_gammaab() const{
		return gammaab_;
	}

	fltp FleiterFit::get_alphaab() const{
		return alphaab_;
	}

	// Parameters for c-plane
	fltp FleiterFit::get_pc() const{
		return pc_;
	}

	fltp FleiterFit::get_qc() const{
		return qc_;
	}

	fltp FleiterFit::get_Bi0c() const{
		return Bi0c_;
	}

	fltp FleiterFit::get_gammac() const{
		return gammac_;
	}

	fltp FleiterFit::get_alphac() const{
		return alphac_;
	}

	// Parameters for anisotropy
	fltp FleiterFit::get_g0() const{
		return g0_;
	}

	fltp FleiterFit::get_g1() const{
		return g1_;
	}

	fltp FleiterFit::get_g2() const{
		return g2_;
	}

	fltp FleiterFit::get_g3() const{
		return g3_;
	}

	fltp FleiterFit::get_nu() const{
		return nu_;
	}

	fltp FleiterFit::get_pkoff() const{
		return pkoff_;
	}

	// type-0 pair flipping
	bool FleiterFit::get_type0_flip() const{
		return type0_flip_;
	}
	
	bool FleiterFit::get_ignore_pkoff() const{
		return ignore_pkoff_;
	}

	bool FleiterFit::get_force_perpendicular()const{
		return force_perpendicular_;
	}

	bool FleiterFit::get_force_parallel()const{
		return force_parallel_;
	}

	// serialization
	// get type
	std::string FleiterFit::get_type(){
		return "rat::mat::fleiterfit";
	}

	// method for serialization into json
	void FleiterFit::serialize(
		Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();
		
		// temperature dependence
		js["Tc0"] = Tc0_;
		js["n"] = n_;
		js["n1"] = n1_;
		js["n2"] = n2_;

		// AB-plane
		js["pab"] = pab_;
		js["qab"] = qab_;
		js["Bi0ab"] = Bi0ab_;
		js["a"] = a_;
		js["gammaab"] = gammaab_;
		js["alphaab"] = alphaab_;

		// C-axis
		js["pc"] = pc_;
		js["qc"] = qc_;
		js["Bi0c"] = Bi0c_;
		js["gammac"] = gammac_;
		js["alphac"] = alphac_;

		// anisotropy
		js["g0"] = g0_;
		js["g1"] = g1_;
		js["g2"] = g2_;
		js["g3"] = g3_;
		js["nu"] = nu_;
		js["pkoff"] = pkoff_;

		// typical layer thicknesses and width
		js["tsc"] = tsc_;

		// type-0 pair flipping
		js["type0_flip"] = type0_flip_;
		js["ignore_pkoff"] = ignore_pkoff_;
		js["force_perpendicular"] = force_perpendicular_;
		js["force_parallel"] = force_parallel_;
		js["disable_clamping"] = disable_clamping_;
	}

	// method for deserialisation from json
	void FleiterFit::deserialize(
		const Json::Value &js, cmn::DSList &list,
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// general
		Tc0_ = js["Tc0"].ASFLTP();
		n_ = js["n"].ASFLTP();
		n1_ = js["n1"].ASFLTP();
		n2_ = js["n2"].ASFLTP();

		// AB-plane
		pab_ = js["pab"].ASFLTP();
		qab_ = js["qab"].ASFLTP();
		Bi0ab_ = js["Bi0ab"].ASFLTP();
		a_ = js["a"].ASFLTP();
		gammaab_ = js["gammaab"].ASFLTP();
		alphaab_ = js["alphaab"].ASFLTP();

		// C-axis
		pc_ = js["pc"].ASFLTP();
		qc_ = js["qc"].ASFLTP();
		Bi0c_ = js["Bi0c"].ASFLTP();
		gammac_ = js["gammac"].ASFLTP();
		alphac_ = js["alphac"].ASFLTP();

		// anisotropy
		g0_ = js["g0"].ASFLTP();
		g1_ = js["g1"].ASFLTP();
		g2_ = js["g2"].ASFLTP();
		g3_ = js["g3"].ASFLTP();
		nu_ = js["nu"].ASFLTP();
		pkoff_ = js["pkoff"].ASFLTP();

		// typical layer thickness
		tsc_ = js["tsc"].ASFLTP();

		// type-0 pair flipping
		type0_flip_ = js["type0_flip"].asBool();
		ignore_pkoff_ = js["ignore_pkoff"].asBool();
		force_perpendicular_ = js["force_perpendicular"].asBool();
		force_parallel_ = js["force_parallel"].asBool();
		disable_clamping_ = js["disable_clamping"].asBool();
	}

}}