// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "rat/common/extra.hh"
#include "interp2fit.hh"

namespace rat { namespace mat {

	// constructor
	Interp2Fit::Interp2Fit() {}

	// overloaded constructor
	Interp2Fit::Interp2Fit(const arma::Col<fltp>& x, const arma::Col<fltp>& y, const arma::Mat<fltp>& v) {
		set_x(x);
		set_y(y);
		set_v(v);

		if(is_linspaced()) set_use_fast(true);
	}

	// destructor
	// Interp2Fit::~Interp2Fit() {}

	// factory
	ShInterp2FitPr Interp2Fit::create() {
		return std::make_shared<Interp2Fit>();
	}

	// overload
	ShInterp2FitPr Interp2Fit::create(const arma::Col<fltp>& x, const arma::Col<fltp>& y, const arma::Mat<fltp>& v) {
		return std::make_shared<Interp2Fit>(x, y, v);
	}

	// override the materials functions
	fltp Interp2Fit::calc_property(const fltp x, const fltp y) const {
		// return
		return rat::cmn::Extra::interp2(x_, y_, v_, x, y);
	}

	arma::Col<fltp> Interp2Fit::calc_property(const arma::Col<fltp>& x, const arma::Col<fltp>& y) const {

		// allocate
		arma::Col<fltp> vo = arma::Col<fltp>(x.n_elem);

		if(use_fast_) {
			rat::cmn::Extra::lininterp2f(x_, y_, v_, x, y, vo);
		} else {
			rat::cmn::Extra::interp2(x_, y_, v_, x, y, vo);
		}

		// return
		return vo;
	}

	// setters
	void Interp2Fit::set_x(const arma::Col<fltp>& x) {
		// set
		x_ = x;
	}

	void Interp2Fit::set_y(const arma::Col<fltp>& y) {
		// set
		y_ = y;
	}

	void Interp2Fit::set_v(const arma::Mat<fltp>& v) {
		// set but check first
		assert(v.n_cols == x_.n_elem);
		assert(v.n_rows == y_.n_elem);
		v_ = v;
	}

	bool Interp2Fit::is_linspaced()const{

		// check each variable is linearly spaced
		arma::Col<fltp> dx = x_.subvec(1, x_.n_elem - 1) - x_.subvec(0, x_.n_elem - 2);
		arma::Col<fltp> dy = y_.subvec(1, y_.n_elem - 1) - y_.subvec(0, y_.n_elem - 2);

		if(!x_.is_sorted()) return false;
		if(!y_.is_sorted()) return false;
		if(!arma::all(arma::unique(dx) == dx.max())) return false;
		if(!arma::all(arma::unique(dy) == dy.max())) return false;

		// success
		return true;
	}

	void Interp2Fit::set_use_fast(bool use_fast) {
		// override

		// check
		if(use_fast) assert(is_linspaced());

		// set
		use_fast_ = use_fast;
	}

	// get type
	std::string Interp2Fit::get_type(){
		return "rat::mat::interp2fit";
	}

	// method for serialization into json
	void Interp2Fit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// lookup table
		for(arma::uword i=0;i<x_.n_elem;i++)
			js["x"].append(x_(i));
		for(arma::uword i=0;i<y_.n_elem;i++)
			js["y"].append(y_(i));
        js["v"] = rat::cmn::Node::serialize_matrix(v_);

		js["use_fast"] = use_fast_;

		// extrapolation
		js["extrap"] = extrap_;
	}

	// method for deserialisation from json
	void Interp2Fit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// lookup table
		x_.set_size(js["x"].size()); arma::uword idx = 0;
		for(auto it = js["x"].begin();it!=js["x"].end();it++)
			x_(idx++) = (*it).ASFLTP();
		y_.set_size(js["y"].size()); idx = 0;
		for(auto it = js["y"].begin();it!=js["y"].end();it++)
			y_(idx++) = (*it).ASFLTP();
        v_ = rat::cmn::Node::deserialize_matrix(js["v"]);

		// extrapolation
		extrap_ = js["extrap"].asBool();
		use_fast_ = js["use_fast"].asBool();
	}

}} // namespace rat::mat
