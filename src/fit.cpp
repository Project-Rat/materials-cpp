// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// setters
	void Fit::set_is_public(const bool is_public){
		is_public_ = is_public;
	}

	void Fit::set_source(const std::string &source){
		source_ = source;
	}

	void Fit::set_property_str(const std::string &property_str){
		property_str_ = property_str;
	}

	void Fit::set_link(const std::string &link){
		link_ = link;
	}

	void Fit::set_standard(const std::string &standard){
		standard_ = standard;
	}

	void Fit::set_temperature_range(const fltp t1, const fltp t2){
		if(t2<t1)rat_throw_line("upper temperature must be larger than lower temperature");
		set_t1(t1); set_t2(t2);
	}

	void Fit::set_t1(const fltp t1){
		t1_ = t1;
	}

	void Fit::set_t2(const fltp t2){
		t2_ = t2;
	}


	// getters
	bool Fit::get_is_public()const{
		return is_public_;
	}

	std::string Fit::get_source()const{
		return source_;
	}

	std::string Fit::get_property_str()const{
		return property_str_;
	}

	std::string Fit::get_standard()const{
		return standard_;
	}

	std::string Fit::get_link()const{
		return link_;
	}

	fltp Fit::get_t1()const{
		return t1_;
	}

	fltp Fit::get_t2()const{
		return t2_;
	}


	// calculate property as function of scalar temperature
	fltp Fit::calc_property(
		const fltp /*temperature*/) const{
		rat_throw_line("insufficient parameters supplied or scalar property functions not overriden");
		return 0;
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	fltp Fit::calc_property(
		const fltp temperature,
		const fltp /*magnetic_field_magnitude*/) const{
		return calc_property(temperature);
	}

	// calculate property as function of scalar
	// temperature and magnetic field magnitude
	fltp Fit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp /*magnetic_field_angle*/) const{
		return calc_property(temperature, magnetic_field_magnitude);
	}

	// calculate property as function of vector temperature
	arma::Col<fltp> Fit::calc_property(
		const arma::Col<fltp> &/*temperature*/) const{
		rat_throw_line("insufficient parameters supplied or vector property functions not overriden");
		return arma::Col<fltp>{};
	}

	// calculate property as function of vector
	// temperature and magnetic field magnitude
	arma::Col<fltp> Fit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &/*magnetic_field_magnitude*/) const{
		return calc_property(temperature);
	}

	// calculate property as function of vector
	// temperature, magnetic field magnitude and angle
	arma::Col<fltp> Fit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &/*magnetic_field_angle*/) const{
		return calc_property(temperature, magnetic_field_magnitude);
	}


	// serialization
	std::string Fit::get_type(){
		return "rat::mat::fit";
	}

	// method for serialization into json
	void Fit::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		cmn::Node::serialize(js,list);
		js["type"] = get_type();
		js["is_public"] = is_public_;
		js["source"] = source_;
		js["property_str"] = property_str_;
		js["link"] = link_;
		js["standard"] = standard_;

		// temperature range
		js["t1"] = t1_;
		js["t2"] = t2_;
	}

	// method for deserialization from json
	void Fit::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		
		// parent
		cmn::Node::deserialize(js,list,factory_list,pth);
		if(js.isMember("is_public"))is_public_ = js["is_public"].asBool();
		if(js.isMember("source"))source_ = js["source"].asString();
		if(js.isMember("property_str"))property_str_ = js["property_str"].asString();
		if(js.isMember("link"))link_ = js["link"].asString();
		if(js.isMember("standard"))standard_ = js["standard"].asString();

		// temperature range
		set_t1(js["t1"].ASFLTP());
		set_t2(js["t2"].ASFLTP());
	}
	
}}