// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "serializer.hh"

// serializable objects
#include "conductor.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"
#include "seriesconductor.hh"

// wrappers
#include "htscore.hh"
#include "htstape.hh"
#include "ltswire.hh"

// fits
#include "nist8sumfit.hh"
#include "nist8fracfit.hh"
#include "polyfit.hh"
#include "lubellkramerfit.hh"
#include "constfit.hh"
#include "multifit.hh"
#include "godekefit.hh"
#include "fleiterfit.hh"
#include "fleiterbristowfit.hh"
#include "cudifit.hh"
#include "wflfit.hh"
#include "rutherfordcable.hh"
#include "htscore.hh"
#include "anisotropic.hh"
#include "debeyeresistivityfit.hh"
#include "linearfit.hh"
#include "partialinsulation.hh"
#include "superconductor.hh"
#include "rhoedgefit.hh"
#include "lininterpfit.hh"
#include "interp2fit.hh"
#include "interp3fit.hh"
#include "v2o3resistivityfit.hh"
#include "scalefit.hh"

// code specific to Raccoon
namespace rat{namespace mat{
	
	// constructor
	Serializer::Serializer(){
		register_constructors();
	}

	// constructor
	Serializer::Serializer(
		const boost::filesystem::path &fname){
		register_constructors();
		import_json(fname);
	}

	// factory
	ShSerializerPr Serializer::create(){
		return std::make_shared<Serializer>();
	}

	// factory
	ShSerializerPr Serializer::create(
		const boost::filesystem::path &fname){
		return std::make_shared<Serializer>(fname);
	}

	// registry
	void Serializer::register_constructors(){
		// conductors
		register_factory<BaseConductor>();
		register_factory<ParallelConductor>();
		register_factory<SeriesConductor>();

		// HTS core and tape wrappers
		register_factory<HTSCore>();
		register_factory<HTSTape>();

		// LTS wire wrappers
		register_factory<LTSWire>();

		// Fits
		register_factory<Nist8SumFit>();
		register_factory<Nist8FracFit>();
		register_factory<PolyFit>();
		register_factory<LubellKramerFit>();
		register_factory<ConstFit>();
		register_factory<MultiFit>();
		register_factory<GodekeFit>();
		register_factory<FleiterFit>();
		register_factory<FleiterBristowFit>();
		register_factory<CudiFit>();
		register_factory<WFLFit>();
		register_factory<RutherfordCable>();
		register_factory<Anisotropic>();
		register_factory<LinearFit>();
		register_factory<DebeyeResistivityFit>();
		register_factory<PartialInsulation>();
		register_factory<SuperConductor>();
		register_factory<RhoEdgeFit>();
		register_factory<LinInterpFit>();
		register_factory<Interp2Fit>();
		register_factory<Interp3Fit>();
		register_factory<V2O3ResistivityFit>();
		register_factory<ScaleFit>();
	}

}}
