// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "rat/common/extra.hh"

#include "interp3fit.hh"

namespace rat { namespace mat {
	// constructor
	Interp3Fit::Interp3Fit() {}

	// overloaded constructor
	Interp3Fit::Interp3Fit(
		const arma::Col<fltp>& x, 
		const arma::Col<fltp>& y,
		const arma::Col<fltp>& z, 
		const arma::Cube<fltp>& v) {

		set_x(x);
		set_y(y);
		set_z(z);
		set_v(v);

	    // check linspacing for fast interpolator
		if(is_linspaced()) set_use_fast(true);
	}

	// destructor
	// Interp3Fit::~Interp3Fit(){}

	// factory
	ShInterp3FitPr Interp3Fit::create() {
		return std::make_shared<Interp3Fit>();
	}

	ShInterp3FitPr Interp3Fit::create(
		const arma::Col<fltp>& x, 
		const arma::Col<fltp>& y,
		const arma::Col<fltp>& z, 
		const arma::Cube<fltp>& v) {
		return std::make_shared<Interp3Fit>(x,y,z,v);
	}

	fltp Interp3Fit::calc_property(const fltp x, const fltp y, const fltp z) const {
		// return interpolated
		// use slow for single value
		return rat::cmn::Extra::interp3(x_, y_, z_, v_, x, y, z);
	}
	arma::Col<fltp> Interp3Fit::calc_property(const arma::Col<fltp>& x,
		const arma::Col<fltp>& y,
		const arma::Col<fltp>& z) const {

		// allocate
		arma::Col<fltp> vo = arma::Col<fltp>(x.n_elem).fill(0.0);

		// return
		if(use_fast_) {
			rat::cmn::Extra::lininterp3f(x_, y_, z_, v_, x, y, z, vo);
		} else {
			rat::cmn::Extra::interp3(x_, y_, z_, v_, x, y, z, vo);
		}

		return vo;
	}

	// setters
	void Interp3Fit::set_x(const arma::Col<fltp>& x) {
		// set
		x_ = x;
	}

	void Interp3Fit::set_y(const arma::Col<fltp>& y) {
		// set
		y_ = y;
	}
	void Interp3Fit::set_z(const arma::Col<fltp>& z) {
		// set
		z_ = z;
	}


	void Interp3Fit::set_v(const arma::Cube<fltp>& v) {
		// set but check first
		assert(v.n_cols == x_.n_elem);
		assert(v.n_rows == y_.n_elem);
		assert(v.n_slices == z_.n_elem);
		v_ = v;
	}

	bool Interp3Fit::is_linspaced()const{

		// check set data
		assert(!x_.is_empty() && !y_.is_empty() && !z_.is_empty());

		// check each variable is linearly spaced
		arma::Col<fltp> dx = x_.subvec(1, x_.n_elem - 1) - x_.subvec(0, x_.n_elem - 2);
		arma::Col<fltp> dy = y_.subvec(1, y_.n_elem - 1) - y_.subvec(0, y_.n_elem - 2);
		arma::Col<fltp> dz = z_.subvec(1, z_.n_elem - 1) - z_.subvec(0, z_.n_elem - 2);

		if(!x_.is_sorted()) return false;
		if(!y_.is_sorted()) return false;
		if(!z_.is_sorted()) return false;
		if(!arma::all(arma::abs(arma::unique(dx) - dx.max())<1e-9)) return false;
		if(!arma::all(arma::abs(arma::unique(dy) - dy.max())<1e-9)) return false;
		if(!arma::all(arma::abs(arma::unique(dz) - dz.max())<1e-9)) return false;

		// success
		return true;
	}

	void Interp3Fit::set_use_fast(bool use_fast) {
		// check
		if(use_fast) assert(is_linspaced());
		use_fast_ = use_fast;
	}

	// get type
	std::string Interp3Fit::get_type(){
		return "rat::mat::interp3fit";
	}

	// method for serialization into json
	void Interp3Fit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// lookup table
		for(arma::uword i=0;i<x_.n_elem;i++)
			js["x"].append(x_(i));
		for(arma::uword i=0;i<y_.n_elem;i++)
			js["y"].append(y_(i));
		for(arma::uword i=0;i<z_.n_elem;i++)
			js["z"].append(z_(i));
        js["v"] = rat::cmn::Node::serialize_cube(v_);

		js["use_fast"] = use_fast_;

		// extrapolation
		js["extrap"] = extrap_;
	}

	// method for deserialisation from json
	void Interp3Fit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);

		// lookup table
		x_.set_size(js["x"].size()); arma::uword idx = 0;
		for(auto it = js["x"].begin();it!=js["x"].end();it++)
			x_(idx++) = (*it).ASFLTP();
		y_.set_size(js["y"].size()); idx = 0;
		for(auto it = js["y"].begin();it!=js["y"].end();it++)
			y_(idx++) = (*it).ASFLTP();
		z_.set_size(js["z"].size()); idx = 0;
		for(auto it = js["z"].begin();it!=js["z"].end();it++)
			z_(idx++) = (*it).ASFLTP();
        v_ = rat::cmn::Node::deserialize_cube(js["v"]);

		// extrapolation
		extrap_ = js["extrap"].asBool();
		use_fast_ = js["use_fast"].asBool();
	}

}} // namespace rat::mat
