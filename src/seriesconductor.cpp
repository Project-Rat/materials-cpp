// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "seriesconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	SeriesConductor::SeriesConductor(){
		set_name("series conductor");
		abstol_bisection_ = RAT_CONST(1e-7);
	}

	// constructor
	SeriesConductor::SeriesConductor(
		const fltp fraction, ShConductorPr conductor) : SeriesConductor(){
		add_conductor(fraction, conductor);
	}

	// constructor
	SeriesConductor::SeriesConductor(
		const fltp fraction1, ShConductorPr conductor1,
		const fltp fraction2, ShConductorPr conductor2) : SeriesConductor(fraction1,conductor1){
		add_conductor(fraction2, conductor2);
	}

	// constructor
	SeriesConductor::SeriesConductor(
		const FracShConPrList &conductor_list) : SeriesConductor(){
		for(auto it=conductor_list.begin();it!=conductor_list.end();it++)
			add_conductor((*it).first,(*it).second);
	}

	// factory
	ShSeriesConductorPr SeriesConductor::create(){
		return std::make_shared<SeriesConductor>();
	}

	// factory
	ShSeriesConductorPr SeriesConductor::create(
		const fltp fraction, ShConductorPr conductor){
		return std::make_shared<SeriesConductor>(fraction, conductor);
	}

	// factory
	ShSeriesConductorPr SeriesConductor::create(
		const fltp fraction1, ShConductorPr conductor1,
		const fltp fraction2, ShConductorPr conductor2){
		return std::make_shared<SeriesConductor>(fraction1, conductor1, fraction2, conductor2);
	}

	ShSeriesConductorPr SeriesConductor::create(
		const FracShConPrList &conductor_list){
		return std::make_shared<SeriesConductor>(conductor_list);
	}

	// accumulate individual conductors in a list
	// if a series conductor is encountered it is added as whole
	void SeriesConductor::accu_series_conductors(
		FracConPrList &series_conductors, 
		const Direction dir) const{

		// this conductor is parallel so we take our children
		for(auto it = conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// allocate a new list
			FracConPrList series_conductors_child;

			// add conductors to the list
			conductor->accu_series_conductors(series_conductors_child, dir);

			// multiply factions and add conductor to the general list
			for(auto it2 = series_conductors_child.begin();
				it2!=series_conductors_child.end();it2++)(*it2).first *= fraction;

			// combine lists
			series_conductors.insert(
				series_conductors.end(), 
				series_conductors_child.begin(), 
				series_conductors_child.end());
		}
	}

	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	fltp SeriesConductor::calc_specific_heat(
		const fltp temperature) const{

		// calculate density for each conductor
		arma::Col<fltp> density(conductor_list_.size()); 
		arma::Col<fltp> volume_fraction(conductor_list_.size());
		arma::Col<fltp> specific_heat(conductor_list_.size());
		arma::uword cnt=0llu; 
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,cnt++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			density(cnt) = conductor->calc_density(temperature); // kg/m^3
			volume_fraction(cnt) = fcp.first; 
			specific_heat(cnt) = conductor->calc_specific_heat(temperature); // [J kg^-1 K^-1]
		}

		// convert volume fractions to mass fractions
		const arma::Col<fltp> mass_fraction = volume_fraction%density/arma::accu(volume_fraction%density); // unitless

		// add specific heats by mass fraction
		const fltp combined_specific_heat = arma::accu(mass_fraction%specific_heat); // [J kg^-1 K^-1]

		// return values
		return combined_specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	fltp SeriesConductor::calc_thermal_conductivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		fltp thermal_resistivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// calculate thermal conductivity of this layer
			const fltp thermal_conductivity = conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude, dir);

			// accumulate fractional thermal resistivities
			thermal_resistivity += fraction/thermal_conductivity;
		}

		// return values
		return 1.0/thermal_resistivity;
	}

	// electrical resistivity [Ohm m^2]
	fltp SeriesConductor::calc_electrical_resistivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		fltp electrical_resistivity = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional electrical conductivities
			electrical_resistivity += fraction*
				conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude,
				dir);
		}

		// take inverse to get resistivity and return
		return electrical_resistivity;
	}

	// is insulating
	bool SeriesConductor::is_insulator(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(conductor->is_insulator(dir)){
				return true;
			}
		}
		return false;
	}

	// is thermally insulating
	bool SeriesConductor::is_thermal_insulator(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(conductor->is_thermal_insulator(dir)){
				return true;
			}
		}
		return false;
	}

	// is insulating
	bool SeriesConductor::is_superconductor(const Direction dir)const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(!conductor->is_superconductor(dir)){
				return false;
			}
		}
		return true;
	}

	// is anisotropic
	bool SeriesConductor::is_anisotropic()const{
		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			if(conductor->is_anisotropic()){
				return true;
			}
		}
		return false;
	}
	
	// material density [kg m^-3]
	fltp SeriesConductor::calc_density(
		const fltp temperature) const{

		// allocate
		fltp density = 0;

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	fltp SeriesConductor::calc_critical_current_density(
		const fltp /*temperature*/,
		const fltp /*magnetic_field_magnitude*/,
		const fltp /*magnetic_field_angle*/,
		const fltp /*scaling_factor*/,
		const Direction /*dir*/) const{

		// return values
		return 0;
	}

	// calculate power law nvalue
	fltp SeriesConductor::calc_nvalue(
		const fltp /*temperature*/,
		const fltp /*magnetic_field_magnitude*/,
		const fltp /*magnetic_field_angle*/,
		const Direction /*dir*/) const{

		// return values
		return 0;
	}


	// electric field [V/m]
	fltp SeriesConductor::calc_electric_field(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
	
		// get list of series conductors
		FracConPrList series_conductors;
		accu_series_conductors(series_conductors, dir);

		// accumulate electric field
		fltp electric_field = 0;
		for(auto it=series_conductors.begin();it!=series_conductors.end();it++){
			const fltp fraction = (*it).first;
			const Conductor* con = (*it).second;
			electric_field += fraction*con->calc_electric_field(
				current_density,temperature,magnetic_field_magnitude,
				magnetic_field_angle,scaling_factor,dir);
		}

		// return electric field
		return electric_field;

		// calculate field using Ohms Law
		// return current_density*calc_electrical_resistivity(
		// 	temperature, magnetic_field_magnitude, dir);
	}

	// // electric field using pre-calculated electrical 
	// // resistivity en critical current density [V/m]
	// // this uses bisection method
	// fltp SeriesConductor::calc_electric_field_fast(
	// 	const fltp current_density,
	// 	const arma::Row<fltp> &electrical_resistivity_vec,
	// 	const arma::Row<fltp> &critical_current_density_vec,
	// 	const arma::Row<fltp> &nvalue_vec,
	// 	const Direction dir) const{

	// 	// get list of series conductors
	// 	FracConPrList series_conductors;
	// 	accu_series_conductors(series_conductors, dir);

	// 	// accumulate electric field
	// 	fltp electric_field = 0; arma::uword idx = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
	// 		const fltp fraction = (*it).first;
	// 		const Conductor* con = (*it).second;
	// 		electric_field += fraction*con->calc_electric_field_fast(
	// 			current_density, electrical_resistivity_vec.col(idx), 
	// 			critical_current_density_vec.col(idx), 
	// 			nvalue_vec.col(idx), dir);
	// 	}

	// 	// return electric field
	// 	return electric_field;
	// }

	// electric field [V/m]
	fltp SeriesConductor::calc_current_density(
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// current can not run in insulator
		if(is_insulator(dir))return 0;

		// if it is not a superconductor it is linear
		if(!is_superconductor(dir))return electric_field/calc_electrical_resistivity(
			temperature,magnetic_field_magnitude,dir);

		// create a list of series conductors
		FracConPrList series_conductors;

		// add conductors to the list
		accu_series_conductors(series_conductors, dir);
		if(series_conductors.empty())rat_throw_line("there is no conductors");

		// estimate lower bound
		fltp lower_current_density = RAT_CONST(0.0); 
		fltp higher_current_density = RAT_CONST(0.0);

		// walk over conductors
		// this is the field you get by running
		// all current through the least resistive layer
		arma::uword idx = 0;
		for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
			// get conductor
			const fltp fraction = (*it).first;
			const Conductor* conductor = (*it).second;

			// we assume all electric field goes through this material
			const fltp con_electric_field = std::abs(electric_field)/fraction;

			// calculate current density
			const fltp con_current_density = conductor->calc_current_density(
				con_electric_field, 
				temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor, dir);

			// find lowest electric field value
			higher_current_density = std::max(higher_current_density, con_current_density);
		}

		

		// check if already converged
		if(higher_current_density - lower_current_density<abstol_bisection_)
			return rat::cmn::Extra::sign(electric_field)*higher_current_density;

		// allocate midpoint output electric field
		fltp mid_current_density;

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(arma::uword i=0;;i++){
			// update mid point
			mid_current_density = (lower_current_density + higher_current_density)/2;

			// calculate intermeditae value
			const fltp vmid = calc_electric_field(
				mid_current_density, temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor, dir) - 
				std::abs(electric_field);

			// update mid-point
			if(vmid<0)lower_current_density = mid_current_density;
			else higher_current_density = mid_current_density;

			// check convergence
			if((higher_current_density-lower_current_density)<abstol_bisection_)break;
			if((higher_current_density-lower_current_density)/higher_current_density<reltol_bisection_)break;
			
			// limit
			if(i>bisection_max_iter_)rat_throw_line("bisection method does not converge");
		}
		
		// return signed electric field
		return rat::cmn::Extra::sign(electric_field)*mid_current_density;
	}

	// // electric field [V/m]
	// fltp SeriesConductor::calc_current_density(
	// 	const fltp electric_field,
	// 	const fltp temperature,
	// 	const fltp magnetic_field_magnitude,
	// 	const fltp magnetic_field_angle,
	// 	const fltp scaling_factor,
	// 	const Direction dir) const{
		
	// 	// create a list of parallel conductors
	// 	FracConPrList series_conductors;

	// 	// add conductors to the list
	// 	accu_series_conductors(series_conductors, dir);
	// 	if(series_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = series_conductors.size();

	// 	// allocate
	// 	arma::Row<fltp> rho_vec(num_conductors);
	// 	arma::Row<fltp> jc_vec(num_conductors);
	// 	arma::Row<fltp> nvalue_vec(num_conductors);

	// 	// walk over conductors
	// 	arma::uword idx = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_vec(idx)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_vec(idx) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, 
	// 			magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_vec(idx) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, 
	// 			magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_current_density_fast(electric_field, rho_vec, jc_vec, nvalue_vec, dir);
	// }

	// // calculate current density [A m^-2]
	// fltp SeriesConductor::calc_current_density_fast(
	// 	const fltp electric_field,
	// 	const arma::Row<fltp> &electrical_resistivity_vec,
	// 	const arma::Row<fltp> &critical_current_density_vec,
	// 	const arma::Row<fltp> &nvalue_vec,
	// 	const Direction dir) const{

	// 	// create a list of series conductors
	// 	FracConPrList series_conductors;

	// 	// add conductors to the list
	// 	accu_series_conductors(series_conductors, dir);
	// 	if(series_conductors.empty())rat_throw_line("there is no conductors");

	// 	// check input
	// 	if(electrical_resistivity_vec.n_elem!=series_conductors.size())
	// 		rat_throw_line("wrong number of resistivities in table");
	// 	if(critical_current_density_vec.n_elem!=series_conductors.size())
	// 		rat_throw_line("wrong number of critical currents in table");
	// 	if(nvalue_vec.n_elem!=series_conductors.size())
	// 		rat_throw_line("wrong number of nvalues in table");

	// 	// estimate lower bound
	// 	fltp lower_current_density = RAT_CONST(0.0); 
	// 	fltp higher_current_density = RAT_CONST(0.0);

	// 	// walk over conductors
	// 	// this is the field you get by running
	// 	// all current through the least resistive layer
	// 	arma::uword idx = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
	// 		// get conductor
	// 		const fltp fraction = (*it).first;
	// 		const Conductor* conductor = (*it).second;

	// 		// we assume all electric field goes through this material
	// 		const fltp con_electric_field = std::abs(electric_field)/fraction;

	// 		// calculate current density
	// 		const fltp con_current_density = conductor->calc_current_density_fast(
	// 			con_electric_field, 
	// 			electrical_resistivity_vec.col(idx), 
	// 			critical_current_density_vec.col(idx), 
	// 			nvalue_vec.col(idx), dir);

	// 		// find lowest electric field value
	// 		higher_current_density = std::max(higher_current_density, con_current_density);
	// 	}

		

	// 	// check if already converged
	// 	if(higher_current_density - lower_current_density<tol_bisection_)
	// 		return rat::cmn::Extra::sign(electric_field)*higher_current_density;

	// 	// allocate midpoint output electric field
	// 	fltp mid_current_density;

	// 	// start bisection algorithm
	// 	// find an electric field where all the 
	// 	// supplied current density is soaked by the materials
	// 	for(arma::uword i=0;;i++){
	// 		// update mid point
	// 		mid_current_density = (lower_current_density + higher_current_density)/2;

	// 		// calculate intermeditae value
	// 		const fltp vmid = calc_electric_field_fast(
	// 			mid_current_density, electrical_resistivity_vec,
	// 			critical_current_density_vec, nvalue_vec, dir) - 
	// 			std::abs(electric_field);

	// 		// update mid-point
	// 		if(vmid<0)lower_current_density = mid_current_density;
	// 		else higher_current_density = mid_current_density;

	// 		// check convergence
	// 		if((higher_current_density-lower_current_density)<tol_bisection_)break;
			
	// 		// limit
	// 		if(i>bisection_max_iter_)rat_throw_line("bisection method does not converge");
	// 	}
		
	// 	// return signed electric field
	// 	return rat::cmn::Extra::sign(electric_field)*mid_current_density;
	// }


	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<fltp> SeriesConductor::calc_specific_heat(
		const arma::Col<fltp> &temperature) const{

		// calculate density for each conductor
		arma::Mat<fltp> density(temperature.n_elem, conductor_list_.size()); 
		arma::Row<fltp> volume_fraction(conductor_list_.size());
		arma::Mat<fltp> specific_heat(temperature.n_elem, conductor_list_.size());
		arma::uword cnt=0llu; 
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++,cnt++){
			const FracShConPr& fcp = (*it).second;
			const ShConductorPr &conductor = fcp.second;
			density.col(cnt) = conductor->calc_density(temperature); // kg/m^3
			volume_fraction(cnt) = fcp.first; 
			specific_heat.col(cnt) = conductor->calc_specific_heat(temperature); // [J kg^-1 K^-1]
		}

		// convert volume fractions to mass fractions
		const arma::Mat<fltp> mass_fraction = (volume_fraction%density.each_row()).each_col()/arma::sum(volume_fraction%density.each_row(),1); // unitless

		// add specific heats by mass fraction
		const arma::Col<fltp> combined_specific_heat = arma::sum(specific_heat%mass_fraction,1); // [J kg^-1 K^-1]

		// return values
		return combined_specific_heat;
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<fltp> SeriesConductor::calc_thermal_conductivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> thermal_resistivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// calculate conductivity of this layer
			const arma::Col<fltp> thermal_conductivity = 
				conductor->calc_thermal_conductivity(
				temperature, magnetic_field_magnitude, dir);

			// accumulate fractional thermal conductivities
			thermal_resistivity += fraction/thermal_conductivity;
		}

		// return values
		return 1.0/thermal_resistivity;
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<fltp> SeriesConductor::calc_electrical_resistivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		
		// allocate
		arma::Col<fltp> electrical_resistivity(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional electrical conductivities
			electrical_resistivity += fraction*
				conductor->calc_electrical_resistivity(
				temperature, magnetic_field_magnitude, dir);
		}

		// take inverse to get resistivity and return
		return electrical_resistivity;
	}

	// material density [kg m^-3]
	arma::Col<fltp> SeriesConductor::calc_density(
		const arma::Col<fltp> &temperature) const{
		
		// allocate
		arma::Col<fltp> density(temperature.n_elem, arma::fill::zeros);

		// walk over conductors
		for(auto it=conductor_list_.begin();it!=conductor_list_.end();it++){
			// get conductor
			const FracShConPr& fcp = (*it).second;
			const fltp fraction = fcp.first;
			const ShConductorPr &conductor = fcp.second;

			// accumulate fractional densities
			density += fraction*conductor->calc_density(temperature);
		}

		// return values
		return density;
	}

	// critical current density [J m^-2]
	arma::Col<fltp> SeriesConductor::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &/*magnetic_field_magnitude*/,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const arma::Col<fltp> &/*scaling_factor*/,
		const Direction /*dir*/) const{

		// return values
		return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> SeriesConductor::calc_nvalue(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &/*magnetic_field_magnitude*/,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const Direction /*dir*/) const{

		// return values
		return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}


	// electric field calculation [V/m]
	arma::Col<fltp> SeriesConductor::calc_electric_field(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// get list of series conductors
		FracConPrList series_conductors;
		accu_series_conductors(series_conductors, dir);

		// accumulate electric field
		arma::Col<fltp> electric_field(current_density.n_elem, arma::fill::zeros);
		for(auto it=series_conductors.begin();it!=series_conductors.end();it++){
			const fltp fraction = (*it).first;
			const Conductor* con = (*it).second;
			electric_field += fraction*con->calc_electric_field(
				current_density,temperature,magnetic_field_magnitude,
				magnetic_field_angle,scaling_factor,dir);
		}

		// return electric field
		return electric_field;



		// calculate field using Ohms Law
		// return current_density%calc_electrical_resistivity(
		// 	temperature, magnetic_field_magnitude, dir);
	}

	// // electric field [V m^-1] section averaged
	// arma::Col<fltp> SeriesConductor::calc_electric_field_av(
	// 	const arma::Col<fltp> &current_density,
	// 	const arma::Col<fltp> &temperature,
	// 	const arma::Col<fltp> &magnetic_field_magnitude,
	// 	const arma::Col<fltp> &/*magnetic_field_angle*/,
	// 	const arma::Col<fltp> &/*scaling_factor*/,
	// 	const arma::Mat<arma::uword> &nelements,
	// 	const arma::Row<fltp> &cross_areas,
	// 	const Direction dir) const{

	// 	// calculate cross section averaged values
	// 	const arma::Mat<fltp> rho_section = 
	// 		Conductor::nodes2section(calc_electrical_resistivity(
	// 		temperature, magnetic_field_magnitude, dir), nelements, cross_areas);
	// 	const arma::Col<fltp> j_section = 
	// 		Conductor::nodes2section(current_density, nelements, cross_areas);

	// 	// calculate electric field for each section
	// 	const arma::Col<fltp> electric_field_section = rho_section%j_section;

	// 	// back to nodes
	// 	return Conductor::section2nodes(
	// 		electric_field_section, nelements, cross_areas);
	// }

	// // electric field using pre-calculated electrical 
	// // resistivity en critical current density [V/m]
	// arma::Col<fltp> SeriesConductor::calc_electric_field_fast(
	// 	const arma::Col<fltp> &current_density,
	// 	const arma::Mat<fltp> &electrical_resistivity,
	// 	const arma::Mat<fltp> &critical_current_density, 
	// 	const arma::Mat<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// get list of series conductors
	// 	FracConPrList series_conductors;
	// 	accu_series_conductors(series_conductors, dir);

	// 	// accumulate electric field
	// 	arma::Col<fltp> electric_field(current_density.n_elem, arma::fill::zeros); arma::uword idx = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
	// 		const fltp fraction = (*it).first;
	// 		const Conductor* con = (*it).second;
	// 		electric_field += fraction*con->calc_electric_field_fast(
	// 			current_density, electrical_resistivity.col(idx), 
	// 			critical_current_density.col(idx), 
	// 			nvalue.col(idx), dir);
	// 	}

	// 	// return electric field
	// 	return electric_field;


	// 	// // check if column vector supplied
	// 	// assert(electrical_resistivity.n_cols==1);

	// 	// // electric field based on resistivity
	// 	// return electrical_resistivity*current_density;
	// }

	// calculate current density [A m^-2]
	arma::Col<fltp> SeriesConductor::calc_current_density(
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// check insulation
		if(is_insulator(dir))return 0;

		// if it is not a superconductor it is linear
		if(!is_superconductor(dir))return electric_field/calc_electrical_resistivity(
			temperature,magnetic_field_magnitude,dir);

		// create a list of parallel conductors
		FracConPrList series_conductors;

		// add conductors to the list
		accu_series_conductors(series_conductors, dir);
		if(series_conductors.empty())rat_throw_line("there is no conductors");

		// estimate lower bound
		arma::Col<fltp> lower_current_density(electric_field.n_elem,arma::fill::zeros);
		arma::Col<fltp> higher_current_density(electric_field.n_elem,arma::fill::zeros); 
		// higher_electric_field.fill(arma::Datum<fltp>::inf);

		// walk over conductors
		arma::uword idx = 0;
		for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
			// get conductor
			const fltp fraction  = (*it).first; assert(fraction>0);
			const Conductor* conductor = (*it).second;

			// we assume all current runs through this material
			const arma::Col<fltp> con_electric_field = arma::abs(electric_field)/fraction;
			
			// calculate electric field
			const arma::Col<fltp> con_current_density = conductor->calc_current_density(
				con_electric_field, 
				temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor,dir);
			
			// find lowest electric field value
			higher_current_density = arma::max(higher_current_density,con_current_density);
		}

		// calculate current density at these bounds
		// arma::Row<fltp> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma,dir) - J;
		// arma::Row<fltp> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma,dir) - J;

		// allocate midpoint output electric field
		arma::Col<fltp> mid_current_density(electric_field.n_elem);

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(arma::uword j=0;;j++){
			// update mid point
			mid_current_density = (lower_current_density + higher_current_density)/2;

			// calculate intermeditae value
			const arma::Col<fltp> vmid = calc_electric_field(
				mid_current_density,temperature,
				magnetic_field_magnitude,
				magnetic_field_angle,
				scaling_factor,dir) - 
				arma::abs(electric_field);

			// update lower and upper bounds
			const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
			const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

			// update mid-point
			if(!idx1.is_empty())lower_current_density(idx1) = mid_current_density(idx1); 
			if(!idx2.is_empty())higher_current_density(idx2) = mid_current_density(idx2);

			// assert(arma::all(higher_electric_field>=lower_electric_field));

			// std::cout<<higher_electric_field<<std::endl;

			// check convergence
			if(arma::max(higher_current_density-lower_current_density)<abstol_bisection_)break;
			if(arma::max((higher_current_density-lower_current_density)/higher_current_density)<reltol_bisection_)break;

			// throw error if too many iter
			if(j>bisection_max_iter_)rat_throw_line("bisection method does not converge");
		}

		// return signed electric field
		return arma::sign(electric_field)%mid_current_density;
	}


	// // calculate current density [A m^-2]
	// arma::Col<fltp> SeriesConductor::calc_current_density(
	// 	const arma::Col<fltp> &electric_field,
	// 	const arma::Col<fltp> &temperature,
	// 	const arma::Col<fltp> &magnetic_field_magnitude,
	// 	const arma::Col<fltp> &magnetic_field_angle,
	// 	const arma::Col<fltp> &scaling_factor,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList series_conductors;

	// 	// add conductors to the list
	// 	accu_series_conductors(series_conductors, dir);
	// 	if(series_conductors.empty())rat_throw_line("there is no conductors");

	// 	// get number of conductors
	// 	const arma::uword num_conductors = series_conductors.size();

	// 	// allocate
	// 	arma::Mat<fltp> rho_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> jc_mat(temperature.n_elem,num_conductors);
	// 	arma::Mat<fltp> nvalue_mat(temperature.n_elem,num_conductors);

	// 	// walk over conductors
	// 	arma::uword i = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,i++){
	// 		// get conductor
	// 		const Conductor* conductor = (*it).second;

	// 		// calculate electrical resistivity 
	// 		rho_mat.col(i)  = conductor->calc_electrical_resistivity(
	// 			temperature, magnetic_field_magnitude, dir);

	// 		// calculate critical current 
	// 		jc_mat.col(i) = conductor->calc_critical_current_density(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

	// 		// calculate nvalue 
	// 		nvalue_mat.col(i) = conductor->calc_nvalue(
	// 			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);
	// 	}

	// 	// calculate using regular method
	// 	return calc_current_density_fast(electric_field, rho_mat, jc_mat, nvalue_mat, dir);
	// }

	// // current density from electric field 
	// // output in [A m^-2]
	// arma::Col<fltp> SeriesConductor::calc_current_density_fast(
	// 	const arma::Col<fltp> &electric_field,
	// 	const arma::Mat<fltp> &electrical_resistivity,
	// 	const arma::Mat<fltp> &critical_current_density, 
	// 	const arma::Mat<fltp> &nvalue,
	// 	const Direction dir) const{

	// 	// create a list of parallel conductors
	// 	FracConPrList series_conductors;

	// 	// add conductors to the list
	// 	accu_series_conductors(series_conductors, dir);
	// 	if(series_conductors.empty())rat_throw_line("there is no conductors");

	// 	// check sizes
	// 	assert(electrical_resistivity.n_cols==parallel_conductors.size());
	// 	assert(critical_current_density.n_cols==parallel_conductors.size());
	// 	assert(nvalue.n_cols==parallel_conductors.size());
	// 	assert(current_density.n_rows==electrical_resistivity.n_rows);
	// 	assert(current_density.n_rows==critical_current_density.n_rows);
	// 	assert(current_density.n_rows==nvalue.n_rows);

	// 	// estimate lower bound
	// 	arma::Col<fltp> lower_current_density(electric_field.n_elem,arma::fill::zeros);
	// 	arma::Col<fltp> higher_current_density(electric_field.n_elem,arma::fill::zeros); 
	// 	// higher_electric_field.fill(arma::Datum<fltp>::inf);

	// 	// walk over conductors
	// 	arma::uword idx = 0;
	// 	for(auto it=series_conductors.begin();it!=series_conductors.end();it++,idx++){
	// 		// get conductor
	// 		const fltp fraction  = (*it).first; assert(fraction>0);
	// 		const Conductor* conductor = (*it).second;

	// 		// we assume all current runs through this material
	// 		const arma::Col<fltp> con_electric_field = arma::abs(electric_field)/fraction;
			
	// 		// calculate electric field
	// 		const arma::Col<fltp> con_current_density = conductor->calc_current_density_fast(
	// 			con_electric_field, 
	// 			electrical_resistivity.col(idx), 
	// 			critical_current_density.col(idx), 
	// 			nvalue.col(idx), dir);
			
	// 		// find lowest electric field value
	// 		higher_current_density = arma::max(higher_current_density,con_current_density);
	// 	}

	// 	// calculate current density at these bounds
	// 	// arma::Row<fltp> vlow = calc_current_density_fast(Elow,Bm,T,alpha,Jc,sigma,dir) - J;
	// 	// arma::Row<fltp> vhigh = calc_current_density_fast(Ehigh,Bm,T,alpha,Jc,sigma,dir) - J;

	// 	// allocate midpoint output electric field
	// 	arma::Col<fltp> mid_current_density(electric_field.n_elem);

	// 	// start bisection algorithm
	// 	// find an electric field where all the 
	// 	// supplied current density is soaked by the materials
	// 	for(arma::uword j=0;;j++){
	// 		// update mid point
	// 		mid_current_density = (lower_current_density + higher_current_density)/2;

	// 		// calculate intermeditae value
	// 		const arma::Col<fltp> vmid = calc_electric_field_fast(
	// 			mid_current_density,electrical_resistivity,
	// 			critical_current_density,nvalue,dir) - 
	// 			arma::abs(electric_field);

	// 		// update lower and upper bounds
	// 		const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
	// 		const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

	// 		// update mid-point
	// 		if(!idx1.is_empty())lower_current_density(idx1) = mid_current_density(idx1); 
	// 		if(!idx2.is_empty())higher_current_density(idx2) = mid_current_density(idx2);

	// 		assert(arma::all(higher_electric_field>=lower_electric_field));

	// 		// std::cout<<higher_electric_field<<std::endl;

	// 		// check convergence
	// 		if(arma::max(higher_current_density-lower_current_density)<tol_bisection_)break;

	// 		// throw error if too many iter
	// 		if(j>bisection_max_iter_)rat_throw_line("bisection method does not converge");
	// 	}

	// 	// return signed electric field
	// 	return arma::sign(electric_field)%mid_current_density;
	// }


	// serialization type
	std::string SeriesConductor::get_type(){
		return "rat::mat::seriesconductor";
	}

	// serialization
	void SeriesConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Conductor::serialize(js,list);
		ConductorList::serialize(js,list);

		// type
		js["type"] = get_type();
	}

	// deserialization
	void SeriesConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		Conductor::deserialize(js,list,factory_list,pth);
		ConductorList::deserialize(js,list,factory_list,pth);
	}

}}