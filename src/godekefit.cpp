// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "godekefit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	GodekeFit::GodekeFit(){
		set_name("godfit");  set_t1(1); set_t2(20);
	}

	// default constructor
	GodekeFit::GodekeFit(
		const fltp Ca1, const fltp Ca2,
		const fltp eps_0a, const fltp C1,
		const fltp Bc2m, const fltp Tcm,
		const fltp p, const fltp q,
		const fltp eps_ax) : GodekeFit(){
		Ca1_ = Ca1; Ca2_ = Ca2; eps_0a_ = eps_0a;
		C1_ = C1; Bc2m_ = Bc2m; Tcm_ = Tcm; p_ = p; q_ = q; 
		eps_ax_ = eps_ax;
	}

	// factory
	ShGodekeFitPr GodekeFit::create(){
		return std::make_shared<GodekeFit>();
	}

	// factory
	ShGodekeFitPr GodekeFit::create(
		const fltp Ca1, const fltp Ca2,
		const fltp eps_0a, const fltp C1,
		const fltp Bc2m, const fltp Tcm,
		const fltp p, const fltp q,
		const fltp eps_ax){
		return std::make_shared<GodekeFit>(
			Ca1,Ca2,eps_0a,C1,Bc2m,Tcm,p,q,eps_ax);
	}

	// calc critical current without field angle nor magnitude
	fltp GodekeFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	fltp GodekeFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude) const{

		// prevent zero field at which a anomalous behaviour occurs
		const fltp B = std::max(magnetic_field_magnitude,RAT_CONST(0.1));
		const fltp T = std::max(temperature,RAT_CONST(1.0));

		// strain dependence
		const fltp eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const fltp S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const fltp Tc = Tcm_*std::pow(S,(1.0/3));
		const fltp t = T/Tc;
		const fltp Bc2 = Bc2m_*S*(1.0-std::pow(t,1.52));
		const fltp h = B/Bc2;

		// Godeke Equation
		fltp critical_current = 
			C1_*S*(1.0-std::pow(t,1.52))*
			(1.0-std::pow(t,2))*std::pow(h,p_)*
			std::pow(1.0-h,q_)/B;

		// prevent formula from rising again after reaching Bc
		if(B>Bc2 || t>RAT_CONST(1.0))critical_current = 0;
		if(B==0)critical_current = arma::Datum<fltp>::inf;

		// return critical current
		return 1e6*critical_current;
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> GodekeFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<fltp> GodekeFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude) const{

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<fltp> B = arma::clamp(magnetic_field_magnitude,0.01,arma::Datum<fltp>::inf);
		const arma::Col<fltp> T = arma::clamp(temperature,RAT_CONST(1.0),arma::Datum<fltp>::inf);

		// strain dependence
		const fltp eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const fltp S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const fltp Tc = Tcm_*std::pow(S,(1.0/3));
		const arma::Col<fltp> t = T/Tc;
		const arma::Col<fltp> Bc2 = Bc2m_*S*(1.0 - arma::pow(t,1.52));
		const arma::Col<fltp> h = B/Bc2;

		// Godeke Equation
		arma::Col<fltp> critical_current = 
			C1_*S*(1.0-arma::pow(t,1.52))%
			(1.0-arma::pow(t,2))%arma::pow(h,p_)%
			arma::pow(1.0-h,q_)/B;

		// prevent formula from rising again after reaching Bc
		critical_current(arma::find(B>Bc2)).fill(0);
		critical_current(arma::find(B==0)).fill(arma::Datum<fltp>::inf);

		// return critical current
		return 1e6*critical_current;
	}


	

	// get type
	std::string GodekeFit::get_type(){
		return "rat::mat::godekefit";
	}

	// method for serialization into json
	void GodekeFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// serialization type
		js["type"] = get_type();
		
		// fit parameters
		js["Ca1"] = Ca1_;
		js["Ca2"] = Ca2_;
		js["eps_0a"] = eps_0a_;
		js["C1"] = C1_;

		// superconducting parameters
		js["Bc2m"] = Bc2m_;
		js["Tcm"] = Tcm_;

		// magnetic field dependence parameters
		js["p"] = p_;
		js["q"] = q_;

		// environmental parameters
		js["eps_ax"] = eps_ax_; // axial strain
	}

	// method for deserialisation from json
	void GodekeFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		
		// fit parameters
		Ca1_ = js["Ca1"].ASFLTP();
		Ca2_ = js["Ca2"].ASFLTP();
		eps_0a_ = js["eps_0a"].ASFLTP(); // normalized
		C1_ = js["C1"].ASFLTP(); // [kAT mm^-2]

		// superconducting parameters
		Bc2m_ = js["Bc2m"].ASFLTP(); // [T]
		Tcm_ = js["Tcm"].ASFLTP(); // [K]

		// magnetic field dependence parameters
		p_ = js["p"].ASFLTP();
		q_ = js["q"].ASFLTP();

		// environmental parameters
		eps_ax_ = js["eps_ax"].ASFLTP();
	}

}}