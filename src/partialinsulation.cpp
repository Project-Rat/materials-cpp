// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "partialinsulation.hh"

#include "database.hh"

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	PartialInsulation::PartialInsulation(){
		set_name("pi"); set_classification("special");
		conductor_ = Database::stainless316();
	}

	// constructor with input
	PartialInsulation::PartialInsulation(
		const ShBaseConductorPr &base,
		const fltp required_voltage,
		const fltp operating_current,
		const fltp num_turns,
		const fltp turn_length,
		const fltp cable_width,
		const fltp dinsu,
		const fltp temperature,
		const fltp field_magnitude) : PartialInsulation(){
		
		// set to self
		// set_base(base);
		// set_required_voltage(required_voltage);
		// set_operating_current(operating_current);
		// set_num_turns(num_turns);
		// set_turn_length(turn_length);
		// set_cable_width(cable_width);
		// set_dinsu(dinsu);
		// set_temperature(temperature);
		// set_field_magnitude(field_magnitude);

		// set all
		conductor_ = base;
		required_voltage_ = required_voltage;
		operating_current_ = operating_current;
		num_turns_ = num_turns;
		turn_length_ = turn_length;
		cable_width_ = cable_width;
		dinsu_ = dinsu;
		temperature_ = temperature;
		field_magnitude_ = field_magnitude;
		
		// call setup
		setup();
	}

	// factory
	ShPartialInsulationPr PartialInsulation::create(){
		return std::make_shared<PartialInsulation>();
	}
	
	// factory
	ShPartialInsulationPr PartialInsulation::create(
		const ShBaseConductorPr &base,
		const fltp required_voltage,
		const fltp operating_current,
		const fltp num_turns,
		const fltp turn_length,
		const fltp cable_width,
		const fltp dinsu,
		const fltp temperature,
		const fltp field_magnitude){
		return std::make_shared<PartialInsulation>(
			base,required_voltage,operating_current,num_turns,turn_length,
			cable_width,dinsu,temperature,field_magnitude);
	}

	// setters
	void PartialInsulation::set_is_transverse(const bool is_transverse){
		is_transverse_ = is_transverse;
	}

	void PartialInsulation::set_base(const ShBaseConductorPr &base){
		conductor_ = base; setup();
	}

	void PartialInsulation::set_required_voltage(const fltp required_voltage){
		required_voltage_ = required_voltage; setup();
	}

	void PartialInsulation::set_operating_current(const fltp operating_current){
		operating_current_ = operating_current; setup();
	}

	void PartialInsulation::set_num_turns(const fltp num_turns){
		num_turns_ = num_turns; setup();
	}

	void PartialInsulation::set_turn_length(const fltp turn_length){
		turn_length_ = turn_length; setup();
	}

	void PartialInsulation::set_cable_width(const fltp cable_width){
		cable_width_ = cable_width; setup();
	}

	void PartialInsulation::set_dinsu(const fltp dinsu){
		dinsu_ = dinsu; setup();
	}

	void PartialInsulation::set_temperature(const fltp temperature){
		temperature_ = temperature; setup();
	}

	void PartialInsulation::set_field_magnitude(const fltp field_magnitude){
		field_magnitude_ = field_magnitude; setup();
	}


	// getters
	bool PartialInsulation::get_is_transverse()const{
		return is_transverse_;
	}

	ShConductorPr PartialInsulation::get_base() const{
		return conductor_;
	}

	fltp PartialInsulation::get_required_voltage() const{
		return required_voltage_;
	}

	fltp PartialInsulation::get_operating_current() const{
		return operating_current_;
	}

	fltp PartialInsulation::get_num_turns() const{
		return num_turns_;
	}

	fltp PartialInsulation::get_turn_length() const{
		return turn_length_;
	}

	fltp PartialInsulation::get_cable_width() const{
		return cable_width_;
	}

	fltp PartialInsulation::get_dinsu() const{
		return dinsu_;
	}

	fltp PartialInsulation::get_temperature() const{
		return temperature_;
	}

	fltp PartialInsulation::get_field_magnitude() const{
		return field_magnitude_;
	}


	// setup function
	void PartialInsulation::setup(){
		// check validity
		if(!is_valid(false))return;

		// calculate required resitivity
		const fltp rho_req = turn_length_*cable_width_*required_voltage_/(operating_current_*num_turns_*dinsu_);

		// calculate resistivity of the copper
		const fltp rho_base = conductor_->calc_electrical_resistivity(temperature_, field_magnitude_);

		// add layers for thermal conductivity
		const mat::ShConductorPr g10 = Database::g10_normal();

		// G10 with only heat capacity and density
		const mat::ShConductorPr g10_hc = BaseConductor::create(Database::g10_cp(), NULL, NULL, Database::g10_d());

		// stainless electrical resistivity only
		// const mat::ShConductorPr pi_rho_only = 
		// 	mat::BaseConductor::create(NULL,NULL,pi_rho_fit,NULL);

		// combine fractions of materials
		const mat::ShParallelConductorPr partial = 
			mat::ParallelConductor::create({
			{rho_base/rho_req, conductor_},
			{RAT_CONST(1.0), g10}});

		// combine to anisotropic material
		set_longitudinal(g10_hc);
		set_normal(is_transverse_ ? g10 : partial);
		set_transverse(!is_transverse_ ? g10 : partial);
	}

	// check validity
	bool PartialInsulation::is_valid(const bool enable_throws) const{
		// check mesh
		if(!InputConductor::is_valid(enable_throws))return false;
		if(conductor_==NULL){if(enable_throws){rat_throw_line("must have input conductor");} return false;}
		if(cable_width_<=0){if(enable_throws){rat_throw_line("cable width must be larger than zero");} return false;}
		if(turn_length_<=0){if(enable_throws){rat_throw_line("turn length must be larger than zero");} return false;}
		if(required_voltage_<=0){if(enable_throws){rat_throw_line("required voltage must be larger than zero");} return false;}
		if(operating_current_<=0){if(enable_throws){rat_throw_line("operating current must be larger than zero");} return false;}
		if(num_turns_<=0){if(enable_throws){rat_throw_line("num turns must be larger than zero");} return false;}
		if(dinsu_<=0){if(enable_throws){rat_throw_line("insulation thickness must be larger than zero");} return false;}
		if(temperature_<=0){if(enable_throws){rat_throw_line("temperature must be larger than zero");} return false;}
		if(field_magnitude_<0){if(enable_throws){rat_throw_line("field magnitude be larger than or equal to zero");} return false;}
		return true;
	}

	// serialization type
	std::string PartialInsulation::get_type(){
		return "rat::mat::pi";
	}

	// serialization
	void PartialInsulation::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// name
		InputConductor::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// properties
		js["required_voltage"] = required_voltage_;
		js["operating_current"] = operating_current_;
		js["num_turns"] = num_turns_;
		js["turn_length"] = turn_length_;
		js["cable_width"] = cable_width_;
		js["dinsu"] = dinsu_;
		js["temperature"] = temperature_;
		js["field_magnitude"] = field_magnitude_;
		js["is_transverse"] = is_transverse_;
	}

	// deserialization
	void PartialInsulation::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// name
		InputConductor::deserialize(js,list,factory_list,pth);

		// backwards compatibility with v2.011.0
		// remove later when no longer needed
		if(js.isMember("in_plane_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["in_plane_conductor"], list, factory_list, pth);
		if(js.isMember("transverse_conductor"))
			cmn::Node::deserialize_node<Conductor>(js["transverse_conductor"], list, factory_list, pth);
		
		// properties
		is_transverse_ = js["is_transverse"].asBool();
		required_voltage_ = js["required_voltage"].ASFLTP();
		operating_current_ = js["operating_current"].ASFLTP();
		num_turns_ = js["num_turns"].ASFLTP();
		turn_length_ = js["turn_length"].ASFLTP();
		cable_width_ = js["cable_width"].ASFLTP();
		dinsu_ = js["dinsu"].ASFLTP();
		temperature_ = js["temperature"].ASFLTP();
		field_magnitude_ = js["field_magnitude"].ASFLTP();

		// setup as parallel conductor
		setup();
	}

}}