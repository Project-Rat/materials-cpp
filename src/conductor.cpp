// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "conductor.hh"

// common headers
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	Conductor::Conductor(){
		// tolerance for bisection method
		#ifdef RAT_DOUBLE_PRECISION
		abstol_bisection_ = RAT_CONST(1e-9);
		#else
		abstol_bisection_ = RAT_CONST(1e-7); // raccoon doesn't seem to like this value!
		#endif
		reltol_bisection_ = RAT_CONST(1e-6);
		bisection_max_iter_ = 1000llu;
	}

	// setters
	void Conductor::set_classification(
		const std::string &classification){
		classification_ = classification;
	}

	// getters
	std::string Conductor::get_classification() const{
		return classification_;
	}

	// volumetric specific heat [J m^-3 K^-1]
	fltp Conductor::calc_volumetric_specific_heat(
		const fltp temperature) const{

		// calculate density and specific heat
		const fltp d = calc_density(temperature); // kg m^-3
		const fltp sh = calc_specific_heat(temperature); // J kg^-1 K^-1
		
		// muliply to convert to volumetric
		return d*sh;
	}

	// volumetric specific heat [J m^-3 K^-1]
	arma::Col<fltp> Conductor::calc_volumetric_specific_heat(
		const arma::Col<fltp> &temperature) const{

		// calculate density and specific heat
		const arma::Col<fltp> d = calc_density(temperature); // kg m^-3
		const arma::Col<fltp> sh = calc_specific_heat(temperature); // J kg^-1 K^-1

		// muliply to convert to volumetric
		return d%sh;
	}

	// accumulation method
	void Conductor::accu_parallel_conductors(
		FracConPrList &parallel_conductors,
		const Direction /*dir*/) const{
		parallel_conductors.push_back(FracConPr(RAT_CONST(1.0), this));
	}

	// accumulation method for series conductors
	void Conductor::accu_series_conductors(
		FracConPrList &series_conductors,
		const Direction /*dir*/) const{
		series_conductors.push_back(FracConPr(RAT_CONST(1.0), this));
	}

	// set bisection tolerance
	void Conductor::set_abstol_bisection(const fltp abstol_bisection){
		if(abstol_bisection<=0)rat_throw_line("tolerance must be positive");
		abstol_bisection_ = abstol_bisection;
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Col<fltp> Conductor::calc_percent_load(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature, 
		const arma::Col<fltp> &magnetic_field_magnitude, 
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir,
		const fltp current_density_increment,
		const fltp tol_bisection) const{

		// if not a superconductor return zeros instead
		if(!is_superconductor(dir)){
			return arma::Col<fltp>(current_density.n_elem, arma::fill::zeros);
		}

		// get number of elements
		const arma::uword num_elements = current_density.n_elem;

		// slope of load line [T/A]
		arma::Col<fltp> load_line_slope = 
			magnetic_field_magnitude/arma::abs(current_density);
		load_line_slope(arma::find(current_density==0)).fill(RAT_CONST(0.0));

		// find upper critical field
		arma::Col<fltp> upper_current_density(num_elements, arma::fill::zeros);

		// search roughly the critical surface
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<fltp> critical_current_density = calc_critical_current_density(
				temperature, load_line_slope%upper_current_density, magnetic_field_angle, scaling_factor, dir);

			// find indexes of elements below Jc
			const arma::Col<arma::uword> idx = arma::find(
				critical_current_density>upper_current_density);
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_current_density(idx) += current_density_increment;
		}

		// find current density below critical current
		arma::Col<fltp> lower_current_density = arma::clamp(
			upper_current_density - current_density_increment, 
			RAT_CONST(0.0), arma::Datum<rat::fltp>::inf);

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<fltp> intermediate_current_density = 
				(upper_current_density + lower_current_density)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density(
				temperature, load_line_slope%intermediate_current_density, magnetic_field_angle, scaling_factor, dir);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<intermediate_current_density);
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=intermediate_current_density);

			// update temperatures
			upper_current_density(idx1) = intermediate_current_density(idx1);
			lower_current_density(idx2) = intermediate_current_density(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_current_density - 
				upper_current_density)<tol_bisection))break;
		}

		// calculate percentage on loadline
		return 100*arma::abs(current_density)/upper_current_density;
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Col<fltp> Conductor::calc_percent_load_av(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature, 
		const arma::Col<fltp> &magnetic_field_magnitude, 
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir,
		const fltp current_density_increment,
		const fltp tol_bisection) const{

		// if not a superconductor return zeros instead
		if(!is_superconductor(dir)){
			return arma::Col<fltp>(current_density.n_elem, arma::fill::zeros);
		}
		
		// get number of elements
		const arma::uword num_elements = current_density.n_elem;

		// slope of load line [T/A]
		arma::Col<fltp> load_line_slope = 
			magnetic_field_magnitude/arma::abs(current_density);
		load_line_slope(arma::find(current_density==0)).fill(RAT_CONST(0.0));

		// find upper critical field
		arma::Col<fltp> upper_current_density(num_elements, arma::fill::zeros);

		// search roughly the critical surface
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<fltp> critical_current_density = calc_critical_current_density_av(
				temperature, load_line_slope%upper_current_density, 
				magnetic_field_angle, scaling_factor, 
				nelements, cross_num_nodes, cross_areas, dir);

			// find indexes of elements below Jc
			const arma::Col<arma::uword> idx = arma::find(
				critical_current_density>upper_current_density);
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_current_density(idx) += current_density_increment;
		}

		// find current density below critical current
		arma::Col<fltp> lower_current_density = arma::clamp(
			upper_current_density - current_density_increment,
			RAT_CONST(0.0), arma::Datum<rat::fltp>::inf);

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<fltp> intermediate_current_density = 
				(upper_current_density + lower_current_density)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density_av(
				temperature, load_line_slope%intermediate_current_density, 
				magnetic_field_angle, scaling_factor, 
				nelements, cross_num_nodes, cross_areas, dir);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<intermediate_current_density);
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=intermediate_current_density);

			// update temperatures
			upper_current_density(idx1) = intermediate_current_density(idx1);
			lower_current_density(idx2) = intermediate_current_density(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_current_density - 
				upper_current_density)<tol_bisection))break;
		}

		// calculate percentage on loadline
		return 100*arma::abs(current_density)/upper_current_density;
	}


	// electric field [V m^-1] section averaged
	arma::Col<fltp> Conductor::calc_electric_field_av(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir) const{

		// // calculate area of section
		// // const fltp section_area = arma::accu(cross_areas);
		// if(nelements.n_cols==1){
		// 	return calc_electric_field(current_density, temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
		// }

		// calculate cross section averaged values
		const arma::Col<fltp> current_density_section = arma::abs(nodes2section(current_density, nelements, cross_num_nodes, cross_areas));

		// number of sections
		const arma::uword num_sections = current_density_section.n_elem;

		// allocate electric field for each section
		arma::Col<fltp> lower_electric_field(num_sections,arma::fill::zeros);
		arma::Col<fltp> higher_electric_field(num_sections,arma::fill::value(100e-6)); 

		// iteratively find higher electric field
		// for(arma::uword j=0;;j++){
		for(;;){
			// calculate intermeditae value
			const arma::Col<fltp> val = nodes2section(calc_current_density(
				section2nodes(higher_electric_field, nelements, cross_num_nodes, cross_areas), temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir), nelements, cross_num_nodes, cross_areas) - current_density_section;

			// find sections that do not yet conduct sufficient current
			const arma::Col<arma::uword> idx = arma::find(val<0);

			// check if all sections are done
			if(idx.empty())break;

			// move boundaries
			lower_electric_field(idx) = higher_electric_field(idx); higher_electric_field(idx)*=2;
		}

		// allocate midpoint output electric field
		arma::Col<fltp> mid_electric_field(num_sections);

		// start bisection algorithm
		// find an electric field where all the 
		// supplied current density is soaked by the materials
		for(arma::uword j=0;;j++){
			// update mid point
			mid_electric_field = (lower_electric_field + higher_electric_field)/2;

			// calculate intermeditae value
			const arma::Col<fltp> vmid = nodes2section(calc_current_density(
				section2nodes(mid_electric_field, nelements, cross_num_nodes, cross_areas), temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir), nelements, cross_num_nodes, cross_areas) - current_density_section;

			// update lower and upper bounds
			const arma::Col<arma::uword> idx1 = arma::find(vmid<0);
			const arma::Col<arma::uword> idx2 = arma::find(vmid>=0);

			// update mid-point
			if(!idx1.is_empty())lower_electric_field(idx1) = mid_electric_field(idx1); 
			if(!idx2.is_empty())higher_electric_field(idx2) = mid_electric_field(idx2);

			assert(arma::all(higher_electric_field>=lower_electric_field));

			// // std::cout<<higher_electric_field<<std::endl;
			// static std::mutex lock;
			// lock.lock();
			// std::cout<<arma::max(current_density)<<std::endl;
			// std::cout<<__LINE__<<" "<<__FILE__<<" "<<j<<std::endl;
			// std::cout<<arma::join_horiz(lower_electric_field,higher_electric_field,higher_electric_field-lower_electric_field)<<std::endl;
			// lock.unlock();

			// check convergence
			if(arma::max(higher_electric_field-lower_electric_field)<abstol_bisection_)break;
			if(arma::max((higher_electric_field-lower_electric_field)/higher_electric_field)<reltol_bisection_)break;

			// throw error if too many iter
			if(j>bisection_max_iter_)rat_throw_line("bisection method does not converge");
		}

		// return signed electric field
		return arma::sign(current_density)%section2nodes(mid_electric_field, nelements, cross_num_nodes, cross_areas);
		// return section2nodes(nodes2section(calc_electric_field(current_density, temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir), nelements, cross_num_nodes, cross_areas), nelements, cross_num_nodes, cross_areas);
	}

	// default critical current is zero (not superconducting)
	fltp Conductor::calc_cs_temperature(
		const fltp current_density, 
		const fltp magnetic_field_magnitude, 
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir,
		const fltp temperature_increment,
		const fltp tol_bisection) const{

		// allocate critical temperature
		fltp upper_crit_temperature = temperature_increment;

		// clamp current density
		const fltp J = std::max(std::abs(current_density),RAT_CONST(1e-6));

		// check if it is a superconductor
		const fltp Jc0 = calc_critical_current_density(
			0, magnetic_field_magnitude, magnetic_field_angle, scaling_factor);
		if(Jc0==0)return 0;

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const fltp critical_current_density = calc_critical_current_density(
				upper_crit_temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

			// increment temperature 
			if(critical_current_density>J)
				upper_crit_temperature += temperature_increment;

			// until no margin left
			else break;
		}

		// remove last increment
		fltp lower_crit_temperature = upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const fltp intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const fltp crit_current_density = calc_critical_current_density(
				intermediate_temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir);

			// update temperatures
			if(crit_current_density<J)
				upper_crit_temperature = intermediate_temperature;
			else lower_crit_temperature = intermediate_temperature;

			// stop condiction
			if(std::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection)break;
		}

		// return critical temp
		return upper_crit_temperature;
	}

	// calculate the upper critical field
	fltp Conductor::calc_upper_critical_field(
		const fltp temperature,
		const fltp current_density, 
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir,
		const fltp field_increment,
		const fltp tol_bisection) const{

		// allocate critical temperature
		fltp upper_crit_field = field_increment;

		// clamp current density
		const fltp J = std::max(std::abs(current_density),RAT_CONST(1e-6));

		// check if it is a superconductor at this tempertaure
		const fltp Jc0 = calc_critical_current_density(
			temperature, RAT_CONST(0.0), magnetic_field_angle, scaling_factor);
		if(Jc0==RAT_CONST(0.0))return RAT_CONST(0.0);

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const fltp critical_current_density = calc_critical_current_density(
				temperature, upper_crit_field, magnetic_field_angle, scaling_factor, dir);

			// increment temperature 
			if(critical_current_density>J)
				upper_crit_field += field_increment;

			// until no margin left
			else break;
		}

		// remove last increment
		fltp lower_crit_field = upper_crit_field - field_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const fltp intermediate_field = 
				(upper_crit_field + lower_crit_field)/2;

			// calculate critical current in [A/m^2]
			const fltp crit_current_density = calc_critical_current_density(
				temperature, intermediate_field, 
				magnetic_field_angle, scaling_factor, dir);

			// update temperatures
			if(crit_current_density<J)
				upper_crit_field = intermediate_field;
			else lower_crit_field = intermediate_field;

			// stop condiction
			if(std::abs(lower_crit_field - 
				upper_crit_field)<tol_bisection)break;
		}

		// return critical temp
		return upper_crit_field;
	}

	// calculate current sharing temperature
	arma::Col<fltp> Conductor::calc_cs_temperature(
		const arma::Col<fltp> &current_density, 
		const arma::Col<fltp> &magnetic_field_magnitude, 
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir,
		const fltp temperature_increment,
		const fltp tol_bisection) const{

		// clamp current density
		const arma::Col<fltp> J = arma::clamp(arma::abs(current_density),1e-6,arma::Datum<fltp>::inf);

		// allocate critical temperature
		arma::Col<fltp> upper_crit_temperature(J.n_elem); 
		upper_crit_temperature.fill(temperature_increment);

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density(
				upper_crit_temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

			// find indexes of elements below Jc
			const arma::Col<arma::uword> idx = arma::find(
				crit_current_density>arma::abs(J));
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_crit_temperature(idx) += temperature_increment;
		}

		// remove last increment
		arma::Col<fltp> lower_crit_temperature = 
			upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<fltp> intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density(
				intermediate_temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, dir);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<arma::abs(J));
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=arma::abs(J));

			// update temperatures
			upper_crit_temperature(idx1) = intermediate_temperature(idx1);
			lower_crit_temperature(idx2) = intermediate_temperature(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection))break;
		}

		// return critical temp
		return upper_crit_temperature;
	}



	// analysis tools for scalar input
	// calcultae temperature increase due to added heat
	fltp Conductor::calc_temperature_increase(
		const fltp initial_temperature, 
		const fltp volume, 
		const fltp added_heat,
		const fltp delta_temperature) const{

		// check no heat case
		if(added_heat==0)return 0;

		// set temperature
		fltp final_temperature = initial_temperature;
		fltp vol_heat_remain = added_heat/volume; // [J m^-3]

		// perform integration
		for(;;){
			// calculate specific heat
			const fltp vsh = calc_volumetric_specific_heat(
				final_temperature + delta_temperature/2);

			// normal temperature step
			if(vsh*delta_temperature < vol_heat_remain){
				final_temperature += delta_temperature;
				vol_heat_remain -= vsh*delta_temperature;
			}

			// final temperature step
			else{
				const fltp dT = vol_heat_remain/vsh;
				final_temperature += dT;
				vol_heat_remain -= vsh*dT;
				break;
			}
		}

		// return temperature
		return final_temperature - initial_temperature;
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	fltp Conductor::calc_thermal_energy_density(
		const fltp temperature, 
		const fltp delta_temperature) const{

		// calculate number of steps
		const arma::uword num_steps = std::min(std::max(static_cast<int>(temperature/delta_temperature),2),1000);

		// create temperature array from zero to temperature
		const arma::Col<fltp> tt = arma::linspace<arma::Col<fltp> >(RAT_CONST(0.0),temperature,num_steps);

		// calculate specific heat [J m^-3 K^-1]
		const arma::Col<fltp> vsh = calc_volumetric_specific_heat(tt);

		// integrate over temperature to get energy
		return arma::as_scalar(arma::trapz(tt, vsh));
	}

	// calculate the energy density based on the temperature
	// energy density is given in [J m^-3]
	arma::Col<fltp> Conductor::calc_thermal_energy_density(
		const arma::Col<fltp> &temperature, 
		const fltp delta_temperature) const{

		// calculate number of steps
		const arma::uword num_steps = std::min(
			std::max(static_cast<int>(temperature.max()/delta_temperature),2),1000);

		// create temperature array from zero to temperature
		const arma::Col<fltp> tt = arma::linspace<arma::Col<fltp> >(RAT_CONST(0.0),temperature.max(),num_steps);

		// calculate specific heat [J m^-3 K^-1]
		const arma::Col<fltp> vsh = calc_volumetric_specific_heat(tt);

		// cumtrapz
		const arma::Col<fltp> edi = arma::join_vert(arma::Col<fltp>{0},
			arma::cumsum(arma::diff(tt)%(vsh.head_rows(num_steps-1) + vsh.tail_rows(num_steps-1))/2));

		// interpolate
		arma::Col<fltp> energy_density;
		cmn::Extra::lininterp1f(tt,edi,temperature,energy_density,true);

		// integrate over temperature to get energy
		return energy_density;
	}

	// calculate the length of the minimal propagation zone in [m]
	fltp Conductor::calc_theoretical_lmpz(
		const fltp current_density,
		const fltp operating_temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		
		// // calculate the critical current 
		// const fltp jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// // check if it is a superconductor
		// if(jc==0)return 0.0;

		// current sharing temperature
		const fltp cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// critical temperature (at zero current)
		const fltp crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// transition temperature between Tc and Tcs
		const fltp transition_temperature = (cs_temperature + crit_temperature)/2;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const fltp nzp_temperature = (transition_temperature + operating_temperature)/2;


		// calculate resistivity
		const fltp rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// calculate thermal conductivity
		const fltp k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// calculate length of the minimal propagation zone
		const fltp lmpz = std::sqrt(2*k*(transition_temperature - 
			operating_temperature)/(current_density*current_density*rho));

		// return length
		return lmpz;
	}

	// calculate minimal quench energy in [J]
	fltp Conductor::calc_theoretical_mqe(
		const fltp current_density,
		const fltp operating_temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir, 
		const fltp cross_sectional_area) const{

		// // calculate the critical current 
		// const fltp jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// // check if it is a superconductor
		// if(jc==0)return 0.0;

		// current sharing temperature
		const fltp cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// critical temperature (at zero current)
		const fltp crit_temperature = calc_cs_temperature(
			RAT_CONST(0.0), magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// transition temperature between Tc and Tcs
		const fltp transition_temperature = (cs_temperature + crit_temperature)/2;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const fltp nzp_temperature = (transition_temperature + operating_temperature)/2;

		// calculate resistivity
		const fltp rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// calculate thermal conductivity
		const fltp k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// calculate length of the minimal propagation zone
		const fltp lmpz = std::sqrt(2*k*(transition_temperature - 
			operating_temperature)/(current_density*current_density*rho));

		// create temperature range for integration
		const arma::Col<fltp>::fixed<2> temperature_range{operating_temperature,transition_temperature};

		// calculate MQE
		const fltp mqe = lmpz*cross_sectional_area*arma::as_scalar(
			arma::diff(calc_thermal_energy_density(temperature_range)));

		// return length
		return mqe;
	}

	// calculate the normal zone propagation velocity in [m s^-1]
	fltp Conductor::calc_theoretical_vnzp(
		const fltp current_density,
		const fltp operating_temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{
		
		// calculate the critical current 
		// const fltp jc = calc_critical_current_density(
		// 	operating_temperature, magnetic_field_magnitude, magnetic_field_angle);

		// current sharing temperature
		const fltp cs_temperature = calc_cs_temperature(
			current_density, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// critical temperature (at zero current)
		const fltp crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// transition temperature between Tc and Tcs
		fltp ff = RAT_CONST(0.5);
		const fltp transition_temperature = ff*cs_temperature + (RAT_CONST(1.0)-ff)*crit_temperature;

		// check if it is a superconductor
		if(transition_temperature<operating_temperature)return 0.0;

		// material temperature
		const fltp nzp_temperature = (transition_temperature + operating_temperature)/2;

		// calculate resistivity [Ohm m] = [W A^-2 m]
		const fltp rho = calc_electrical_resistivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// calculate thermal conductivity [J s^-1 m^-1 K^-1]
		const fltp k = calc_thermal_conductivity(
			nzp_temperature, magnetic_field_magnitude, dir);

		// claculate heat capacity [J m^-3 K^-1]
		const fltp sh = arma::as_scalar(arma::mean(calc_volumetric_specific_heat(
			arma::linspace<arma::Col<fltp> >(operating_temperature, transition_temperature))));

		// normal zone velocity [m/s]
		const fltp vnzp = (std::abs(current_density)/sh)*
			std::sqrt((rho*k)/(transition_temperature-operating_temperature));

		// return velocity
		return vnzp;
	}

	// calculate time for adiabatic conductor 
	// to reach specified temperature
	// superconductors are assumed to start at Tc
	fltp Conductor::calc_adiabatic_time(
		const fltp max_temperature,
		const fltp current_density,
		const fltp operating_temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir,
		const fltp max_temperature_step) const{

		// check 
		if(max_temperature<operating_temperature)
			return 0;

		// critical temperature (at zero current)
		const fltp crit_temperature = calc_cs_temperature(
			0, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// set start condition
		fltp temperature = std::max(operating_temperature, crit_temperature);
		fltp time = 0;

		// integrate
		while(temperature<max_temperature){
			// calculate electric field
			const fltp electric_field = calc_electric_field(
				current_density, temperature, 
				magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir); // [V/m]

			// calculate heat capacity
			const fltp volumetric_specific_heat = calc_volumetric_specific_heat(temperature); //[J/m^3/K]

			// calculate power density [J/s/m^3]
			const fltp power_density = electric_field*current_density; 

			// calculate change [K/s]
			const fltp dtemperature_dt = power_density/volumetric_specific_heat;

			// calculate required temperature step
			const fltp temperature_step = std::min(
				max_temperature_step, max_temperature-temperature);
			
			// take maximum temperature step
			const fltp dt = temperature_step/dtemperature_dt;
			time += dt; temperature += dt*dtemperature_dt;
		}

		// return time
		return time;
	}

	// calculate partial derivative of electric field 
	// with temperature using finite difference method
	arma::Col<fltp> Conductor::calc_electric_field_dT(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir,
		const fltp delta_temperature) const{

		// calculate electric field
		const arma::Col<fltp> electric_field1 = calc_electric_field(
			current_density, 
			temperature-delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle,
			scaling_factor,
			dir); // [V m^-1]

		// calculate electric field
		const arma::Col<fltp> electric_field2 = calc_electric_field(
			current_density, 
			temperature+delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle,
			scaling_factor,
			dir); // [V m^-1]

		// finite difference
		return (electric_field2 - electric_field1)/delta_temperature;
	}

	// calculate partial derivative of electric field 
	// with temperature using finite difference method
	fltp Conductor::calc_electric_field_dT(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir,
		const fltp delta_temperature) const{

		// calculate electric field
		const fltp electric_field1 = calc_electric_field(
			current_density, 
			temperature-delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle,
			scaling_factor,
			dir); // [V m^-1]

		// calculate electric field
		const fltp electric_field2 = calc_electric_field(
			current_density, 
			temperature+delta_temperature/2, 
			magnetic_field_magnitude,
			magnetic_field_angle,
			scaling_factor,
			dir); // [V m^-1]

		// finite difference
		return (electric_field2 - electric_field1)/delta_temperature;
	}


	// function for calculating section averaged properties
	arma::Mat<fltp> Conductor::nodes2section(
		const arma::Mat<fltp> &node_values, // stored row wise
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas){

		// get counters
		// const arma::uword num_nodes_total = node_values.n_rows;
		// const arma::uword num_nodes_cross = nelements.max() + 1;
		// const arma::uword num_elements = nelements.n_cols;

		// check input
		assert(node_values.n_rows%cross_num_nodes==0);
		assert(cross_areas.n_elem==nelements.n_cols);

		// number of cable intersections
		const arma::uword num_elements_cross = nelements.n_cols;
		const arma::uword num_cis = node_values.n_rows/cross_num_nodes;
		
		// allocate for cross sections
		arma::Mat<fltp> cross_values(num_cis, node_values.n_cols, arma::fill::zeros);

		// add areas
		const fltp total_area = arma::accu(cross_areas);

		// walk over sections
		for(arma::uword i=0;i<num_cis;i++){
			// extract section J at nodes
			const arma::Mat<fltp> cross_values_nodes = node_values.rows(i*cross_num_nodes, (i+1)*cross_num_nodes-1);

			// walk over elements in section
			for(arma::uword j=0;j<num_elements_cross;j++){
				// get average between connected nodes
				const arma::Row<fltp> element_value = arma::mean(cross_values_nodes.rows(nelements.col(j)),0);
				
				// add to cross values
				cross_values.row(i) += cross_areas(j)*element_value/total_area;
			}
		}

		// return section values
		return cross_values;
	}

	// function for interpolating sections to nodes
	// function for calculating section averaged properties
	arma::Mat<fltp> Conductor::section2nodes(
		const arma::Mat<fltp> &section_values,
		const arma::Mat<arma::uword> &/*nelements*/,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &/*cross_areas*/){

		// get counters
		// const arma::uword num_nodes_cross = nelements.n_cols;
		const arma::uword num_cis = section_values.n_rows;
		const arma::uword num_nodes = cross_num_nodes*num_cis;
		// const arma::uword num_elements = nelements.n_cols;

		// send to nodes
		arma::Mat<fltp> node_values(num_nodes, section_values.n_cols);

		// walk over sections
		for(arma::uword i=0;i<num_cis;i++)
			for(arma::uword j=i*cross_num_nodes;j<=(i+1)*cross_num_nodes-1;j++)
				node_values.row(j) = section_values.row(i);

		// return node values
		return node_values;
	}

	// critical current density averaged over cross section [J m^-2]
	arma::Col<fltp> Conductor::calc_critical_current_density_av(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir) const{

		// calculate critical current normally
		const arma::Col<fltp> jc = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// divide over cross section
		return section2nodes(
			nodes2section(jc, nelements, cross_num_nodes, cross_areas), 
			nelements, cross_num_nodes, cross_areas);
	}

	// critical temperature calculation in [A m^-2]
	// using averaged critical current over cross area
	arma::Col<fltp> Conductor::calc_cs_temperature_av(
		const arma::Col<fltp> &current_density, 
		const arma::Col<fltp> &magnetic_field_magnitude, 
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir,
		const fltp temperature_increment,
		const fltp tol_bisection) const{

		// clamp current density
		const arma::Col<fltp> J = arma::clamp(arma::abs(current_density),1e-6,arma::Datum<fltp>::inf);

		// allocate critical temperature
		arma::Col<fltp> upper_crit_temperature(J.n_elem); 
		upper_crit_temperature.fill(temperature_increment);

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density_av(
				upper_crit_temperature, magnetic_field_magnitude,
				magnetic_field_angle, scaling_factor, nelements, 
				cross_num_nodes, cross_areas, dir);

			// find indexes of elements below Jc
			const arma::Row<arma::uword> idx = arma::find(
				crit_current_density>arma::abs(J)).t();
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			upper_crit_temperature(idx) += temperature_increment;
		}

		// remove last increment
		arma::Col<fltp> lower_crit_temperature = 
			upper_crit_temperature - temperature_increment;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Col<fltp> intermediate_temperature = 
				(upper_crit_temperature + lower_crit_temperature)/2;

			// calculate critical current in [A/m^2]
			const arma::Col<fltp> crit_current_density = calc_critical_current_density_av(
				intermediate_temperature, magnetic_field_magnitude, 
				magnetic_field_angle, scaling_factor, nelements, 
				cross_num_nodes, cross_areas, dir);

			// find indexes for which to shift upper/lower temperature
			const arma::Col<arma::uword> idx1 = arma::find(
				crit_current_density<arma::abs(J));
			const arma::Col<arma::uword> idx2 = arma::find(
				crit_current_density>=arma::abs(J));

			// update temperatures
			upper_crit_temperature(idx1) = intermediate_temperature(idx1);
			lower_crit_temperature(idx2) = intermediate_temperature(idx2);

			// stop condiction
			if(arma::all(arma::abs(lower_crit_temperature - 
				upper_crit_temperature)<tol_bisection))break;
		}

		// return critical temp
		return upper_crit_temperature;
	}

	// matrix input wrappers
	// calculate specific heat [J kg^-1 K^-1]
	arma::Mat<fltp> Conductor::calc_specific_heat_mat(
		const arma::Mat<fltp> &temperature) const{
		const arma::Col<fltp> sh = calc_specific_heat(
			arma::vectorise(temperature));
		assert(sh.n_elem==temperature.n_elem);
		return arma::reshape(sh, temperature.n_rows,temperature.n_cols);
	}
	
	// thermal conductivity [W m^-1 K^-1]
	arma::Mat<fltp> Conductor::calc_thermal_conductivity_mat(
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const Direction dir) const{
		
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		
		const arma::Col<fltp> k = calc_thermal_conductivity(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			dir);

		// convert to matrix and return
		assert(k.n_elem==temperature.n_elem);
		return arma::reshape(k,	temperature.n_rows, temperature.n_cols);
	} 
	
	// electrical resistivity [Ohm m]
	arma::Mat<fltp> Conductor::calc_electrical_resistivity_mat(
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const Direction dir) const{

		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		
		const arma::Col<fltp> rho = calc_electrical_resistivity(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),dir);

		// convert to matrix and return
		assert(rho.n_elem==temperature.n_elem);
		return arma::reshape(rho, temperature.n_rows,temperature.n_cols);
	}

	// critical current density [J m^-2]
	arma::Mat<fltp> Conductor::calc_critical_current_density_mat(
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const Direction dir) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==scaling_factor.n_cols);
		assert(temperature.n_rows==scaling_factor.n_rows);
		const arma::Col<fltp> jc = calc_critical_current_density(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			dir);

		// convert to matrix and return
		assert(jc.n_elem == temperature.n_elem);
		return arma::reshape(jc, temperature.n_rows,temperature.n_cols);
	}

	// critical current density [J m^-2]
	arma::Mat<fltp> Conductor::calc_nvalue_mat(
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const Direction dir) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		const arma::Col<fltp> N = calc_nvalue(
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			dir);

		// convert to matrix and return
		assert(N.n_elem == temperature.n_elem);
		return arma::reshape(N, temperature.n_rows,temperature.n_cols);
	}

	// material density [kg m^-3]
	arma::Mat<fltp> Conductor::calc_density_mat(
		const arma::Mat<fltp> &temperature) const{
		const arma::Col<fltp> d = calc_density(
			arma::vectorise(temperature));
		assert(d.n_elem==temperature.n_elem);

		// convert to matrix and return
		return arma::reshape(d, temperature.n_rows,temperature.n_cols);
	}

	// volumetric specific heat [J m^-3 K^-1]
	arma::Mat<fltp> Conductor::calc_volumetric_specific_heat_mat(
		const arma::Mat<fltp> &temperature) const{
		const arma::Col<fltp> vsh = calc_volumetric_specific_heat(
			arma::vectorise(temperature));

		// convert to matrix and return
		assert(vsh.n_elem==temperature.n_elem);
		return arma::reshape(vsh, temperature.n_rows,temperature.n_cols);
	}

	// electric field [V m^-1]
	arma::Mat<fltp> Conductor::calc_electric_field_mat(
		const arma::Mat<fltp> &current_density,
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const Direction dir) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==current_density.n_cols);
		assert(temperature.n_rows==current_density.n_rows);
		assert(temperature.n_cols==scaling_factor.n_cols);
		assert(temperature.n_rows==scaling_factor.n_rows);
		const arma::Col<fltp> E = calc_electric_field(
			arma::vectorise(current_density),
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			dir);
		

		// convert to matrix and return
		assert(E.n_elem==temperature.n_elem);
		return arma::reshape(E, temperature.n_rows,temperature.n_cols);
	}

	// calculate current density [A m^-2]
	arma::Mat<fltp> Conductor::calc_current_density_mat(
		const arma::Mat<fltp> &electric_field,
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const Direction dir) const{
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==electric_field.n_cols);
		assert(temperature.n_rows==electric_field.n_rows);
		assert(temperature.n_cols==scaling_factor.n_cols);
		assert(temperature.n_rows==scaling_factor.n_rows);
		const arma::Col<fltp> J = calc_current_density(
			arma::vectorise(electric_field),
			arma::vectorise(temperature), 
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			dir);

		// convert to matrix and return
		assert(J.n_elem==temperature.n_elem);
		return arma::reshape(J, temperature.n_rows,temperature.n_cols);
	}

	// current sharing temperature calculation in [A m^-2]
	arma::Mat<fltp> Conductor::calc_cs_temperature_mat(
		const arma::Mat<fltp> &current_density, 
		const arma::Mat<fltp> &magnetic_field_magnitude, 
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const Direction dir,
		const fltp temperature_increment,
		const fltp tol_bisection) const{

		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		const arma::Col<fltp> Tcs = calc_cs_temperature(
			arma::vectorise(current_density),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			dir, temperature_increment, tol_bisection);

		// convert to matrix and return
		assert(Tcs.n_elem==current_density.n_elem);
		return arma::reshape(Tcs, current_density.n_rows,current_density.n_cols);
	}

	// thermal energy density
	arma::Mat<fltp> Conductor::calc_thermal_energy_density_mat(
		const arma::Mat<fltp> &temperature, 
		const fltp delta_temperature) const{

		// call regular vectorized function 
		const arma::Col<fltp> energy_density = calc_thermal_energy_density(
			arma::vectorise(temperature),delta_temperature);

		// convert to matrix and return
		assert(energy_density.n_elem==temperature.n_elem);
		return arma::reshape(energy_density, temperature.n_rows,temperature.n_cols);
	}

	

	// critical current density averaged over cross section [J m^-2]
	arma::Mat<fltp> Conductor::calc_critical_current_density_av_mat(
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir) const{

		// check input
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==scaling_factor.n_cols);
		assert(temperature.n_rows==scaling_factor.n_rows);

		// calculate averaged critical current density
		const arma::Col<fltp> Jc = calc_critical_current_density_av(
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle), 
			arma::vectorise(scaling_factor),
			nelements, cross_num_nodes, 
			cross_areas, dir);

		// convert to matrix and return
		assert(Jc.n_elem==temperature.n_elem);
		return arma::reshape(Jc, temperature.n_rows, temperature.n_cols);
	}

	// electric field [V m^-1] section averaged
	// required to supply the cross-sectional areas of the
	// elements making up a section in the same order as
	// the other properties
	arma::Mat<fltp> Conductor::calc_electric_field_av_mat(
		const arma::Mat<fltp> &current_density,
		const arma::Mat<fltp> &temperature,
		const arma::Mat<fltp> &magnetic_field_magnitude,
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir) const{

		// check input
		assert(temperature.n_cols==magnetic_field_magnitude.n_cols);
		assert(temperature.n_rows==magnetic_field_magnitude.n_rows);
		assert(temperature.n_cols==magnetic_field_angle.n_cols);
		assert(temperature.n_rows==magnetic_field_angle.n_rows);
		assert(temperature.n_cols==current_density.n_cols);
		assert(temperature.n_rows==current_density.n_rows);
		assert(temperature.n_cols==scaling_factor.n_cols);
		assert(temperature.n_rows==scaling_factor.n_rows);

		// calculate electric field
		const arma::Col<fltp> E = calc_electric_field_av(
			arma::vectorise(current_density),
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			nelements, cross_num_nodes, cross_areas, dir);

		// convert to matrix and return
		assert(E.n_elem==temperature.n_elem);
		return arma::reshape(E, temperature.n_rows, temperature.n_cols);
	}
	
	// critical temperature calculation in [A m^-2]
	// using averaged critical current over cross area
	arma::Mat<fltp> Conductor::calc_cs_temperature_av_mat(
		const arma::Mat<fltp> &current_density, 
		const arma::Mat<fltp> &magnetic_field_magnitude, 
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir,
		const fltp temperature_increment,
		const fltp tol_bisection) const{
		
		// check input
		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		assert(current_density.n_cols==scaling_factor.n_cols);
		assert(current_density.n_rows==scaling_factor.n_rows);

		// calculate current sharing temperature
		const arma::Col<fltp> Tcs = calc_cs_temperature_av(
			arma::vectorise(current_density),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			nelements, cross_num_nodes, cross_areas, dir,
			temperature_increment, tol_bisection);

		// convert to matrix and return
		assert(Tcs.n_elem==current_density.n_elem);
		return arma::reshape(Tcs, current_density.n_rows, current_density.n_cols);
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Mat<fltp> Conductor::calc_percent_load_mat(
		const arma::Mat<fltp> &current_density,
		const arma::Mat<fltp> &temperature, 
		const arma::Mat<fltp> &magnetic_field_magnitude, 
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const Direction dir,
		const fltp current_density_increment,
		const fltp tol_bisection) const{

		// check input
		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		assert(current_density.n_cols==temperature.n_cols);
		assert(current_density.n_rows==temperature.n_rows);
		assert(current_density.n_cols==scaling_factor.n_cols);
		assert(current_density.n_rows==scaling_factor.n_rows);

		// calculate current sharing temperature
		const arma::Col<fltp> pct_load = calc_percent_load(
			arma::vectorise(current_density),
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			dir, current_density_increment, tol_bisection);

		// convert to matrix and return
		return arma::reshape(pct_load, current_density.n_rows, current_density.n_cols);
	}

	// calculate percentage on the load-line
	// assuming fixed magnetic field angle
	arma::Mat<fltp> Conductor::calc_percent_load_mat_av(
		const arma::Mat<fltp> &current_density,
		const arma::Mat<fltp> &temperature, 
		const arma::Mat<fltp> &magnetic_field_magnitude, 
		const arma::Mat<fltp> &magnetic_field_angle,
		const arma::Mat<fltp> &scaling_factor,
		const arma::Mat<arma::uword> &nelements,
		const arma::uword cross_num_nodes,
		const arma::Row<fltp> &cross_areas,
		const Direction dir,
		const fltp current_density_increment,
		const fltp tol_bisection) const{

		// check input
		assert(current_density.n_cols==magnetic_field_magnitude.n_cols);
		assert(current_density.n_rows==magnetic_field_magnitude.n_rows);
		assert(current_density.n_cols==magnetic_field_angle.n_cols);
		assert(current_density.n_rows==magnetic_field_angle.n_rows);
		assert(current_density.n_cols==temperature.n_cols);
		assert(current_density.n_rows==temperature.n_rows);
		assert(current_density.n_cols==scaling_factor.n_cols);
		assert(current_density.n_rows==scaling_factor.n_rows);

		// calculate current sharing temperature
		const arma::Col<fltp> pct_load = calc_percent_load_av(
			arma::vectorise(current_density),
			arma::vectorise(temperature),
			arma::vectorise(magnetic_field_magnitude),
			arma::vectorise(magnetic_field_angle),
			arma::vectorise(scaling_factor),
			nelements, cross_num_nodes, 
			cross_areas, dir, current_density_increment, 
			tol_bisection);

		// convert to matrix and return
		return arma::reshape(pct_load, current_density.n_rows, current_density.n_cols);
	}

	// display function
	void Conductor::display_property_table(
		const cmn::ShLogPr &lg,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// general info
		lg->msg("Material Name: %s\n",get_name().c_str());
		lg->msg("anisotropic: %i\n", static_cast<int>(is_anisotropic()));
		lg->msg("superconductor: %i/%i/%i\n", 
			static_cast<int>(is_superconductor(Direction::LONGITUDINAL)), 
			static_cast<int>(is_superconductor(Direction::NORMAL)), 
			static_cast<int>(is_superconductor(Direction::TRANSVERSE)));
		lg->msg("insulator: %i/%i/%i\n", 
			static_cast<int>(is_insulator(Direction::LONGITUDINAL)), 
			static_cast<int>(is_insulator(Direction::NORMAL)), 
			static_cast<int>(is_insulator(Direction::TRANSVERSE)));

		// table header
		lg->msg("Material Property Table\n");
		const arma::uword table_size = 99;
		lg->msg("%24s | %51s | %17s\n","parameters","fundamental","superconductor");
		lg->msg("%5s %5s %6s %6s | %6s %8s %8s %8s %8s %8s | %8s %8s\n",
			"fsc","B","alpha","T","d","cp","delH","df","k","rho","Jc","Nv");
		lg->msg("%5s %5s %6s %6s | %6s %8s %8s %8s %8s %8s | %8s %8s\n",
			"[]","T","deg","K","kg/m^3","J/K/kg","J/kg","m^2/s","W/m/K",
			"Ohm.m","A/m^2","unitless");
		lg->msg("="); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"\n");
		
		//. walk over all properties
		for(arma::uword l=0;l<scaling_factor.n_elem;l++){
			for(arma::uword j=0;j<magnetic_field_magnitude.n_elem;j++){
				for(arma::uword k=0;k<magnetic_field_angle.n_elem;k++){
					for(arma::uword i=0;i<temperature.n_elem;i++){
						// get parameters
						const fltp T = temperature(i); const fltp B = magnetic_field_magnitude(j);
						const fltp alpha = magnetic_field_angle(k); const fltp fsc = scaling_factor(l);

						// get fundamental properties
						const fltp d = calc_density(T);
						const fltp cp = calc_specific_heat(T);
						const fltp kt = calc_thermal_conductivity(T,B,dir);
						const fltp rho = calc_electrical_resistivity(T,B,dir);
						const fltp E = calc_thermal_energy_density(T);
						const fltp df = k/(d*cp); // calculate diffusivity

						// superconductor properties
						const fltp jc = calc_critical_current_density(T,B,alpha,fsc,dir);
						const fltp nv = calc_nvalue(T,B,alpha,dir);

						// display properties
						lg->msg("%5.2f %5.2f %6.2f %6.2f | %6.1f %8.2e %8.2e %8.2e %8.2e %8.2e | %8.2e %8.2e\n", 
							fsc, B, 360*alpha/(2*arma::Datum<fltp>::pi), T, d, cp, E, df, kt, rho, jc, nv);
					}
				}
			}
		}

		// table footer
		lg->msg("="); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"\n");
	}

	void Conductor::write_property_table(
		const boost::filesystem::path &fname,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{
		
		// create an output file for writing
		FILE * fp = std::fopen(fname.string().c_str(), "w+");

		// general info
		std::fprintf(fp,"Material Name: %s\n",get_name().c_str());

		// table header
		std::fprintf(fp,"%5s, %5s, %6s, %6s, %6s, %8s, %8s, %8s, %8s, %8s, %8s, %8s\n",
			"fsc","B","alpha","T","d","cp","delH","df","k","rho","Jc","Nv");
		std::fprintf(fp,"%5s, %5s, %6s, %6s, %6s, %8s, %8s, %8s, %8s, %8s, %8s, %8s\n",
			"[]","T","deg","K","kg/m^3","J/K/kg","J/kg","m^2/s","W/m/K",
			"Ohm.m","A/m^2","unitless");

		//. walk over all properties
		for(arma::uword l=0;l<scaling_factor.n_elem;l++){
			for(arma::uword j=0;j<magnetic_field_magnitude.n_elem;j++){
				for(arma::uword k=0;k<magnetic_field_angle.n_elem;k++){
					for(arma::uword i=0;i<temperature.n_elem;i++){
						// get parameters
						const fltp T = temperature(i); const fltp B = magnetic_field_magnitude(j);
						const fltp alpha = magnetic_field_angle(k); const fltp fsc = scaling_factor(l);

						// get fundamental properties
						const fltp d = calc_density(T);
						const fltp cp = calc_specific_heat(T);
						const fltp kt = calc_thermal_conductivity(T,B,dir);
						const fltp rho = calc_electrical_resistivity(T,B,dir);
						const fltp E = calc_thermal_energy_density(T);
						const fltp df = k/(d*cp); // calculate diffusivity

						// superconductor properties
						const fltp jc = calc_critical_current_density(T,B,alpha,fsc,dir);
						const fltp nv = calc_nvalue(T,B,alpha,dir);

						// display properties
						std::fprintf(fp,"%5.2f, %5.2f, %6.2f, %6.2f, %6.1f, %8.2e, %8.2e, %8.2e, %8.2e, %8.2e, %8.2e, %8.2e\n", 
							fsc, B, 360*alpha/(2*arma::Datum<fltp>::pi), T, d, cp, E, df, kt, rho, jc, nv);
					}
				}
			}
		}

		// close file
		std::fclose(fp);
	}

	// serialization
	std::string Conductor::get_type(){
		return "rat::mat::conductor";
	}

	// method for serialization into json
	void Conductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		cmn::Node::serialize(js,list);
		js["type"] = get_type();
		js["classification"] = classification_; 

		// // bisection tolerance
		// js["tol_bisection"] = tol_bisection_;
		// js["bisection_max_iter"] = (int)bisection_max_iter_;
	}

	// method for deserialization from json
	void Conductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){
		
		// parent
		cmn::Node::deserialize(js,list,factory_list,pth);
		if(js.isMember("classification"))
			classification_ = js["classification"].asString();

		// // bisection tolerance
		// if(js.isMember("tol_bisection"))
		// 	tol_bisection_ = js["tol_bisection"].ASFLTP();
		// if(js.isMember("bisection_max_iter"))
		// 	bisection_max_iter_ = js["bisection_max_iter"].asUInt64();
	}

}}