// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "wflfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	WFLFit::WFLFit(){
		set_name("wflfit");
	}

	WFLFit::WFLFit(const ShFitPr &krho_fit) : WFLFit(){
		set_fit(krho_fit);
	}

	// factory
	ShWFLFitPr WFLFit::create(){
		return std::make_shared<WFLFit>();
	}

	// factory from file
	ShWFLFitPr WFLFit::create(const ShFitPr &krho_fit){
		return std::make_shared<WFLFit>(krho_fit);
	}

	// set thermal conductivity fit
	void WFLFit::set_fit(const ShFitPr &krho_fit){
		assert(krho_fit!=NULL);
		krho_fit_ = krho_fit;
	}

	// get fit
	const ShFitPr& WFLFit::get_fit()const{
		return krho_fit_;
	}

	// calc critical current without field angle nor magnitude
	fltp WFLFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// fit functions for vector
	fltp WFLFit::calc_property(
		const fltp temperature, 
		const fltp magnetic_field_magnitude) const{
		
		// limit temperature
		const fltp T = std::max(temperature,t1_);

		// calculate thermal conductivity
		const fltp krho = krho_fit_->calc_property(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const fltp value = lorentz_number_*T/krho;

		// rurn resistivity
		return value;
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> WFLFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}
	
	// fit functions for vector
	arma::Col<fltp> WFLFit::calc_property(
		const arma::Col<fltp> &temperature, 
		const arma::Col<fltp> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		
		// limit temperature
		const arma::Col<fltp> T = arma::clamp(temperature,t1_,arma::Datum<fltp>::inf);

		// calculate thermal conductivity
		const arma::Col<fltp> krho = krho_fit_->calc_property(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const arma::Col<fltp> value = lorentz_number_*T/krho;

		// check output
		assert(value.n_elem==temperature.n_elem);
		assert(value.is_finite());
		assert(arma::all(value>0));

		// rurn resistivity
		return value;
	}

	// validity check
	bool WFLFit::is_valid(const bool enable_throws)const{
		if(krho_fit_==NULL){if(enable_throws){rat_throw_line("k or rho fit not set");} return false;};
		if(!krho_fit_->is_valid(enable_throws)){if(enable_throws){rat_throw_line("k or rho fit not valid");} return false;};
		return true;
	}

	// serialization type
	std::string WFLFit::get_type(){
		return "rat::mat::wflfit";
	}

	// serialization
	void WFLFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);
		js["type"] = get_type();
		js["krho_fit"] = cmn::Node::serialize_node(krho_fit_, list);
	}

	// deserialization
	void WFLFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		set_fit(cmn::Node::deserialize_node<Fit>(js["krho_fit"], list, factory_list, pth));
	}

}}