// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "constfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	ConstFit::ConstFit(){
		set_name("constfit");
	}

	// default constructor
	ConstFit::ConstFit(const fltp value) : ConstFit(){
		set_value(value);
	}

	// factory
	ShConstFitPr ConstFit::create(){
		return std::make_shared<ConstFit>();
	}

	// factory
	ShConstFitPr ConstFit::create(const fltp value){
		return std::make_shared<ConstFit>(value);
	}

	// set fit parameters
	void ConstFit::set_value(const fltp value){
		value_ = value;
	}

	// get fit parameters
	fltp ConstFit::get_value()const{
		return value_;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<fltp> ConstFit::calc_property(const arma::Col<fltp> &temperature)const{
		return arma::Col<fltp>(temperature.n_elem,arma::fill::ones)*value_;
	}

	// specific heat output in [J m^-3 K^-1]
	fltp ConstFit::calc_property(const fltp /*temperature*/)const{
		return value_;
	}

	// get type
	std::string ConstFit::get_type(){
		return "rat::mat::constfit";
	}

	// method for serialization into json
	void ConstFit::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Fit::serialize(js,list);

		// settings
		js["type"] = get_type();

		// fit parameters
		js["value"] = value_;
	}

	// method for deserialisation from json
	void ConstFit::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		value_ = js["value"].ASFLTP();
	}

}}