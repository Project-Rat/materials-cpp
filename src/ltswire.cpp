// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "ltswire.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	LTSWire::LTSWire(){
		set_name("ltswire");
	}

	// factory
	ShLTSWirePr LTSWire::create(){
		return std::make_shared<LTSWire>();
	}

	// setters
	// set dimensions
	void LTSWire::set_diameter(const fltp diameter){
		diameter_ = diameter; setup();
	}

	// set normal conductor to superconductor fraction
	void LTSWire::set_fnc2sc(const fltp fnc2sc){
		fnc2sc_ = fnc2sc; setup();
	}

	// set superconductor
	void LTSWire::set_sc(const ShSuperConductorPr &sc){
		sc_ = sc; setup();
	}

	// set superconductor
	void LTSWire::set_nc(const ShBaseConductorPr &nc){
		nc_ = nc; setup();
	}


	// getters
	// get core width
	fltp LTSWire::get_diameter() const{
		return diameter_;
	}

	// get core thickness
	fltp LTSWire::get_fnc2sc() const{
		return fnc2sc_;
	}

	// get superconductor
	const ShSuperConductorPr& LTSWire::get_sc() const{
		return sc_;
	}

	// get substrate
	const ShBaseConductorPr& LTSWire::get_nc() const{
		return nc_;
	}


	// setup function
	void LTSWire::setup(){
		// check validity
		if(!is_valid(false))return;

		// add conductor by fraction
		clear_conductors();
		add_conductor(1.0/(fnc2sc_+RAT_CONST(1.0)), sc_);
		add_conductor(fnc2sc_/(fnc2sc_+RAT_CONST(1.0)), nc_);
	}

	// is superconductor
	bool LTSWire::is_superconductor(const Direction /*dir*/)const{
		return true;
	}

	// check validity
	bool LTSWire::is_valid(const bool enable_throws) const{
		if(sc_==NULL){if(enable_throws){rat_throw_line("superconductor points to NULL");} return false;}
		if(!sc_->is_valid(enable_throws))return false;
		if(nc_==NULL){if(enable_throws){rat_throw_line("normal conductor points to NULL");} return false;}
		if(!nc_->is_valid(enable_throws))return false;
		if(diameter_<=0){if(enable_throws){rat_throw_line("diameter must be larger than zero");} return false;}
		if(fnc2sc_<=0){if(enable_throws){rat_throw_line("fnc2sc must be larger than zero");} return false;}
		return true;
	}


	// serialization type
	std::string LTSWire::get_type(){
		return "rat::mat::ltswire";
	}

	// serialization
	void LTSWire::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// name
		rat::cmn::Node::serialize(js,list);

		// serialization type
		js["type"] = get_type();

		// dimensions
		js["fnc2sc"] = fnc2sc_;
		js["diameter"] = diameter_;

		// layers
		js["sc"] = cmn::Node::serialize_node(sc_, list);
		js["nc"] = cmn::Node::serialize_node(nc_, list);
	}

	// deserialization
	void LTSWire::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// name
		rat::cmn::Node::deserialize(js,list,factory_list,pth);

		// dimensions
		fnc2sc_ = js["fnc2sc"].ASFLTP();
		diameter_ = js["diameter"].ASFLTP();

		// layers
		sc_ = cmn::Node::deserialize_node<SuperConductor>(js["sc"], list, factory_list, pth);
		nc_ = cmn::Node::deserialize_node<BaseConductor>(js["nc"], list, factory_list, pth);

		// setup as parallel conductor
		setup();
	}

}}