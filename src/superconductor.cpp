// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "superconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	SuperConductor::SuperConductor(){
		set_name("superconductor");
	}


	// constructor with input
	SuperConductor::SuperConductor(
		const ShFitPr &jc_fit, 
		const ShFitPr &nv_fit, 
		const ShFitPr &cp_fit, 
		const ShFitPr &k_fit, 
		const ShFitPr &rho_fit, 
		const ShFitPr &d_fit) : SuperConductor(){
		
		// use set functions to set fits
		set_critical_current_density_fit(jc_fit);
		set_nvalue_fit(nv_fit);
		set_thermal_conductivity_fit(k_fit);
		set_electrical_resistivity_fit(rho_fit);
		set_specific_heat_fit(cp_fit);
		set_density_fit(d_fit);
	}

	// factory
	ShSuperConductorPr SuperConductor::create(){
		return std::make_shared<SuperConductor>();
	}

	// supercondutcor factory
	ShSuperConductorPr SuperConductor::create(
		const ShFitPr &jc_fit, const ShFitPr &nv_fit, 
		const ShFitPr &cp_fit, const ShFitPr &k_fit, 
		const ShFitPr &rho_fit, const ShFitPr &d_fit){
		return std::make_shared<SuperConductor>(
			jc_fit, nv_fit, cp_fit, k_fit, rho_fit, d_fit);
	}


	// set electric field criterion
	void SuperConductor::set_electric_field_criterion(
		const fltp electric_field_criterion){
		electric_field_criterion_ = electric_field_criterion;
	}



	// set critical current density fit
	void SuperConductor::set_critical_current_density_fit(const ShFitPr &jc_fit){
		jc_fit_ = jc_fit;
	}

	// set fit for determining the n-value
	void SuperConductor::set_nvalue_fit(const ShFitPr &nv_fit){
		nv_fit_ = nv_fit;
	}

	// set critical current density fit
	const ShFitPr& SuperConductor::get_critical_current_density_fit()const{
		return jc_fit_;
	}

	// set fit for determining the n-value
	const ShFitPr& SuperConductor::get_nvalue_fit()const{
		return nv_fit_;
	}

	// get superconductor electric field criterion
	fltp SuperConductor::get_electric_field_criterion()const{
		return electric_field_criterion_;
	}

	// is insulating
	bool SuperConductor::is_insulator(const Direction /*dir*/)const{
		return rho_fit_ == NULL && jc_fit_ == NULL;
	}

	// check if it is a superconductor
	bool SuperConductor::is_superconductor(const Direction /*dir*/)const{
		return jc_fit_!=NULL;
	}


	// critical current density [J m^-2]
	fltp SuperConductor::calc_critical_current_density(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction /*dir*/) const{
		if(jc_fit_!=NULL && scaling_factor>0)return scaling_factor*jc_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return 0;
	}

	// calculate power law nvalue
	fltp SuperConductor::calc_nvalue(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const Direction /*dir*/) const{
		if(nv_fit_!=NULL)return nv_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return 0;
	}

	// electric field [V/m]
	fltp SuperConductor::calc_electric_field(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// calculate electrical resistivity 
		const fltp electrical_resistivity = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// calculate critical current 
		const fltp critical_current_density = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// calculate nvalue 
		const fltp nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);

		// electric field based on resistivity
		const fltp electric_field_rho = electrical_resistivity*std::abs(current_density);

		// allocate 
		fltp electric_field_jc;

		// superconducting
		if(critical_current_density>0){
			assert(electric_field_criterion_>0);
			electric_field_jc = electric_field_criterion_*std::pow(
				std::abs(current_density)/critical_current_density,nvalue); 
		}

		// normal conducting
		else{
			electric_field_jc = arma::Datum<fltp>::inf;
		}

		// check sign
		assert(electric_field_jc>=0);
		assert(electric_field_rho>=0);

		// return smallest of the two
		return rat::cmn::Extra::sign(current_density)*
			std::min(electric_field_rho, electric_field_jc);
	}

	// calculate current density [A m^-2]
	fltp SuperConductor::calc_current_density(
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp magnetic_field_angle,
		const fltp scaling_factor,
		const Direction dir) const{

		// check for zero
		if(electric_field==0)return 0;

		// calculate electrical resistivity 
		const fltp electrical_resistivity  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// calculate critical current 
		const fltp critical_current_density = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// calculate nvalue 
		const fltp nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);


		// allocate 
		fltp current_density_sc = 0;

		// superconducting
		if(critical_current_density>0){
			assert(electric_field_criterion_>0);
			current_density_sc = critical_current_density*
				std::pow(std::abs(electric_field)/electric_field_criterion_,RAT_CONST(1.0)/nvalue);
		}

		// normal conducting current 
		const fltp current_density_nc = std::abs(electric_field)/electrical_resistivity;

		// check sign
		assert(current_density_sc>=0);
		assert(current_density_nc>=0);

		// take the highest of teh two
		return rat::cmn::Extra::sign(electric_field)*
			std::max(current_density_nc, current_density_sc);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> SuperConductor::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction /*dir*/) const{
		if(jc_fit_!=NULL)return scaling_factor%jc_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> SuperConductor::calc_nvalue(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const Direction /*dir*/) const{
		if(nv_fit_!=NULL)return nv_fit_->calc_property(
			temperature, magnetic_field_magnitude, magnetic_field_angle);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// electric field calculation [V/m]
	arma::Col<fltp> SuperConductor::calc_electric_field(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// calculate electrical resistivity 
		const arma::Col<fltp> electrical_resistivity = arma::clamp(calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir),RAT_CONST(0.0),arma::Datum<fltp>::inf); // do not allow negative values

		// calculate critical current 
		const arma::Col<fltp> critical_current_density = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// calculate nvalue
		const arma::Col<fltp> nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);

		// get electric field criterion used for fit
		// const fltp electric_field_criterion = jc_fit_->get_electric_field_criterion();

		// electric field based on resistivity
		arma::Col<fltp> electric_field_nc = 
			electrical_resistivity%arma::abs(current_density);

		// electirc field due to critical current density
		// this is the classical power law
		arma::Col<fltp> electric_field_sc(current_density.n_elem);
		for(arma::uword i=0;i<current_density.n_elem;i++){

			// superconducting
			if(critical_current_density(i)>0){
				assert(electric_field_criterion_>0);
				electric_field_sc(i) = electric_field_criterion_*
					std::pow(std::abs(current_density(i))/
					critical_current_density(i),nvalue(i));
			}

			// normal conducting
			else{
				electric_field_sc(i) = arma::Datum<fltp>::inf;
			}
		}

		// check sign
		assert(arma::all(electric_field_nc>=0));
		assert(arma::all(electric_field_sc>=0));

		// return lowest of the two
		return arma::sign(current_density)%
			arma::min(electric_field_nc, electric_field_sc);
	}

	// calculate current density [A m^-2]
	arma::Col<fltp> SuperConductor::calc_current_density(
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &magnetic_field_angle,
		const arma::Col<fltp> &scaling_factor,
		const Direction dir) const{

		// calculate electrical resistivity 
		const arma::Col<fltp> electrical_resistivity  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// calculate critical current 
		const arma::Col<fltp> critical_current_density = calc_critical_current_density(
			temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);

		// calculate nvalue
		const arma::Col<fltp> nvalue = calc_nvalue(
			temperature, magnetic_field_magnitude, magnetic_field_angle, dir);

		// allocate 
		arma::Col<fltp> current_density_sc(electric_field.n_elem, arma::fill::zeros);

		// superconducting current
		for(arma::uword i=0;i<electric_field.n_elem;i++){
			// superconducting
			if(critical_current_density(i)>0){
				assert(electric_field_criterion_>0);
				current_density_sc(i) = critical_current_density(i)*
					std::pow(std::abs(electric_field(i))/electric_field_criterion_,RAT_CONST(1.0)/nvalue(i));
			}
		}

		// normal conducting current
		const arma::Col<fltp> current_density_nc = 
			arma::abs(electric_field)/electrical_resistivity;

		// check sign
		assert(arma::all(current_density_nc>=0));
		assert(arma::all(current_density_sc>=0));

		// take the highest of teh two and return
		const arma::Col<fltp> current_density = 
			arma::sign(electric_field)%arma::max(
			current_density_nc, current_density_sc);
		return current_density;
	}

	// serialization type
	std::string SuperConductor::get_type(){
		return "rat::mat::superconductor";
	}

	// serialization
	void SuperConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		BaseConductor::serialize(js,list);

		// serialize fits
		js["type"] = get_type();
		js["jc_fit"] = cmn::Node::serialize_node(jc_fit_, list);
		js["nv_fit"] = cmn::Node::serialize_node(nv_fit_, list);

		// electric field criterion
		js["E0"] = electric_field_criterion_;
	}

	// deserialization
	void SuperConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		BaseConductor::deserialize(js,list,factory_list,pth);

		// deserialize fits
		set_critical_current_density_fit(cmn::Node::deserialize_node<Fit>(js["jc_fit"], list, factory_list, pth));
		set_nvalue_fit(cmn::Node::deserialize_node<Fit>(js["nv_fit"], list, factory_list, pth));

		// electric field criterion
		set_electric_field_criterion(js["E0"].ASFLTP());
	}

}}