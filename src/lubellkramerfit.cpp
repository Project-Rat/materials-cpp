// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "lubellkramerfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LubellKramerFit::LubellKramerFit(){
		set_name("lkfit"); set_t1(1); set_t2(20);
	}

	// constructor with input
	LubellKramerFit::LubellKramerFit(
		const fltp C0,
		const fltp alpha,
		const fltp beta,
		const fltp gamma,
		const fltp n,
		const fltp Bc20,
		const fltp Tc0,
		const fltp Jref) : LubellKramerFit(){
		C0_ = C0; alpha_ = alpha;
		beta_ = beta; gamma_ = gamma; n_ = n; 
		Bc20_ = Bc20; Tc0_ = Tc0; Jref_ = Jref;
	}

	// factory
	ShLubellKramerFitPr LubellKramerFit::create(){
		return std::make_shared<LubellKramerFit>();
	}

	// factory with input
	ShLubellKramerFitPr LubellKramerFit::create(
		const fltp C0,
		const fltp alpha,
		const fltp beta,
		const fltp gamma,
		const fltp n,
		const fltp Bc20,
		const fltp Tc0,
		const fltp Jref){
		return std::make_shared<LubellKramerFit>(
			C0,alpha,beta,gamma,n,Bc20,Tc0,Jref);
	}


	// setters
	void LubellKramerFit::set_C0(const fltp C0){
		C0_ = C0;
	}

	void LubellKramerFit::set_alpha(const fltp alpha){
		alpha_ = alpha;
	}

	void LubellKramerFit::set_beta(const fltp beta){
		beta_ = beta;
	}

	void LubellKramerFit::set_gamma(const fltp gamma){
		gamma_ = gamma;
	}

	void LubellKramerFit::set_n(const fltp n){
		n_ = n;
	}

	void LubellKramerFit::set_Bc20(const fltp Bc20){
		Bc20_ = Bc20;
	}

	void LubellKramerFit::set_Tc0(const fltp Tc0){
		Tc0_ = Tc0;
	}

	void LubellKramerFit::set_Jref(const fltp Jref){
		Jref_ = Jref;
	}

	// getters
	fltp LubellKramerFit::get_C0()const{
		return C0_;
	}

	fltp LubellKramerFit::get_alpha()const{
		return alpha_;
	}

	fltp LubellKramerFit::get_beta()const{
		return beta_;
	}

	fltp LubellKramerFit::get_gamma()const{
		return gamma_;
	}

	fltp LubellKramerFit::get_n()const{
		return n_;
	}

	fltp LubellKramerFit::get_Bc20()const{
		return Bc20_;
	}

	fltp LubellKramerFit::get_Tc0()const{
		return Tc0_;
	}

	fltp LubellKramerFit::get_Jref()const{
		return Jref_;
	}

	// calc critical current without field angle nor magnitude
	fltp LubellKramerFit::calc_property(
		const fltp temperature) const{
		return calc_property(temperature, RAT_CONST(0.0));
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	fltp LubellKramerFit::calc_property(
		const fltp temperature,
		const fltp magnetic_field_magnitude) const{

		// copy input and limit
		const fltp temperature_lim = std::max(RAT_CONST(0.0),temperature);
		const fltp mgn_field_lim = std::max(RAT_CONST(0.1),magnetic_field_magnitude);

		// calculate scaled values
		const fltp t = temperature_lim/Tc0_;
		const fltp Bc2 = Bc20_*(1.0-std::pow(t,n_)); // Lubell's equation
		const fltp b = mgn_field_lim/Bc2;

		// bottura's equation
		fltp critical_current_density = 
			Jref_*(C0_/mgn_field_lim)*std::pow(b,alpha_)*
			std::pow(1.0-b,beta_)*std::pow(1.0-std::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		if(mgn_field_lim>Bc2)critical_current_density = 0;
		if(mgn_field_lim==0)critical_current_density = arma::Datum<fltp>::inf;

		// return critical current
		return critical_current_density; // A/m^2
	}

	// calc critical current without field angle nor magnitude
	arma::Col<fltp> LubellKramerFit::calc_property(
		const arma::Col<fltp> &temperature) const{
		return calc_property(temperature, arma::Col<fltp>(temperature.n_elem, arma::fill::zeros));
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<fltp> LubellKramerFit::calc_property(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude) const{

		// copy B
		const arma::Col<fltp> mgn_field_lim = arma::clamp(
			magnetic_field_magnitude, RAT_CONST(0.1), arma::Datum<fltp>::inf);
		const arma::Col<fltp> temperature_lim = arma::clamp(
			temperature,0,arma::Datum<fltp>::inf);

		// calculate scaled values
		const arma::Col<fltp> t = temperature_lim/Tc0_;
		const arma::Col<fltp> Bc2 = Bc20_*(1.0-arma::pow(t,n_)); // Lubell's equation
		const arma::Col<fltp> b = mgn_field_lim/Bc2;

		// bottura's equation
		arma::Col<fltp> critical_current_density = 
			Jref_*(C0_/mgn_field_lim)%arma::pow(b,alpha_)%
			arma::pow(1.0-b,beta_)%arma::pow(1.0-arma::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		critical_current_density(arma::find(mgn_field_lim>Bc2)).fill(0);
		critical_current_density(arma::find(mgn_field_lim==0)).fill(arma::Datum<fltp>::inf);

		// return critical current
		return critical_current_density; // A/m^2
	}

	// get type
	std::string LubellKramerFit::get_type(){
		return "rat::mat::lubellkramerfit";
	}

	// method for serialization into json
	void LubellKramerFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);
		js["type"] = get_type();
		js["C0"] = C0_;
		js["alpha"] = alpha_;
		js["beta"] = beta_;
		js["gamma"] = gamma_;
		js["Bc20"] = Bc20_;
		js["Tc0"] = Tc0_;
		js["Jrefsi"] = Jref_;
		js["n"] = n_;
	}

	// method for deserialisation from json
	void LubellKramerFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		C0_ = js["C0"].ASFLTP();
		alpha_ = js["alpha"].ASFLTP();
		beta_ = js["beta"].ASFLTP();
		gamma_ = js["gamma"].ASFLTP();
		Bc20_ = js["Bc20"].ASFLTP();
		Tc0_ = js["Tc0"].ASFLTP();
		// Jref in A/mm^2 needs to be converted to A/m^2
		if(js.isMember("Jref"))Jref_ = 1e6*js["Jref"].ASFLTP(); else Jref_ = js["Jrefsi"].ASFLTP();
		n_ = js["n"].ASFLTP();
	}

}}