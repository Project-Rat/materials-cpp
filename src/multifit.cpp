// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "multifit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	MultiFit::MultiFit(){
		set_name("multifit");
	}

	// factory
	ShMultiFitPr MultiFit::create(){
		return std::make_shared<MultiFit>();
	}

	// add fit
	arma::uword MultiFit::add_fit(const ShFitPr &fit){
		const arma::uword index = fit_list_.size()+1;
		fit_list_.insert({index,fit}); return index;
	}

	// set fit
	void MultiFit::set_fit(const arma::uword index, const ShFitPr &fit){
		fit_list_[index] = fit;
	}

	// retreive model at index
	ShFitPr MultiFit::get_fit(const arma::uword index) const{
		auto it = fit_list_.find(index);
		if(it==fit_list_.end())rat_throw_line("index does not exist");
		return (*it).second;
	}

	// delete model at index
	bool MultiFit::delete_fit(const arma::uword index){	
		auto it = fit_list_.find(index);
		if(it==fit_list_.end())return false;
		(*it).second = NULL; return true;
	}

	// number of fits
	arma::uword MultiFit::num_fits() const{
		return fit_list_.size();
	}

	// re-index nodes after deleting
	void MultiFit::reindex(){
		std::map<arma::uword, ShFitPr> new_fits; arma::uword idx = 1;
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++)
			if((*it).second!=NULL)new_fits.insert({idx++, (*it).second});
		fit_list_ = new_fits;
	}


	// fit functions
	fltp MultiFit::calc_property(const fltp temperature) const{
		// allocate output value
		fltp value = 0;

		// clamp temperature at lower end
		const fltp T = std::max(temperature, t1_);

		// walk over list of fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			// get fit
			const ShFitPr& ft = (*it).second;

			// get temperature range
			const fltp t1 = ft->get_t1(); 
			const fltp t2 = ft->get_t2();

			// check if temperature is in range
			if(T<t2 && T>=t1){
				value = ft->calc_property(T); break;
			}
		}

		// extrapolation beyond higher end
		if(T>t2_){
			const fltp dt = (t2_-t1_)/1000;
			const arma::Col<fltp>::fixed<2> Te = {t2_-dt, t2_-RAT_CONST(1e-10)};
			const arma::Col<fltp>::fixed<2> ve = calc_property(Te);
			value = ve(1) + (T-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// return calculated value
		return value;
	}
	
	// specific heat calculation
	arma::Col<fltp> MultiFit::calc_property(const arma::Col<fltp> &temperature) const{
		// allocate output values
		arma::Col<fltp> values(temperature.n_elem,arma::fill::zeros);

		// find temperature limits
		const arma::Col<fltp> temperature_lim = arma::clamp(
			temperature,t1_,arma::Datum<fltp>::inf);

		// walk over list of fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			// get fit
			const ShFitPr& ft = (*it).second;

			// get temperature range
			const fltp t1 = ft->get_t1(); 
			const fltp t2 = ft->get_t2();

			// find indexes within this range
			const arma::Col<arma::uword> idx = arma::find(temperature_lim>=t1 && temperature_lim<t2);

			// calculate using the corresponding fit
			values(idx) = ft->calc_property(temperature_lim(idx));
		}

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(temperature_lim>t2_);
		if(!extrap.is_empty()){
			const fltp dt = (t2_-t1_)/1000;
			const arma::Col<fltp>::fixed<2> Te = {t2_-dt, t2_-RAT_CONST(1e-10)};
			const arma::Col<fltp>::fixed<2> ve = calc_property(Te);
			values(extrap) = ve(1) + (temperature_lim(extrap)-Te(1))*((ve(1)-ve(0))/(Te(1)-Te(0)));
		}

		// return calculated values
		return values;
	}

	// validity check
	bool MultiFit::is_valid(const bool enable_throws)const{
		if(fit_list_.empty()){if(enable_throws){rat_throw_line("there are not fits set");} return false;};
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			if((*it).second==NULL){if(enable_throws){rat_throw_line("one or more fits is NULL");} return false;};
			if(!(*it).second->is_valid(enable_throws)){if(enable_throws){rat_throw_line("one or more fits is not valid");} return false;};
		}
		return true;
	}


	// get type
	std::string MultiFit::get_type(){
		return "rat::mat::multifit";
	}

	// method for serialization into json
	void MultiFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// create a list of fits
		for(auto it=fit_list_.begin();it!=fit_list_.end();it++){
			js["sub_fits"].append(cmn::Node::serialize_node((*it).second, list));
		}
	}

	// method for deserialisation from json
	void MultiFit::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list, 
		const boost::filesystem::path &pth){

		// parent
		Fit::deserialize(js,list,factory_list,pth);
		
		// deserialize fits
		for(auto it=js["sub_fits"].begin(); it!=js["sub_fits"].end(); ++it){
			add_fit(cmn::Node::deserialize_node<Fit>(*it, list, factory_list, pth));
		}
	}

}}