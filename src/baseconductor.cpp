// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "baseconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	BaseConductor::BaseConductor(){
		set_name("base");
	}

	// constructor with input
	BaseConductor::BaseConductor(
		const ShFitPr &cp_fit, 
		const ShFitPr &k_fit, 
		const ShFitPr &rho_fit, 
		const ShFitPr &d_fit) : BaseConductor(){
		
		// use set functions to set fits
		set_thermal_conductivity_fit(k_fit);
		set_electrical_resistivity_fit(rho_fit);
		set_specific_heat_fit(cp_fit);
		set_density_fit(d_fit);
	}


	// factory
	ShBaseConductorPr BaseConductor::create(){
		return std::make_shared<BaseConductor>();
	}

	// normal conductor factory
	ShBaseConductorPr BaseConductor::create(
		const ShFitPr &cp_fit, const ShFitPr &k_fit, 
		const ShFitPr &rho_fit, const ShFitPr &d_fit){
		return std::make_shared<BaseConductor>(
			cp_fit,k_fit,rho_fit,d_fit);
	}


	// supercondutcor factory
	ShBaseConductorPr BaseConductor::copper(const fltp RRR){
		// create fits
		ShFitPr cp_fit = Nist8SumFit::create(4,300,arma::Col<fltp>::fixed<9>{
			-1.91844,-0.15973,8.61013,-18.996,21.9661,-12.7328,3.54322,-0.3797,0.0});
		ShFitPr rho_fit = CudiFit::create(RRR);
		ShFitPr k_fit = WFLFit::create(rho_fit);
		ShFitPr d_fit = ConstFit::create(8960);

		// create conductor and return
		ShBaseConductorPr copper = std::make_shared<BaseConductor>(
			cp_fit, k_fit, rho_fit, d_fit);
		copper->set_name("Copper OFHC RRR" + std::to_string(RRR));
		return copper;
	}



	// set thermal conductivity fit
	void BaseConductor::set_thermal_conductivity_fit(const ShFitPr &k_fit){
		k_fit_ = k_fit;
	}

	// set electrical resistivity fit
	void BaseConductor::set_electrical_resistivity_fit(const ShFitPr &rho_fit){
		rho_fit_ = rho_fit;
	}

	// set specific heat fit
	void BaseConductor::set_specific_heat_fit(const ShFitPr &cp_fit){
		cp_fit_ = cp_fit;
	}

	// set density fit
	void BaseConductor::set_density_fit(const ShFitPr &d_fit){
		d_fit_ = d_fit;
	}


	// set thermal conductivity fit
	const ShFitPr& BaseConductor::get_thermal_conductivity_fit()const{
		return k_fit_;
	}

	// set electrical resistivity fit
	const ShFitPr& BaseConductor::get_electrical_resistivity_fit()const{
		return rho_fit_;
	}

	// set specific heat fit
	const ShFitPr& BaseConductor::get_specific_heat_fit()const{
		return cp_fit_;
	}

	// set density fit
	const ShFitPr& BaseConductor::get_density_fit()const{
		return d_fit_;
	}

	// is insulating
	bool BaseConductor::is_insulator(const Direction /*dir*/)const{
		return rho_fit_ == NULL;
	}

	// thermal insulator
	bool BaseConductor::is_thermal_insulator(const Direction /*dir*/)const{
		return k_fit_ == NULL;
	}

	// check if it is a superconductor
	bool BaseConductor::is_superconductor(const Direction /*dir*/)const{
		return false;
	}


	// material properties for scalar input
	// calculate specific heat [J kg^-1 K^-1]
	fltp BaseConductor::calc_specific_heat(
		const fltp temperature) const{
		if(cp_fit_!=NULL)return cp_fit_->calc_property(temperature); 
		else return 0;
	}

	// thermal conductivity [W m^-1 K^-1]
	fltp BaseConductor::calc_thermal_conductivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction /*dir*/) const{
		if(k_fit_!=NULL)return k_fit_->calc_property(
			temperature, magnetic_field_magnitude);
		else return 0;
	}

	// electrical resistivity [Ohm m^2]
	fltp BaseConductor::calc_electrical_resistivity(
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const Direction /*dir*/) const{
		if(rho_fit_!=NULL)return rho_fit_->calc_property(
			temperature,magnetic_field_magnitude);
		else return RAT_CONST(1e30);
	}

	// material density [kg m^-3]
	fltp BaseConductor::calc_density(
		const fltp temperature) const{
		if(d_fit_!=NULL)return d_fit_->calc_property(temperature); 
		else return 0;
	}

	// critical current density [J m^-2]
	fltp BaseConductor::calc_critical_current_density(
		const fltp /*temperature*/,
		const fltp /*magnetic_field_magnitude*/,
		const fltp /*magnetic_field_angle*/,
		const fltp /*scaling_factor*/,
		const Direction /*dir*/) const{
		return 0;
	}

	// calculate power law nvalue
	fltp BaseConductor::calc_nvalue(
		const fltp /*temperature*/,
		const fltp /*magnetic_field_magnitude*/,
		const fltp /*magnetic_field_angle*/,
		const Direction /*dir*/) const{
		return 0;
	}

	

	// electric field [V/m]
	fltp BaseConductor::calc_electric_field(
		const fltp current_density,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp /*magnetic_field_angle*/,
		const fltp /*scaling_factor*/,
		const Direction dir) const{

		// calculate electrical resistivity 
		const fltp electrical_resistivity = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// electric field based on resistivity
		return electrical_resistivity*current_density;
	}

	// calculate current density [A m^-2]
	fltp BaseConductor::calc_current_density(
		const fltp electric_field,
		const fltp temperature,
		const fltp magnetic_field_magnitude,
		const fltp /*magnetic_field_angle*/,
		const fltp /*scaling_factor*/,
		const Direction dir) const{

		// check for zero
		if(electric_field==0)return 0;

		// calculate electrical resistivity 
		const fltp electrical_resistivity  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// normal conducting current 
		return electric_field/electrical_resistivity;
	}

	// material properties for vector input
	// calculate specific heat [J kg^-1 K^-1]
	arma::Col<fltp> BaseConductor::calc_specific_heat(
		const arma::Col<fltp> &temperature) const{
		if(cp_fit_!=NULL)return cp_fit_->calc_property(temperature);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// thermal conductivity [W m^-1 K^-1]
	arma::Col<fltp> BaseConductor::calc_thermal_conductivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction /*dir*/) const{
		if(k_fit_!=NULL)return k_fit_->calc_property(temperature, magnetic_field_magnitude);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// electrical resistivity [Ohm m^2]
	arma::Col<fltp> BaseConductor::calc_electrical_resistivity(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const Direction /*dir*/) const{
		if(rho_fit_!=NULL)return rho_fit_->calc_property(temperature,magnetic_field_magnitude);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::ones)*RAT_CONST(1e30);
	}

	// material density [kg m^-3]
	arma::Col<fltp> BaseConductor::calc_density(
		const arma::Col<fltp> &temperature) const{
		if(d_fit_!=NULL)return d_fit_->calc_property(temperature);
		else return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> BaseConductor::calc_critical_current_density(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &/*magnetic_field_magnitude*/,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const arma::Col<fltp> &/*scaling_factor*/,
		const Direction /*dir*/) const{
		return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// critical current density [J m^-2]
	arma::Col<fltp> BaseConductor::calc_nvalue(
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &/*magnetic_field_magnitude*/,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const Direction /*dir*/) const{
		return arma::Col<fltp>(temperature.n_elem, arma::fill::zeros);
	}

	// electric field calculation [V/m]
	arma::Col<fltp> BaseConductor::calc_electric_field(
		const arma::Col<fltp> &current_density,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const arma::Col<fltp> &/*scaling_factor*/,
		const Direction dir) const{
		
		// calculate electrical resistivity 
		const arma::Col<fltp> electrical_resistivity = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// electric field based on resistivity
		return electrical_resistivity%current_density;
	}

	// calculate current density [A m^-2]
	arma::Col<fltp> BaseConductor::calc_current_density(
		const arma::Col<fltp> &electric_field,
		const arma::Col<fltp> &temperature,
		const arma::Col<fltp> &magnetic_field_magnitude,
		const arma::Col<fltp> &/*magnetic_field_angle*/,
		const arma::Col<fltp> &/*scaling_factor*/,
		const Direction dir) const{

		// calculate electrical resistivity 
		const arma::Col<fltp> electrical_resistivity  = calc_electrical_resistivity(
			temperature, magnetic_field_magnitude, dir);

		// normal conducting current
		return electric_field/electrical_resistivity;
	}

	// serialization type
	std::string BaseConductor::get_type(){
		return "rat::mat::baseconductor";
	}

	// serialization
	void BaseConductor::serialize(
		Json::Value &js, 
		cmn::SList &list) const{

		// parent
		Conductor::serialize(js,list);

		// serialize fits
		js["type"] = get_type();
		js["k_fit"] = cmn::Node::serialize_node(k_fit_, list);
		js["rho_fit"] = cmn::Node::serialize_node(rho_fit_, list);
		js["cp_fit"] = cmn::Node::serialize_node(cp_fit_, list);
		js["d_fit"] = cmn::Node::serialize_node(d_fit_, list);
	}

	// deserialization
	void BaseConductor::deserialize(
		const Json::Value &js, 
		cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list,
		const boost::filesystem::path &pth){

		// parent
		Conductor::deserialize(js,list,factory_list,pth);

		// deserialize fits
		set_thermal_conductivity_fit(cmn::Node::deserialize_node<Fit>(js["k_fit"], list, factory_list, pth));
		set_electrical_resistivity_fit(cmn::Node::deserialize_node<Fit>(js["rho_fit"], list, factory_list, pth));
		set_specific_heat_fit(cmn::Node::deserialize_node<Fit>(js["cp_fit"], list, factory_list, pth));
		set_density_fit(cmn::Node::deserialize_node<Fit>(js["d_fit"], list, factory_list, pth));
	}

}}