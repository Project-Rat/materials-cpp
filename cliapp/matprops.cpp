// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat common headers
#include "rat/common/log.hh"
#include "rat/common/extra.hh"

// rat material headers
#include "serializer.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"
#include "version.hh"

// string parse function
arma::Col<rat::fltp> parse_string2vals(const std::string &str, const std::string delimiter){
	// keep track of position
	size_t pos = 0;
	
	// split string into tokens
	std::string s = str;
	std::list<std::string> token_list;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token_list.push_back(s.substr(0, pos));
		s.erase(0, pos + delimiter.length());
	}
	token_list.push_back(s);

	// convert tokens to values
	arma::Col<rat::fltp> values(token_list.size()); arma::uword i=0;
	for(auto it = token_list.begin();it!=token_list.end();it++,i++){
		values(i) = std::stof((*it));
	}

	return values;
}

arma::Col<rat::fltp> parse_string(const std::string &str, const std::string delimiter){
	if(str.empty())rat_throw_line("string is empty");

	// check for linspace option
	std::string keyword = "linspace(";
	if(str.length()>=keyword.length()){
		if(str.substr(0,keyword.length()-1).compare(keyword)){
			if(str.back()==')'){
				arma::Col<rat::fltp> values = parse_string2vals(str.substr(keyword.length(),str.length()-2),delimiter);
				if(values.n_elem!=3){
					rat_throw_line("linspace requires three arguments");
				}	

				// return list
				return arma::linspace<arma::Col<rat::fltp> >(values(0),values(1),(arma::uword)values(2));
			}
		}
	}

	// list of values
	return parse_string2vals(str,delimiter);
}


// main function
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> input_filename_argument(
		"ifname","Input filename",true,
		"json/matprops/metals/CopperOFHC_RRR050.json",
		"string", cmd);

	// table output argument
	TCLAP::ValueArg<std::string> table_filename_argument(
		"","table","Output table file",false,
		"table.csv","string",cmd);

	// temperature input argument
	TCLAP::ValueArg<std::string> temperature_argument(
		"t","temperature","Input temperatures in [K]",
		false,"4.5,10,20,40,60,77", "string", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<std::string> field_argument(
		"b","fluxdensity","Input magnetic flux densities in [T]",
		false,"5,10,20,30", "string", cmd);

	// magnetic field angle input argument
	TCLAP::ValueArg<std::string> field_angle_argument(
		"a","fieldangle","Magnetic field angle in [deg]",
		false,"0,90", "string", cmd);

	// magnetic field angle input argument
	TCLAP::ValueArg<std::string> scaling_factor_argument(
		"f","scaling","Scaling factor for critical current",
		false,"0.9,1.0", "string", cmd);

	// show numerical solutions
	TCLAP::SwitchArg longitudinal_switch_argument(
		"","longitudinal","calculate in longitudinal direction", cmd, false);
	TCLAP::SwitchArg normal_switch_argument(
		"","normal","calculate in normal direction", cmd, false);
	TCLAP::SwitchArg transverse_switch_argument(
		"","transverse","calculate in transverse direction", cmd, false);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// get direction
	rat::mat::Direction dir = rat::mat::Direction::LONGITUDINAL; // default
	if(normal_switch_argument.isSet())dir = rat::mat::Direction::NORMAL;
	if(transverse_switch_argument.isSet())dir = rat::mat::Direction::TRANSVERSE;

	// defaults
	arma::Col<rat::fltp> temperature{1.9,4.5,10,20,30,40,50,60,77};
	arma::Col<rat::fltp> magnetic_field_magnitude{5};
	arma::Col<rat::fltp> magnetic_field_angle{0};
	arma::Col<rat::fltp> scaling_factor{1.0};

	// get temperature
	if(temperature_argument.isSet())temperature = parse_string(temperature_argument.getValue(), ",");

	// get magnetic field
	if(field_argument.isSet())magnetic_field_magnitude = parse_string(field_argument.getValue(), ",");

	// get magnetic field angle
	if(field_angle_argument.isSet())magnetic_field_angle = 2*arma::Datum<rat::fltp>::pi*parse_string(field_angle_argument.getValue(), ",")/360;
	
	// get magnetic field angle
	if(scaling_factor_argument.isSet())scaling_factor = parse_string(scaling_factor_argument.getValue(), ",");

	// get file name
	const boost::filesystem::path& ifname = input_filename_argument.getValue();

	// create a log
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// deserializer file
	const rat::mat::ShConductorPr mat = rat::mat::Serializer::create<rat::mat::Conductor>(ifname);

	// write table
	if(table_filename_argument.isSet()){
		mat->write_property_table(table_filename_argument.getValue(),
			temperature, magnetic_field_magnitude, 
			magnetic_field_angle, scaling_factor, dir);
	}

	// display table
	else{
		mat->display_property_table(lg, temperature, magnetic_field_magnitude, magnetic_field_angle, scaling_factor, dir);
	}
	
}