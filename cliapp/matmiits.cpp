// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat common headers
#include "rat/common/log.hh"
#include "rat/common/extra.hh"

// rat material headers
#include "conductor.hh"
#include "serializer.hh"
#include "version.hh"

// main function
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> input_filename_argument(
		"ifname","Input filename",true,
		"json/matprops/ltswire/nbti.json",
		"string", cmd);

	// temperature input argument
	TCLAP::ValueArg<rat::fltp> tstart_argument(
		"","t1","start temperature [K]",
		false,4.5,"double", cmd);

	// temperature input argument
	TCLAP::ValueArg<rat::fltp> tend_argument(
		"","t2","end temperature [K]",
		false,1000.0,"double", cmd);

	// temperature input argument
	TCLAP::ValueArg<int> nstep_argument(
		"n","nstep","number of steps",
		false,100,"double", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<rat::fltp> area_argument(
		"a","area","area [m^2]",
		false,1e-6,"double", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<rat::fltp> current_argument(
		"i","current","current [A]",
		false,400,"double", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<rat::fltp> magnetic_field_argument(
		"b","fluxdensity","magnetic field [T]",
		false,5.0,"double", cmd);

	// table output argument
	TCLAP::ValueArg<std::string> table_filename_argument(
		"","table","Output table file",false,
		"table.csv","string",cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// get values
	const boost::filesystem::path ifname = input_filename_argument.getValue();
	const rat::fltp start_temperature = tstart_argument.getValue();
	const rat::fltp end_temperature = tend_argument.getValue();
	const arma::uword num_steps = nstep_argument.getValue();
	
	const rat::fltp area = area_argument.getValue();
	const rat::fltp current = current_argument.getValue();
	const rat::fltp magnetic_field = magnetic_field_argument.getValue();

	// create logger
	const rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// deserializer file
	const rat::mat::ShConductorPr mat = rat::mat::Serializer::create<rat::mat::Conductor>(ifname);

	// calculate number of tempertaure steps
	const rat::fltp temperature_step_size = (end_temperature-start_temperature)/num_steps;

	// create temperatures
	const arma::Col<rat::fltp> T = arma::linspace<arma::Col<rat::fltp> >(start_temperature, end_temperature, num_steps);
	const arma::Col<rat::fltp> B = arma::linspace<arma::Col<rat::fltp> >(magnetic_field,magnetic_field,num_steps);
	const arma::Col<rat::fltp> I = arma::linspace<arma::Col<rat::fltp> >(current,current,num_steps);

	// calculate properties
	const arma::Col<rat::fltp> rho = mat->calc_electrical_resistivity(T,B); // Ohm m
	const arma::Col<rat::fltp> Cp = mat->calc_specific_heat(T)%mat->calc_density(T); // J/(m^3 K)

	// calculate resistivity per unit length
	const arma::Col<rat::fltp> rho_ell = rho/area; // Ohm/m

	// calculate specific heat per unit length
	const arma::Col<rat::fltp> Cp_ell = Cp*area; // J/(mK)

	// calculate power
	const arma::Col<rat::fltp> P_ell = rho_ell%I%I; // W/m

	// change of temperature
	const arma::Col<rat::fltp> dTdt = P_ell/Cp_ell; // K/s

	// change of time
	const arma::Col<rat::fltp> dt = temperature_step_size/dTdt; // s

	// integrate time steps
	const arma::Col<rat::fltp> t = arma::cumsum(dt); // s

	// calculate Miits
	const arma::Col<rat::fltp> iis = rat::cmn::Extra::cumtrapz(t.t(), (I%I).t(), 1).t();

	// write to table
	if(table_filename_argument.isSet()){
		arma::field<std::string> header(3);
		header(0) = "time";
		header(1) = "temperature";
		header(2) = "IIs";
		const arma::Mat<rat::fltp> M = arma::join_horiz(t,T,iis);
		M.save(arma::csv_name(table_filename_argument.getValue(), header));
	}else{
		// table header
		lg->msg("%12s %12s %12s\n","time","temperature","iits");

		// walk over steps
		for(arma::uword i=0;i<num_steps;i++){
			lg->msg("%12.4e %12.4e %12.4e\n",t(i),T(i),iis(i));
		}
	}

}

