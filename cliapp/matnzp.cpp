// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat common headers
#include "rat/common/node.hh"
#include "rat/common/extra.hh"

// rat material headers
#include "serializer.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"
#include "nzpmodel.hh"
#include "version.hh"

// string parse function
arma::Col<rat::fltp> parse_string(const std::string &str, const std::string delimiter){
	// keep track of position
	size_t pos = 0;
	
	// split string into tokens
	std::string s = str;
	std::list<std::string> token_list;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token_list.push_back(s.substr(0, pos));
		s.erase(0, pos + delimiter.length());
	}
	token_list.push_back(s);

	// convert tokens to values
	arma::Col<rat::fltp> values(token_list.size()); arma::uword i=0;
	for(auto it = token_list.begin();it!=token_list.end();it++,i++){
		values(i) = std::stof((*it));
	}

	// return values
	return values;
}


// main function
int main(int argc, char * argv[]){

	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> filename_argument(
		"fname","Input filename",true,
		"json/matprops/metals/CopperOFHC_RRR050.json",
		"string", cmd);

	// temperature input argument
	TCLAP::ValueArg<std::string> temperature_argument(
		"t","temperature","Input temperatures in [K]",
		false,"4.5,10,20,40,60,77", "string", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<std::string> field_argument(
		"b","fluxdensity","Input magnetic flux densities in [T]",
		false,"5,10,20,30", "string", cmd);

	// field angle input argument
	TCLAP::ValueArg<std::string> field_angle_argument(
		"a","fieldangle","Magnetic field angle in [deg]",
		false,"0,90", "string", cmd);

	// current density input argument
	TCLAP::ValueArg<std::string> current_density_argument(
		"j","curdens","Curent density in [A/mm^2]",
		false,"500,700,900", "string", cmd);

	// show numerical solutions
	TCLAP::SwitchArg numerical_switch_argument(
		"n","numerical","Use numerical calculations", cmd, false);

	// show numerical solutions
	TCLAP::SwitchArg verbose_switch_argument(
		"v","verbose","Output calculatoins to terminal", cmd, false);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// default settings
	arma::Col<rat::fltp> current_density = 1e6*arma::Col<rat::fltp>{900};
	arma::Col<rat::fltp> temperature = {1.9,4.5,10,20,30,40,50,60,77};
	arma::Col<rat::fltp> magnetic_field_magnitude = {5};
	arma::Col<rat::fltp> magnetic_field_angle = {0};

	// get temperature
	if(temperature_argument.isSet()){
		temperature = parse_string(temperature_argument.getValue(), ",");
	}
	
	// get magnetic field
	if(field_argument.isSet()){
		magnetic_field_magnitude = parse_string(field_argument.getValue(), ",");
	}

	// get magnetic field angle
	if(field_angle_argument.isSet()){
		magnetic_field_angle = 2*arma::Datum<rat::fltp>::pi*parse_string(field_angle_argument.getValue(), ",")/360;
	}

	// get magnetic field angle
	if(current_density_argument.isSet()){
		current_density = 1e6*parse_string(current_density_argument.getValue(), ",");
	}

	// use numerical
	const bool numerical = numerical_switch_argument.getValue();
	const bool verbose = verbose_switch_argument.getValue();

	// get file name
	boost::filesystem::path ifname = filename_argument.getValue();

	// construct conductor
	rat::mat::ShConductorPr mat = rat::mat::Serializer::create<rat::mat::Conductor>(ifname);

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// calculate number of datasets
	const arma::uword num_sets = 
		temperature.n_elem*
		current_density.n_elem*
		magnetic_field_magnitude.n_elem*
		magnetic_field_angle.n_elem;

	// allocate normal zone velocity
	arma::Col<rat::fltp> J(num_sets);
	arma::Col<rat::fltp> T(num_sets);
	arma::Col<rat::fltp> B(num_sets);
	arma::Col<rat::fltp> a(num_sets);

	// create list of combinations
	arma::uword cnt = 0;
	for(arma::uword l=0;l<current_density.n_elem;l++){
		for(arma::uword j=0;j<magnetic_field_magnitude.n_elem;j++){
			for(arma::uword k=0;k<magnetic_field_angle.n_elem;k++){
				for(arma::uword i=0;i<temperature.n_elem;i++){
					// get variables
					J(cnt) = current_density(l); T(cnt) = temperature(i);
					B(cnt) = magnetic_field_magnitude(j); a(cnt) = magnetic_field_angle(k);
					
					// increment counter
					cnt++;
				}
			}
		}
	}
	assert(cnt==num_sets);

	// allocate required properties
	arma::Col<rat::fltp> vnzp(num_sets);
	arma::Col<rat::fltp> t300(num_sets);
	arma::Col<rat::fltp> R200(num_sets);
	arma::Col<rat::fltp> MQE(num_sets);

	// check if numerical required
	if(numerical){
		for(arma::uword i=0;i<num_sets;i++){
			// table header
			if(verbose)lg->msg(2,"%sSolving NZP Model: %sJ=%4.0f [A/mm^2], B=%4.1f [T], a=%4.2f [deg], T=%4.1f [K]%s\n",
				KBLU,KYEL,1e-6*J(i),B(i),360*a(i)/(2*arma::Datum<rat::fltp>::pi),T(i),KNRM);

			// create model
			rat::mat::ShNZPModelPr nzp_model = rat::mat::NZPModel::create(mat); 

			// set conditions
			nzp_model->set_current_density(J(i)); 
			nzp_model->set_operating_temperature(T(i));
			nzp_model->set_magnetic_field_magnitude(B(i));
			nzp_model->set_magnetic_field_angle(a(i));
			nzp_model->set_tape_length(200e-3);
			nzp_model->set_element_size(2e-3);
			
			// solve model
			if(verbose)nzp_model->solve(lg); else nzp_model->solve();

			// get required values
			vnzp(i) = nzp_model->calc_velocity().back();
			t300(i) = nzp_model->calc_burn_time(100e-3,300);
			R200(i) = nzp_model->calc_resistance(200);
			MQE(i) = 0;

			// solving done
			if(verbose)lg->msg(-2);
		}
	}

	// theoretical values
	else{
		for(arma::uword i=0;i<num_sets;i++){
			// get required values
			vnzp(i) = mat->calc_theoretical_vnzp(J(i),T(i),B(i),a(i));
			MQE(i) = mat->calc_theoretical_mqe(J(i),T(i),B(i),a(i));
			t300(i) = mat->calc_adiabatic_time(300,J(i),T(i),B(i),a(i),2.0);
			R200(i) = 0;
		}
	}

	// create for loop
	lg->msg("Material Name: %s\n",mat->get_name().c_str());
	lg->msg("Analysis Table\n");

	// create header for table
	const arma::uword table_size = 95;
	lg->msg("%s%28s | %26s | %35s%s\n",KNRM,"parameters","fundamental","numerical/theoretical",KNRM);
	lg->msg("%s%7s %6s %6s %6s | %8s %8s %8s | %8s %8s %8s %8s%s\n",
		KNRM,"crdns.","flux","angle","temp","Je","Tcs","E","t300","Vnzp","R200","MQE",KNRM);
	lg->msg("%s%7s %6s %6s %6s | %8s %8s %8s | %8s %8s %8s %8s%s\n",
		KNRM,"A/mm^2","T","rad","K","A/mm^2","K","V/m","s","m/s","Ohm m^2","J m^-2",KNRM);

	// horizontal line
	lg->msg("%s=",KNRM); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"%s\n",KNRM);

	// walk over combinations
	for(arma::uword i=0;i<num_sets;i++){
		// calculate additional material properties
		const rat::fltp Je = mat->calc_critical_current_density(T(i),B(i),a(i));
		const rat::fltp Tcs = mat->calc_cs_temperature(J(i),B(i),a(i));
		const rat::fltp E = mat->calc_electric_field(J(i),T(i),B(i),a(i));

		// display properties
		lg->msg("%s%7.1f %6.2f %6.2f %6.2f | %8.2e %8.2e %8.2e | %8.2e %8.2e %8.2e %8.2e%s\n", 
			KNRM,1e-6*J(i), B(i), 360*a(i)/(2*arma::Datum<rat::fltp>::pi), T(i), Je, Tcs, E, t300(i), vnzp(i), R200(i), MQE(i), KNRM);
	}
	
	// horizontal line
	lg->msg("%s=",KNRM); for(arma::uword i=0;i<table_size-1;i++)lg->msg(0,"="); lg->msg(0,"%s\n",KNRM);

	// done
	return 0;
}