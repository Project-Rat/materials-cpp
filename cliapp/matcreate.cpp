// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// common headers
#include "rat/common/extra.hh"

// rat material headers
#include "serializer.hh"
#include "database.hh"
#include "version.hh"

// main function
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> json_path_argument(
		"od","Output dir",false,"./json", "string", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// database path
	boost::filesystem::path json_path = json_path_argument.getValue();

	// serializer
	const rat::mat::ShSerializerPr slzr = rat::mat::Serializer::create();

	// create directory
	boost::filesystem::create_directories(json_path);

	// list
	std::list<rat::mat::ShConductorPr> list;

	list.push_back(rat::mat::Database::copper_ofhc(50));
	list.push_back(rat::mat::Database::copper_ofhc(100));
	list.push_back(rat::mat::Database::copper_ofhc(150));
	list.push_back(rat::mat::Database::copper_ofhc(200));
	list.push_back(rat::mat::Database::copper_ofhc(300));
	list.push_back(rat::mat::Database::copper_ofhc(500));

	list.push_back(rat::mat::Database::brass_65_35());
	list.push_back(rat::mat::Database::stainless316());
	list.push_back(rat::mat::Database::aluminum5083O());
	list.push_back(rat::mat::Database::aluminum6061t6());
	list.push_back(rat::mat::Database::aluminumUNSA91100());
	list.push_back(rat::mat::Database::polyimide());

	list.push_back(rat::mat::Database::nbti_lhc());
	list.push_back(rat::mat::Database::nbti_wire_lhc());
	list.push_back(rat::mat::Database::nbti_twente());
	list.push_back(rat::mat::Database::nbti_wire_twente());
	list.push_back(rat::mat::Database::nbti_cct_cable());

	list.push_back(rat::mat::Database::nb3sn_rrp());
	list.push_back(rat::mat::Database::nb3sn_wire_rrp());
	list.push_back(rat::mat::Database::nb3sn_fresca2_cable(false));
	list.push_back(rat::mat::Database::nb3sn_fresca2_cable(true));
	list.push_back(rat::mat::Database::nb3sn_pit());
	
	list.push_back(rat::mat::Database::rebco_fujikura_cern());
	list.push_back(rat::mat::Database::rebco_fujikura_core_cern());
	list.push_back(rat::mat::Database::rebco_fujikura_tape_cern());

	list.push_back(rat::mat::Database::rebco_fujikura_fesc_rat());
	list.push_back(rat::mat::Database::rebco_fujikura_fesc_core_rat());
	list.push_back(rat::mat::Database::rebco_fujikura_fesc_tape_rat());

	list.push_back(rat::mat::Database::rebco_theva_proline_rat());
	list.push_back(rat::mat::Database::rebco_theva_proline_core_rat());
	list.push_back(rat::mat::Database::rebco_theva_proline_tape_rat());

	list.push_back(rat::mat::Database::rebco_superpower_adv_pinning_rat());
	list.push_back(rat::mat::Database::rebco_superpower_adv_pinning_core_rat());
	list.push_back(rat::mat::Database::rebco_superpower_adv_pinning_tape_rat());

	list.push_back(rat::mat::Database::rebco_theva_cern());
	list.push_back(rat::mat::Database::rebco_theva_core_cern());
	list.push_back(rat::mat::Database::rebco_theva_tape_cern());

	list.push_back(rat::mat::Database::rebco_shanghai_cern());
	list.push_back(rat::mat::Database::rebco_shanghai_core_cern());
	list.push_back(rat::mat::Database::rebco_shanghai_tape_cern());

	list.push_back(rat::mat::Database::rebco_shanghai_high_field_rat());
	list.push_back(rat::mat::Database::rebco_shanghai_high_field_core_rat());
	list.push_back(rat::mat::Database::rebco_shanghai_high_field_tape_rat());

	list.push_back(rat::mat::Database::rebco_faraday_rat());
	list.push_back(rat::mat::Database::rebco_faraday_core_rat());
	list.push_back(rat::mat::Database::rebco_faraday_tape_rat());

	list.push_back(rat::mat::Database::rebco_superox_ybco_rat());
	list.push_back(rat::mat::Database::rebco_superox_ybco_core_rat());
	list.push_back(rat::mat::Database::rebco_superox_ybco_tape_rat());

	list.push_back(rat::mat::Database::g10());
	list.push_back(rat::mat::Database::g10_normal());
	list.push_back(rat::mat::Database::g10_warp());

	list.push_back(rat::mat::Database::indium());
	list.push_back(rat::mat::Database::silver());


	// walk over list
	for(auto it = list.begin();it!=list.end();it++){	
		// get conductor
		const rat::mat::ShConductorPr& mat = *it;

		// create path
		const boost::filesystem::path pth = json_path/"materials"/mat->get_classification();

		// create directory
		boost::filesystem::create_directories(pth);

		// get name and replace spaces
		std::string name = mat->get_name();
		std::replace(name.begin(), name.end(), ' ', '_'); 

		// serialize
		slzr->flatten_tree(mat);
		slzr->export_json(pth/(name+".json"));
	}

}