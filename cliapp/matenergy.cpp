// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// command line parser
#include <tclap/CmdLine.h>

// rat common headers
#include "rat/common/log.hh"
#include "rat/common/extra.hh"

// rat material headers
#include "conductor.hh"
#include "serializer.hh"
#include "version.hh"

// main function
int main(int argc, char * argv[]){
	// create tclap object
	TCLAP::CmdLine cmd("Rat Materials Library",' ',rat::cmn::Extra::create_version_tag(
		MAT_PROJECT_VER_MAJOR,MAT_PROJECT_VER_MINOR,MAT_PROJECT_VER_PATCH));

	// filename input argument
	TCLAP::UnlabeledValueArg<std::string> input_filename_argument(
		"ifname","Input filename",true,
		"json/matprops/metals/CopperOFHC_RRR050.json",
		"string", cmd);

	// temperature input argument
	TCLAP::ValueArg<rat::fltp> t1_argument(
		"","t1","lower temperature [K]",
		true,4.5,"double", cmd);

	// magnetic field input argument
	TCLAP::ValueArg<rat::fltp> t2_argument(
		"","t2","upper temperature [K]",
		true,20.0,"double", cmd);

	// parse the command line arguments
	cmd.parse(argc, argv);

	// get values
	const boost::filesystem::path ifname = input_filename_argument.getValue();
	const rat::fltp t1 = t1_argument.getValue();
	const rat::fltp t2 = t2_argument.getValue();

	// 
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// deserializer file
	rat::mat::ShConductorPr mat = rat::mat::Serializer::create<rat::mat::Conductor>(ifname);

	// show energy density
	lg->msg("Energy Density: %8.3e [J/m^3]\n",
		mat->calc_thermal_energy_density(t2) - 
		mat->calc_thermal_energy_density(t1));
}