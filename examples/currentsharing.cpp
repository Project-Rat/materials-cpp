// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

#include "rat/common/node.hh"

#include "serializer.hh"
#include "constfit.hh"
#include "parallelconductor.hh"
#include "baseconductor.hh"
#include "htscore.hh"
#include "htstape.hh"
#include "database.hh"

// main function
int main(){

	// Settings
	const rat::fltp nvalue = 20;
	const rat::fltp wcore = 12.00e-3;
	const rat::fltp wtape = 12.04e-3;
	const rat::fltp tsubstr = 50e-6;
	const rat::fltp tsc = 2e-6;
	const rat::fltp tcore = 60e-6;
	const rat::fltp ttape = 100e-6;

	// construct individual layers
	rat::mat::ShSuperConductorPr sc = rat::mat::Database::rebco_fujikura_cern();
	rat::mat::ShBaseConductorPr stab = rat::mat::Database::copper_ofhc(100);
	rat::mat::ShBaseConductorPr substr = rat::mat::Database::stainless316();
	
	// construct core and tape
	rat::mat::ShHTSCorePr core = rat::mat::HTSCore::create(
		wcore,tcore,tsc,sc,tsubstr,substr);
	rat::mat::ShHTSTapePr tape = rat::mat::HTSTape::create(
		wtape,ttape,core,stab);

	// override Nvalue
	sc->set_nvalue_fit(rat::mat::ConstFit::create(nvalue));

	rat::mat::ShConductorPr matcpy = rat::mat::Serializer::create()->copy(tape);

	// calculate electric field
	arma::uword num_step = 20;
	const arma::Col<rat::fltp> J = arma::linspace<arma::Col<rat::fltp> >(0,2000,num_step);
	const arma::Col<rat::fltp> T = arma::linspace<arma::Col<rat::fltp> >(20,20,num_step);
	const arma::Col<rat::fltp> B = arma::linspace<arma::Col<rat::fltp> >(5,5,num_step);
	const arma::Col<rat::fltp> a = arma::linspace<arma::Col<rat::fltp> >(0,0,num_step);
	const arma::Col<rat::fltp> fsc(num_step,arma::fill::ones);

	const arma::Col<rat::fltp> E = matcpy->calc_electric_field(1e6*J,T,B,a,fsc);
	const rat::fltp Esc = matcpy->calc_electric_field(1e6*J(10),T(10),B(10),a(10),fsc(10));

	std::cout<<E<<Esc<<std::endl;

	std::cout<<matcpy->calc_critical_current_density(T,B,a,fsc);
	std::cout<<matcpy->calc_critical_current_density(T,B,a,fsc);

	// calculate current density in each layer
	const arma::Col<rat::fltp> Isc = wcore*tsc*sc->calc_current_density(E,T,B,a,fsc);
	const arma::Col<rat::fltp> Inc = (wtape*ttape - wcore*tcore)*stab->calc_current_density(E,T,B,a,fsc) + tsubstr*wcore*substr->calc_current_density(E,T,B,a,fsc);
	
	std::cout<<arma::join_horiz(J,E,Isc,Inc)<<std::endl;

	return 0;
}