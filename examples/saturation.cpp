// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// DESCRIPTION
// Minimalist example of a solenoid model

#include "rat/common/node.hh"

#include "serializer.hh"
#include "constfit.hh"
#include "parallelconductor.hh"
#include "baseconductor.hh"
#include "htscore.hh"
#include "htstape.hh"
#include "database.hh"

// main function
int main(){

	// Settings
	const rat::fltp nvalue = 20;
	const rat::fltp wcore = 12.00e-3;
	const rat::fltp wtape = 12.04e-3;
	const rat::fltp tsubstr = 50e-6;
	const rat::fltp tsc = 2e-6;
	const rat::fltp tcore = 60e-6;
	const rat::fltp ttape = 100e-6;

	// construct individual layers
	rat::mat::ShSuperConductorPr sc = rat::mat::Database::rebco_fujikura_cern();
	rat::mat::ShBaseConductorPr stab = rat::mat::Database::copper_ofhc(100);
	rat::mat::ShBaseConductorPr substr = rat::mat::Database::stainless316();
	
	// construct core and tape
	rat::mat::ShHTSCorePr core = rat::mat::HTSCore::create(
		wcore,tcore,tsc,sc,tsubstr,substr);
	rat::mat::ShHTSTapePr tape = rat::mat::HTSTape::create(
		wtape,ttape,core,stab);

	// override Nvalue
	sc->set_nvalue_fit(rat::mat::ConstFit::create(nvalue));




	rat::fltp radius = 0.025; // radius
	rat::fltp ell = 2*arma::Datum<rat::fltp>::pi*radius;
	rat::fltp V = 0.5e-3/266;

	rat::fltp J1 = tape->calc_current_density(V/ell, 20, 10, 0, 1.0, rat::mat::Direction::LONGITUDINAL);
	rat::fltp J2 = tape->calc_current_density(V/tape->get_thickness(), 20, 10, 0, 1.0, rat::mat::Direction::NORMAL);

	rat::fltp Jc1 = tape->calc_critical_current_density(20, 10, 0, 1.0, rat::mat::Direction::LONGITUDINAL);

	rat::fltp I1 = J1*tape->get_thickness()*tape->get_width();
	rat::fltp Ic1 = Jc1*tape->get_thickness()*tape->get_width();
	rat::fltp I2 = J2*tape->get_width()*ell;

	std::cout<<I1<<std::endl;
	std::cout<<Ic1<<std::endl;
	std::cout<<I2<<std::endl;



	return 0;
}