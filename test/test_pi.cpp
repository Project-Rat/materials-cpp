// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "database.hh"
#include "partialinsulation.hh"

// main
int main(){
	// copper
	const rat::fltp RRR = RAT_CONST(50.0); // [#]
	const rat::fltp required_voltage = RAT_CONST(1000.0); // [V]
	const rat::fltp operating_current = RAT_CONST(10000.0); // [A]
	const rat::fltp num_turns = RAT_CONST(100.0); // [#]
	const rat::fltp turn_length = RAT_CONST(5.0); // [m]
	const rat::fltp cable_width = RAT_CONST(12e-3); // [m]
	const rat::fltp dinsu = RAT_CONST(0.2e-3); // [m]
	const rat::fltp temperature = RAT_CONST(50.0); // [K]
	const rat::fltp field_magnitude = RAT_CONST(10.0); // [T]
	const rat::fltp tolerance = RAT_CONST(1e-5);

	// create base
	const rat::mat::ShBaseConductorPr base = rat::mat::Database::copper_ofhc(RRR);

	// create PI
	const rat::mat::ShPartialInsulationPr pi = rat::mat::PartialInsulation::create(
		base, required_voltage, operating_current, num_turns, turn_length,
		cable_width, dinsu, temperature, field_magnitude);

	// calculate 
	const rat::fltp rho_pi = pi->calc_electrical_resistivity(temperature, field_magnitude, rat::mat::Direction::NORMAL);
	const rat::fltp calculated_voltage = num_turns*operating_current*rho_pi*dinsu/(cable_width*turn_length);

	// check 
	if(std::abs(calculated_voltage-required_voltage)/std::abs(required_voltage)>tolerance)
		rat_throw_line("calculated voltage is outside of tolerance");

	// insulation
	if(pi->is_insulator(rat::mat::Direction::LONGITUDINAL)==false)
		rat_throw_line("longitudinal direction must be insulating");
	if(pi->is_insulator(rat::mat::Direction::NORMAL)==true)
		rat_throw_line("normal direction must be electrically conductive");
	if(pi->is_insulator(rat::mat::Direction::TRANSVERSE)==false)
		rat_throw_line("transverse direction must be insulating");
}