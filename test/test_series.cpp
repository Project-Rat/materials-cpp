// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "database.hh"
#include "seriesconductor.hh"

// main
int main(){
	// settings
	arma::uword num_iter = 20;
	const rat::fltp area = 1e-4;
	const rat::fltp current = -200.0;
	const rat::fltp temperature = 20.0;
	const rat::fltp magnetic_field_magnitude = 5.0;
	const rat::fltp magnetic_field_angle = 0.0;
	const rat::fltp scaling_factor = 1.0;
	const rat::mat::Direction dir = rat::mat::Direction::LONGITUDINAL;
	const rat::fltp tol = 1e-5;

	// create parallel material to test
	const rat::mat::ShSeriesConductorPr sc = 
		rat::mat::SeriesConductor::create(
		0.9,rat::mat::Database::copper_ofhc(100), 
		0.1,rat::mat::Database::brass_65_35());

	// calculate current density
	rat::fltp current_density = current/area;
	rat::fltp electric_field;

	// calculate current density
	arma::Col<rat::fltp> current_density_vec{current/area};
	arma::Col<rat::fltp> electric_field_vec;

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// keep converting
	for(arma::uword i=0;i<num_iter;i++){

		// calculate electric field
		electric_field = sc->calc_electric_field(
			current_density, temperature, magnetic_field_magnitude,
			magnetic_field_angle, scaling_factor, dir);

		// calculate current density
		current_density = sc->calc_current_density(
			electric_field, temperature, magnetic_field_magnitude,
			magnetic_field_angle, scaling_factor, dir);

		// calculate electric field
		electric_field_vec = sc->calc_electric_field(
			current_density_vec, {temperature}, {magnetic_field_magnitude},
			{magnetic_field_angle}, {scaling_factor}, dir);

		// calculate current density
		current_density_vec = sc->calc_current_density(
			electric_field_vec, {temperature}, {magnetic_field_magnitude},
			{magnetic_field_angle}, {scaling_factor}, dir);

		// calculate error
		const rat::fltp eps_scalar = std::abs((current_density - current/area)/(current/area));
		const rat::fltp eps_vector = std::abs((arma::as_scalar(current_density_vec) - current/area)/(current/area));

		// display error
		lg->msg("%6.2e %6.2e\n",eps_scalar,eps_vector);

		// check
		if(eps_scalar>tol)rat_throw_line("could not achieve tolerance for scalar");
		if(eps_vector>tol)rat_throw_line("could not achieve tolerance for vector");

	}
}