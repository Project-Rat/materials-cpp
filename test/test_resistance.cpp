// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "database.hh"

// main
int main(){
	const rat::fltp tol = 1e-3;
	const rat::fltp current = 1.0;
	const rat::fltp temperature = 20;
	const rat::fltp magnetic_field_magnitude = 0;
	const rat::fltp magnetic_field_angle = 0;
	const rat::fltp critical_current_scaling = 1.0;
	const rat::mat::Direction dir = rat::mat::Direction::NORMAL;

	// create hts tape
	const rat::mat::ShHTSTapePr htstape = rat::mat::Database::rebco_fujikura_tape_cern();

	// calculate current density
	const rat::fltp current_density = current/htstape->get_area(); 

	// calculate electric field from material
	const rat::fltp electric_field1 = 
		htstape->calc_electric_field(
		current_density, temperature, 
		magnetic_field_magnitude, magnetic_field_angle,
		critical_current_scaling,dir);

	// calculate resistivity
	const rat::fltp resistivity = 
		htstape->calc_electrical_resistivity(
		temperature, magnetic_field_magnitude,dir);

	// calculate from resistivity
	const rat::fltp electric_field2 = resistivity*current_density;

	// check 
	if(std::abs(electric_field1 - electric_field2)/electric_field2>tol)
		rat_throw_line("accuracy not achieved");
}