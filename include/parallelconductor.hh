// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_PARALLEL_CONDUCTOR_HH
#define MAT_PARALLEL_CONDUCTOR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"
#include "rat/common/typedefs.hh"

#include "conductor.hh"
#include "conductorlist.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ParallelConductor> ShParallelConductorPr;

	// template for materials
	class ParallelConductor: public Conductor, public ConductorList{
		// methods
		public:
			// constructor
			ParallelConductor();
			ParallelConductor(
				const fltp fraction, const ShConductorPr& conductor);
			ParallelConductor(
				const fltp fraction1, const ShConductorPr& conductor1, 
				const fltp fraction2, const ShConductorPr& conductor2);
			explicit ParallelConductor(
				const FracShConPrList &conductor_list);

			// factory
			static ShParallelConductorPr create();
			static ShParallelConductorPr create(
				const fltp fraction, const ShConductorPr& conductor);
			static ShParallelConductorPr create(
				const fltp fraction1, const ShConductorPr& conductor1, 
				const fltp fraction2, const ShConductorPr& conductor2);
			static ShParallelConductorPr create(
				const FracShConPrList &conductor_list);

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			fltp calc_specific_heat(
				const fltp temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			fltp calc_thermal_conductivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 
			
			// electrical resistivity [Ohm m^2]
			fltp calc_electrical_resistivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// is insulating
			bool is_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_thermal_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_anisotropic()const override;

			// critical current density [J m^-2]
			virtual fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			fltp calc_nvalue(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			fltp calc_density(
				const fltp temperature) const override;

			// electric field [V m^-1]
			fltp calc_electric_field(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V m^-1]
			// fltp calc_electric_field_fast(
			// 	const fltp current_density,
			// 	const arma::Row<fltp> &electrical_resistivity,
			// 	const arma::Row<fltp> &critical_current_density,
			// 	const arma::Row<fltp> &nvalue,
			// 	const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			fltp calc_current_density(
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			fltp calc_current_density(
				const FracConPrList& parallel_conductors,
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor,
				const Direction dir) const;

			// // calculate current density [A m^-2]
			// fltp calc_current_density_fast(
			// 	const fltp electric_field,
			// 	const arma::Row<fltp> &electrical_resistivity,
			// 	const arma::Row<fltp> &critical_current_density,
			// 	const arma::Row<fltp> &nvalue,
			// 	const Direction dir = Direction::LONGITUDINAL) const override;



			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Col<fltp> calc_thermal_conductivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 
			
			// electrical resistivity [Ohm m^2]
			arma::Col<fltp> calc_electrical_resistivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			arma::Col<fltp> calc_nvalue(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			arma::Col<fltp> calc_density(
				const arma::Col<fltp> &temperature) const override;

			// electric field [V/m]
			arma::Col<fltp> calc_electric_field(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // electric field [V m^-1] section averaged
			// // required to supply the cross-sectional areas of the
			// // elements making up a section in the same order as
			// // the other properties
			// arma::Col<fltp> calc_electric_field_av(
			// 	const arma::Col<fltp> &current_density,
			// 	const arma::Col<fltp> &temperature,
			// 	const arma::Col<fltp> &magnetic_field_magnitude,
			// 	const arma::Col<fltp> &magnetic_field_angle,
			// 	const arma::Col<fltp> &scaling_factor,
			// 	const arma::Mat<arma::uword> &nelements,
			// 	const arma::Row<fltp> &cross_areas,
			// 	const Direction dir = Direction::LONGITUDINAL) const override;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V/m]
			// arma::Col<fltp> calc_electric_field_fast(
			// 	const arma::Col<fltp> &current_density,
			// 	const arma::Mat<fltp> &electrical_resistivity,
			// 	const arma::Mat<fltp> &critical_current_density, 
			// 	const arma::Mat<fltp> &nvalue,
			// 	const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			arma::Col<fltp> calc_current_density(
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			// with list of parallel conductors
			arma::Col<fltp> calc_current_density(
				const FracConPrList& parallel_conductors,
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir) const;
			
			// // current density from electric field 
			// // output in [A m^-2]
			// arma::Col<fltp> calc_current_density_fast(
			// 	const arma::Col<fltp> &electric_field,
			// 	const arma::Mat<fltp> &electrical_resistivity,
			// 	const arma::Mat<fltp> &critical_current_density, 
			// 	const arma::Mat<fltp> &nvalue,
			// 	const Direction dir = Direction::LONGITUDINAL) const override;

			// get parallel conductors
			void accu_parallel_conductors(FracConPrList &parallel_conductors, const Direction dir) const override;


			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif