// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_WFL_FIT_HH
#define MAT_WFL_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class WFLFit> ShWFLFitPr;

	// fit rho with wiedemann franz law
	class WFLFit: public Fit{
		private: 
			// lorentz number
			const fltp lorentz_number_ = 2.44e-8; // [W Ohm K-2]

			// thermal conductivity fit
			ShFitPr krho_fit_;

		public:
			// constructor
			WFLFit();
			WFLFit(const ShFitPr &krho_fit);

			// factory
			static ShWFLFitPr create();
			static ShWFLFitPr create(const ShFitPr &krho_fit);

			// setting
			void set_fit(const ShFitPr &krho_fit);

			// getting
			const ShFitPr& get_fit()const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;
			fltp calc_property(const fltp temperature, const fltp magnetic_field_magnitude) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature, const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif