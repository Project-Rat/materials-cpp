// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_SERIALIZER_HH
#define MAT_SERIALIZER_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Serializer> ShSerializerPr;

	// use a class for storage
	class Serializer: virtual public rat::cmn::Serializer{
		public:
			// constructor
			Serializer();
			explicit Serializer(const boost::filesystem::path &fname);

			// factory
			static ShSerializerPr create();
			static ShSerializerPr create(const boost::filesystem::path &fname);

			// registry
			virtual void register_constructors() override;

			// instantiate conductor from file
			template<typename T> static std::shared_ptr<T> create(const boost::filesystem::path &fname);
	};

	// instantiate conductor from file
	template<typename T> std::shared_ptr<T> Serializer::create(const boost::filesystem::path &fname){
		ShSerializerPr slzr = Serializer::create(fname);
		return slzr->construct_tree<T>();
	}


}}

#endif