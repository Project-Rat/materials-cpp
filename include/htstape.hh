// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_HTS_TAPE_HH
#define MAT_HTS_TAPE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"
#include "htscore.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class HTSTape> ShHTSTapePr;

	// wrapper for representing HTS tape 
	// consisting of core and stabilizer
	class HTSTape: public Anisotropic{
		protected: 
			// flipped pair flag
			bool is_flipped_pair_ = false;
			
			// dimensions
			fltp width_ = RAT_CONST(12e-3);
			fltp thickness_ = RAT_CONST(100e-6);

			// number of corse
			arma::uword num_cores_ = 1;

			// core material
			ShHTSCorePr core_;

			// substrate material
			ShBaseConductorPr stab_;

			

		public:
			// constructor
			HTSTape();
			HTSTape(
				const fltp width, 
				const fltp thickness, 
				const ShHTSCorePr &core, 
				const ShBaseConductorPr &stab);

			// factory
			static ShHTSTapePr create();
			static ShHTSTapePr create(
				const fltp width, 
				const fltp thickness, 
				const ShHTSCorePr &core, 
				const ShBaseConductorPr &stab);

			// setters
			void set_width(const fltp width);
			void set_thickness(const fltp thickness);
			void set_is_flipped_pair(const bool is_flipped_pair = true);
			void set_core(const ShHTSCorePr &core);
			void set_stab(const ShBaseConductorPr &stab);
			void set_num_cores(const arma::uword num_cores);

			// getters
			fltp get_width() const;
			fltp get_thickness() const;
			bool get_is_flipped_pair()const;
			const ShHTSCorePr& get_core() const;
			const ShBaseConductorPr& get_stab() const;
			arma::uword get_num_cores() const;

			// calculated values
			fltp get_area() const;
			// fltp get_tcore() const;
			// fltp get_tstab() const;

			// setup function
			void setup();

			// critical current density [J m^-2]
			fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;
			
			// critical current density [J m^-2]
			arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// is superconductor
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif