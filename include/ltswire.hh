// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_LTS_WIRE_HH
#define MAT_LTS_WIRE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"
#include "superconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LTSWire> ShLTSWirePr;

	// wrapper for representing LTS wire
	class LTSWire: public ParallelConductor{
		protected: 
			// dimensions
			fltp fnc2sc_ = RAT_CONST(1.9);
			fltp diameter_ = RAT_CONST(1e-3);

			// superconducting material
			ShSuperConductorPr sc_;

			// normal conducting part such as stabilizer and/or matrix
			ShBaseConductorPr nc_;

		public:
			// constructor
			LTSWire();

			// factory
			static ShLTSWirePr create();

			// set layer conductors
			void set_sc(const ShSuperConductorPr &sc);
			void set_nc(const ShBaseConductorPr &nc);

			// get conductors
			const ShSuperConductorPr& get_sc() const;
			const ShBaseConductorPr& get_nc() const;

			// set dimensions
			void set_diameter(const fltp diameter);
			void set_fnc2sc(const fltp fnc2sc);

			// get dimensions
			fltp get_diameter() const;
			fltp get_fnc2sc() const;

			// setup function
			void setup();

			// is superconductor
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif