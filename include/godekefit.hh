// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Description:
// Calculates the critical current density of Nb3Sn using the A. God
// formula. The initial parameters can be set using two standard
// variants: Powder in Tube (PIT) and Rod-Restack Process (RRP). It must
// be noted that the formulas are not accurate in the low field region.

// references:
// A. Godeke, Msc. Thesis: Performance Boundaries in Nb3Sn Superconductors,
// Twente University 2005, p168

#ifndef MAT_GODEKE_FIT_HH
#define MAT_GODEKE_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class GodekeFit> ShGodekeFitPr;

	// Lubell/Kramer scaling relation
	class GodekeFit: public Fit{
		private:
			// deformation related parameters
			fltp Ca1_;
			fltp Ca2_;
			fltp eps_0a_; // normalized
			fltp C1_; // [kAT mm^-2]

			// superconducting parameters
			fltp Bc2m_; // [T]
			fltp Tcm_; // [K]

			// magnetic field dependence parameters
			fltp p_;
			fltp q_;

			// environmental parameters
			fltp eps_ax_ = 0; // axial strain

		public:
			// constructor
			GodekeFit();
			GodekeFit(
				const fltp Ca1,
				const fltp Ca2,
				const fltp eps_0a,
				const fltp C1,
				const fltp Bc2m,
				const fltp Tcm,
				const fltp p,
				const fltp q,
				const fltp eps_ax = 0);

			// factory
			static ShGodekeFitPr create();
			static ShGodekeFitPr create(
				const fltp Ca1,
				const fltp Ca2,
				const fltp eps_0a,
				const fltp C1,
				const fltp Bc2m,
				const fltp Tcm,
				const fltp p,
				const fltp q,
				const fltp eps_ax = 0);

			// property calculation with less input
			fltp calc_property(const fltp temperature) const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude) const override;

			// property calculation with less input
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif