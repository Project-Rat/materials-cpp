// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_LININTERP_FIT_HH
#define MAT_LININTERP_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LinInterpFit> ShLinInterpFitPr;

	// calculate rho from Linear
	class LinInterpFit: public Fit{
		// properties
		private:
			// table
			bool extrap_ = true;
			bool is_monotonic_ = true;
			arma::Col<fltp> temperatures_;
			arma::Col<fltp> values_;

		public:
			// constructor
			LinInterpFit();
			LinInterpFit(
				const arma::Col<fltp> &temperatures, 
				const arma::Col<fltp> &values);

			// factory
			static ShLinInterpFitPr create();
			static ShLinInterpFitPr create(
				const arma::Col<fltp> &temperatures, 
				const arma::Col<fltp> &values);

			// setters
			void set_temperatures(const arma::Col<fltp> &temperatures);
			void set_values(const arma::Col<fltp> &values);
			void set_is_monotonic(const bool is_monotonic);

			// getters
			const arma::Col<fltp>& get_temperatures()const;
			const arma::Col<fltp>& get_values()const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif