// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_SCALE_FIT_HH
#define MAT_SCALE_FIT_HH

#include "constfit.hh"
#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ScaleFit> ShScaleFitPr;

	// template for materials
	class ScaleFit: public Fit{
		// properties
		private:
			fltp scale_factor_ = 1.0;
			ShFitPr fit_;

		// methods
		public:
			// constructor
			ScaleFit();
			ScaleFit(const fltp scale_factor, const ShFitPr& fit);

			// factory
			static ShScaleFitPr create();
			static ShScaleFitPr create(const fltp scale_factor, const ShFitPr& fit);

			// setters
			void set_scale_factor(const fltp scale_factor);
			void set_fit(const ShFitPr& fit);

			// getters
			fltp get_scale_factor()const;
			const ShFitPr& get_fit()const;

			// calculate property as function of scalar temperature
			virtual fltp calc_property(const fltp temperature) const override;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude) const override;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const override;

			// calculate property as function of vector temperature
			virtual arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// calculate property as function of vector
			// temperature and magnetic field magnitude
			virtual arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude) const override;
			
			// calculate property as function of vector
			// temperature, magnetic field magnitude and angle
			virtual arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle) const override;
			

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif