// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_NZPMODEL_HH
#define MAT_NZPMODEL_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"

#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class NZPModel> ShNZPModelPr;
	typedef arma::field<ShNZPModelPr> ShNZPModelPrList;

	// toy normal zone propagation model
	class NZPModel{
		private:
			// the to-be analysed conductor
			ShConductorPr conductor_;

			// operating conditions
			fltp current_density_ = 900e6; // [A m^-2]
			fltp magnetic_field_magnitude_ = 5; // [T]
			fltp magnetic_field_angle_ = 0; // [rad]
			fltp operating_temperature_ = 20; // [K]

			// geometry
			fltp tape_length_ = 0.15; // [m]
			fltp element_size_ = 1e-3; // [m]
			
			// time stepping 
			fltp max_temperature_step_ = 10.0; // [K]
			fltp max_time_step_ = 0.1; // [s]
			fltp timestep_ini_ = 1e-3; // [s]
			fltp abs_tol_ = 1e-4; // [K]
			arma::uword max_iter_ = 50;

			// stop conditions
			fltp max_temperature_ = 500.0;
			fltp max_time_ = 4.0;

			// parallel option
			bool use_parallel_ = true;

			// temperature profile data
			arma::Col<fltp> position_vec_;
			std::list<std::pair<fltp,arma::Col<fltp> > > nzp_data_;


		// methods
		public:
			// constructor
			NZPModel();
			explicit NZPModel(const ShConductorPr& conductor);

			// factory
			static ShNZPModelPr create();
			static ShNZPModelPr create(const ShConductorPr& conductor);

			// setters
			void set_conductor(const ShConductorPr& conductor);

			// set conditions
			void set_current_density(const fltp current_density);
			void set_magnetic_field_magnitude(const fltp magnetic_field_magnitude);
			void set_magnetic_field_angle(const fltp magnetic_field_angle);
			void set_operating_temperature(const fltp operating_temperature);
			void set_element_size(const fltp element_size);
			void set_tape_length(const fltp tape_length);

			// solve system
			void solve(cmn::ShLogPr lg = cmn::NullLog::create());

			// system function to solve
			arma::Col<fltp> bdf_system_fun(
				const arma::Col<fltp> &temperature_prev,
				const arma::Col<fltp> &temperature_next,
				const fltp dt) const;

			// calculate normal zone propagation velocity
			arma::Col<fltp> calc_velocity() const;
			arma::Col<fltp> calc_voltage() const;
			arma::Col<fltp> calc_resistance() const;
			arma::Col<fltp> calc_peak_temperature() const;
			arma::Col<fltp> get_times() const;
			fltp calc_resistance(const fltp peak_temperature) const;
			fltp calc_burn_time(const fltp voltage_treshold, const fltp temperature_limit) const;

	};

}}

#endif