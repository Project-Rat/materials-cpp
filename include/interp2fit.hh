// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_INTERP2_FIT_HH
#define MAT_INTERP2_FIT_HH

#include <memory>

#include "fit.hh"

namespace rat{namespace mat{

	typedef std::shared_ptr<class Interp2Fit> ShInterp2FitPr;

	class Interp2Fit: public Fit {

		// properties
		protected:
			bool extrap_ = false;
			bool use_fast_ = true;
			arma::Col<fltp> x_;
			arma::Col<fltp> y_;
			arma::Mat<fltp> v_;

		public:
			// methods
			// constructor
			Interp2Fit();
			Interp2Fit(
				const arma::Col<fltp>& x, 
				const arma::Col<fltp>& y, 
				const arma::Mat<fltp>& v);

			// factory
			static ShInterp2FitPr create();
			static ShInterp2FitPr create(
				const arma::Col<fltp>& x, 
				const arma::Col<fltp>& y, 
				const arma::Mat<fltp>& v);

			// setters
			void set_table(const arma::Mat<fltp>& tab);
			void set_x(const arma::Col<fltp>& x);
			void set_y(const arma::Col<fltp>& y);
			void set_v(const arma::Mat<fltp>& v);
			void set_use_fast(const bool use_fast = true);
			bool is_linspaced()const;

			// overrides
			fltp calc_property(
				const fltp x, 
				const fltp y) const override;
			arma::Col<fltp> calc_property(
				const arma::Col<fltp>& x, 
				const arma::Col<fltp>& y) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}} // namespace rat::mat

#endif
