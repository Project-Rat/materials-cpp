// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_FLEITER_FIT_HH
#define MAT_FLEITER_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class FleiterFit> ShFleiterFitPr;

	// fixed critical current density
	class FleiterFit: public Fit{
		private:
			// typical layer thickness
			fltp tsc_;

			// general parameters
			fltp Tc0_; // [K]
			fltp n_;
			fltp n1_;
			fltp n2_;

			// Parameters for ab-plane
			fltp pab_;
			fltp qab_;
			fltp Bi0ab_; // [T]
			fltp a_;
			fltp gammaab_;
			fltp alphaab_; // [AT/m^2]

			// Parameters for c-plane
			fltp pc_;
			fltp qc_;
			fltp Bi0c_; // [T]
			fltp gammac_;
			fltp alphac_; // [AT/m^2]

			// Parameters for anisotropy
			fltp g0_;
			fltp g1_;
			fltp g2_;
			fltp g3_;
			fltp nu_;
			fltp pkoff_;

			// type-0 pair flipping
			bool type0_flip_;
			bool ignore_pkoff_;
			bool force_perpendicular_ = false;
			bool force_parallel_ = false;
			bool disable_clamping_ = false;
			
		public:
			// constructor
			FleiterFit();

			// factory
			static ShFleiterFitPr create();

			// property calculation with less input
			fltp calc_property(const fltp temperature) const override;
			fltp calc_property(const fltp temperature, const fltp magnetic_field_magnitude) const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const override;

			// property calculation with less input
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature, const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle) const override;

			// typical layer thickness
			void set_tsc(const fltp tsc);

			// general parameters
			void set_Tc0(const fltp Tc0);
			void set_n(const fltp n);
			void set_n1(const fltp n1);
			void set_n2(const fltp n2);

			// Parameters for ab-plane
			void set_pab(const fltp pab);
			void set_qab(const fltp qab);
			void set_Bi0ab(const fltp Bi0ab);
			void set_a(const fltp a);
			void set_gammaab(const fltp gammaab);
			void set_alphaab(const fltp alphaab);

			// Parameters for c-plane
			void set_pc(const fltp pc);
			void set_qc(const fltp qc);
			void set_Bi0c(const fltp Bi0c);
			void set_gammac(const fltp gammac);
			void set_alphac(const fltp alphac);

			// Parameters for anisotropy
			void set_g0(const fltp g0);
			void set_g1(const fltp g1);
			void set_g2(const fltp g2);
			void set_g3(const fltp g3);
			void set_nu(const fltp nu);
			void set_pkoff(const fltp pkoff);

			// type-0 pair flipping
			void set_type0_flip(const bool type0_flip);
			void set_ignore_pkoff(const bool ignore_pkoff);
			void set_force_perpendicular(const bool force_perpendicular =  true);
			void set_force_parallel(const bool force_parallel =  true);
			void set_disable_clamping(const bool disable_clamping = true);

			// typical layer thickness
			fltp get_tsc() const;

			// general parameters
			fltp get_Tc0() const;
			fltp get_n() const;
			fltp get_n1() const;
			fltp get_n2() const;

			// Parameters for ab-plane
			fltp get_pab() const;
			fltp get_qab() const;
			fltp get_Bi0ab() const;
			fltp get_a() const;
			fltp get_gammaab() const;
			fltp get_alphaab() const;

			// Parameters for c-plane
			fltp get_pc() const;
			fltp get_qc() const;
			fltp get_Bi0c() const;
			fltp get_gammac() const;
			fltp get_alphac() const;

			// Parameters for anisotropy
			fltp get_g0() const;
			fltp get_g1() const;
			fltp get_g2() const;
			fltp get_g3() const;
			fltp get_nu() const;
			fltp get_pkoff() const;

			// type-0 pair flipping
			bool get_type0_flip() const;
			bool get_ignore_pkoff() const;
			bool get_force_perpendicular()const;
			bool get_force_parallel()const;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif