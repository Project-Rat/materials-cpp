// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_LINEAR_FIT_HH
#define MAT_LINEAR_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LinearFit> ShLinearFitPr;

	// calculate rho from Linear
	class LinearFit: public Fit{
		// properties
		private:
			// fit coefficients
			fltp C0_; // y-intesect
			fltp C1_; // slope

			// temperature range
			arma::Col<fltp>::fixed<2> fit_temperature_range_;

		public:
			// constructor
			LinearFit();
			LinearFit(const fltp C0, const fltp C1);
			// LinearFit(const fltp t0, const fltp t1, const fltp C0, const fltp C1);

			// factory
			static ShLinearFitPr create();
			static ShLinearFitPr create(const fltp C0, const fltp C1);
			// static ShLinearFitPr create(const fltp t0, const fltp t1, const fltp C0, const fltp C1);

			// setters
			void set_C0(const fltp C0);
			void set_C1(const fltp C1);
			
			// getters
			fltp get_C0()const;
			fltp get_C1()const;

			// fit functions
			fltp calc_property(const fltp myX) const override;
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &myX) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif