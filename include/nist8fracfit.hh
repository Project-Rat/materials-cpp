// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_NIST8FRAC_FIT_HH
#define MAT_NIST8FRAC_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Nist8FracFit> ShNist8FracFitPr;

	// template for materials
	class Nist8FracFit: public Fit{
		// properties
		private:
			// coefficients of fit
			arma::Col<fltp>::fixed<9> fit_parameters_; // abcdefghi

			// temperature range
			arma::Col<fltp>::fixed<2> fit_temperature_range_;

		// methods
		public:
			// virtual destructor
			Nist8FracFit();
			Nist8FracFit(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters);

			// factory
			static ShNist8FracFitPr create();
			static ShNist8FracFitPr create(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters);

			// set fit parameters
			void set_fit_parameters(const arma::Col<fltp> &fit_parameters);
			void set_fit_parameters(const arma::uword index, const fltp value);

			// get fit parameters
			fltp get_fit_parameters(const arma::uword index)const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif