// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CONDUCTOR_HH
#define MAT_CONDUCTOR_HH

#include <memory>
#include <cassert>
#include <armadillo>

#include "rat/common/log.hh"
#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Conductor> ShConductorPr;
	typedef arma::field<ShConductorPr> ShConductorPrList;

	// typedef for list containing both fraction and conductor
	typedef std::pair<fltp, ShConductorPr> FracShConPr;
	typedef std::list<FracShConPr> FracShConPrList;
	typedef std::pair<fltp, const Conductor*> FracConPr;
	typedef std::list<FracConPr> FracConPrList;

	// direction
	enum class Direction{LONGITUDINAL,NORMAL,TRANSVERSE};

	// template for materials
	class Conductor: virtual public cmn::Node{
		// properties
		protected:
			// classification for output files
			std::string classification_ = "none";

			// tolerance for bisection method
			fltp abstol_bisection_;
			fltp reltol_bisection_;
			arma::uword bisection_max_iter_;

		// methods
		public:
			// constructor
			Conductor();

			// virtual destructor
			~Conductor(){};

			// cathegorization
			void set_classification(const std::string &classification);
			std::string get_classification() const;

			// set bisection tolerance
			void set_abstol_bisection(const fltp tol_bisection);

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual fltp calc_specific_heat(
				const fltp temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual fltp calc_thermal_conductivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const = 0; 
			
			// electrical resistivity [Ohm m]
			virtual fltp calc_electrical_resistivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// is insulating
			virtual bool is_insulator(const Direction dir = Direction::LONGITUDINAL)const = 0;
			virtual bool is_thermal_insulator(const Direction dir = Direction::LONGITUDINAL)const = 0;
			virtual bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const = 0;
			virtual bool is_anisotropic()const{return false;};

			// critical current density [J m^-2]
			virtual fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// critical current density [J m^-2]
			virtual fltp calc_nvalue(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// material density [kg m^-3]
			virtual fltp calc_density(
				const fltp temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual fltp calc_volumetric_specific_heat(
				const fltp temperature) const;

			// electric field [V/m]
			virtual fltp calc_electric_field(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V/m]
			// virtual fltp calc_electric_field_fast(
			// 	const fltp current_density,
			// 	const arma::Row<fltp> &electrical_resistivity_vec,
			// 	const arma::Row<fltp> &critical_current_density_vec,
			// 	const arma::Row<fltp> &nvalue_vec,
			// 	const Direction dir = Direction::LONGITUDINAL) const = 0;
			
			// calculate current density [A m^-2]
			virtual fltp calc_current_density(
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// // calculate current density [A m^-2]
			// virtual fltp calc_current_density_fast(
			// 	const fltp electric_field,
			// 	const arma::Row<fltp> &electrical_resistivity_vec,
			// 	const arma::Row<fltp> &critical_current_density_vec,
			// 	const arma::Row<fltp> &nvalue_vec,
			// 	const Direction dir = Direction::LONGITUDINAL) const = 0;

			// calculate the critical temperature 
			// output in [K]
			virtual fltp calc_cs_temperature(
				const fltp current_density, 
				const fltp magnetic_field_magnitude, 
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 1e-3) const;

			// calculate the upper critical field
			virtual fltp calc_upper_critical_field(
				const fltp temperature,
				const fltp current_density, 
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL,
				const fltp field_increment = 1.0,
				const fltp tol_bisection = 1e-3) const;

			// calculate partial derivative of electric field 
			// with temperature using finite difference method
			fltp calc_electric_field_dT(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL,
				const fltp delta_temperature = 0.1) const;


			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual arma::Col<fltp> calc_thermal_conductivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const = 0; 
			
			// electrical resistivity [Ohm m]
			virtual arma::Col<fltp> calc_electrical_resistivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_nvalue(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// material density [kg m^-3]
			virtual arma::Col<fltp> calc_density(
				const arma::Col<fltp> &temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual arma::Col<fltp> calc_volumetric_specific_heat(
				const arma::Col<fltp> &temperature) const;

			// electric field [V m^-1]
			virtual arma::Col<fltp> calc_electric_field(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V m^-1]
			// virtual arma::Col<fltp> calc_electric_field_fast(
			// 	const arma::Col<fltp> &current_density,
			// 	const arma::Mat<fltp> &electrical_resistivity_mat,
			// 	const arma::Mat<fltp> &critical_current_density_mat, 
			// 	const arma::Mat<fltp> &nvalue_mat,
			// 	const Direction dir = Direction::LONGITUDINAL) const = 0;

			// calculate current density [A m^-2]
			virtual arma::Col<fltp> calc_current_density(
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const = 0;

			// // current density from electric field 
			// // output in [A m^-2]
			// virtual arma::Col<fltp> calc_current_density_fast(
			// 	const arma::Col<fltp> &electric_field,
			// 	const arma::Mat<fltp> &electrical_resistivity_mat,
			// 	const arma::Mat<fltp> &critical_current_density_mat, 
			// 	const arma::Mat<fltp> &nvalue_mat,
			// 	const Direction dir = Direction::LONGITUDINAL) const = 0;

			// critical temperature calculation in [A m^-2]
			virtual arma::Col<fltp> calc_cs_temperature(
				const arma::Col<fltp> &current_density, 
				const arma::Col<fltp> &magnetic_field_magnitude, 
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 1e-3) const;

			// calculate partial derivative of electric field [V m^-1 K^-1]
			// with temperature using finite difference method
			arma::Col<fltp> calc_electric_field_dT(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp dT = 1e-2) const;

			// calculate percentage on the load-line in [pct]
			// assuming fixed magnetic field angle
			arma::Col<fltp> calc_percent_load(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature, 
				const arma::Col<fltp> &magnetic_field_magnitude, 
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp current_density_increment = 100e6,
				const fltp tol_bisection = 0.1) const;

			// calculate percentage on the load-line
			// assuming fixed magnetic field angle
			arma::Col<fltp> calc_percent_load_av(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature, 
				const arma::Col<fltp> &magnetic_field_magnitude, 
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp current_density_increment = 100e6,
				const fltp tol_bisection = 0.1) const;

			// material properties for matrices 
			// these are essentially wrappers
			// for the vector material properties
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Mat<fltp> calc_specific_heat_mat(
				const arma::Mat<fltp> &temperature) const;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Mat<fltp> calc_thermal_conductivity_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const; 
			
			// electrical resistivity [Ohm m]
			arma::Mat<fltp> calc_electrical_resistivity_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const;

			// critical current density [J m^-2]
			arma::Mat<fltp> calc_critical_current_density_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const;

			// critical current density [J m^-2]
			arma::Mat<fltp> calc_nvalue_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const;

			// material density [kg m^-3]
			arma::Mat<fltp> calc_density_mat(
				const arma::Mat<fltp> &temperature) const;

			// volumetric specific heat [J m^-3 K^-1]
			arma::Mat<fltp> calc_volumetric_specific_heat_mat(
				const arma::Mat<fltp> &temperature) const;

			// electric field [V m^-1]
			arma::Mat<fltp> calc_electric_field_mat(
				const arma::Mat<fltp> &current_density,
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const;

			// calculate current density [A m^-2]
			arma::Mat<fltp> calc_current_density_mat(
				const arma::Mat<fltp> &electric_field,
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const;

			// critical temperature calculation in [A m^-2]
			arma::Mat<fltp> calc_cs_temperature_mat(
				const arma::Mat<fltp> &current_density, 
				const arma::Mat<fltp> &magnetic_field_magnitude, 
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 1e-3) const;

			// thermal energy density [J/m^3]
			arma::Mat<fltp> calc_thermal_energy_density_mat(
				const arma::Mat<fltp> &temperature, 
				const fltp delta_temperature = RAT_CONST(1.0)) const;

			// critical current density averaged over cross section [J m^-2]
			arma::Mat<fltp> calc_critical_current_density_av_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			arma::Mat<fltp> calc_electric_field_av_mat(
				const arma::Mat<fltp> &current_density,
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude,
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL) const;
			
			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			arma::Mat<fltp> calc_cs_temperature_av_mat(
				const arma::Mat<fltp> &current_density, 
				const arma::Mat<fltp> &magnetic_field_magnitude, 
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 1e-3) const;

			// calculate percentage on the load-line
			// assuming fixed magnetic field angle
			arma::Mat<fltp> calc_percent_load_mat(
				const arma::Mat<fltp> &current_density,
				const arma::Mat<fltp> &temperature, 
				const arma::Mat<fltp> &magnetic_field_magnitude, 
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp current_density_increment = 100e6,
				const fltp tol_bisection = 0.1) const;

			// calculate percentage on the load-line
			// assuming fixed magnetic field angle
			arma::Mat<fltp> calc_percent_load_mat_av(
				const arma::Mat<fltp> &current_density,
				const arma::Mat<fltp> &temperature, 
				const arma::Mat<fltp> &magnetic_field_magnitude, 
				const arma::Mat<fltp> &magnetic_field_angle,
				const arma::Mat<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp current_density_increment = 100e6,
				const fltp tol_bisection = 0.1) const;

			// analysis tools for scalar input
			// ===
			// calcultae temperature increase due to added heat
			// output in [K]
			fltp calc_temperature_increase(
				const fltp initial_temperature, 
				const fltp volume, 
				const fltp added_heat,
				const fltp delta_temperature = 1.0) const;

			// calculate the energy density based on the temperature
			// output in [J m^-3]
			fltp calc_thermal_energy_density(
				const fltp temperature,
				const fltp delta_temperature = 1.0) const;

			// calculate the length of the minimal propagation zone
			// output in [m]
			fltp calc_theoretical_lmpz(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const;

			// calculate the normal zone propagation velocity
			// output in [m s^-1]
			fltp calc_theoretical_vnzp(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const;

			// calculate minimal quench energy in [J] or 
			// [J/m^2] if cross sectional area is not provided
			fltp calc_theoretical_mqe(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL,
				const fltp cross_sectional_area = 1.0) const;

			// calculate adiabatic normal zone temperature
			fltp calc_adiabatic_time(
				const fltp max_temperature,
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL,
				const fltp max_temperature_step = 0.5) const;


			// analysis tools for vector input
			// ===
			// calcultae temperature increase due to added heat
			arma::Col<fltp> calc_temperature_increase(
				const arma::Col<fltp> &initial_temperature, 
				const arma::Col<fltp> &volume, 
				const arma::Col<fltp> &added_heat) const;

			// calculate the energy density based on the temperature
			arma::Col<fltp> calc_thermal_energy_density(
				const arma::Col<fltp> &temperature,
				const fltp delta_temperature = 1.0) const;


			// accumulation method for parallel conductors
			virtual void accu_parallel_conductors(
				FracConPrList &parallel_conductors,
				const Direction dir) const;

			// accumulation method for series conductors
			virtual void accu_series_conductors(
				FracConPrList &series_conductors,
				const Direction dir) const;


			// functions averaged over cross section of conductor
			// ===
			// critical current density averaged over cross section [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density_av(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			virtual arma::Col<fltp> calc_electric_field_av(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL) const;
			
			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			virtual arma::Col<fltp> calc_cs_temperature_av(
				const arma::Col<fltp> &current_density, 
				const arma::Col<fltp> &magnetic_field_magnitude, 
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 1e-3) const;



			// helper functions
			// ===
			// cross section averaging
			static arma::Mat<fltp> nodes2section(
				const arma::Mat<fltp> &node_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas);

			// cross section values to nodes
			static arma::Mat<fltp> section2nodes(
				const arma::Mat<fltp> &section_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas);

			// Display function
			// make ss a table in the supplied log
			void display_property_table(
				const cmn::ShLogPr &lg = cmn::Log::create(),
				const arma::Col<fltp> &temperature = {1.9,4.5,10.0,20.0,30.0,40.0,50.0,60.0,77.0},
				const arma::Col<fltp> &magnetic_field_magnitude = {5.0},
				const arma::Col<fltp> &magnetic_field_angle = {0.0},
				const arma::Col<fltp> &scaling_factor = {1.0},
				const Direction dir = Direction::LONGITUDINAL) const;

			// write a table
			void write_property_table(
				const boost::filesystem::path &fname,
				const arma::Col<fltp> &temperature = {1.9,4.5,10.0,20.0,30.0,40.0,50.0,60.0,77.0},
				const arma::Col<fltp> &magnetic_field_magnitude = {5.0},
				const arma::Col<fltp> &magnetic_field_angle = {0.0},
				const arma::Col<fltp> &scaling_factor = {1.0},
				const Direction dir = Direction::LONGITUDINAL) const;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};


}}

#endif