// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_DEBEYE_RESISTIVITY_FIT_HH
#define MAT_DEBEYE_RESISTIVITY_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class DebeyeResistivityFit> ShDebeyeResistivityFitPr;

	// calculate rho from DebeyeResistivity
	class DebeyeResistivityFit: public Fit{
		// properties
		private:
			// fit coefficients
			fltp C0_; // y-intesect							
			fltp C1_; // slope				
			fltp C2_; // base

			// temperature range
			arma::Col<fltp>::fixed<2> fit_temperature_range_;

		public:
			// constructor
			DebeyeResistivityFit();
			DebeyeResistivityFit(const fltp C0, const fltp C1, const fltp C2);
			DebeyeResistivityFit(const fltp t0, const fltp t1,  const fltp C0, const fltp C1, const fltp C2);

			// factory
			static ShDebeyeResistivityFitPr create();
			static ShDebeyeResistivityFitPr create(const fltp C0, const fltp C1, const fltp C2);
			static ShDebeyeResistivityFitPr create(const fltp t0, const fltp t1, const fltp C0, const fltp C1, const fltp C2);

			// setters
			void set_C0(const fltp C0);
			void set_C1(const fltp C1);
			void set_C2(const fltp C2);

			// getters
			fltp get_C0()const;
			fltp get_C1()const;
			fltp get_C2()const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif