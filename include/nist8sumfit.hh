// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_NIST8SUM_FIT_HH
#define MAT_NIST8SUM_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Nist8SumFit> ShNist8SumFitPr;

	// implements the fit:
	// y = 10^(a+b(log10(T)) + c(log10(T))^2 + d(log10(T))^3 + 
	// e(log10(T))^4 + f(log10(T))^5 + g(log10(T))^6 + h(log10(T))^7 + i(log10(T))^8)
	// source NIST: https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
	class Nist8SumFit: public Fit{
		private: 
			// coefficients of fit
			arma::Col<fltp>::fixed<9> fit_parameters_; // abcdefghi

			// temperature range
			arma::Col<fltp>::fixed<2> fit_temperature_range_;

		public:
			// constructor
			Nist8SumFit();
			Nist8SumFit(const fltp t1, const fltp t2, const arma::Col<fltp>::fixed<9> &fit_parameters);

			// factory
			static ShNist8SumFitPr create();
			static ShNist8SumFitPr create(const fltp t1, const fltp t2, const arma::Col<fltp>::fixed<9> &fit_parameters);

			// set fit parameters
			void set_fit_parameters(const arma::Col<fltp>::fixed<9> &fit_parameters);
			void set_fit_parameters(const arma::uword index, const fltp value);

			// get fit parameters
			fltp get_fit_parameters(const arma::uword index)const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif