// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_DATABASE_HH
#define MAT_DATABASE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "conductor.hh"
#include "baseconductor.hh"
#include "anisotropic.hh"
#include "htscore.hh"
#include "htstape.hh"
#include "ltswire.hh"
#include "rutherfordcable.hh"
#include "superconductor.hh"
#include "fleiterfit.hh"
#include "fleiterbristowfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Database> ShDatabasePr;

	// wrapper for multifit with only two fits
	class Database{
		// methods
		public:
			// COPPER OFHC
			static ShFitPr copper_ofhc_cp();
			static ShFitPr copper_ofhc_rho(const fltp RRR = 50);
			static ShFitPr copper_ofhc_k(const fltp RRR = 50);
			static ShFitPr copper_ofhc_d();
			static ShBaseConductorPr copper_ofhc(const fltp RRR = 50);
			
			// BRASS 65-35
			static ShFitPr brass_65_35_cp();
			static ShFitPr brass_65_35_rho();
			static ShFitPr brass_65_35_k();
			static ShFitPr brass_65_35_d();
			static ShBaseConductorPr brass_65_35();

			// STAINLESS 316
			static ShFitPr stainless316_cp();
			static ShFitPr stainless316_rho();
			static ShFitPr stainless316_k();
			static ShFitPr stainless316_d();
			static ShBaseConductorPr stainless316();

			// ALUMINUM 5083-O
			static ShFitPr aluminumUNSA91100_cp();
			static ShFitPr aluminumUNSA91100_rho();
			static ShFitPr aluminumUNSA91100_k();
			static ShFitPr aluminumUNSA91100_d();
			static ShBaseConductorPr aluminumUNSA91100();

			// ALUMINUM 5083-O
			static ShFitPr aluminum5083O_cp();
			static ShFitPr aluminum5083O_rho();
			static ShFitPr aluminum5083O_k();
			static ShFitPr aluminum5083O_d();
			static ShBaseConductorPr aluminum5083O();

			// ALUMINUM 6061-T6
			static ShFitPr aluminum6061t6_cp();
			static ShFitPr aluminum6061t6_rho();
			static ShFitPr aluminum6061t6_k();
			static ShFitPr aluminum6061t6_d();
			static ShBaseConductorPr aluminum6061t6();

			// POLYIMIDE
			static ShFitPr polyimide_cp();
			static ShFitPr polyimide_k();
			static ShFitPr polyimide_d();
			static ShBaseConductorPr polyimide();
			static ShBaseConductorPr kapton();

			// G10
			static ShFitPr g10_cp();
			static ShFitPr g10_d();
			static ShAnisotropicPr g10();

			// G10 NORMAL DIRECTION
			static ShFitPr g10_normal_k();
			static ShBaseConductorPr g10_normal();

			// G10 WARP DIRECTION
			static ShFitPr g10_warp_k();
			static ShBaseConductorPr g10_warp();

			// INDIUM
			static ShFitPr indium_cp();
			static ShFitPr indium_rho();
			static ShFitPr indium_k();
			static ShFitPr indium_d();
			static ShBaseConductorPr indium();

			// SILVER
			static ShFitPr silver_cp();
			static ShFitPr silver_rho();
			static ShFitPr silver_k();
			static ShFitPr silver_d();
			static ShBaseConductorPr silver();


			// SUPERCONDUCTORS
			// NbTi LHC
			static ShFitPr nbti_lhc_cp(); // <- need doing
			static ShFitPr nbti_lhc_rho();
			static ShFitPr nbti_lhc_k(); // <- need doing
			static ShFitPr nbti_lhc_d();
			static ShFitPr nbti_lhc_jc();
			static ShFitPr nbti_lhc_nv();
			static ShSuperConductorPr nbti_lhc();

			// NbTi LHC
			static ShFitPr nbti_twente_cp(); // <- need doing
			static ShFitPr nbti_twente_rho();
			static ShFitPr nbti_twente_k(); // <- need doing
			static ShFitPr nbti_twente_d();
			static ShFitPr nbti_twente_jc();
			static ShFitPr nbti_twente_nv();
			static ShSuperConductorPr nbti_twente();

			// Nb3Sn RRP
			static ShFitPr nb3sn_rrp_cp();
			static ShFitPr nb3sn_rrp_rho();
			static ShFitPr nb3sn_rrp_k();
			static ShFitPr nb3sn_rrp_d();
			static ShFitPr nb3sn_rrp_jc();
			static ShFitPr nb3sn_rrp_nv();
			static ShSuperConductorPr nb3sn_rrp();

			// Nb3Sn PIT
			static ShFitPr nb3sn_pit_cp();
			static ShFitPr nb3sn_pit_rho();
			static ShFitPr nb3sn_pit_k();
			static ShFitPr nb3sn_pit_d();
			static ShFitPr nb3sn_pit_jc();
			static ShFitPr nb3sn_pit_nv();
			static ShSuperConductorPr nb3sn_pit();

			// Nb3Sn ITER
			static ShFitPr nb3sn_iter_cp();
			static ShFitPr nb3sn_iter_rho();
			static ShFitPr nb3sn_iter_k();
			static ShFitPr nb3sn_iter_d();
			static ShFitPr nb3sn_iter_jc();
			static ShFitPr nb3sn_iter_nv();
			static ShSuperConductorPr nb3sn_iter();

			// LTS WIRES
			// nbti lhc
			static ShLTSWirePr nbti_wire_lhc(
				const fltp diameter = RAT_CONST(0.8e-3), 
				const fltp fcu2sc = RAT_CONST(1.9), 
				const fltp RRR = RAT_CONST(150.0));
			
			// nbti twente
			static ShLTSWirePr nbti_wire_twente(
				const fltp diameter = RAT_CONST(0.8e-3), 
				const fltp fcu2sc = RAT_CONST(1.9), 
				const fltp RRR = RAT_CONST(150.0));

			// nb3sn
			static ShLTSWirePr nb3sn_wire_rrp(
				const fltp diameter = RAT_CONST(1.0e-3), 
				const fltp fcu2sc = RAT_CONST(1.25), 
				const fltp RRR = RAT_CONST(150.0));

			// LTS CABLES
			// Nb3Sn cable for fresca2
			// https://indico.cern.ch/event/119615/contributions/81251/attachments/63049/90584/Conductor_FRESCAII.pdf
			static ShRutherfordCablePr nb3sn_fresca2_cable(
				const bool insulated = false);
			static ShRutherfordCablePr nbti_cct_cable(
				const fltp spacing = RAT_CONST(1.0e-3),
				const fltp diameter = RAT_CONST(0.8e-3), 
				const fltp fcu2sc = RAT_CONST(1.9), 
				const fltp RRR = RAT_CONST(150.0));


			// REBCO FUJIKURA CERN
			static ShFitPr rebco_fujikura_cern_cp();
			static ShFitPr rebco_fujikura_cern_rho();
			static ShFitPr rebco_fujikura_cern_k();
			static ShFitPr rebco_fujikura_cern_d();
			static ShFleiterFitPr rebco_fujikura_cern_jc();
			static ShFitPr rebco_fujikura_cern_nv();
			static ShSuperConductorPr rebco_fujikura_cern();
			static ShHTSCorePr rebco_fujikura_core_cern(
				const fltp width = RAT_CONST(12e-3),
				const fltp thickness = RAT_CONST(60e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_fujikura_tape_cern(
				const fltp width = RAT_CONST(12.04e-3), 
				const fltp thickness = RAT_CONST(100e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(20e-6), 
				const fltp RRR = RAT_CONST(50.0));

			// REBCO THEVA CERN
			static ShFitPr rebco_theva_cern_cp();
			static ShFitPr rebco_theva_cern_rho();
			static ShFitPr rebco_theva_cern_k();
			static ShFitPr rebco_theva_cern_d();
			static ShFleiterFitPr rebco_theva_cern_jc();
			static ShFitPr rebco_theva_cern_nv();
			static ShSuperConductorPr rebco_theva_cern();
			static ShHTSCorePr rebco_theva_core_cern(
				const fltp width = 12e-3,
				const fltp thickness = RAT_CONST(110e-6),
				const fltp tsubstr = RAT_CONST(100e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_theva_tape_cern(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(130e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0));

			// REBCO SHANGHAI CERN
			static ShFitPr rebco_shanghai_cern_cp();
			static ShFitPr rebco_shanghai_cern_rho();
			static ShFitPr rebco_shanghai_cern_k();
			static ShFitPr rebco_shanghai_cern_d();
			static ShFleiterFitPr rebco_shanghai_cern_jc();
			static ShFitPr rebco_shanghai_cern_nv();
			static ShSuperConductorPr rebco_shanghai_cern();
			static ShHTSCorePr rebco_shanghai_core_cern(
				const fltp width = 10e-3,
				const fltp thickness = RAT_CONST(60e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_shanghai_tape_cern(
				const fltp width = RAT_CONST(10.02e-3), 
				const fltp thickness = RAT_CONST(86e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0));

			// REBCO FUJIKURA FESC RAT
			static ShFitPr rebco_fujikura_fesc_rat_cp();
			static ShFitPr rebco_fujikura_fesc_rat_rho();
			static ShFitPr rebco_fujikura_fesc_rat_k();
			static ShFitPr rebco_fujikura_fesc_rat_d();
			static ShFleiterFitPr rebco_fujikura_fesc_rat_jc();
			static ShFitPr rebco_fujikura_fesc_rat_nv();
			static ShSuperConductorPr rebco_fujikura_fesc_rat();
			static ShHTSCorePr rebco_fujikura_fesc_core_rat(
				const fltp width = 12e-3,
				const fltp thickness = RAT_CONST(57e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(1e-6));
			static ShHTSTapePr rebco_fujikura_fesc_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(77e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0),
				const arma::uword num_cores = 1llu);


			// REBCO THEVA PROLINE RAT
			static ShFitPr rebco_theva_proline_rat_cp();
			static ShFitPr rebco_theva_proline_rat_rho();
			static ShFitPr rebco_theva_proline_rat_k();
			static ShFitPr rebco_theva_proline_rat_d();
			static ShFleiterFitPr rebco_theva_proline_rat_jc();
			static ShFitPr rebco_theva_proline_rat_nv();
			static ShSuperConductorPr rebco_theva_proline_rat();
			static ShHTSCorePr rebco_theva_proline_core_rat(
				const fltp width = 12e-3,
				const fltp thickness = RAT_CONST(110e-6),
				const fltp tsubstr = RAT_CONST(100e-6), 
				const fltp tsc = RAT_CONST(1e-6));
			static ShHTSTapePr rebco_theva_proline_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(130e-6),
				const fltp tsubstr = RAT_CONST(100e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0),
				const arma::uword num_cores = 1llu);


			// REBCO THEVA PROLINE ADVANCED PINNING RAT
			static ShFitPr rebco_theva_proline_adv_rat_cp();
			static ShFitPr rebco_theva_proline_adv_rat_rho();
			static ShFitPr rebco_theva_proline_adv_rat_k();
			static ShFitPr rebco_theva_proline_adv_rat_d();
			static ShFleiterFitPr rebco_theva_proline_adv_rat_jc();
			static ShFitPr rebco_theva_proline_adv_rat_nv();
			static ShSuperConductorPr rebco_theva_proline_adv_rat();
			static ShHTSCorePr rebco_theva_proline_adv_core_rat(
				const fltp width = 12e-3,
				const fltp thickness = RAT_CONST(110e-6),
				const fltp tsubstr = RAT_CONST(100e-6), 
				const fltp tsc = RAT_CONST(1e-6));
			static ShHTSTapePr rebco_theva_proline_adv_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(130e-6),
				const fltp tsubstr = RAT_CONST(100e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0),
				const arma::uword num_cores = 1llu);

			// REBCO SUPERPOWER ADV PINNING RAT
			static ShFitPr rebco_superpower_adv_pinning_rat_cp();
			static ShFitPr rebco_superpower_adv_pinning_rat_rho();
			static ShFitPr rebco_superpower_adv_pinning_rat_k();
			static ShFitPr rebco_superpower_adv_pinning_rat_d();
			static ShFleiterFitPr rebco_superpower_adv_pinning_rat_jc();
			static ShFitPr rebco_superpower_adv_pinning_rat_nv();
			static ShSuperConductorPr rebco_superpower_adv_pinning_rat();
			static ShHTSCorePr rebco_superpower_adv_pinning_core_rat(
				const fltp width = 12e-3,
				const fltp thickness = RAT_CONST(57e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(1e-6));
			static ShHTSTapePr rebco_superpower_adv_pinning_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(77e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0),
				const arma::uword num_cores = 1llu);

			// REBCO SHANGHAI HIGH FIELD RAT
			static ShFitPr rebco_shanghai_high_field_rat_cp();
			static ShFitPr rebco_shanghai_high_field_rat_rho();
			static ShFitPr rebco_shanghai_high_field_rat_k();
			static ShFitPr rebco_shanghai_high_field_rat_d();
			static ShFleiterFitPr rebco_shanghai_high_field_rat_jc();
			static ShFitPr rebco_shanghai_high_field_rat_nv();
			static ShSuperConductorPr rebco_shanghai_high_field_rat();
			static ShHTSCorePr rebco_shanghai_high_field_core_rat(
				const fltp width = 10e-3,
				const fltp thickness = RAT_CONST(60e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_shanghai_high_field_tape_rat(
				const fltp width = RAT_CONST(10.02e-3), 
				const fltp thickness = RAT_CONST(86e-6),
				const fltp tsubstr = RAT_CONST(50e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0),
				const arma::uword num_cores = 1llu);

			// REBCO FARADAY FACTORY RAT
			static ShFitPr rebco_faraday_rat_cp();
			static ShFitPr rebco_faraday_rat_rho();
			static ShFitPr rebco_faraday_rat_k();
			static ShFitPr rebco_faraday_rat_d();
			static ShFleiterFitPr rebco_faraday_rat_jc();
			static ShFitPr rebco_faraday_rat_nv();
			static ShSuperConductorPr rebco_faraday_rat();
			static ShHTSCorePr rebco_faraday_core_rat(
				const fltp width = RAT_CONST(12e-3),
				const fltp thickness = RAT_CONST(50e-6),
				const fltp tsubstr = RAT_CONST(40e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_faraday_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(70e-6),
				const fltp tsubstr = RAT_CONST(40e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0));

			// REBCO SUPEROX YBCO RAT
			static ShFitPr rebco_superox_ybco_rat_cp();
			static ShFitPr rebco_superox_ybco_rat_rho();
			static ShFitPr rebco_superox_ybco_rat_k();
			static ShFitPr rebco_superox_ybco_rat_d();
			static ShFleiterFitPr rebco_superox_ybco_rat_jc();
			static ShFitPr rebco_superox_ybco_rat_nv();
			static ShSuperConductorPr rebco_superox_ybco_rat();
			static ShHTSCorePr rebco_superox_ybco_core_rat(
				const fltp width = RAT_CONST(12e-3),
				const fltp thickness = RAT_CONST(50e-6),
				const fltp tsubstr = RAT_CONST(40e-6), 
				const fltp tsc = RAT_CONST(2e-6));
			static ShHTSTapePr rebco_superox_ybco_tape_rat(
				const fltp width = RAT_CONST(12.02e-3), 
				const fltp thickness = RAT_CONST(70e-6),
				const fltp tsubstr = RAT_CONST(40e-6), 
				const fltp tsc = RAT_CONST(2e-6), 
				const fltp tstab = RAT_CONST(10e-6), 
				const fltp RRR = RAT_CONST(50.0));

			// void material
			static ShBaseConductorPr void_material();
	};

}}

#endif