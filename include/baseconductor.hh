// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_NORMAL_CONDUCTOR_HH
#define MAT_NORMAL_CONDUCTOR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"

#include "conductor.hh"

#include "fit.hh"

// for making copper
#include "nist8sumfit.hh"
#include "cudifit.hh"
#include "wflfit.hh"
#include "constfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class BaseConductor> ShBaseConductorPr;
	typedef arma::field<ShBaseConductorPr> ShBaseConductorPrList;

	// template for materials
	class BaseConductor: public Conductor{
		protected:
			// specific heat
			ShFitPr cp_fit_;

			// thermal conductivity
			ShFitPr k_fit_;

			// electrical resistivity
			ShFitPr rho_fit_;

			// density fit
			ShFitPr d_fit_;

		public:
			// base condutcor
			BaseConductor();
			BaseConductor(
				const ShFitPr &cp_fit, const ShFitPr &k_fit, 
				const ShFitPr &rho_fit, const ShFitPr &d_fit);

			// factories
			static ShBaseConductorPr create();
			static ShBaseConductorPr create(
				const ShFitPr &cp_fit, const ShFitPr &k_fit, 
				const ShFitPr &rho_fit, const ShFitPr &d_fit);

			// standard implementations
			static ShBaseConductorPr copper(const fltp RRR = 100);

			// setting the fit functions
			void set_thermal_conductivity_fit(const ShFitPr &k_fit);
			void set_electrical_resistivity_fit(const ShFitPr &rho_fit);
			void set_specific_heat_fit(const ShFitPr &cp_fit);
			void set_density_fit(const ShFitPr &d_fit);

			// getting teh fit functions
			const ShFitPr& get_thermal_conductivity_fit()const;
			const ShFitPr& get_electrical_resistivity_fit()const;
			const ShFitPr& get_specific_heat_fit()const;
			const ShFitPr& get_density_fit()const;

			// is insulating
			bool is_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_thermal_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;


			// material property calculation for scalars
			// calculate specific heat [J kg^-1 K^-1]
			fltp calc_specific_heat(
				const fltp temperature) const override;

			// thermal conductivity [W m^-1 K^-1]
			fltp calc_thermal_conductivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 

			// electrical resistivity [Ohm m^2]
			fltp calc_electrical_resistivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			fltp calc_density(
				const fltp temperature) const override;

			// electric field [V/m]
			virtual fltp calc_electric_field(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			virtual fltp calc_current_density(
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			virtual fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;
			
			// power law nvalue calculation
			virtual fltp calc_nvalue(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;



			// material property calculation for vectors
			// calculate specific heat [J kg^-1 K^-1]
			arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const override;

			// thermal conductivity [W m^-1 K^-1]
			arma::Col<fltp> calc_thermal_conductivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 

			// electrical resistivity [Ohm m^2]
			arma::Col<fltp> calc_electrical_resistivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			arma::Col<fltp> calc_density(
				const arma::Col<fltp> &temperature) const override;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// power law nvalue calculation
			arma::Col<fltp> calc_nvalue(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// electric field [V/m]
			virtual arma::Col<fltp> calc_electric_field(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			virtual arma::Col<fltp> calc_current_density(
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif