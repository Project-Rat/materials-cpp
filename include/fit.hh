// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_FIT_HH
#define MAT_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Fit> ShFitPr;

	// template for materials
	class Fit: public cmn::Node{
		// properties
		protected:
			// general info
			bool is_public_ = true;
			std::string source_ = "unknown";
			std::string property_str_ = "unknown";
			std::string standard_ = "unknown";
			std::string link_ = "-";

			// fit temperature range
			fltp t1_ = 1.0;
			fltp t2_ = 300.0;


		// methods
		public:
			// virtual destructor
			~Fit(){};

			// settings
			void set_is_public(const bool is_public);
			void set_source(const std::string &source);
			void set_property_str(const std::string &property);
			void set_standard(const std::string &standard);
			void set_link(const std::string &link);
			void set_temperature_range(const fltp t1, const fltp t2);
			void set_t1(const fltp t1);
			void set_t2(const fltp t2);

			// getters
			bool get_is_public()const;
			std::string get_source()const;
			std::string get_property_str()const;
			std::string get_standard()const;
			std::string get_link()const;
			fltp get_t1()const;
			fltp get_t2()const;

			// get property
			std::string get_property()const;

			// calculate property as function of scalar temperature
			virtual fltp calc_property(
				const fltp temperature) const;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude) const;

			// calculate property as function of scalar
			// temperature and magnetic field magnitude
			virtual fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const;

			// calculate property as function of vector temperature
			virtual arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature) const;

			// calculate property as function of vector
			// temperature and magnetic field magnitude
			virtual arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude) const;

			// calculate property as function of vector
			// temperature, magnetic field magnitude and angle
			virtual arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle) const;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif