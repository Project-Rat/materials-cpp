// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_HTS_CORE_HH
#define MAT_HTS_CORE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "anisotropic.hh"
#include "superconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class HTSCore> ShHTSCorePr;

	// wrapper for representing HTS tape core
	// this consists of substrate and superconducting layer
	class HTSCore: public Anisotropic{
		// properties
		protected: 
			// dimensions
			fltp width_ = RAT_CONST(12e-3);
			fltp thickness_ = RAT_CONST(60e-6);
			fltp tsc_ = RAT_CONST(1e-6);
			fltp tsubstr_ = RAT_CONST(50e-6);

			// core material
			ShSuperConductorPr sc_;

			// substrate material
			ShBaseConductorPr substr_;

		// methods
		public:
			// constructor
			HTSCore();
			HTSCore(
				const fltp width, 
				const fltp thickness, 
				const fltp tsc, 
				const ShSuperConductorPr &sc, 
				const fltp tsubstr, 
				const ShBaseConductorPr &substr);

			// factory
			static ShHTSCorePr create();
			static ShHTSCorePr create(
				const fltp width, 
				const fltp thickness, 
				const fltp tsc, 
				const ShSuperConductorPr &sc, 
				const fltp tsubstr, 
				const ShBaseConductorPr &substr);

			// set properties
			void set_width(const fltp width);
			void set_thickness(const fltp thickness);
			void set_tsubstr(const fltp tsubstr);
			void set_tsc(const fltp tsc);

			// set core dimensions
			fltp get_width() const;
			fltp get_thickness() const;
			fltp get_area() const;
			fltp get_tsc() const;
			fltp get_tsubstr() const;
			
			// surface 2D critical current density 
			// given per unit width [A m^-1]
			fltp calc_critical_current_per_width(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0)) const;

			// get layer conductors
			const ShSuperConductorPr& get_sc() const;
			const ShBaseConductorPr& get_substr() const;

			// set layer conductors
			void set_sc(const ShSuperConductorPr &sc);
			void set_substr(const ShBaseConductorPr &substr);

			// setup function
			void setup();

			// is superconductor
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif