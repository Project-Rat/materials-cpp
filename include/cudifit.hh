// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CUDI_FIT_HH
#define MAT_CUDI_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class CudiFit> ShCudiFitPr;

	// calculate rho from CUDI
	class CudiFit: public Fit{
		// properties
		private:
			// fit parameters for temperature dependence
			fltp C0_;
			fltp C1_; 
			fltp C2_; 
			fltp C3_;

			// fit parameters for field and RRR dependence
			fltp p_; 
			fltp q_;

			// triple-R
			fltp RRR_;

			// temperature range
			arma::Col<fltp>::fixed<2> fit_temperature_range_;

		public:
			// constructor
			CudiFit();
			CudiFit(const fltp RRR);

			// factory
			static ShCudiFitPr create();
			static ShCudiFitPr create(const fltp RRR);

			// setters
			void set_RRR(const fltp RRR);
			void set_C0(const fltp C0);
			void set_C1(const fltp C1);
			void set_C2(const fltp C2);
			void set_C3(const fltp C3);
			void set_p(const fltp p);
			void set_q(const fltp q);

			// getters
			fltp get_RRR()const;
			fltp get_C0()const;
			fltp get_C1()const;
			fltp get_C2()const;
			fltp get_C3()const;
			fltp get_p()const;
			fltp get_q()const;

			// property calculation with only temperature
			fltp calc_property(const fltp temperature) const override;

			// calculate resistivity
			fltp calc_property(
				const fltp temperature, 
				const fltp magnetic_field_magnitude) const override;

			// property calculation with only temperature
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// calculate resistivity
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature, 
				const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif