// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_LUBKRA_FIT_HH
#define MAT_LUBKRA_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class LubellKramerFit> ShLubellKramerFitPr;

	// Lubell Kramer Jc Fit
	class LubellKramerFit: public Fit{
		// properties
		private:
			// fitting parameters
			fltp C0_ = 31.4; // [T]
			fltp alpha_ = 0.63;
			fltp beta_ = 1.0;
			fltp gamma_ = 2.3;
			fltp n_ = 1.7;

			// materialistic parameters
			fltp Bc20_ = 14.5; // [T] upper critical magnetic flux at 0K
			fltp Tc0_ = 9.2; // [K] critical temperature at zero magnetic flux
			fltp Jref_ = 3000; // [A/mm^2]critical current density at 4.2K and 5T

		public:
			// constructor
			LubellKramerFit();
			LubellKramerFit(
				const fltp C0,
				const fltp alpha,
				const fltp beta,
				const fltp gamma,
				const fltp n,
				const fltp Bc20,
				const fltp Tc0,
				const fltp Jref);

			// factory
			static ShLubellKramerFitPr create();
			static ShLubellKramerFitPr create(
				const fltp C0,
				const fltp alpha,
				const fltp beta,
				const fltp gamma,
				const fltp n,
				const fltp Bc20,
				const fltp Tc0,
				const fltp Jref);

			// setters
			void set_C0(const fltp C0);
			void set_alpha(const fltp alpha);
			void set_beta(const fltp beta);
			void set_gamma(const fltp gamma);
			void set_n(const fltp n);
			void set_Bc20(const fltp Bc20);
			void set_Tc0(const fltp Tc0);
			void set_Jref(const fltp Jref);

			// getters
			fltp get_C0()const;
			fltp get_alpha()const;
			fltp get_beta()const;
			fltp get_gamma()const;
			fltp get_n()const;
			fltp get_Bc20()const;
			fltp get_Tc0()const;
			fltp get_Jref()const;

			// property calculation with less input
			fltp calc_property(const fltp temperature) const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude) const override;

			// property calculation with less input
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif