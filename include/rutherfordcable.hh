// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_RUTHERFORD_CABLE_HH
#define MAT_RUTHERFORD_CABLE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "parallelconductor.hh"
#include "ltswire.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class RutherfordCable> ShRutherfordCablePr;

	// wrapper for representing LTS wire
	class RutherfordCable: public ParallelConductor{
		protected: 
			// cabling parameters
			arma::uword num_strands_ = 0; // total number of strands in cable [#]

			// cable dimensions
			fltp width_  = 0; // bare cable width [m]
			fltp thickness_ = 0; // bare cable thickness [m]
			fltp keystone_ = 0; // keystone angle [rad]
			fltp dinsu_ = 0; // insulation thickness [m]

			// degradation due to cabling
			fltp fcabling_ = 1.0;

			// cabling pitch
			fltp pitch_ = 0; // twist pitch [m]

			// strand conductor
			ShLTSWirePr strand_; // material used in strands
			ShConductorPr insu_; // material used in insulation layer
			ShConductorPr voids_; // material used in voids between strands

		public:
			// constructor
			RutherfordCable();

			// factory
			static ShRutherfordCablePr create();

			// set layer conductors
			void set_strand(const ShLTSWirePr &strand);
			void set_insu(const ShConductorPr &insu);
			void set_voids(const ShConductorPr &voids);
			
			// get conductors
			ShLTSWirePr get_strand() const;

			// set dimensions
			void set_num_strands(const arma::uword num_strands);
			void set_width(const fltp width);
			void set_thickness(const fltp thickness);
			void set_keystone(const fltp keystone);
			void set_pitch(const fltp pitch);
			void set_dinsu(const fltp dinsu);
			void set_fcabling(const fltp fcabling);

			// set dimensions
			arma::uword get_num_strands()const;
			fltp get_width()const;
			fltp get_thickness()const;
			fltp get_area()const;
			fltp get_keystone()const;
			fltp get_pitch()const;
			fltp get_dinsu()const;
			fltp get_fcabling()const;

			// setup function
			void setup();

			// critical current density [J m^-2]
			fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// is superconductor
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;
			
			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif