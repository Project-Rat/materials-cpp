// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_ANISOTROPIC_HH
#define MAT_ANISOTROPIC_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Anisotropic> ShAnisotropicPr;

	// template for materials
	class Anisotropic: public Conductor{
		// add list of conductors
		protected:
			// list of input conductors: 0. not used,
			// 1. longitudinal, 2. normal, 3. transverse
			std::map<arma::uword, ShConductorPr> conductor_list_;

		// methods
		public:
			// constructor
			Anisotropic();
			Anisotropic(
				const ShConductorPr &longitudinal, 
				const ShConductorPr &normal, 
				const ShConductorPr &transverse);

			// factory
			static ShAnisotropicPr create();
			static ShAnisotropicPr create(
				const ShConductorPr &longitudinal, 
				const ShConductorPr &normal);
			static ShAnisotropicPr create(
				const ShConductorPr &longitudinal, 
				const ShConductorPr &normal, 
				const ShConductorPr &transverse);

			// modify conductor list
			arma::uword add_conductor(const ShConductorPr &conductor);
			const ShConductorPr& get_conductor(const arma::uword index)const;
			bool delete_conductor(const arma::uword index);
			arma::uword num_conductors() const;
			void reindex() override;

			// set conductor
			bool is_dir(const Direction dir)const;
			void set_longitudinal(const ShConductorPr &longitudinal_conductor);
			void set_normal(const ShConductorPr &normal_conductor);
			void set_transverse(const ShConductorPr &dir_conductor);

			// get conductor
			const ShConductorPr& get_conductor(const Direction dir)const;
			const ShConductorPr& get_longitudinal()const;
			const ShConductorPr& get_normal()const;
			const ShConductorPr& get_transverse()const;

			// accumulation method for parallel conductors
			void accu_parallel_conductors(
				FracConPrList &parallel_conductors,
				const Direction dir) const override;

			// accumulation method for series conductors
			void accu_series_conductors(
				FracConPrList &series_conductors,
				const Direction dir) const override;

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			fltp calc_specific_heat(
				const fltp temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			fltp calc_thermal_conductivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 
			
			// electrical resistivity [Ohm m^2]
			fltp calc_electrical_resistivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// is insulating
			bool is_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_thermal_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			virtual bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_anisotropic()const override{return true;};

			// critical current density [J m^-2]
			virtual fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			fltp calc_nvalue(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			fltp calc_density(
				const fltp temperature) const override;

			// electric field [V m^-1]
			fltp calc_electric_field(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V m^-1]
			// fltp calc_electric_field_fast(
			// 	const fltp current_density,
			// 	const arma::Row<fltp> &electrical_resistivity,
			// 	const arma::Row<fltp> &critical_current_density,
			// 	const arma::Row<fltp> &nvalue,
			// 	const Direction dir) const override;

			// calculate current density [A m^-2]
			fltp calc_current_density(
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // calculate current density [A m^-2]
			// fltp calc_current_density_fast(
			// 	const fltp electric_field,
			// 	const arma::Row<fltp> &electrical_resistivity,
			// 	const arma::Row<fltp> &critical_current_density,
			// 	const arma::Row<fltp> &nvalue,
			// 	const Direction dir) const override;



			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const override;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Col<fltp> calc_thermal_conductivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override; 
			
			// electrical resistivity [Ohm m^2]
			arma::Col<fltp> calc_electrical_resistivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			arma::Col<fltp> calc_nvalue(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// material density [kg m^-3]
			arma::Col<fltp> calc_density(
				const arma::Col<fltp> &temperature) const override;

			// electric field [V/m]
			arma::Col<fltp> calc_electric_field(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			arma::Col<fltp> calc_electric_field_av(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const arma::Mat<arma::uword> &nelements,
				const arma::uword cross_num_nodes,
				const arma::Row<fltp> &cross_areas,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // electric field using pre-calculated electrical 
			// // resistivity en critical current density [V/m]
			// arma::Col<fltp> calc_electric_field_fast(
			// 	const arma::Col<fltp> &current_density,
			// 	const arma::Mat<fltp> &electrical_resistivity,
			// 	const arma::Mat<fltp> &critical_current_density, 
			// 	const arma::Mat<fltp> &nvalue,
			// 	const Direction dir) const override;

			// calculate current density [A m^-2]
			arma::Col<fltp> calc_current_density(
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// // current density from electric field 
			// // output in [A m^-2]
			// arma::Col<fltp> calc_current_density_fast(
			// 	const arma::Col<fltp> &electric_field,
			// 	const arma::Mat<fltp> &electrical_resistivity,
			// 	const arma::Mat<fltp> &critical_current_density, 
			// 	const arma::Mat<fltp> &nvalue,
			// 	const Direction dir) const override;

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif