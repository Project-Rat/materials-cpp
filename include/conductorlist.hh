// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CONDUCTOR_LIST_HH
#define MAT_CONDUCTOR_LIST_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"
#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ConductorList> ShConductorListPr;
	typedef arma::field<ShConductorListPr> ShConductorListPrList;

	// conductor list map
	typedef std::map<arma::uword, FracShConPr> FracShConPrMap;

	// calculate rho from DebeyeResistivity
	class ConductorList: virtual public cmn::Node{
		// properties
		protected:
			// list of conductors <fraction, pointer>
			FracShConPrMap conductor_list_;

		// methods
		public:
			// modify conductor list
			arma::uword add_conductor(const ShConductorPr &conductor);
			arma::uword add_conductor(const FracShConPr &conductor);
			arma::uword add_conductor(const fltp fraction, const ShConductorPr &conductor);
			const ShConductorPr& get_conductor(const arma::uword index)const;
			const FracShConPrMap& get_conductors()const;
			fltp get_fraction(const arma::uword index);
			void set_fraction(const arma::uword index, const fltp fraction);
			bool delete_conductor(const arma::uword index);
			void clear_conductors();
			arma::uword num_conductors() const;
			void reindex() override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif