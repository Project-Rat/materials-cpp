// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_PARTIAL_INSULATION_HH
#define MAT_PARTIAL_INSULATION_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "anisotropic.hh"
#include "database.hh"
#include "inputconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class PartialInsulation> ShPartialInsulationPr;

	// wrapper for representing LTS wire
	class PartialInsulation: public Anisotropic, public InputConductor{
		protected: 
			// ShBaseConductorPr base_ = mat::Database::stainless316();
			bool is_transverse_ = false;
			fltp required_voltage_ = RAT_CONST(100.0);
			fltp operating_current_ = RAT_CONST(10000.0);
			fltp num_turns_ = RAT_CONST(10.0);
			fltp turn_length_ = RAT_CONST(5.0);
			fltp cable_width_ = RAT_CONST(12e-3);
			fltp dinsu_ = RAT_CONST(0.1e-3);
			fltp temperature_ = RAT_CONST(77.0);
			fltp field_magnitude_ = RAT_CONST(0.0);

		public:
			// constructor
			PartialInsulation();
			PartialInsulation(
				const ShBaseConductorPr &base,
				const fltp required_voltage,
				const fltp operating_current,
				const fltp num_turns,
				const fltp turn_length,
				const fltp cable_width,
				const fltp dinsu,
				const fltp temperature,
				const fltp field_magnitude);

			// factory
			static ShPartialInsulationPr create();
			static ShPartialInsulationPr create(
				const ShBaseConductorPr &base,
				const fltp required_voltage,
				const fltp operating_current,
				const fltp num_turns,
				const fltp turn_length,
				const fltp cable_width,
				const fltp dinsu,
				const fltp temperature = RAT_CONST(77.0),
				const fltp field_magnitude = RAT_CONST(0.0));

			// setters
			void set_is_transverse(const bool is_transverse);
			void set_base(const ShBaseConductorPr &base);
			void set_required_voltage(const fltp required_voltage);
			void set_operating_current(const fltp operating_current);
			void set_num_turns(const fltp num_turns);
			void set_turn_length(const fltp turn_length);
			void set_cable_width(const fltp cable_width);
			void set_dinsu(const fltp dinsu);
			void set_temperature(const fltp temperature);
			void set_field_magnitude(const fltp field_magnitude);

			// getters
			bool get_is_transverse()const;
			ShConductorPr get_base() const;
			fltp get_required_voltage() const;
			fltp get_operating_current() const;
			fltp get_num_turns() const;
			fltp get_turn_length() const;
			fltp get_cable_width() const;
			fltp get_dinsu() const;
			fltp get_temperature() const;
			fltp get_field_magnitude() const;

			// setup function
			void setup();

			// validity check
			bool is_valid(const bool enable_throws) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif