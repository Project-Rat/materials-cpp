// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_SUPER_CONDUCTOR_HH
#define MAT_SUPER_CONDUCTOR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "baseconductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class SuperConductor> ShSuperConductorPr;
	typedef arma::field<ShSuperConductorPr> ShSuperConductorPrList;

	// template for materials
	class SuperConductor: public BaseConductor{
		private:
			// N-value fit
			ShFitPr nv_fit_;

			// critical current density fit
			ShFitPr jc_fit_;

			// electric field criterion
			fltp electric_field_criterion_ = 0;

		public:
			// base condutcor
			SuperConductor();
			SuperConductor(
				const ShFitPr &jc_fit, const ShFitPr &nv_fit, 
				const ShFitPr &cp_fit, const ShFitPr &k_fit, 
				const ShFitPr &rho_fit, const ShFitPr &d_fit); // superconductor

			// factories
			static ShSuperConductorPr create();
			static ShSuperConductorPr create(
				const ShFitPr &jc_fit, const ShFitPr &nv_fit, 
				const ShFitPr &cp_fit, const ShFitPr &k_fit, 
				const ShFitPr &rho_fit, const ShFitPr &d_fit); // superconductor

			// setting the fit functions
			void set_electric_field_criterion(const fltp electric_field_criterion);
			void set_critical_current_density_fit(const ShFitPr &jc_fit);
			void set_nvalue_fit(const ShFitPr &nv_fit);

			// getting teh fit functions
			fltp get_electric_field_criterion()const;
			const ShFitPr& get_critical_current_density_fit()const;
			const ShFitPr& get_nvalue_fit()const;

			// is insulating
			bool is_insulator(const Direction dir = Direction::LONGITUDINAL)const override;
			bool is_superconductor(const Direction dir = Direction::LONGITUDINAL)const override;

			// critical current density [J m^-2]
			virtual fltp calc_critical_current_density(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;
			
			// power law nvalue calculation
			fltp calc_nvalue(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// electric field [V/m]
			fltp calc_electric_field(
				const fltp current_density,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			fltp calc_current_density(
				const fltp electric_field,
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp scaling_factor = RAT_CONST(1.0),
				const Direction dir = Direction::LONGITUDINAL) const override;

			// critical current density [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// power law nvalue calculation
			arma::Col<fltp> calc_nvalue(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// electric field [V/m]
			arma::Col<fltp> calc_electric_field(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// calculate current density [A m^-2]
			arma::Col<fltp> calc_current_density(
				const arma::Col<fltp> &electric_field,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Col<fltp> &scaling_factor,
				const Direction dir = Direction::LONGITUDINAL) const override;

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list,
				const boost::filesystem::path &pth) override;
	};

}}

#endif