// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_POLYFIT_HH
#define MAT_POLYFIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class PolyFit> ShPolyFitPr;

	// template for materials
	class PolyFit: public Fit{
		// properties
		private:
			// polynomial
			arma::Col<fltp> fit_parameters_; // abcdefghi

		// methods
		public:
			// virtual destructor
			PolyFit();
			// PolyFit(const fltp density);
			PolyFit(const arma::Row<fltp> &T, const arma::Row<fltp> &v, const arma::uword N);
			PolyFit(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters);

			// factory
			static ShPolyFitPr create();
			static ShPolyFitPr create(const arma::Row<fltp> &T, const arma::Row<fltp> &v, const arma::uword N);
			static ShPolyFitPr create(const fltp t1, const fltp t2, const arma::Col<fltp> &fit_parameters);

			// setters
			void set_fit_parameters(const arma::Col<fltp> &fit_parameters);
			
			// getters
			const arma::Col<fltp>& get_fit_parameters()const;

			// fit functions
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif