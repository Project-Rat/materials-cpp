// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_RHO_EDGE_FIT_HH
#define MAT_RHO_EDGE_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class RhoEdgeFit> ShRhoEdgeFitPr;

	// fit rho with wiedemann franz law
	class RhoEdgeFit: public Fit{
		private: 
			// thickness
			fltp stabilizer_thickness_ = RAT_CONST(10e-6); // [m]
			fltp surface_resistivity_ = RAT_CONST(2.05e-12); // [Ohm/m^2] join resistance of type 0 pair

			// conversion to volumetric
			fltp tape_width_ = RAT_CONST(12e-3);

			// thermal conductivity fit
			ShFitPr rho_fit_;

		public:
			// constructor
			RhoEdgeFit();
			RhoEdgeFit(
				const ShFitPr &rho_fit, 
				const fltp stabilizer_thickness, 
				const fltp tape_width, 
				const fltp surface_resistivity);

			// factory
			static ShRhoEdgeFitPr create();
			static ShRhoEdgeFitPr create(
				const ShFitPr &rho_fit,
				const fltp stabilizer_thickness, 
				const fltp tape_width, 
				const fltp surface_resistivity = RAT_CONST(2.05e-12));

			// setting
			void set_fit(const ShFitPr &rho_fit);

			// getting
			const ShFitPr& get_fit()const;

			// property calculation with less input
			fltp calc_property(const fltp temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;
			fltp calc_property(const fltp temperature, const fltp magnetic_field_magnitude) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature, const arma::Col<fltp> &magnetic_field_magnitude) const override;

			// validity check
			virtual bool is_valid(const bool enable_throws)const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth) override;
	};

}}

#endif