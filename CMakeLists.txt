## PROJECT SETUP
cmake_minimum_required(VERSION 3.20 FATAL_ERROR)
project(Rat-Materials VERSION 2.018.5 LANGUAGES CXX)

# set all policies to this version of cmake
cmake_policy(VERSION ${CMAKE_VERSION})

# switchboard
option(ENABLE_TESTING "Build unit/system tests" ON)
option(ENABLE_EXAMPLES "Build example applications" ON)
option(ENABLE_CLIAPP "Build commandline applications" ON)

# ensure out of core build
if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
	message(FATAL_ERROR "In-source builds not allowed. Please make a new directory (called a build directory) and run CMake from there.\n")
endif()

# set build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# C++ standard
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# export cmake commands for lsp
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# compiler options
if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
	message(STATUS "using GCC settings")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3")
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
	message(STATUS "using Clang settings")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3 -flto -funroll-loops")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
	message(STATUS "using MSVC settings")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /utf-8")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /O2 /arch:AVX2 /Ot")
else()
	message(STATUS "Compiler not recognized")
endif()

# windows specific
if(WIN32)
set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
endif()

# output paths
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/bin CACHE PATH "" FORCE)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib CACHE PATH "" FORCE)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/lib CACHE PATH "" FORCE)

# report
message(STATUS "building in ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}")


## EXTERNAL LIBRARIES
# where are the custom CMake modules located
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

# find the rat common library
find_package(RatCommon 2.018.5 REQUIRED)

## LIBRARY
# add the library
add_library(ratmat SHARED
	src/conductor.cpp
	src/conductorlist.cpp
	src/baseconductor.cpp
	src/parallelconductor.cpp
	src/htscore.cpp
	src/htstape.cpp
	src/ltswire.cpp
	src/nzpmodel.cpp
	src/fit.cpp
	src/nist8sumfit.cpp
	src/nist8fracfit.cpp
	src/polyfit.cpp
	src/linearfit.cpp
	src/interp2fit.cpp
	src/interp3fit.cpp
	src/lubellkramerfit.cpp
	src/constfit.cpp
	src/multifit.cpp
	src/godekefit.cpp
	src/fleiterfit.cpp
	src/fleiterbristowfit.cpp
	src/cudifit.cpp
	src/wflfit.cpp
	src/rutherfordcable.cpp
	src/serializer.cpp
	src/seriesconductor.cpp
	src/database.cpp
	src/anisotropic.cpp
	src/debeyeresistivityfit.cpp
	src/inputconductor.cpp
	src/superconductor.cpp
	src/partialinsulation.cpp
	src/rhoedgefit.cpp
	src/lininterpfit.cpp
	src/v2o3resistivityfit.cpp
	src/scalefit.cpp
)

# Add an alias so that library can be used inside the build tree, e.g. when testing
add_library(Rat::Materials ALIAS ratmat)

# disable windows warnings
if(WIN32)
	target_compile_definitions(ratmat PUBLIC -D_CRT_SECURE_NO_WARNINGS)
endif()

# properties
set_target_properties(ratmat PROPERTIES VERSION ${PROJECT_VERSION})
set_target_properties(ratmat PROPERTIES SOVERSION 1)

# add include directory
target_include_directories(ratmat
	PUBLIC 
		$<INSTALL_INTERFACE:include>    
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/include>
	PRIVATE
		${CMAKE_CURRENT_SOURCE_DIR}/src
)

# enable warnings
target_compile_options(ratmat PRIVATE 
	$<$<OR:$<CXX_COMPILER_ID:Clang>,$<CXX_COMPILER_ID:AppleClang>,$<CXX_COMPILER_ID:GNU>>:
		-Wpedantic -Wall -Wextra -Wsuggest-override -Wold-style-cast>
	$<$<CXX_COMPILER_ID:MSVC>:
		/W3>)

# link libraries
target_link_libraries(ratmat PUBLIC Rat::Common)

# Version header file
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_MAJOR_STRIPPED ${PROJECT_VERSION_MAJOR})
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_MINOR_STRIPPED ${PROJECT_VERSION_MINOR})
string(REGEX REPLACE "^0+" "" PROJECT_VERSION_PATCH_STRIPPED ${PROJECT_VERSION_PATCH})
configure_file(
	${CMAKE_CURRENT_SOURCE_DIR}/cmake/version.hh.in 
	${CMAKE_CURRENT_BINARY_DIR}/include/version.hh)

## UNIT/SYSTEM TESTS
# check switchboard
if(ENABLE_TESTING)
	# use cmake's platform for unit testing
	enable_testing()

	# add test directory
	add_subdirectory(test)
endif()

## Applications
# check switchboard
if(ENABLE_EXAMPLES)
	add_subdirectory(examples)
endif()

# check switchboard
if(ENABLE_CLIAPP)
	add_subdirectory(cliapp)
endif()



## copy database files to build directory
# file(COPY json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

## INSTALLATION
# get install directories
include(GNUInstallDirs)
set(INSTALL_CONFIGDIR ${CMAKE_INSTALL_LIBDIR}/cmake/ratmat)
set(INSTALL_DATABASEDIR ${CMAKE_INSTALL_LIBDIR})

install(TARGETS ratmat
	EXPORT ratmat-targets
	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}/rat
	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)

# This is required so that the exported target has the name RatCmn and not ratcmn
set_target_properties(ratmat PROPERTIES EXPORT_NAME Mat)

install(DIRECTORY include/ DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/mat)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/include/version.hh DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/rat/mat)

# Export the targets to a script
install(EXPORT ratmat-targets
	FILE
		RatMatTargets.cmake
	NAMESPACE
		Rat::
	DESTINATION
		${INSTALL_CONFIGDIR}
)

# Create a ConfigVersion.cmake file
include(CMakePackageConfigHelpers)
write_basic_package_version_file(
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfigVersion.cmake
	VERSION ${PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

configure_package_config_file(${CMAKE_CURRENT_LIST_DIR}/cmake/RatMatConfig.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfig.cmake
	INSTALL_DESTINATION ${INSTALL_CONFIGDIR}
)

# Install the config, configversion and custom find modules
install(FILES
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfig.cmake
	${CMAKE_CURRENT_BINARY_DIR}/RatMatConfigVersion.cmake
	DESTINATION ${INSTALL_CONFIGDIR}
)

# # Install the database directory
# install(DIRECTORY 
# 	${CMAKE_CURRENT_BINARY_DIR}/json
# 	DESTINATION ${INSTALL_DATABASEDIR})

# export the cmake files
export(EXPORT ratmat-targets
	FILE ${CMAKE_CURRENT_BINARY_DIR}/RatMatTargets.cmake
	NAMESPACE Rat::)

# export the data inspector
if(ENABLE_CLIAPP)
  if(WIN32)
  	install(FILES 
	 		${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matprops.exe
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matnzp.exe
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matdt.exe
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matenergy.exe
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matcreate.exe
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matmiits.exe
			DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
			PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE 
				GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
  else()
		install(FILES 
	 		${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matprops
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matnzp
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matdt
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matenergy
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matcreate
			${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/matmiits
			DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
			PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE 
				GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
  endif()
endif()

# Register package in user's package registry
export(PACKAGE RatMat)

#####################
# Setup install paths
#####################

# Set RPATHS
SET(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)
list(APPEND CMAKE_INSTALL_RPATH "$ORIGIN/../${CMAKE_INSTALL_LIBDIR}")

# Create Debian package, must be the last thing
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/cmake")
include(GenerateDebianPackage)
