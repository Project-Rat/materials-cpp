// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// electrical conductivity derived from resistivity
	double ResistivityFit::calc_electrical_conductivity(
		const double temperature,
		const double magnetic_field_magnitude) const{
		return 1.0/calc_electrical_resistivity(temperature, magnetic_field_magnitude);
	}

	// electrical conductivity derived from resistivity
	arma::Col<double> ResistivityFit::calc_electrical_conductivity(
		const arma::Col<double> temperature,
		const arma::Col<double> magnetic_field_magnitude) const{
		return 1.0/calc_electrical_resistivity(temperature, magnetic_field_magnitude);
	}

}}