// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// Description:
// Calculates the critical current density of Nb3Sn using the A. God
// formula. The initial parameters can be set using two standard
// variants: Powder in Tube (PIT) and Rod-Restack Process (RRP). It must
// be noted that the formulas are not accurate in the low field region.

// references:
// A. Godeke, Msc. Thesis: Performance Boundaries in Nb3Sn Superconductors,
// Twente University 2005, p168

#ifndef MAT_GODEKE_NB3SN_HH
#define MAT_GODEKE_NB3SN_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "jcfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class GodekeNb3Sn> ShGodekeNb3SnPr;

	// Lubell/Kramer scaling relation
	class GodekeNb3Sn: public JcFit{
		private:
			// deformation related parameters
			double Ca1_;
			double Ca2_;
			double eps_0a_; // normalized
			double C1_; // [kAT mm^-2]

			// superconducting parameters
			double Bc2m_; // [T]
			double Tcm_; // [K]

			// magnetic field dependence parameters
			double p_;
			double q_;

			// fixed power law N-value
			double nvalue_;

			// power law electric field criterion
			double electric_field_criterion_;

			// environmental parameters
			double eps_ax_ = 0; // axial strain

		public:
			// constructor
			GodekeNb3Sn();

			// factory
			static ShGodekeNb3SnPr create();

			// get function for electric field criterion
			double get_electric_field_criterion() const override;
			void set_nvalue(const double nvalue);
			void set_electric_field_criterion(const double electric_field_criterion);

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;
			
			// function for calculating the power law n-value
			// using scalar input and output
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating the power law n-value
			// using vector input and output
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;
			
			// copy function
			ShJcFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif