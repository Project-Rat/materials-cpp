// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_FIXED_DENSITY_HH
#define MAT_FIXED_DENSITY_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "densityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class FixedDensity> ShFixedDensityPr;

	// template for materials
	class FixedDensity: public DensityFit{
		// properties
		private:
			double density_;

		// methods
		public:
			// virtual destructor
			FixedDensity();
			FixedDensity(const std::string &fname);
			FixedDensity(const double density);

			// factory
			static ShFixedDensityPr create();
			static ShFixedDensityPr create(const std::string &fname);
			static ShFixedDensityPr create(const double density);

			// set fit parameters
			void set_density(const double density);

			// fit functions
			double calc_density(const double temperature) const override;
			arma::Col<double> calc_density(const arma::Col<double> &temperature) const override;
			
			// copy function
			ShDensityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// read data from json value
			void import_json(const Json::Value &js);
	};

}}

#endif