// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_NIST_SH_HH
#define MAT_NIST_SH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "specificheatfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class NistSH> ShNistSHPr;

	// NIST specific heat fit
	// source NIST: https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
	class NistSH: public SpecificHeatFit{
		private: 
			// coefficients of fit
			arma::Col<double>::fixed<9> fit_parameters_; // abcdefghi

			// temperature range
			double lower_temperature_;
			double upper_temperature_;

		public:
			// constructor
			NistSH();
			NistSH(const std::string &fname);
			NistSH(const double a, const double b, const double c, const double d, const double e, const double f, const double g, const double h, const double i, const double lower_temperature, const double upper_temperature);

			// factory
			static ShNistSHPr create();
			static ShNistSHPr create(const std::string &fname);
			static ShNistSHPr create(const double a, const double b, const double c, const double d, const double e, const double f, const double g, const double h, const double i, const double lower_temperature, const double upper_temperature);

			// get temperature range
			double get_lower_temperature() const;
			double get_upper_temperature() const;

			// set fit parameters
			void set_fit_parameters(const arma::Col<double> &fit_parameters);
			void set_temperature_range(const double lower_temperature, const double upper_temperature);

			// fit functions
			double calc_specific_heat(const double temperature) const override;
			arma::Col<double> calc_specific_heat(const arma::Col<double> &temperature) const override;

			// copy function
			ShSpecificHeatFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// read data from json value
			void import_json(const Json::Value &js);
	};

}}

#endif