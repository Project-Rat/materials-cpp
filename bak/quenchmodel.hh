// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_QUENCH_MODEL_HH
#define MAT_QUENCH_MODEL_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/parfor.hh"

#include "conductor.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class QuenchModel> ShQuenchModelPr;
	typedef arma::field<ShQuenchModelPr> ShQuenchModelPrList;

	class Coil{
		private:
			// name
			std::string name_;

			// material the coil is made of
			fltp ShConductorPr conductor_;
			fltp ShConductorPr insulation_;

			// geometry
			fltp cable_width_;
			fltp cable_thickness_;
			fltp insulation_thickness_;

			// simulation domain
			fltp sim_length_;
			arma::uword sim_num_turns_;
			fltp element_size_;

			// voltage treshold for quench detection
			fltp detection_voltage_;
			fltp detection_delay_time_;

		public:

	};

	class Circuit{
		private:
			// name of the circuit
			std::string name_;
			
			// list of coils
			std::map<arma::uword, ShCoilPr> coils_;

			// protection settings
			fltp Rdump_;
			fltp switch_time_;

		public:
			void add_coil(const ShCoilPr& coil){
				coils_[coils_.size()] = coil;
			}

			const ShCoilPr& get_coil(arma::uword index)const{
				auto it = coils_.find(index);
				if(it==coils_.end())rat_throw_line("could not find coil");
				return (*it);
			}
	};



	// toy normal zone propagation model
	class QuenchModel{
		private:
			// materials
			std::map<arma::uword, Circuit> circuits_;

			// inductance matrix for circuits
			arma::Mat<fltp> M_;

		// methods
		public:
			// constructor
			QuenchModel();

			// factory
			static ShQuenchModelPr create();



			// runge kutta of any order using matrix
			// source:
			// https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods#Forward_Euler
			
			// the matrix a_{ij} must be lower triangular (explicit methods)

			// e_{n+1} = h \sum_{i=1}{s} (b_i - b_i*)k_i
			// k_i = f(tn + c_i*h, y_n + h\sum a_{ij} k_i)

			// for example classical fourth order runge kutta 
			// can be applied using this representation
			// 0   | 0   0   0   0
			// 1/2 | 1/2 0   0   0
			// 1/2 | 0   1/2 0   0
			// 1   | 0   0   1   0
			// ------------------
			//     | 1/6 1/3 1/3 1/6
			arma::Mat<fltp> runge_kutta_general(
				const arma::Mat<fltp> &a,
				const arma::Row<fltp> &b,
				const arma::Col<fltp> &c,
				const fltp t, const fltp dt, 
				const arma::Mat<fltp> &T){

				// check matrix
				assert(!a.empty()); assert(!b.empty()); assert(!c.empty());
				assert(c.n_elem==a.n_rows);
				assert(b.n_elem==a.n_cols);
				assert(a.is_square());
				assert(a.is_trimatl());

				// allocate storage for k
				arma::field<arma::Mat<fltp> > k(a.n_rows);

				// call system function
				for(arma::uword i=0;i<a.n_rows;i++){
					// calculate the sum
					arma::Mat<fltp> kk(RV.n_rows, RV.n_cols, arma::fill::zeros);
					for(arma::uword j=0;j<i;j++)kk += a(i,j)*k(j);

					// run system function on next row
					k(i) = system_function(t + c(i)*dt, RV + dt*kk, q, m, tracking_grid);
				}

				// apply weights
				arma::Mat<fltp> dRV(RV.n_rows, RV.n_cols, arma::fill::zeros);
				for(arma::uword i=0;i<a.n_cols;i++)
					dRV += dt*b(i)*k(i);

				// particle fallen off grid
				for(arma::uword i=0;i<a.n_cols;i++)
					dRV.cols(arma::find(arma::all(k(i)==0,0))).fill(0);

				// return result
				return dRV;
			}
	};

}}

#endif