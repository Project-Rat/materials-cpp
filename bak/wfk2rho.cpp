// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "wfk2rho.hh"
#include "conductivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	WFK2Rho::WFK2Rho(){

	}

	WFK2Rho::WFK2Rho(ShConductivityFitPr k_fit){
		set_thermal_conductivity_fit(k_fit);
	}

	// factory
	ShWFK2RhoPr WFK2Rho::create(){
		return std::make_shared<WFK2Rho>();
	}

	// factory from file
	ShWFK2RhoPr WFK2Rho::create(ShConductivityFitPr k_fit){
		return std::make_shared<WFK2Rho>(k_fit);
	}

	// set thermal conductivity fit
	void WFK2Rho::set_thermal_conductivity_fit(ShConductivityFitPr k_fit){
		assert(k_fit!=NULL);
		k_fit_ = k_fit;
	}

	// fit functions for vector
	double WFK2Rho::calc_electrical_resistivity(
		const double temperature, 
		const double magnetic_field_magnitude) const{
		
		// limit temperature
		const double T = std::max(temperature,1.0);

		// calculate thermal conductivity
		const double k = k_fit_->calc_thermal_conductivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const double rho = lorentz_number_*T/k;

		// rurn resistivity
		return rho;
	}

	// fit functions for vector
	arma::Col<double> WFK2Rho::calc_electrical_resistivity(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		
		// limit temperature
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);

		// calculate thermal conductivity
		const arma::Col<double> k = k_fit_->calc_thermal_conductivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law
		const arma::Col<double> rho = lorentz_number_*T/k;

		// check output
		assert(rho.n_elem==temperature.n_elem);
		assert(rho.is_finite());
		assert(arma::all(rho>0));

		// rurn resistivity
		return rho;
	}

	// copy constructor
	ShResistivityFitPr WFK2Rho::copy() const{
		return WFK2Rho::create(k_fit_->copy());
	}

	// serialization type
	std::string WFK2Rho::get_type(){
		return "rat::mat::wfk2rho";
	}

	// serialization
	void WFK2Rho::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["k_fit"] = cmn::Node::serialize_node(k_fit_, list);
	}

	// deserialization
	void WFK2Rho::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		set_thermal_conductivity_fit(cmn::Node::deserialize_node<ConductivityFit>(js["k_fit"], list, factory_list));
	}

	

}}