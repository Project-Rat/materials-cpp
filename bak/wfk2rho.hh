// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_WF_K2RHO_HH
#define MAT_WF_K2RHO_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class WFK2Rho> ShWFK2RhoPr;

	// fit rho with wiedemann franz law
	class WFK2Rho: public ResistivityFit{
		private: 
			// lorentz number
			const double lorentz_number_ = 2.44e-8; // [W Ohm K-2]

			// thermal conductivity fit
			std::shared_ptr<class ConductivityFit> k_fit_;

		public:
			// constructor
			WFK2Rho();
			WFK2Rho(std::shared_ptr<ConductivityFit> kfit);

			// factory
			static ShWFK2RhoPr create();
			static ShWFK2RhoPr create(std::shared_ptr<ConductivityFit> k_fit);

			// setting
			void set_thermal_conductivity_fit(std::shared_ptr<ConductivityFit> k_fit);

			// fit functions
			double calc_electrical_resistivity(const double temperature, const double magnetic_field_magnitude) const override;
			arma::Col<double> calc_electrical_resistivity(const arma::Col<double> &temperature, const arma::Col<double> &magnetic_field_magnitude) const override;

			// copy function
			ShResistivityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif