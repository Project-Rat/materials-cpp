// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "nistconductivity2.hh"

// code specific to Rat
namespace rat{namespace mat{


	// default constructor
	NistConductivity2::NistConductivity2(){

	}

	// constructor from file
	NistConductivity2::NistConductivity2(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// factory
	ShNistConductivity2Pr NistConductivity2::create(){
		return std::make_shared<NistConductivity2>();
	}

	// factory from file
	ShNistConductivity2Pr NistConductivity2::create(const std::string &fname){
		return std::make_shared<NistConductivity2>(fname);
	}

	// set fit parameters
	void NistConductivity2::set_fit_parameters(const arma::Col<double> &fit_parameters){
		if(fit_parameters.is_empty())rat_throw_line("parameter list is empty");
		fit_parameters_ = fit_parameters;
	}

	// set fit parameters
	void NistConductivity2::set_temperature_range(const double lower_temperature, const double upper_temperature){
		if(upper_temperature<lower_temperature)rat_throw_line("upper temperature must be larger than lower temperature");
		lower_temperature_ = lower_temperature;
		upper_temperature_ = upper_temperature;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> NistConductivity2::calc_thermal_conductivity(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude)const{
		
		// check input
		assert(temperature.is_finite());

		// check if fit parameters set
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// calculate nist fit using for loop to avoid power
		arma::Col<double> A(temperature.n_elem,arma::fill::zeros);
		arma::Col<double> log10T = arma::log10(temperature);
		arma::Col<double> plt(temperature); plt.fill(1.0);
		for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
			A += fit_parameters_(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Col<double> conductivity = arma::exp10(A);

		// extrapolation beyond end
		const arma::Col<arma::uword> extrap = arma::find(temperature>upper_temperature_);
		if(!extrap.is_empty()){
			const double dt = (upper_temperature_-lower_temperature_)/1000;
			const arma::Col<double>::fixed<2> Te = {upper_temperature_-dt, upper_temperature_-1e-10};
			const arma::Col<double>::fixed<2> Be = {0,0};
			const arma::Col<double>::fixed<2> ke = calc_thermal_conductivity(Te,Be);
			conductivity(extrap) = ke(1) + (temperature(extrap)-Te(1))*((ke(1)-ke(0))/(Te(1)-Te(0)));
		}

		// fix value when below range
		const arma::Col<arma::uword> idx = arma::find(temperature<lower_temperature_);
		if(!idx.is_empty()){
			// create temperature array
			arma::Col<double> Text(idx.n_elem); Text.fill(lower_temperature_+1e-10);
			arma::Col<double> Bext(idx.n_elem,arma::fill::zeros);
			conductivity(idx) = calc_thermal_conductivity(Text,Bext);
		}

		// check output
		assert(conductivity.n_elem==temperature.n_elem);
		assert(conductivity.is_finite());

		// return answer
		return conductivity;

	}

	// specific heat output in [J m^-3 K^-1]
	double NistConductivity2::calc_thermal_conductivity(
		const double temperature,
		const double magnetic_field_magnitude)const{

		// check input
		if(fit_parameters_.is_empty())rat_throw_line("parameter list is not set");

		// allocate
		double conductivity = 0;

		// extrapolation beyond end
		if(temperature>upper_temperature_){
			const double dt = (upper_temperature_-lower_temperature_)/1000;
			const double t1 = upper_temperature_-dt;
			const double t2 = upper_temperature_;
			const double s1 = calc_thermal_conductivity(t1,magnetic_field_magnitude);
			const double s2 = calc_thermal_conductivity(t2,magnetic_field_magnitude);
			conductivity = s2 + (temperature - upper_temperature_)*((s2-s1)/(t2-t1));
		}

		// fix value when below range
		else if(temperature<lower_temperature_){
			conductivity = calc_thermal_conductivity(lower_temperature_,magnetic_field_magnitude);
		}

		// calculate normally
		else{
			double A = 0, plt = 1.0;
			double log10T = std::log10(temperature);
			for(arma::uword i=0;i<fit_parameters_.n_elem;i++){
				A += fit_parameters_(i)*plt; plt*=log10T;
			}

			// 10^A and multiply with density
			conductivity = std::pow(10.0,A);
		}

		// return answer
		return conductivity;
	}

	// copy constructor
	ShConductivityFitPr NistConductivity2::copy() const{
		return std::make_shared<NistConductivity2>(*this);
	}

	// get type
	std::string NistConductivity2::get_type(){
		return "rat::mat::nistconductivity2";
	}

	// method for serialization into json
	void NistConductivity2::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["a"] = fit_parameters_(0);
		js["b"] = fit_parameters_(1);
		js["c"] = fit_parameters_(2);
		js["d"] = fit_parameters_(3);
		js["e"] = fit_parameters_(4);
		js["f"] = fit_parameters_(5);
		js["g"] = fit_parameters_(6);
		js["h"] = fit_parameters_(7);
		js["i"] = fit_parameters_(8);

		// temperature range
		js["lower_temperature"] = lower_temperature_;
		js["upper_temperature"] = upper_temperature_;
	}

	// method for deserialisation from json
	void NistConductivity2::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void NistConductivity2::import_json(const Json::Value &js){
		// fit parameters
		fit_parameters_(0) = js["a"].ASFLTP();
		fit_parameters_(1) = js["b"].ASFLTP();
		fit_parameters_(2) = js["c"].ASFLTP();
		fit_parameters_(3) = js["d"].ASFLTP();
		fit_parameters_(4) = js["e"].ASFLTP();
		fit_parameters_(5) = js["f"].ASFLTP();
		fit_parameters_(6) = js["g"].ASFLTP();
		fit_parameters_(7) = js["h"].ASFLTP();
		fit_parameters_(8) = js["i"].ASFLTP();

		// temperature range
		lower_temperature_ = js["lower_temperature"].ASFLTP();
		upper_temperature_ = js["upper_temperature"].ASFLTP();
	}

}}