// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_SPECIFIC_HEAT_FIT_HH
#define MAT_SPECIFIC_HEAT_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class SpecificHeatFit> ShSpecificHeatFitPr;

	// specific heat fit class template
	class SpecificHeatFit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~SpecificHeatFit(){};

			// function for calculating specific heat
			// using scalar input and output
			// output is in [J kg^-1 K^-1]
			virtual fltp calc_specific_heat(
				const fltp temperature) const = 0;

			// function for calculating specific heat
			// using vector input and output
			// output is in [J kg^-1 K^-1]
			virtual arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const = 0;
	};

}}

#endif