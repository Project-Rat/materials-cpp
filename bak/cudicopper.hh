// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CUDI_COPPER_HH
#define MAT_CUDI_COPPER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class CudiCopper> ShCudiCopperPr;

	// calculate rho from CUDI
	class CudiCopper: public ResistivityFit{
		// properties
		private:
			// fit coefficients
			double C0_;
			double C1_; 
			double C2_; 
			double C3_;

			// triple-R
			double RRR_;

			// temperature range
			double lower_temperature_;
			double upper_temperature_;

		public:
			// constructor
			CudiCopper();
			CudiCopper(const std::string &fname);
			CudiCopper(const double C0, const double C1, const double C2, const double C3, const double RRR, const double lower_temperature, const double upper_temperature);

			// factory
			static ShCudiCopperPr create();
			static ShCudiCopperPr create(const std::string &fname);
			static ShCudiCopperPr create(const double C0, const double C1, const double C2, const double C3, const double RRR, const double lower_temperature, const double upper_temperature);

			// fit functions
			double calc_electrical_resistivity(const double temperature, const double magnetic_field_magnitude = 0) const override;
			arma::Col<double> calc_electrical_resistivity(const arma::Col<double> &temperature, const arma::Col<double> &magnetic_field_magnitude) const override;

			// copy function
			ShResistivityFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// import json
			void import_json(const Json::Value &js);
	};

}}

#endif