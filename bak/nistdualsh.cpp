// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "nistdualsh.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	NistDualSH::NistDualSH(){

	}

	// factory
	ShNistDualSHPr NistDualSH::create(){
		return std::make_shared<NistDualSH>();
	}

	// fit functions
	double NistDualSH::calc_specific_heat(const double temperature) const{
		// allocate
		double specific_heat = 0;

		// split calculation
		if(temperature<sh_fit1_->get_upper_temperature())
			specific_heat = sh_fit1_->calc_specific_heat(temperature);
		if(temperature>=sh_fit1_->get_upper_temperature())
			specific_heat = sh_fit2_->calc_specific_heat(temperature);

		// return specific heat
		return specific_heat;
	}
	
	// specific heat calculation
	arma::Col<double> NistDualSH::calc_specific_heat(const arma::Col<double> &temperature) const{
		// check input
		assert(temperature.is_finite());

		// allocate
		arma::Col<double> specific_heat(temperature.n_elem,arma::fill::zeros);

		// split calculation
		const arma::Col<arma::uword> idx1 = arma::find(temperature<sh_fit1_->get_upper_temperature());
		const arma::Col<arma::uword> idx2 = arma::find(temperature>=sh_fit1_->get_upper_temperature());
		if(!idx1.is_empty())specific_heat(idx1) = sh_fit1_->calc_specific_heat(temperature(idx1));
		if(!idx2.is_empty())specific_heat(idx2) = sh_fit2_->calc_specific_heat(temperature(idx2));

		// check output
		assert(specific_heat.is_finite());
		assert(arma::all(specific_heat>0));

		// return specific heat
		return specific_heat;
	}

	// copy constructor
	ShSpecificHeatFitPr NistDualSH::copy() const{
		ShNistDualSHPr dual = NistDualSH::create();
		dual->sh_fit1_ = std::dynamic_pointer_cast<NistSH>(sh_fit1_->copy());
		dual->sh_fit2_ = std::dynamic_pointer_cast<NistSH>(sh_fit2_->copy());
		return dual;
	}

	// get type
	std::string NistDualSH::get_type(){
		return "rat::mat::nistdualsh";
	}

	// method for serialization into json
	void NistDualSH::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["sh_fit1"] = cmn::Node::serialize_node(sh_fit1_, list);
		js["sh_fit2"] = cmn::Node::serialize_node(sh_fit2_, list);
	}

	// method for deserialisation from json
	void NistDualSH::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		sh_fit1_ = cmn::Node::deserialize_node<NistSH>(js["sh_fit1"], list, factory_list);
		sh_fit2_ = cmn::Node::deserialize_node<NistSH>(js["sh_fit2"], list, factory_list);
	}

}}