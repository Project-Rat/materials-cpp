// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "fixedjc.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FixedJc::FixedJc(){

	}

	// constructor with critical current input
	FixedJc::FixedJc(const double critical_current_density, const double nvalue){
		set_critical_current_density(critical_current_density);
		set_nvalue(nvalue);
	}

	// factory
	ShFixedJcPr FixedJc::create(){
		return std::make_shared<FixedJc>();
	}

	// factory from file
	ShFixedJcPr FixedJc::create(const double critical_current_density, const double nvalue){
		return std::make_shared<FixedJc>(critical_current_density, nvalue);
	}

	// get function for electric field criterion
	double FixedJc::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void FixedJc::set_critical_current_density(const double critical_current_density){
		if(critical_current_density<0)rat_throw_line("critical current can not be negative");
		critical_current_density_ = critical_current_density;
	}

	// set function for critical currnet
	void FixedJc::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void FixedJc::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double FixedJc::calc_critical_current_density(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return critical_current_density_;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> FixedJc::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*critical_current_density_;
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double FixedJc::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> FixedJc::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr FixedJc::copy() const{
		return std::make_shared<FixedJc>(*this);
	}

	// serialization
	// get type
	std::string FixedJc::get_type(){
		return "rat::mat::fixedjc";
	}

	// method for serialization into json
	void FixedJc::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();
		js["critical_current_density"] = critical_current_density_;
		js["electric_field_criterion"] = electric_field_criterion_;
		js["nvalue"] = nvalue_;
	}

	// method for deserialisation from json
	void FixedJc::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		set_critical_current_density(js["critical_current_density"].ASFLTP());
		set_electric_field_criterion(js["electric_field_criterion"].ASFLTP());
		set_nvalue(js["nvalue"].ASFLTP());
	}

}}