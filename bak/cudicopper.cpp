// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "cudicopper.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	CudiCopper::CudiCopper(){

	}

	// constructor from file
	CudiCopper::CudiCopper(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// default constructor
	CudiCopper::CudiCopper(const double C0, const double C1, const double C2, 
		const double C3, const double RRR, const double lower_temperature, 
		const double upper_temperature){
		C0_ = C0; C1_ = C1; C2_ = C2; C3_ = C3; RRR_ = RRR; 
		lower_temperature_ = lower_temperature; 
		upper_temperature_ = upper_temperature;
	}

	// factory
	ShCudiCopperPr CudiCopper::create(){
		return std::make_shared<CudiCopper>();
	}

	// factory from file
	ShCudiCopperPr CudiCopper::create(const std::string &fname){
		return std::make_shared<CudiCopper>(fname);
	}

	// factory from file
	ShCudiCopperPr CudiCopper::create(const double C0, const double C1, const double C2, 
		const double C3, const double RRR, const double lower_temperature, 
		const double upper_temperature){
		return std::make_shared<CudiCopper>(C0,C1,C2,C3,RRR,lower_temperature,upper_temperature);
	}

	// vector fit function
	arma::Col<double> CudiCopper::calc_electrical_resistivity(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);

		// clamp temperature as to avoid negative values in fit
		const arma::Col<double> T = arma::clamp(temperature,lower_temperature_,arma::datum::inf);
		const arma::Col<double> B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const arma::Col<double> TTT = T%T%T; 
		const arma::Col<double> TTTTT = TTT%T%T;
		const arma::Col<double> rho = (C0_/RRR_ + 1.0/(C1_/TTTTT + C2_/TTT + C3_/T))*1e-8 + B*(0.37 + 0.0005*RRR_)*1e-10;

		// return calculated conductivity
		return rho; // [Ohm m]
	}

	// scalar fit function
	double CudiCopper::calc_electrical_resistivity(
		const double temperature, 
		const double magnetic_field_magnitude) const{
		
		// clamp temperature as to avoid negative values in fit
		const double T = std::max(temperature,lower_temperature_);
		const double B = magnetic_field_magnitude;

		// Equation [Ohm.m]
		const double TTT = T*T*T; 
		const double TTTTT = TTT*T*T;
		const double rho = (C0_/RRR_ + 1.0/(C1_/TTTTT + C2_/TTT + C3_/T))*1e-8 + B*(0.37 + 0.0005*RRR_)*1e-10;

		// return calculated conductivity
		return rho; // [Ohm m]
	}

	// copy constructor
	ShResistivityFitPr CudiCopper::copy() const{
		return std::make_shared<CudiCopper>(*this);
	}

	// get type
	std::string CudiCopper::get_type(){
		return "rat::mat::cudicopper";
	}

	// method for serialization into json
	void CudiCopper::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["C0"] = C0_;
		js["C1"] = C1_;
		js["C2"] = C2_;
		js["C3"] = C3_;
		js["RRR"] = RRR_;
		js["lower_temperature"] = lower_temperature_;
		js["upper_temperature"] = upper_temperature_;
	}

	// method for deserialisation from json
	void CudiCopper::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void CudiCopper::import_json(const Json::Value &js){
		// fit parameters
		C0_ = js["C0"].ASFLTP();
		C1_ = js["C1"].ASFLTP();
		C2_ = js["C2"].ASFLTP();
		C3_ = js["C3"].ASFLTP();
		RRR_ = js["RRR"].ASFLTP();
		lower_temperature_ = js["lower_temperature"].ASFLTP();
		upper_temperature_ = js["upper_temperature"].ASFLTP();
	}

}}