// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "fixeddensity.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	FixedDensity::FixedDensity(){

	}

	// constructor from file
	FixedDensity::FixedDensity(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// constructor from file
	FixedDensity::FixedDensity(const double density){
		set_density(density);
	}

	// factory
	ShFixedDensityPr FixedDensity::create(){
		return std::make_shared<FixedDensity>();
	}

	// factory from file
	ShFixedDensityPr FixedDensity::create(const std::string &fname){
		return std::make_shared<FixedDensity>(fname);
	}

	// factory from file
	ShFixedDensityPr FixedDensity::create(const double density){
		return std::make_shared<FixedDensity>(density);
	}


	// set fit parameters
	void FixedDensity::set_density(const double density){
		density_ = density;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Col<double> FixedDensity::calc_density(const arma::Col<double> &temperature)const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*density_;
	}

	// specific heat output in [J m^-3 K^-1]
	double FixedDensity::calc_density(const double /*temperature*/)const{
		return density_;
	}

	// copy constructor
	ShDensityFitPr FixedDensity::copy() const{
		return std::make_shared<FixedDensity>(*this);
	}

	// get type
	std::string FixedDensity::get_type(){
		return "rat::mat::fixeddensity";
	}

	// method for serialization into json
	void FixedDensity::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();

		// fit parameters
		js["density"] = density_;
	}

	// method for deserialisation from json
	void FixedDensity::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		import_json(js);
	}

	// create reader
	void FixedDensity::import_json(const Json::Value &js){
		density_ = js["density"].ASFLTP();
	}

}}