// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_REBCO_CERN_HH
#define MAT_REBCO_CERN_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "jcfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ReBCOCERN> ShReBCOCERNPr;

	// fixed critical current density
	class ReBCOCERN: public JcFit{
		private:
			// typical layer thickness
			double tsc_;

			// general parameters
			double Tc0_; // [K]
			double n_;
			double n1_;
			double n2_;

			// Parameters for ab-plane
			double pab_;
			double qab_;
			double Bi0ab_; // [T]
			double a_;
			double gammaab_;
			double alphaab_; // [AT/m^2]

			// Parameters for c-plane
			double pc_;
			double qc_;
			double Bi0c_; // [T]
			double gammac_;
			double alphac_; // [AT/m^2]

			// Parameters for anisotropy
			double g0_;
			double g1_;
			double g2_;
			double g3_;
			double nu_;
			double pkoff_;

			// fixed power law N-value
			double nvalue_;

			// power law electric field criterion
			double electric_field_criterion_;

			// type-0 pair flipping
			bool type0_flip_;
			bool ignore_pkoff_;

		public:
			// constructor
			ReBCOCERN();
			ReBCOCERN(const std::string &fname);

			// factory
			static ShReBCOCERNPr create();
			static ShReBCOCERNPr create(const std::string &fname);

			// get function for electric field criterion
			double get_electric_field_criterion() const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			double calc_critical_current_density(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<double> calc_critical_current_density(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;
			
			// function for calculating the power law n-value
			// using scalar input and output
			double calc_nvalue(
				const double temperature,
				const double magnetic_field_magnitude,
				const double magnetic_field_angle) const override;

			// function for calculating the power law n-value
			// using vector input and output
			arma::Col<double> calc_nvalue(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude,
				const arma::Col<double> &magnetic_field_angle) const override;

			// copy function
			ShJcFitPr copy() const override;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const override;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

			// read data from json value
			void import_json(const Json::Value &js);
	};

}}

#endif