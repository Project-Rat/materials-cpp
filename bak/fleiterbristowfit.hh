// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_FLEITER_BRISTOW_FIT_HH
#define MAT_FLEITER_BRISTOW_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "fit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class FleiterBristowFit> ShFleiterBristowFitPr;

	// fixed critical current density
	class FleiterBristowFit: public Fit{
		private:
			// typical layer thickness
			fltp tsc_;

			// general parameters
			fltp Tc0_; // [K]

			// Parameters for ab-plane
			fltp pab_;
			fltp qab1_;
			fltp qab2_;
			fltp Bi0ab_; // [T]
			fltp gammaab_;
			fltp alphaab_; // [AT/m^2]
			fltp n_;
			fltp n1_;
			fltp n2_;
			fltp a_;
			
			// Parameters for c-plane
			fltp pc_;
			fltp qc1_;
			fltp qc2_;
			fltp Bi0c_; // [T]
			fltp gammac_;
			fltp alphac_; // [AT/m^2]
			
			// Parameters for r-plane
			fltp pr_;
			fltp qr1_;
			fltp qr2_;
			fltp Bi0r_; // [T]
			fltp gammar_;
			fltp alphar_; // [AT/m^2]
			fltp nr_;
			fltp nr1_;
			fltp nr2_;
			fltp ar_;

			// Parameters for anisotropy
			fltp k0_;
			fltp k1_;
			fltp k2_;
			fltp k3_;
			fltp xi_;
			fltp g0_;
			fltp g1_;
			fltp g2_;
			fltp g3_;
			fltp nu_;
			fltp pkoff_;
			fltp spkoff_ = 0.0;

			// type-0 pair flipping
			bool type0_flip_ = false;
			bool ignore_pkoff_ = false;
			bool force_perpendicular_ = false;
			bool force_parallel_ = false;
			
		public:
			// constructor
			FleiterBristowFit();

			// factory
			static ShFleiterBristowFitPr create();

			// property calculation with less input
			fltp calc_property(const fltp temperature) const override;
			fltp calc_property(const fltp temperature, const fltp magnetic_field_magnitude) const override;

			// function for calculating critical current density
			// using scalar input and output
			// output is in [A m^-2]
			fltp calc_property(
				const fltp temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const override;

			// property calculation with less input
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature) const override;
			arma::Col<fltp> calc_property(const arma::Col<fltp> &temperature, const arma::Col<fltp> &magnetic_field_magnitude) const override;


			// function for brining angle in interval 0 to 2*pi
			static arma::Col<fltp> angle_mod(const arma::Col<fltp> &alpha);
			
			// irreversibility field equation output in fraction Birr / Bi0
			// in Fleiter fit for c-plane n1=0.0, n2=1.0 and a=1.0;
			static arma::Col<fltp> calc_irreversibility_field(const arma::Col<fltp> &t, 
				const fltp n, const fltp n1 = RAT_CONST(0.0), 
				const fltp n2 = RAT_CONST(1.0), const fltp a = RAT_CONST(1.0));

			// calculate temperature dependence
			// in Fleiter fit for c-plane n1=0.0, n2=1.0 and a=1.0;
			static arma::Col<fltp> calc_temperature_dependence(
				const arma::Col<fltp> &t, const arma::Col<fltp>& birr, const fltp gamma);

			// calculate field dependence
			static arma::Col<fltp> calc_field_dependence(const arma::Col<fltp> &b, 
				const fltp p, const fltp q1, const fltp q2);
			
			// angular dependence factor
			static arma::Col<fltp> calc_angular_dependence_factor(
				const arma::Col<fltp> &B, const arma::Col<fltp> &T, 
				const fltp g0, const fltp g1, const fltp g2, const fltp g3);
			
			// calculate angular dependence for primary peak
			static arma::Col<fltp> calc_angular_dependence_primary(
				const arma::Col<fltp> &alpha, const arma::Col<fltp> &g, 
				const fltp nu, const fltp peak_offset = RAT_CONST(0.0));
			
			// calculate angular dependence for secondary peak
			static arma::Col<fltp> calc_angular_dependence_secondary(
				const arma::Col<fltp> &alpha, const fltp k, const fltp peak_offset);
			
			// angular dependence
			static arma::Col<fltp> calc_critical_current(
				const arma::Col<fltp> &alpha,
				const arma::Col<fltp> &Jcc, const arma::Col<fltp> &Jcab, 
				const arma::Col<fltp> &Jcr, const arma::Col<fltp> &g, 
				const fltp nu, const arma::Col<fltp> &k, const fltp xi, 
				const fltp peak_offset, 
				const fltp second_peak_offset);

			// critical current density calculation at fixed angle
			static arma::Col<fltp> calc_Jc(
				const arma::Col<fltp> &T, const fltp Bi0, 
				const arma::Col<fltp> &B, const fltp Tc0,
				const fltp n, const fltp n1, const fltp n2, const fltp a, 
				const fltp p, const fltp q1, const fltp q2, const fltp gamma, const fltp alpha);

			// function for calculating critical current density
			// using vector input and output
			// output is in [A m^-2]
			arma::Col<fltp> calc_property(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle) const override;

			// typical layer thickness
			void set_tsc(const fltp tsc){tsc_ = tsc;};

			// general parameters
			void set_Tc0(const fltp Tc0){Tc0_ = Tc0;};
			void set_n(const fltp n){n_ = n;};
			void set_n1(const fltp n1){n1_ = n1;};
			void set_n2(const fltp n2){n2_ = n2;};

			// Parameters for ab-plane
			void set_pab(const fltp pab){pab_ = pab;};
			void set_qab1(const fltp qab1){qab1_ = qab1;};
			void set_qab2(const fltp qab2){qab2_ = qab2;};
			void set_Bi0ab(const fltp Bi0ab){Bi0ab_ = Bi0ab;};
			void set_a(const fltp a){a_ = a;};
			void set_gammaab(const fltp gammaab){gammaab_ = gammaab;};
			void set_alphaab(const fltp alphaab){alphaab_ = alphaab;};

			// Parameters for c-plane
			void set_pc(const fltp pc){pc_ = pc;};
			void set_qc1(const fltp qc1){qc1_ = qc1;};
			void set_qc2(const fltp qc2){qc2_ = qc2;};
			void set_Bi0c(const fltp Bi0c){Bi0c_ = Bi0c;};
			void set_gammac(const fltp gammac){gammac_ = gammac;};
			void set_alphac(const fltp alphac){alphac_ = alphac;};

			// Parameters for r-plane
			void set_pr(const fltp pr){pr_ = pr;};
			void set_qr1(const fltp qr1){qr1_ = qr1;};
			void set_qr2(const fltp qr2){qr2_ = qr2;};
			void set_Bi0r(const fltp Bi0r){Bi0r_ = Bi0r;};
			void set_gammar(const fltp gammar){gammar_ = gammar;};
			void set_alphar(const fltp alphar){alphar_ = alphar;};
			void set_nr(const fltp nr){nr_ = nr;};
			void set_nr1(const fltp nr1){nr1_ = nr1;};
			void set_nr2(const fltp nr2){nr2_ = nr2;};
			void set_ar(const fltp ar){ar_ = ar;};

			// Parameters for anisotropy
			void set_k0(const fltp k0){k0_ = k0;};
			void set_k1(const fltp k1){k1_ = k1;};
			void set_k2(const fltp k2){k2_ = k2;};
			void set_k3(const fltp k3){k3_ = k3;};
			void set_xi(const fltp xi){xi_ = xi;};
			void set_g0(const fltp g0){g0_ = g0;};
			void set_g1(const fltp g1){g1_ = g1;};
			void set_g2(const fltp g2){g2_ = g2;};
			void set_g3(const fltp g3){g3_ = g3;};
			void set_nu(const fltp nu){nu_ = nu;};
			void set_pkoff(const fltp pkoff){pkoff_ = pkoff;};
			void set_spkoff(const fltp spkoff){spkoff_ = spkoff;};

			// type-0 pair flipping
			void set_type0_flip(const bool type0_flip){type0_flip_ = type0_flip;};
			void set_ignore_pkoff(const bool ignore_pkoff){ignore_pkoff_ = ignore_pkoff;};
			void set_force_perpendicular(const bool force_perpendicular =  true){force_perpendicular_ = force_perpendicular;};
			void set_force_parallel(const bool force_parallel =  true){force_parallel_ = force_parallel;};

			// typical layer thickness
			fltp get_tsc() const{return tsc_;};

			// general parameters
			fltp get_Tc0() const{return Tc0_;};
			fltp get_n() const{return n_;};
			fltp get_n1() const{return n1_;};
			fltp get_n2() const{return n2_;};

			// Parameters for ab-plane
			fltp get_pab() const{return pab_;};
			fltp get_qab1() const{return qab1_;};
			fltp get_qab2() const{return qab2_;};
			fltp get_Bi0ab() const{return Bi0ab_;};
			fltp get_a() const{return a_;};
			fltp get_gammaab() const{return gammaab_;};
			fltp get_alphaab() const{return alphaab_;};

			// Parameters for c-plane
			fltp get_pc() const{return pc_;};
			fltp get_qc1() const{return qc1_;};
			fltp get_qc2() const{return qc2_;};
			fltp get_Bi0c() const{return Bi0c_;};
			fltp get_gammac() const{return gammac_;};
			fltp get_alphac() const{return alphac_;};

			// Parameters for r-plane
			fltp get_pr() const{return pr_;};
			fltp get_qr1() const{return qr1_;};
			fltp get_qr2() const{return qr2_;};
			fltp get_Bi0r() const{return Bi0r_;};
			fltp get_gammar() const{return gammar_;};
			fltp get_alphar() const{return alphar_;};
			fltp get_nr1() const{return nr1_;};
			fltp get_nr2() const{return nr2_;};
			fltp get_ar() const{return ar_;};

			// Parameters for anisotropy
			fltp get_g0() const{return g0_;};
			fltp get_g1() const{return g1_;};
			fltp get_g2() const{return g2_;};
			fltp get_g3() const{return g3_;};
			fltp get_nu() const{return nu_;};
			fltp get_pkoff() const{return pkoff_;};

			// type-0 pair flipping
			bool get_type0_flip() const{return type0_flip_;};
			bool get_ignore_pkoff() const{return ignore_pkoff_;};
			bool get_force_perpendicular()const{return force_perpendicular_;};
			bool get_force_parallel()const{return force_parallel_;};

			// serialization
			static std::string get_type();
			void serialize(
				Json::Value &js, 
				cmn::SList &list) const override;
			void deserialize(
				const Json::Value &js, 
				cmn::DSList &list, 
				const cmn::NodeFactoryMap &factory_list, 
				const boost::filesystem::path &pth) override;
	};

}}

#endif