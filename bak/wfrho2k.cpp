// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "wfrho2k.hh"
#include "resistivityfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	WFRho2K::WFRho2K(){

	}

	WFRho2K::WFRho2K(ShResistivityFitPr rho_fit){
		set_electrical_resistivity_fit(rho_fit);
	}

	// factory
	ShWFRho2KPr WFRho2K::create(){
		return std::make_shared<WFRho2K>();
	}

	// factory from file
	ShWFRho2KPr WFRho2K::create(ShResistivityFitPr rho_fit){
		return std::make_shared<WFRho2K>(rho_fit);
	}

	// set thermal conductivity fit
	void WFRho2K::set_electrical_resistivity_fit(ShResistivityFitPr rho_fit){
		assert(rho_fit!=NULL);
		rho_fit_ = rho_fit;
	}

	// fit functions for vector
	double WFRho2K::calc_thermal_conductivity(
		const double temperature, 
		const double magnetic_field_magnitude) const{
		
		// limit temperature
		const double T = std::max(temperature,1.0);

		// calculate thermal conductivity
		const double rho = rho_fit_->calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law [W Ohm K-2]*[K]/[Ohm m^2] = [W/K m^-2]
		const double k = lorentz_number_*T/rho;
		
		// check output
		assert(k>0);

		// thermal conductivity
		return k;
	}

	// fit functions for vector
	arma::Col<double> WFRho2K::calc_thermal_conductivity(
		const arma::Col<double> &temperature, 
		const arma::Col<double> &magnetic_field_magnitude) const{

		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);

		// limit temperature
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);

		// calculate electrical resistivity [Ohm m^2]
		const arma::Col<double> rho = rho_fit_->calc_electrical_resistivity(
			temperature, magnetic_field_magnitude);

		// Wiedemann Franz Law [W Ohm K-2]*[K]/[Ohm m^2] = [W/K m^-2]
		const arma::Col<double> k = lorentz_number_*T/rho;

		// check output
		assert(k.n_elem==temperature.n_elem);
		assert(k.is_finite());
		assert(arma::all(k>0));

		// thermal conductivity 
		return k;
	}

	// copy constructor
	ShConductivityFitPr WFRho2K::copy() const{
		return WFRho2K::create(rho_fit_->copy());
	}

	// serialization type
	std::string WFRho2K::get_type(){
		return "rat::mat::wfrho2k";
	}

	// serialization
	void WFRho2K::serialize(Json::Value &js, cmn::SList &list) const{
		js["type"] = get_type();
		js["rho_fit"] = cmn::Node::serialize_node(rho_fit_, list);
	}

	// deserialization
	void WFRho2K::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		set_electrical_resistivity_fit(cmn::Node::deserialize_node<ResistivityFit>(js["rho_fit"], list, factory_list));
	}

}}
