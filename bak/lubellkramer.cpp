// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "lubellkramer.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	LubellKramer::LubellKramer(){

	}

	// factory
	ShLubellKramerPr LubellKramer::create(){
		return std::make_shared<LubellKramer>();
	}

	// get function for electric field criterion
	double LubellKramer::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void LubellKramer::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void LubellKramer::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double LubellKramer::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{

		// copy input and limit
		const double mgn_field_lim = std::max(0.1,magnetic_field_magnitude);
		
		// calculate scaled values
		const double t = temperature/Tc0_;
		const double Bc2 = Bc20_*(1.0-std::pow(t,n_)); // Lubell's equation
		const double b = mgn_field_lim/Bc2;

		// bottura's equation
		double critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)*std::pow(b,alpha_)*
			std::pow(1.0-b,beta_)*std::pow(1.0-std::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		if(mgn_field_lim>Bc2)critical_current_density = 0;
		if(mgn_field_lim==0)critical_current_density = arma::datum::inf;

		// return critical current
		return critical_current_density; // A/m^2
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> LubellKramer::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		
		// copy B
		const arma::Col<double> mgn_field_lim = arma::clamp(
			magnetic_field_magnitude, 0.1, arma::datum::inf);
		
		// calculate scaled values
		const arma::Col<double> t = temperature/Tc0_;
		const arma::Col<double> Bc2 = Bc20_*(1.0-arma::pow(t,n_)); // Lubell's equation
		const arma::Col<double> b = mgn_field_lim/Bc2;

		// bottura's equation
		arma::Col<double> critical_current_density = 
			1e6*Jref_*(C0_/mgn_field_lim)%arma::pow(b,alpha_)%
			arma::pow(1.0-b,beta_)%arma::pow(1.0-arma::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		critical_current_density(arma::find(mgn_field_lim>Bc2)).fill(0);
		critical_current_density(arma::find(mgn_field_lim==0)).fill(arma::datum::inf);

		// return critical current
		return critical_current_density; // A/m^2
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double LubellKramer::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> LubellKramer::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr LubellKramer::copy() const{
		return std::make_shared<LubellKramer>(*this);
	}

	// get type
	std::string LubellKramer::get_type(){
		return "rat::mat::lubellkramer";
	}

	// method for serialization into json
	void LubellKramer::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["E0"] = electric_field_criterion_;
		js["N"] = nvalue_;
		js["C0"] = C0_;
		js["alpha"] = alpha_;
		js["beta"] = beta_;
		js["gamma"] = gamma_;
		js["n"] = n_;
		js["Bc20"] = Bc20_;
		js["Tc0"] = Tc0_;
		js["Jref"] = Jref_;
	}

	// method for deserialisation from json
	void LubellKramer::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		electric_field_criterion_ = js["E0"].ASFLTP();
		nvalue_ = js["N"].ASFLTP();
		C0_ = js["C0"].ASFLTP();
		alpha_ = js["alpha"].ASFLTP();
		beta_ = js["beta"].ASFLTP();
		gamma_ = js["gamma"].ASFLTP();
		n_ = js["n"].ASFLTP();
		Bc20_ = js["Bc20"].ASFLTP();
		Tc0_ = js["Tc0"].ASFLTP();
		Jref_ = js["Jref"].ASFLTP();
	}

}}