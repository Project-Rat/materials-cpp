// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CONDUCTOR_HH
#define MAT_CONDUCTOR_HH

#include <armadillo>
#include <memory>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/nameable.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class Conductor> ShConductorPr;
	typedef arma::field<ShConductorPr> ShConductorPrList;

	// typedef for list containing both fraction and conductor
	typedef std::pair<fltp, ShConductorPr> FracShConPr;
	typedef std::list<FracShConPr> FracShConPrList;
	typedef std::pair<fltp, const Conductor*> FracConPr;
	typedef std::list<FracConPr> FracConPrList;

	// template for materials
	class Conductor: public cmn::Nameable{
		// methods
		public:
			// virtual destructor
			~Conductor(){};

			// material properties for scalar input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual fltp calc_specific_heat(
				const fltp temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual fltp calc_thermal_conductivity(
				const fltp temperature,
				const fltp magnetic_field_magnitude) const = 0; 

			// material density [kg m^-3]
			virtual fltp calc_density(
				const fltp temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual fltp calc_volumetric_specific_heat(
				const fltp temperature) const;

			// material properties for vector input
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			virtual arma::Col<fltp> calc_specific_heat(
				const arma::Col<fltp> &temperature) const = 0;
			
			// thermal conductivity [W m^-1 K^-1]
			virtual arma::Col<fltp> calc_thermal_conductivity(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude) const = 0; 

			// material density [kg m^-3]
			virtual arma::Col<fltp> calc_density(
				const arma::Col<fltp> &temperature) const = 0;

			// volumetric specific heat [J m^-3 K^-1]
			virtual arma::Col<fltp> calc_volumetric_specific_heat(
				const arma::Col<fltp> &temperature) const;


			// material properties for matrices 
			// these are essentially wrappers
			// for the vector material properties
			// ===
			// calculate specific heat [J kg^-1 K^-1]
			arma::Mat<fltp> calc_specific_heat_mat(
				const arma::Mat<fltp> &temperature) const;
			
			// thermal conductivity [W m^-1 K^-1]
			arma::Mat<fltp> calc_thermal_conductivity_mat(
				const arma::Mat<fltp> &temperature,
				const arma::Mat<fltp> &magnetic_field_magnitude) const; 
			
			// material density [kg m^-3]
			arma::Mat<fltp> calc_density_mat(
				const arma::Mat<fltp> &temperature) const;

			// volumetric specific heat [J m^-3 K^-1]
			arma::Mat<fltp> calc_volumetric_specific_heat_mat(
				const arma::Mat<fltp> &temperature) const;


			// analysis tools for scalar input
			// ===
			// calcultae temperature increase due to added heat
			// output in [K]
			fltp calc_temperature_increase(
				const fltp initial_temperature, 
				const fltp volume, 
				const fltp added_heat,
				const fltp delta_temperature = 1.0) const;

			// calculate the energy density based on the temperature
			// output in [J m^-3]
			fltp calc_thermal_energy_density(
				const fltp temperature,
				const fltp delta_temperature = 1.0) const;

			// calculate the length of the minimal propagation zone
			// output in [m]
			fltp calc_theoretical_lmpz(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const;

			// calculate the normal zone propagation velocity
			// output in [m s^-1]
			fltp calc_theoretical_vnzp(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle) const;

			// calculate minimal quench energy in [J] or 
			// [J/m^2] if cross sectional area is not provided
			fltp calc_theoretical_mqe(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp cross_sectional_area = 1.0) const;

			// calculate adiabatic normal zone temperature
			fltp calc_adiabatic_time(
				const fltp current_density,
				const fltp operating_temperature,
				const fltp magnetic_field_magnitude,
				const fltp magnetic_field_angle,
				const fltp max_temperature,
				const fltp max_temperature_step = 0.5) const;


			// analysis tools for vector input
			// ===
			// calcultae temperature increase due to added heat
			arma::Col<fltp> calc_temperature_increase(
				const arma::Col<fltp> &initial_temperature, 
				const arma::Col<fltp> &volume, 
				const arma::Col<fltp> &added_heat) const;

			// calculate the energy density based on the temperature
			arma::Col<fltp> calc_thermal_energy_density(
				const arma::Col<fltp> &temperature,
				const fltp delta_temperature = 1.0) const;


			// accumulation method
			virtual void accu_parallel_conductors(
				FracConPrList &parallel_conductors) const;


			// calculate normal zone propagation velocity

			// calculate minimal quench energy

			// run simple NZP model?

			// functions averaged over cross section of conductor
			// ===
			// critical current density averaged over cross section [J m^-2]
			virtual arma::Col<fltp> calc_critical_current_density_av(
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<fltp> &cross_areas) const;

			// electric field [V m^-1] section averaged
			// required to supply the cross-sectional areas of the
			// elements making up a section in the same order as
			// the other properties
			virtual arma::Col<fltp> calc_electric_field_av(
				const arma::Col<fltp> &current_density,
				const arma::Col<fltp> &temperature,
				const arma::Col<fltp> &magnetic_field_magnitude,
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<fltp> &cross_areas) const = 0;

			// critical temperature calculation in [A m^-2]
			// using averaged critical current over cross area
			virtual arma::Col<fltp> calc_cs_temperature_av(
				const arma::Col<fltp> &current_density, 
				const arma::Col<fltp> &magnetic_field_magnitude, 
				const arma::Col<fltp> &magnetic_field_angle,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<fltp> &cross_areas,
				const fltp temperature_increment = 10.0,
				const fltp tol_bisection = 0.1) const;



			// helper functions
			// ===
			// cross section averaging
			static arma::Mat<fltp> nodes2section(
				const arma::Mat<fltp> &node_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<fltp> &cross_areas);

			// cross section values to nodes
			static arma::Mat<fltp> section2nodes(
				const arma::Mat<fltp> &section_values,
				const arma::Mat<arma::uword> &nelements,
				const arma::Row<fltp> &cross_areas);

			// Display function
			// make ss a table in the supplied log
			void display_property_table(
				cmn::ShLogPr lg = cmn::Log::create(),
				const arma::Col<fltp> &temperature = {1.9,4.5,10.0,20.0,30.0,40.0,50.0,60.0,77.0},
				const arma::Col<fltp> &magnetic_field_magnitude = {5.0},
				const arma::Col<fltp> &magnetic_field_angle = {0.0}) const;
	};


}}

#endif