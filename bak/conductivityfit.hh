// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_CONDUCTIVITY_FIT_HH
#define MAT_CONDUCTIVITY_FIT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mat{

	// shared pointer definition
	typedef std::shared_ptr<class ConductivityFit> ShConductivityFitPr;

	// template for materials
	class ConductivityFit: public cmn::Node{
		// methods
		public:
			// virtual destructor
			~ConductivityFit(){};

			// function for calculating electrical resistivity
			// using scalar input and output
			// output is in [W m^-1 K^-1]
			virtual double calc_thermal_conductivity(
				const double temperature,
				const double magnetic_field_magnitude) const = 0;

			// function for calculating thermal conductivity
			// using vector input and output
			// output is in [W m^-1 K^-1]
			virtual arma::Col<double> calc_thermal_conductivity(
				const arma::Col<double> &temperature,
				const arma::Col<double> &magnetic_field_magnitude) const = 0;

			// copy function
			virtual ShConductivityFitPr copy() const = 0;
	};

}}

#endif