// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "rebcocern.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	ReBCOCERN::ReBCOCERN(){

	}

	// constructor with critical current input
	ReBCOCERN::ReBCOCERN(const std::string &fname){
		// allocate json
		Json::Value js = cmn::Node::parse_json(fname);

		// import data
		import_json(js);
	}

	// factory
	ShReBCOCERNPr ReBCOCERN::create(){
		return std::make_shared<ReBCOCERN>();
	}

	// factory from file
	ShReBCOCERNPr ReBCOCERN::create(const std::string &fname){
		return std::make_shared<ReBCOCERN>(fname);
	}

	// get function for electric field criterion
	double ReBCOCERN::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double ReBCOCERN::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double magnetic_field_angle) const{

		// prevent zero field at which a anomalous behaviour occurs
		const double B = std::max(magnetic_field_magnitude,0.1);
		const double T = std::max(temperature,1.0);
		const double alpha = magnetic_field_angle;
		
		// calculate unitless temperature
		const double t = T/Tc0_;

		// irreversibility field for ab-plane
		const double Bi_ab = Bi0ab_*(
			std::pow(1.0-std::pow(t,n1_),n2_)+a_*(1.0-std::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const double bab = B/Bi_ab;

		// irreversibility field for c-plane
		const double Bi_c = Bi0c_*(1.0-std::pow(t,n_));

		// unitless field for c-plane
		const double bc = B/Bi_c;

		// critical current density c-plane
		double Jc_c = (alphac_/B)*std::pow(bc,pc_)*
			std::pow(1.0-bc,qc_)*std::pow(1.0-std::pow(t,n_),gammac_);

		// critical current density ab-plane
		double Jc_ab = (alphaab_/B)*std::pow(bab,pab_)*std::pow(1.0-bab,qab_)*
			std::pow(std::pow(1.0-std::pow(t,n1_),n2_)+a_*(1.0-std::pow(t,n_)),gammaab_);

		// temperature over Tcs
		if(t>=1.0){Jc_c = 0; Jc_ab = 0;};
		if(bc>=1.0)Jc_c = 0;
		if(bab>=1.0)Jc_ab = 0;
		
		// anisotropy factor
		const double g = g0_ + g1_*std::exp(-(g2_*std::exp(g3_*T))*B);

		// Sort out angular dependence
		const double pkoff_rad = 2*arma::datum::pi*pkoff_/360.0;

		// critical current density calculation
		double Jc = std::min(Jc_c,Jc_ab)+
			(std::max(Jc_ab-Jc_c,0.0)/
			(1.0+std::pow((std::abs(std::abs(alpha + 
			!ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g, nu_)));

		// flipped type-0 pair
		if(type0_flip_){
			Jc += std::min(Jc_c,Jc_ab)+
			(std::max(Jc_ab-Jc_c,0.0)/
			(1.0+std::pow((std::abs(std::abs(alpha - 
			!ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g, nu_)));
			Jc/=2;
		}

		// return critical current density
		return Jc;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> ReBCOCERN::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &magnetic_field_angle) const{
		
		// check input
		assert(temperature.n_elem==magnetic_field_magnitude.n_elem);
		assert(temperature.n_elem==magnetic_field_angle.n_elem);
		assert(temperature.is_finite());
		assert(magnetic_field_magnitude.is_finite());
		assert(magnetic_field_angle.is_finite());

		// prevent zero field at which a anomalous behaviour occurs
		const arma::Col<double> B = arma::clamp(magnetic_field_magnitude,0.1,arma::datum::inf);
		const arma::Col<double> T = arma::clamp(temperature,1.0,arma::datum::inf);
		arma::Col<double> alpha = magnetic_field_angle;

		//.make sure alpha is between 0 and 2*pi
		for(arma::uword i=0;i<alpha.n_elem;i++)
			alpha(i) = std::fmod(alpha(i),2*arma::datum::pi);

		// calculate unitless temperature
		const arma::Col<double> t = T/Tc0_;

		// irreversibility field for ab-plane
		const arma::Col<double> Bi_ab = Bi0ab_*(
			arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const arma::Col<double> bab = B/Bi_ab;

		// irreversibility field for c-plane
		const arma::Col<double> Bi_c = Bi0c_*(1.0-arma::pow(t,n_));

		// unitless field for c-plane
		const arma::Col<double> bc = B/Bi_c;

		// critical current density c-plane
		arma::Col<double> Jc_c = (alphac_/B)%arma::pow(bc,pc_)%
			arma::pow(1.0-bc,qc_)%arma::pow(1.0-arma::pow(t,n_),gammac_);

		// critical current density ab-plane
		arma::Col<double> Jc_ab = (alphaab_/B)%arma::pow(bab,pab_)%arma::pow(1.0-bab,qab_)%
			arma::pow(arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)),gammaab_);

		// temperature over Tcs
		Jc_c(arma::find(t>=1.0)).fill(0.0); 
		Jc_ab(arma::find(t>=1.0)).fill(0.0);

		// field over Bi
		Jc_c(arma::find(bc>=1.0)).fill(0.0); 
		Jc_ab(arma::find(bab>=1.0)).fill(0.0);

		// anisotropy factor
		const arma::Col<double> g = g0_ + g1_*arma::exp(-(g2_*arma::exp(g3_*T))%B);

		// Sort out angular dependence
		const double pkoff_rad = 2*arma::datum::pi*pkoff_/360.0;

		// critical current density calculation
		arma::Col<double> Jc = arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(arma::abs(alpha + !ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g,nu_)));

		// flipped type-0 pair
		if(type0_flip_){
			Jc += arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(arma::abs(alpha - !ignore_pkoff_*pkoff_rad) - arma::datum::pi/2.0))/g,nu_)));
			Jc/=2;
		}

		// check output
		assert(Jc.n_elem==temperature.n_elem);
		assert(Jc.is_finite());

		// return critical current density
		return Jc;
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double ReBCOCERN::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> ReBCOCERN::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr ReBCOCERN::copy() const{
		return std::make_shared<ReBCOCERN>(*this);
	}

	// serialization
	// get type
	std::string ReBCOCERN::get_type(){
		return "rat::mat::rebcocern";
	}

	// method for serialization into json
	void ReBCOCERN::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// settings
		js["type"] = get_type();
		
		// temperature dependence
		js["Tc0"] = Tc0_;
		js["n"] = n_;
		js["n1"] = n1_;
		js["n2"] = n2_;

		// AB-plane
		js["pab"] = pab_;
		js["qab"] = qab_;
		js["Bi0ab"] = Bi0ab_;
		js["a"] = a_;
		js["gammaab"] = gammaab_;
		js["alphaab"] = alphaab_;

		// C-axis
		js["pc"] = pc_;
		js["qc"] = qc_;
		js["Bi0c"] = Bi0c_;
		js["gammac"] = gammac_;
		js["alphac"] = alphac_;

		// anisotropy
		js["g0"] = g0_;
		js["g1"] = g1_;
		js["g2"] = g2_;
		js["g3"] = g3_;
		js["nu"] = nu_;
		js["pkoff"] = pkoff_;

		// power law parameters
		js["N"] = nvalue_;
		js["E0"] = electric_field_criterion_;

		// typical layer thicknesses and width
		js["tsc"] = tsc_;

		// type-0 pair flipping
		js["type0_flip"] = type0_flip_;
		js["ignore_pkoff"] = ignore_pkoff_;
	}

	// method for deserialisation from json
	void ReBCOCERN::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		// import parameters
		import_json(js);
	}

	// read data from json value
	void ReBCOCERN::import_json(const Json::Value &js){
		// general
		Tc0_ = js["Tc0"].ASFLTP();
		n_ = js["n"].ASFLTP();
		n1_ = js["n1"].ASFLTP();
		n2_ = js["n2"].ASFLTP();

		// AB-plane
		pab_ = js["pab"].ASFLTP();
		qab_ = js["qab"].ASFLTP();
		Bi0ab_ = js["Bi0ab"].ASFLTP();
		a_ = js["a"].ASFLTP();
		gammaab_ = js["gammaab"].ASFLTP();
		alphaab_ = js["alphaab"].ASFLTP();

		// C-axis
		pc_ = js["pc"].ASFLTP();
		qc_ = js["qc"].ASFLTP();
		Bi0c_ = js["Bi0c"].ASFLTP();
		gammac_ = js["gammac"].ASFLTP();
		alphac_ = js["alphac"].ASFLTP();

		// anisotropy
		g0_ = js["g0"].ASFLTP();
		g1_ = js["g1"].ASFLTP();
		g2_ = js["g2"].ASFLTP();
		g3_ = js["g3"].ASFLTP();
		nu_ = js["nu"].ASFLTP();
		pkoff_ = js["pkoff"].ASFLTP();

		// power law parameters
		nvalue_ = js["N"].ASFLTP();
		electric_field_criterion_ = js["E0"].ASFLTP();

		// typical layer thickness
		tsc_ = js["tsc"].ASFLTP();

		// type-0 pair flipping
		type0_flip_ = js["type0_flip"].asBool();
		ignore_pkoff_ = js["ignore_pkoff"].asBool();
	}


}}