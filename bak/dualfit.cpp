// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "dualfit.hh"

// code specific to Rat
namespace rat{namespace mat{

	// constructor
	DualFit::DualFit(){
		set_name("dualfit");
	}

	// factory
	ShDualFitPr DualFit::create(){
		return std::make_shared<DualFit>();
	}

	// acces fit 1
	ShFitPr DualFit::get_fit1() const{
		return (std::get<2>(*(fit_list_.begin())));
	}

	// acces fit 2
	ShFitPr DualFit::get_fit2() const{
		return (std::get<2>(*(++fit_list_.begin())));
	}

	// get type
	std::string DualFit::get_type(){
		return "rat::mat::dualfit";
	}

	// method for serialization into json
	void DualFit::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Fit::serialize(js,list);
		js["type"] = get_type();
		js["fit1"] = cmn::Node::serialize_node(get_fit1(),list);
		js["fit2"] = cmn::Node::serialize_node(get_fit2(),list);
		js["t12"] = t12_;
	}

	// method for deserialisation from json
	void DualFit::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list, const boost::filesystem::path &pth){
		// parent
		Fit::deserialize(js,list,factory_list,pth);
		t12_ = js["t12"].ASFLTP();
		add_fit(-1e99, t12_, cmn::Node::deserialize_node<Fit>(js["fit1"], list, factory_list, pth));
		add_fit(t12_, 1e99, cmn::Node::deserialize_node<Fit>(js["fit2"], list, factory_list, pth));
	}

}}