// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#ifndef MAT_FACTORY_LIST_HH
#define MAT_FACTORY_LIST_HH

#include "rat/common/node.hh"
#include "rat/common/serializer.hh"

#include "fixedjc.hh"
#include "rebcocern.hh"
#include "lubellkramer.hh"
#include "densityfit.hh"
#include "baseconductor.hh"
#include "parallelconductor.hh"
#include "cudicopper.hh"
#include "nistconductivity.hh"
#include "nistsh.hh"
#include "nistdualsh.hh"
#include "wfrho2k.hh"
#include "wfk2rho.hh"
#include "fixeddensity.hh"

// code specific to Rat
namespace rat{namespace mat{

	// use a class for storage
	class FactoryList{
		public:
			static void register_constructors(rat::cmn::ShSerializerPr slzr){
				// conductors
				slzr->register_factory<BaseConductor>();
				slzr->register_factory<ParallelConductor>();
				
				// jc fits
				slzr->register_factory<FixedJc>();
				slzr->register_factory<LubellKramer>();
				slzr->register_factory<ReBCOCERN>();

				// electrical resistivity fits
				slzr->register_factory<CudiCopper>();
				slzr->register_factory<WFK2Rho>();

				// thermal conductivity fits
				slzr->register_factory<WFRho2K>();
				slzr->register_factory<NistConductivity>();

				// specific heat fits
				slzr->register_factory<NistSH>();
				slzr->register_factory<NistDualSH>();

				// density fits
				slzr->register_factory<FixedDensity>();
			}
	};

}}

#endif