// Copyright 2025 Jeroen van Nugteren

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

// include header
#include "godekenb3sn.hh"

// code specific to Rat
namespace rat{namespace mat{

	// default constructor
	GodekeNb3Sn::GodekeNb3Sn(){

	}

	// factory
	ShGodekeNb3SnPr GodekeNb3Sn::create(){
		return std::make_shared<GodekeNb3Sn>();
	}

	// get function for electric field criterion
	double GodekeNb3Sn::get_electric_field_criterion() const{
		return electric_field_criterion_;
	}

	// set function for critical currnet
	void GodekeNb3Sn::set_nvalue(const double nvalue){
		if(nvalue<0)rat_throw_line("N-value can not be negative");
		nvalue_ = nvalue;
	}

	// set function for critical currnet
	void GodekeNb3Sn::set_electric_field_criterion(const double electric_field_criterion){
		if(electric_field_criterion<0)rat_throw_line("electric field criterion can not be negative");
		electric_field_criterion_ = electric_field_criterion;
	}

	// function for calculating critical current density
	// using scalar input and output
	// output is in [A m^-2]
	double GodekeNb3Sn::calc_critical_current_density(
		const double temperature,
		const double magnetic_field_magnitude,
		const double /*magnetic_field_angle*/) const{

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const double t = temperature/Tc;
		const double Bc2 = Bc2m_*S*(1.0-std::pow(t,1.52));
		const double h = magnetic_field_magnitude/Bc2;

		// Godeke Equation
		double critical_current = 
			C1_*S*(1.0-std::pow(t,1.52))*
			(1.0-std::pow(t,2))*std::pow(h,p_)*
			std::pow(1.0-h,q_)/magnetic_field_magnitude;

		// prevent formula from rising again after reaching Bc
		if(magnetic_field_magnitude>Bc2 || t>1.0)critical_current = 0;
		if(magnetic_field_magnitude==0)critical_current = arma::datum::inf;

		// return critical current
		return 1e6*critical_current;
	}

	// function for calculating critical current density
	// using vector input and output
	// output is in [A m^-2]
	arma::Col<double> GodekeNb3Sn::calc_critical_current_density(
		const arma::Col<double> &temperature,
		const arma::Col<double> &magnetic_field_magnitude,
		const arma::Col<double> &/*magnetic_field_angle*/) const{

		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const arma::Col<double> t = temperature/Tc;
		const arma::Col<double> Bc2 = Bc2m_*S*(1.0 - arma::pow(t,1.52));
		const arma::Col<double> h = magnetic_field_magnitude/Bc2;

		// Godeke Equation
		arma::Col<double> critical_current = 
			C1_*S*(1.0-arma::pow(t,1.52))%
			(1.0-arma::pow(t,2))%arma::pow(h,p_)%
			arma::pow(1.0-h,q_)/magnetic_field_magnitude;

		// prevent formula from rising again after reaching Bc
		critical_current(arma::find(magnetic_field_magnitude>Bc2)).fill(0);
		critical_current(arma::find(magnetic_field_magnitude==0)).fill(arma::datum::inf);

		// return critical current
		return 1e6*critical_current;
	}

	// function for calculating the power law n-value
	// using scalar input and output
	double GodekeNb3Sn::calc_nvalue(
		const double /*temperature*/,
		const double /*magnetic_field_magnitude*/,
		const double /*magnetic_field_angle*/) const{
		return nvalue_;
	}

	// function for calculating the power law n-value
	// using vector input and output
	arma::Col<double> GodekeNb3Sn::calc_nvalue(
		const arma::Col<double> &temperature,
		const arma::Col<double> &/*magnetic_field_magnitude*/,
		const arma::Col<double> &/*magnetic_field_angle*/) const{
		return arma::Col<double>(temperature.n_elem,arma::fill::ones)*nvalue_;
	}

	// copy constructor
	ShJcFitPr GodekeNb3Sn::copy() const{
		return std::make_shared<GodekeNb3Sn>(*this);
	}

	// get type
	std::string GodekeNb3Sn::get_type(){
		return "rat::mat::godekenb3sn";
	}

	// method for serialization into json
	void GodekeNb3Sn::serialize(Json::Value &js, cmn::SList &) const{
		// serialization type
		js["type"] = get_type();
		
		// fit parameters
		js["Ca1"] = Ca1_;
		js["Ca2"] = Ca2_;
		js["eps_0a"] = eps_0a_;
		js["C1"] = C1_;

		// superconducting parameters
		js["Bc2m"] = Bc2m_;
		js["Tcm"] = Tcm_;

		// magnetic field dependence parameters
		js["p"] = p_;
		js["q"] = q_;

		// environmental parameters
		js["eps_ax"] = eps_ax_; // axial strain

		// power law parameters
		js["E0"] = electric_field_criterion_;
		js["N"] = nvalue_;
	}

	// method for deserialisation from json
	void GodekeNb3Sn::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// fit parameters
		Ca1_ = js["Ca1"].ASFLTP();
		Ca2_ = js["Ca2"].ASFLTP();
		eps_0a_ = js["eps_0a"].ASFLTP(); // normalized
		C1_ = js["C1"].ASFLTP(); // [kAT mm^-2]

		// superconducting parameters
		Bc2m_ = js["Bc2m"].ASFLTP(); // [T]
		Tcm_ = js["Tcm"].ASFLTP(); // [K]

		// magnetic field dependence parameters
		p_ = js["p"].ASFLTP();
		q_ = js["q"].ASFLTP();

		// environmental parameters
		eps_ax_ = js["eps_ax"].ASFLTP();

		// power law parameters
		electric_field_criterion_ = js["E0"].ASFLTP();
		nvalue_ = js["N"].ASFLTP();
	}

}}